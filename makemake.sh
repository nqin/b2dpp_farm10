#make clean
#make foo=Fit
#cp bin/Fit qnFit/
#cd qnFit
#./Fit | tee logFit.log

make clean
make foo=Addeff
cp bin/Addeff qnFit/
cd qnFit
./Addeff | tee logAddeff.log

#make clean
#make foo=Ploteff
#cp bin/Ploteff qnFit/
#cd qnFit
#./Ploteff | tee logPloteff.log

#make clean
#make foo=FractionError
#cp bin/FractionError qnFit/
#cd qnFit
#./FractionError | tee logFractionError.log
