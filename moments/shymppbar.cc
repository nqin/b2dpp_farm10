#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "RooPlot.h"
#include "RooClassFactory.h"
#include "TROOT.h"

double fbg=0.058;
using namespace RooFit ;
void shy(int Y)
{  
  
// U s e   i n s t a n c e   o f   c r e a t e d   c l a s s 
  // ---------------------------------------------------------
 
  // Compile MyPdfV3 class (only when running in CINT)
  //////////////////////////
   // Call LHCb style file
   /////////////////////////
  gROOT->ProcessLine(".x lhcbStyle.C");
//  lhcbStyle();
  //RooAbsReal::defaultIntegratorConfig()->setEpsAbs(1e-6) ;
  //RooAbsReal::defaultIntegratorConfig()->setEpsRel(1e-6) ;
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);  
  gStyle->SetPalette(1);   
  gROOT->ProcessLine(".x ~/lhcbStyle.C");
  gStyle->SetPadLeftMargin(0.15);
  gStyle->SetTitleOffset(1.0,"Y");
  ifstream in;
  double ymin(0.95), ymax(2.3);
  
// Create observables

  RooRealVar mppbar("mppbar","mppbar",0);
  RooRealVar mD0p("mD0p","mD0p",0);
  RooRealVar mD0pbar("mD0pbar","mD0pbar",0);
  RooRealVar cosTheta_X("cosTheta_X","cosTheta_X",-1,1);
  RooRealVar cosTheta_L("cosTheta_L","cosTheta_L",-1,1);
  RooArgList *obs = new RooArgList();

  obs->add(mppbar);
  obs->add(mD0p);
  obs->add(mD0pbar);
  obs->add(cosTheta_X);
  obs->add(cosTheta_L);

  TChain *toytree = new TChain("tree");
//  toytree->Add("~/workdir/B2Dpp/disk101/Work/qnFit/toyFitmemeda.root");
//  toytree->Add("../qnFit/toyFitmemeda.root");
  toytree->Add("../qnFit/mcweff.root");
  TChain *datree = new TChain("mytree");
//  datree->Add("~/workdir/B2Dpp/disk101/Work/qnFit/data.root");
  datree->Add("./dataweff.root");
  TTree *datree1 = (TTree*)datree->CopyTree("abs(B_mass_con-5279.32)<20");
  TTree *datbkg1 = (TTree*)datree->CopyTree("abs(B_mass_con-5279.32)>50&&abs(B_mass_con-5279.32)<70");
//  RooRealVar cosTheta_L("cosTheta_Kst","cosTheta_L",-1,1);
//  RooRealVar eff("eff","eff",0);//1.e-4,1000);
  RooRealVar eff("eff","eff",0);//1.e-4,1000);
  RooRealVar sw("w","sw",0);
  obs->add(eff);

  RooArgList *obs2 = new RooArgList();
  obs2->add(*obs);
  obs2->add(sw);
  //put your data here
   RooDataSet *datars = new RooDataSet("datars","",datree1,*obs);
   RooDataSet *databg = new RooDataSet("databg","",datbkg1,*obs);
   RooDataSet *mctoy = new RooDataSet("mctoy","",toytree, *obs2);
   RooDataSet *mctoy_w = new RooDataSet(TString(mctoy->GetName())+TString("new"),mctoy->GetTitle(), mctoy, *mctoy->get(),0,"w");
//  double sc = datars->numEntries()*(1.-fbg)/(double)(mctoy_w->sumEntries());

   cout<<"datars->numEntries()"<<datars->numEntries()<<endl;
   cout<<"mctoy->numEntries()"<<mctoy->numEntries()<<endl;
   cout<<"databg->numEntries()"<<databg->numEntries()<<endl;
  double sc = datars->numEntries()*(1.-fbg)/(double)(mctoy_w->sumEntries());
  double scbg = datars->numEntries()*fbg/(double)(databg->numEntries());
  std::cout << "scbg sc " << scbg << " " << sc << std::endl;
  
  RooRealVar PI("PI","PI",TMath::Pi());
   //SHY 0
  RooFormulaVar *wL = new RooFormulaVar("wL","wL","1.",RooArgSet(PI));
  if(Y==8) {
    wL = new RooFormulaVar("wL","wL","1./128.*(35-1260*@1*@1+6930*(@1^4)-12012*(@1^6)+6435*(@1^8))",RooArgSet(PI,cosTheta_X));
  } else if(Y==4) {
    wL = new RooFormulaVar("wL","wL","1.0/8.*(3-30*(@1^2)+35*(@1^4))",RooArgSet(PI,cosTheta_X));
  } else if(Y==1) {
    cout << "Calculate Y1" << endl;
    wL = new RooFormulaVar("wL","wL","@1",RooArgSet(PI,cosTheta_X));
  } else if(Y==2) {
    wL = new RooFormulaVar("wL","wL","1.0/2.0*(-1.0+3*@1*@1)",RooArgSet(PI,cosTheta_X));
  }else if(Y==3) {
    wL = new RooFormulaVar("wL","wL","1.0/2.0*(-3*@1+5*(@1^3))",RooArgSet(PI,cosTheta_X));
  }else if(Y==5) {
    wL = new RooFormulaVar("wL","wL","1.0/8.0*(15*@1-70*(@1^3)+63*(@1^5))",RooArgSet(PI,cosTheta_X));
  }else if(Y==6) {
    wL = new RooFormulaVar("wL","wL","1.0/16.0*(-5+105*(@1^2)-315*(@1^4)+231*(@1^6))",RooArgSet(PI,cosTheta_X));
  }else if(Y==7) {
    wL = new RooFormulaVar("wL","wL","1./16.*(-35*@1+315*(@1^3)-693*(@1^5)+429*(@1^7))",RooArgSet(PI,cosTheta_X));
  }else if(Y==10) {
    wL = new RooFormulaVar("wL","wL","1./256.*(-63+3465*(@1^2)-30030*(@1^4)+90090*(@1^6)-109395*(@1^8)+46189*(@1^10))",RooArgSet(PI,cosTheta_X));
  }else if(Y==9) {
    wL = new RooFormulaVar("wL","wL","1./128.*(315*@1-4620*(@1^3)+18018*(@1^5)-25740*(@1^7)+12155*(@1^9))",RooArgSet(PI,cosTheta_X));
  }
  RooFormulaVar wFunc("weightmc","weight","@1/@0/1000.",RooArgSet(eff,*wL));
  RooFormulaVar wFunc1("weightmc1","weight","@1*@2/@0/1000.",RooArgSet(eff,*wL,sw));  
//  RooFormulaVar wFunc("weightmc","weight","@1/1000.",RooArgSet(eff,*wL));
//  RooFormulaVar wFunc1("weightmc1","weight","@1*@2/1000.",RooArgSet(eff,*wL,sw));  
  RooRealVar *w = (RooRealVar*)datars->addColumn(wFunc);
  RooRealVar *wbg = (RooRealVar*)databg->addColumn(wFunc);  
  RooRealVar *w1 = (RooRealVar*)mctoy->addColumn(wFunc1);  

  RooDataSet *datars_w = new RooDataSet(TString(datars->GetName())+TString("new"),datars->GetTitle(), datars, *datars->get(),0,"weightmc");
  RooDataSet *databg_w = new RooDataSet(TString(databg->GetName())+TString("new"),databg->GetTitle(), databg, *databg->get(),0,"weightmc");  
  RooDataSet *toy_w = new RooDataSet(TString(mctoy->GetName())+TString("www"),mctoy->GetTitle(), mctoy, *mctoy->get(),0,"weightmc1");  

 //=====plot===================

  int nbin(80);
  int binsize = 20;
//  if(Y==0) nbin=90;
  double xl = 1.87;
  double xr = 3.6;
  xr = xl + binsize/1000.0*nbin;
  TH1F *h100 = new TH1F("h100","",nbin,xl,xr);
  h100->Sumw2();
  TH1F *h200 = new TH1F("h200","",nbin,xl,xr);
  h200->Sumw2();  
  TH1F *h300 = new TH1F("h300","",nbin,xl,xr);
  h300->Sumw2();    
  TH1F *h400 = new TH1F("h400","",nbin,xl,xr);
  h400->Sumw2();      
  TH1F *h500 = new TH1F("h500","",nbin,xl,xr);
  h500->Sumw2();      
  
//  datars_w->fillHistogram(h100,mphik);
//  databg_w->fillHistogram(h300,mphik);
//  toy_w->fillHistogram(h200,mphik);

  datars_w->fillHistogram(h100,mppbar);
  databg_w->fillHistogram(h300,mppbar);
     toy_w->fillHistogram(h200,mppbar);

  

  h100->Add(h300,-scbg);
  h200->Scale(sc);
  TCanvas* c1 = new TCanvas("c1","");

  h200->SetMarkerColor(kBlue);
//  h200->SetLineColor(2);
//  h200->SetLineStyle(1);
//  h200->SetMarkerColor(kBlue);
  h200->SetLineColor(kBlue);  
  h200->SetLineStyle(1);
  h200->SetMarkerStyle(24);
  h100->SetMarkerStyle(20);
  h100->SetMarkerSize(1.2);
  h100->SetLineWidth(2.0);
  h100->SetLineColor(kBlack);

  h100->GetXaxis()->SetTitle("#font[52]{m_{p#bar{p}}} [GeV]");
  char title[30];
  sprintf(title,"#LTP_{%i}#GT / %i MeV",Y,binsize);
  h100->GetYaxis()->SetTitle(title);
//  h100->SetMaximum(1.2*TMath::Max(h200->GetMaximum(),h100->GetMaximum()));
//  h100->SetMinimum(1.2*TMath::Min(h200->GetMinimum(),h100->GetMinimum()));

  
  if(Y==1){
    h100->SetMaximum(0.12);
    h100->SetMinimum(-0.24);
  }
  else if(Y==2 || Y==3){
    h100->SetMaximum(0.12);
    h100->SetMinimum(-0.1);
  }
  else{
    h100->SetMaximum(0.08);
    h100->SetMinimum(-0.07);
  }

  h100->Draw("E1");  
  h200->Draw("same,e");  
  h200->Draw("same,hist");    
  h100->Draw("Same,E1");

  if(Y==1){
    TLegend *leg = new TLegend(0.6,0.7,0.9,0.9);
    leg->SetBorderSize(0);
    leg->SetLineColor(1);
    leg->SetTextFont(132);
    leg->SetLineWidth(3);
    leg->SetFillColor(0);
    leg->SetFillStyle(0);  
    leg->AddEntry(h100,"Data");
    leg->AddEntry(h200,"Fit");
    leg->Draw("same");
  }

#if 0
  if(Y==1) {
   TLegend *leg = new TLegend(0.56,0.58,0.96,0.88,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetLineColor(1);
//   leg->SetLineStyle(lineStyle);
   leg->SetLineWidth(3);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);  
   leg->AddEntry(h100,"data");
   leg->AddEntry(h500,"FitEx + Pcs","l");        
   leg->AddEntry(h200,"Fit + Pcs","l");   
    leg->Draw();
  } 
 #endif 
   TLine *line = new TLine(xl,0,xr,0);

   line->SetLineColor(kRed);
   line->SetLineStyle(kDashed);
   line->SetLineWidth(2);
   line->Draw();


  char alpha[]= "abcdefghij";
  char toprint[30];
  sprintf(toprint,"#LTP_{%i}#GT",Y);
  c1->Update();  
  //lhcbLatex->DrawLatex(0.05*(-c1->GetUxmin()+c1->GetUxmax())+c1->GetUxmin(),  0.92*(-c1->GetUymin()+c1->GetUymax())+c1->GetUymin(), toprint);

  char name[20];
//  sprintf(name,"eps/shy%s_%i.eps",mppbar.GetName(),Y);   c1->SaveAs(name);  
  sprintf(name,"eps/shy%s_%i.pdf",mppbar.GetName(),Y);   c1->SaveAs(name);   
//  sprintf(name,"eps/shy%s_%i.png",mppbar.GetName(),Y);   c1->SaveAs(name);   
//  sprintf(name,"eps/shy%s_%i.C",  mppbar.GetName(),Y);   c1->SaveAs(name);  
  
}
