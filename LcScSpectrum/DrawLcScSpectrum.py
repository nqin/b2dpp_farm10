import pandas as pd
import matplotlib.pyplot as plt
import ROOT
import numpy as np

nLdict = {"Lc1S":0,
          "Lc2S":0,
          "Lc3S":0,
          "Lc1P":0,
          "Lc2P":0,
          "Lc3P":0,
          "Lc1D":0,
          "Lc2D":0,
          "Sc1S":0,
          "Sc2S":0,
          "Sc3S":0,
          "Sc1P":0,
          "Sc2P":0,
          "Sc3P":0,
          "Sc1D":0,
          "Sc2D":0}

def whichBin(dfrow:pd.Series)->int:
    if dfrow["Name"] == "Lc":
        if dfrow["L"] == "S":
            return 2
        elif dfrow["L"] == "P":
            if dfrow["Spin"] == 0.5:
                return 3
            if dfrow["Spin"] == 1.5:
                return 4
        elif dfrow["L"] == "D":
            if dfrow["Spin"] == 1.5:
                return 5
            if dfrow["Spin"] == 2.5:
                return 6
        else:
            print("ERROR: Lc L not found")
            return -1
    elif dfrow["Name"] == "Sc":
        if dfrow["L"] == "S":
            if dfrow["Spin"] == 0.5:
                return 2
            if dfrow["Spin"] == 1.5:
                return 3
        if dfrow["L"] == "P":
            if dfrow["Spin"] == 0.5:
                return 4
            if dfrow["Spin"] == 1.5:
                return 5
            if dfrow["Spin"] == 2.5:
                return 6
        if dfrow["L"] == "D":
            if dfrow["Spin"] == 0.5:
                return 7
            if dfrow["Spin"] == 1.5:
                return 8
            if dfrow["Spin"] == 2.5:
                return 9
            if dfrow["Spin"] == 3.5:
                return 10
        else:
            print("ERROR: Sc L not found")
            return -1
    else:
        return -1

def oneLcTheory(dfrow:pd.Series)->ROOT.TH1D:
    tempName = dfrow["Name"] + "_" + str(dfrow["NameNo"])
    htemp = ROOT.TH1D(tempName,tempName,10,0,10)
    nbin = whichBin(dfrow)
    htemp.SetBinContent(nbin,dfrow["Ebert"])
    return htemp

def oneLcFound(dfrow:pd.Series):
    nbin = whichBin(dfrow)
    down = dfrow["Mass_exp"]-dfrow["Mass_sigma_down"]
    up = dfrow["Mass_exp"]+dfrow["Mass_sigma_up"]
    boxtemp = ROOT.TBox(nbin-1,down,nbin,up)
    return boxtemp

def DrawName(dfrow:pd.Series):
    if dfrow["Found"] == 1:
        nbin = whichBin(dfrow)
        Mass = dfrow["NameNo"]
        if dfrow["Name"] == "Lc":
            text = "#Lambda_{c}" + "(" + str(Mass) + ")"
        if dfrow["Name"] == "Sc":
            text = "#Sigma_{c}" + "(" + str(Mass) + ")"
        lat1 = ROOT.TLatex()
        lat1.SetTextSize(0.03)
        lat1.SetTextColor(ROOT.kRed)
        if dfrow["Name"] == "Lc":
            lat1.DrawLatex(nbin-0.85,Mass+15,text)
        if dfrow["Name"] == "Sc":
            lat1.DrawLatex(nbin-1,Mass+15,text)
        return
    else:
        return

def DrawnL(dfrow:pd.Series):
    nLName = dfrow["Name"] + str(dfrow["n"]) + dfrow["L"]
    nL = str(dfrow["n"]) + dfrow["L"]
    if nLdict[nLName] == 0:
        nLdict[nLName] += 1
        nbin = whichBin(dfrow)
        Mass = dfrow["NameNo"]
        lat1 = ROOT.TLatex()
        lat1.SetTextSize(0.04)
        lat1.SetTextColorAlpha(ROOT.kGray+3,0.5)
        if dfrow["Name"] == "Lc":
            lat1.DrawLatex(nbin-1.3,Mass,nL)
        if dfrow["Name"] == "Sc":
            lat1.DrawLatex(nbin-1.5,Mass,nL)
        return
    else:
        return


def DrawSc2800():
    Mass2800 = 2792
    Mass_sigma_down = 5
    Mass_sigma_up = 14
    down = Mass2800 - Mass_sigma_down
    up = Mass2800 + Mass_sigma_up
    box2800 = ROOT.TBox(3,down,6,up)
    lat2800 = ROOT.TLatex()
    lat2800.SetTextSize(0.03)
    lat2800.SetTextColor(ROOT.kRed)
    text2800 = "#Sigma_{c}(2800) predicted #font[12]{J^{P}} = #font[132]{#frac{1}{2}^{#minus}, #frac{3}{2}^{#minus}, #frac{5}{2}^{#minus}}"
    #text2800 = "#splitline{#Sigma_{c}(2800)}{predicted #font[12]{J^{P}} = #font[132]{#frac{1}{2}^{#minus}, #frac{3}{2}^{#minus}, #frac{5}{2}^{#minus}}}"
    lat2800.DrawLatex(2.5,Mass2800+35,text2800)
    return box2800

def DrawLc():
    df = pd.read_csv("./LcSc.csv")
    h1 = ROOT.TH1D("h1", "h1", 7,0,7)
    h1.SetStats(0)
    h1.GetYaxis().SetTitle("Mass / MeV")
    ROOT.gStyle.SetOptTitle(0)
    ax1 = h1.GetXaxis()
    ax1.CenterLabels(ROOT.kTRUE)
    ax1.SetNdivisions(-7)
    ax1.SetLabelOffset(0.02)
    ax1.ChangeLabel(1,-1,-1,-1,-1,-1,"#font[12]{J^{P}} ")
    ax1.ChangeLabel(2,-1,-1,-1,-1,-1,"#font[132]{#frac{1}{2}^{+}}")
    ax1.ChangeLabel(3,-1,-1,-1,-1,-1,"#font[132]{#frac{1}{2}^{#minus}}")
    ax1.ChangeLabel(4,-1,-1,-1,-1,-1,"#font[132]{#frac{3}{2}^{#minus}}")
    ax1.ChangeLabel(5,-1,-1,-1,-1,-1,"#font[132]{#frac{3}{2}^{+}}")
    ax1.ChangeLabel(6,-1,-1,-1,-1,-1,"#font[132]{#frac{5}{2}^{+}}")
    ax1.ChangeLabel(7,-1,-1,-1,-1,-1," ")
#    ax1.ChangeLabel(8,-1,-1,-1,-1,-1,"#font[132]{#frac{1}{2}^{+}}")
#    ax1.ChangeLabel(9,-1,-1,-1,-1,-1,"#font[132]{#frac{3}{2}^{+}}")
#    ax1.ChangeLabel(10,-1,-1,-1,-1,-1," ")

    c1 = ROOT.TCanvas("c1", "c1", 800, 600)
    h1.Draw()
    h1.GetYaxis().SetRangeUser(2200,3500)

    l1 = ROOT.TLine(0,2802,7,2802)
    l1.SetLineStyle(2)
    l1.SetLineWidth(2)
    l1.SetLineColorAlpha(ROOT.kBlue,0.5)
    l1.Draw("SAME")
    DpLat = ROOT.TLatex()
    DpLat.SetTextFont(12)
    DpLat.SetTextColorAlpha(ROOT.kBlue,0.5)
    DpLat.DrawLatex(6.3,2830,"D^{0}p")
    LcLat = ROOT.TLatex()
    LcLat.SetTextFont(132)
    LcLat.SetTextSize(0.15)
    LcLat.SetTextColorAlpha(ROOT.kGreen+3,0.2)
    LcLat.DrawLatex(6,2300,"#Lambda_{c}")

    hLcTheory = []
    hLcFound = []
    for i in range(0,len(df)):
        row = df.iloc[i]
        if row["Name"] == "Sc":
            continue

        DrawnL(row)
        hLcTheory.append(oneLcTheory(row))
        hLcTheory[i].Draw("same,][")
        hLcTheory[i].SetLineWidth(2)
        hLcTheory[i].SetLineColor(ROOT.kBlack)

        hLcFound.append(oneLcFound(row))
        hLcFound[i].Draw("same")
        hLcFound[i].SetLineColor(ROOT.kRed)
        hLcFound[i].SetLineWidth(1)
        hLcFound[i].SetFillStyle(0)

        DrawName(row)
    c1.SaveAs("LcSpectrum.pdf")

def DrawSc():
    df = pd.read_csv("./LcSc.csv")
    h1 = ROOT.TH1D("h1", "h1", 11,0,11)
    h1.SetStats(0)
    h1.GetYaxis().SetTitle("Mass / MeV")
    ROOT.gStyle.SetOptTitle(0)
    ax1 = h1.GetXaxis()
    ax1.CenterLabels(ROOT.kTRUE)
    ax1.SetNdivisions(-11)
    ax1.SetLabelOffset(0.02)
    ax1.ChangeLabel(1,-1,-1,-1,-1,-1,"#font[12]{J^{P}} ")
    ax1.ChangeLabel(2,-1,-1,-1,-1,-1,"#font[132]{#frac{1}{2}^{+}}")
    ax1.ChangeLabel(3,-1,-1,-1,-1,-1,"#font[132]{#frac{3}{2}^{+}}")
    ax1.ChangeLabel(4,-1,-1,-1,-1,-1,"#font[132]{#frac{1}{2}^{#minus}}")
    ax1.ChangeLabel(5,-1,-1,-1,-1,-1,"#font[132]{#frac{3}{2}^{#minus}}")
    ax1.ChangeLabel(6,-1,-1,-1,-1,-1,"#font[132]{#frac{5}{2}^{#minus}}")
    ax1.ChangeLabel(7,-1,-1,-1,-1,-1,"#font[132]{#frac{1}{2}^{+}}")
    ax1.ChangeLabel(8,-1,-1,-1,-1,-1,"#font[132]{#frac{3}{2}^{+}}")
    ax1.ChangeLabel(9,-1,-1,-1,-1,-1,"#font[132]{#frac{5}{2}^{+}}")
    ax1.ChangeLabel(10,-1,-1,-1,-1,-1,"#font[132]{#frac{7}{2}^{+}}")
    ax1.ChangeLabel(11,-1,-1,-1,-1,-1," ")

    c1 = ROOT.TCanvas("c1", "c1", 800, 600)
    h1.Draw()
    h1.GetYaxis().SetRangeUser(2350,3550)

    l1 = ROOT.TLine(0,2802,11,2802)
    l1.SetLineStyle(2)
    l1.SetLineWidth(2)
    l1.SetLineColorAlpha(ROOT.kBlue,0.5)
    l1.Draw("SAME")
    DpLat = ROOT.TLatex()
    DpLat.SetTextFont(12)
    DpLat.SetTextColorAlpha(ROOT.kBlue,0.5)
    DpLat.DrawLatex(10,2830,"D^{0}p")
    LcLat = ROOT.TLatex()
    LcLat.SetTextFont(132)
    LcLat.SetTextSize(0.15)
    LcLat.SetTextColorAlpha(ROOT.kGreen+3,0.2)
    LcLat.DrawLatex(9.5,2450,"#Sigma_{c}")

    hLcTheory = []
    hLcFound = []
    for i in range(0,len(df)):
        row = df.iloc[i]
        if row["Name"] == "Lc":
            hLcTheory.append(oneLcTheory(row))
            hLcFound.append(oneLcFound(row))
            continue

        DrawnL(row)

        hLcTheory.append(oneLcTheory(row))
        hLcTheory[i].Draw("same,][")
        hLcTheory[i].SetLineWidth(2)
        hLcTheory[i].SetLineColor(ROOT.kBlack)
        
        #if i < len(df):
        #    hLcFound.append(oneLcFound(row))
        #else:
        #    hLcFound.append(DrawSc2800())
        hLcFound.append(oneLcFound(row))
        hLcFound[i].Draw("same")
        hLcFound[i].SetLineColor(ROOT.kRed)
        hLcFound[i].SetLineWidth(1)
        hLcFound[i].SetFillStyle(0)

        DrawName(row)
    
    box2800 = DrawSc2800()
    box2800.Draw("same")
    box2800.SetLineColor(ROOT.kRed)
    box2800.SetLineWidth(1)
    box2800.SetFillStyle(0)
    
    c1.SaveAs("ScSpectrum.pdf")




if __name__ == "__main__":
    DrawLc()
    DrawSc()
