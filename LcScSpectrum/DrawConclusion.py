import pandas as pd
import matplotlib.pyplot as plt
import ROOT
import numpy as np

nLdict = {"Lc1S":0,
          "Lc2S":0,
          "Lc3S":0,
          "Lc1P":0,
          "Lc2P":0,
          "Lc3P":0,
          "Lc1D":0,
          "Lc2D":0,
          "Sc1S":0,
          "Sc2S":0,
          "Sc3S":0,
          "Sc1P":0,
          "Sc2P":0,
          "Sc3P":0,
          "Sc1D":0,
          "Sc2D":0}

def whichBin(dfrow:pd.Series)->int:
    if dfrow["Name"] == "Lc":
        if dfrow["L"] == "S":
            return 2
        elif dfrow["L"] == "P":
            if dfrow["Spin"] == 0.5:
                return 3
            if dfrow["Spin"] == 1.5:
                return 4
        elif dfrow["L"] == "D":
            if dfrow["Spin"] == 1.5:
                return 5
            if dfrow["Spin"] == 2.5:
                return 6
        else:
            print("ERROR: Lc L not found")
            return -1
    elif dfrow["Name"] == "Sc":
        if dfrow["L"] == "S":
            if dfrow["Spin"] == 0.5:
                return 2
            if dfrow["Spin"] == 1.5:
                return 3
        if dfrow["L"] == "P":
            if dfrow["Spin"] == 0.5:
                return 4
            if dfrow["Spin"] == 1.5:
                return 5
            if dfrow["Spin"] == 2.5:
                return 6
        if dfrow["L"] == "D":
            if dfrow["Spin"] == 0.5:
                return 7
            if dfrow["Spin"] == 1.5:
                return 8
            if dfrow["Spin"] == 2.5:
                return 9
            if dfrow["Spin"] == 3.5:
                return 10
        else:
            print("ERROR: Sc L not found")
            return -1
    else:
        return -1

def oneLcTheory(dfrow:pd.Series)->ROOT.TH1D:
    tempName = dfrow["Name"] + "_" + str(dfrow["NameNo"])
    htemp = ROOT.TH1D(tempName,tempName,10,0,10)
    nbin = whichBin(dfrow)
    htemp.SetBinContent(nbin,dfrow["Ebert"])
    return htemp

def oneLcFound(dfrow:pd.Series):
    nbin = whichBin(dfrow)
    down = dfrow["Mass_exp"]-dfrow["Mass_sigma_down"]
    up = dfrow["Mass_exp"]+dfrow["Mass_sigma_up"]
    boxtemp = ROOT.TBox(nbin-1,down,nbin,up)
    return boxtemp

def DrawName(dfrow:pd.Series):
    if dfrow["Found"] == 1:
        nbin = whichBin(dfrow)
        Mass = dfrow["NameNo"]
        if dfrow["Name"] == "Lc":
            text = "#Lambda_{c}" + "(" + str(Mass) + ")"
        if dfrow["Name"] == "Sc":
            text = "#Sigma_{c}" + "(" + str(Mass) + ")"
        lat1 = ROOT.TLatex()
        lat1.SetTextSize(0.03)
        lat1.SetTextColor(ROOT.kRed)
        if dfrow["Name"] == "Lc":
            lat1.DrawLatex(nbin-0.85,Mass+15,text)
        if dfrow["Name"] == "Sc":
            lat1.DrawLatex(nbin-1,Mass+15,text)
        return
    else:
        return

def DrawnL(dfrow:pd.Series):
    nLName = dfrow["Name"] + str(dfrow["n"]) + dfrow["L"]
    nL = str(dfrow["n"]) + dfrow["L"]
    if nLdict[nLName] == 0:
        nLdict[nLName] += 1
        nbin = whichBin(dfrow)
        Mass = dfrow["NameNo"]
        lat1 = ROOT.TLatex()
        lat1.SetTextSize(0.04)
        lat1.SetTextColorAlpha(ROOT.kGray+3,0.5)
        if dfrow["Name"] == "Lc":
            lat1.DrawLatex(nbin-1.3,Mass,nL)
        if dfrow["Name"] == "Sc":
            lat1.DrawLatex(nbin-1.5,Mass,nL)
        return
    else:
        return

def DrawLcone_1m():
    mass = 2816
    mass_sigma_up = 8.60
    mass_sigma_down = 13.00
    up = mass + mass_sigma_up
    down = mass - mass_sigma_down
    box = ROOT.TBox(2,down,3,up)
    box.SetFillColorAlpha(ROOT.kCyan-1,0.5)
    return box
def DrawLcone_3p():
    mass = 2816
    mass_sigma_up = 8.60
    mass_sigma_down = 13.00
    up = mass + mass_sigma_up
    down = mass - mass_sigma_down
    box = ROOT.TBox(4,down,5,up)
    box.SetFillColorAlpha(ROOT.kCyan-1,0.5)
    return box
def DrawLctwo_3m():
    mass = 3259
    mass_sigma_up = 12.04
    mass_sigma_down = 20.25 
    up = mass + mass_sigma_up
    down = mass - mass_sigma_down
    box = ROOT.TBox(3,down,4,up)
    box.SetFillColorAlpha(ROOT.kOrange,0.5)
    return box
def DrawLcthr_1m():
    mass = 2977
    mass_sigma_up = 16.76
    mass_sigma_down = 20.61
    up = mass + mass_sigma_up
    down = mass - mass_sigma_down
    box = ROOT.TBox(2,down,3,up)
    box.SetFillColorAlpha(ROOT.kBlue,0.3)
    return box
def DrawLcthr_1p():
    mass = 2977
    mass_sigma_up = 16.76
    mass_sigma_down = 20.61
    up = mass + mass_sigma_up
    down = mass - mass_sigma_down
    box = ROOT.TBox(1,down,2,up)
    box.SetFillColorAlpha(ROOT.kBlue,0.3)
    return box

def DrawScone_1m():
    mass = 2816
    mass_sigma_up = 8.60
    mass_sigma_down = 13.00
    up = mass + mass_sigma_up
    down = mass - mass_sigma_down
    box = ROOT.TBox(3,down,4,up)
    box.SetFillColorAlpha(ROOT.kCyan-1,0.5)
    return box
def DrawScone_3p_1():
    mass = 2816
    mass_sigma_up = 8.60
    mass_sigma_down = 13.00
    up = mass + mass_sigma_up
    down = mass - mass_sigma_down
    box = ROOT.TBox(2,down,3,up)
    box.SetFillColorAlpha(ROOT.kCyan-1,0.5)
    return box
def DrawScone_3p_2():
    mass = 2816
    mass_sigma_up = 8.60
    mass_sigma_down = 13.00
    up = mass + mass_sigma_up
    down = mass - mass_sigma_down
    box = ROOT.TBox(7,down,8,up)
    box.SetFillColorAlpha(ROOT.kCyan-1,0.5)
    return box
def DrawSctwo_3m():
    mass = 3259
    mass_sigma_up = 12.04
    mass_sigma_down = 20.25 
    up = mass + mass_sigma_up
    down = mass - mass_sigma_down
    box = ROOT.TBox(4,down,5,up)
    box.SetFillColorAlpha(ROOT.kOrange,0.5)
    return box
def DrawScthr_1m():
    mass = 2977
    mass_sigma_up = 16.76
    mass_sigma_down = 20.61
    up = mass + mass_sigma_up
    down = mass - mass_sigma_down
    box = ROOT.TBox(3,down,4,up)
    box.SetFillColorAlpha(ROOT.kBlue,0.3)
    return box
def DrawScthr_1p_1():
    mass = 2977
    mass_sigma_up = 16.76
    mass_sigma_down = 20.61
    up = mass + mass_sigma_up
    down = mass - mass_sigma_down
    box = ROOT.TBox(1,down,2,up)
    box.SetFillColorAlpha(ROOT.kBlue,0.3)
    return box
def DrawScthr_1p_2():
    mass = 2977
    mass_sigma_up = 16.76
    mass_sigma_down = 20.61
    up = mass + mass_sigma_up
    down = mass - mass_sigma_down
    box = ROOT.TBox(6,down,7,up)
    box.SetFillColorAlpha(ROOT.kBlue,0.3)
    return box

def DrawSc2800():
    Mass2800 = 2792
    Mass_sigma_down = 5
    Mass_sigma_up = 14
    down = Mass2800 - Mass_sigma_down
    up = Mass2800 + Mass_sigma_up
    box2800 = ROOT.TBox(3,down,6,up)
    lat2800 = ROOT.TLatex()
    lat2800.SetTextSize(0.03)
    lat2800.SetTextColor(ROOT.kRed)
    #text2800 = "#Sigma_{c}(2800) predicted #font[12]{J^{P}} = #font[132]{#frac{1}{2}^{#minus}, #frac{3}{2}^{#minus}, #frac{5}{2}^{#minus}}"
    text2800 = "#Sigma_{c}(2800)"
    lat2800.DrawLatex(4.5,Mass2800+35,text2800)
    return box2800

def DrawLc():
    df = pd.read_csv("./LcSc.csv")
    h1 = ROOT.TH1D("h1", "h1", 7,0,7)
    h1.SetStats(0)
    h1.GetYaxis().SetTitle("Mass / MeV")
    ROOT.gStyle.SetOptTitle(0)
    ax1 = h1.GetXaxis()
    ax1.CenterLabels(ROOT.kTRUE)
    ax1.SetNdivisions(-7)
    ax1.SetLabelOffset(0.02)
    ax1.ChangeLabel(1,-1,-1,-1,-1,-1,"#font[12]{J^{P}} ")
    ax1.ChangeLabel(2,-1,-1,-1,-1,-1,"#font[132]{#frac{1}{2}^{+}}")
    ax1.ChangeLabel(3,-1,-1,-1,-1,-1,"#font[132]{#frac{1}{2}^{#minus}}")
    ax1.ChangeLabel(4,-1,-1,-1,-1,-1,"#font[132]{#frac{3}{2}^{#minus}}")
    ax1.ChangeLabel(5,-1,-1,-1,-1,-1,"#font[132]{#frac{3}{2}^{+}}")
    ax1.ChangeLabel(6,-1,-1,-1,-1,-1,"#font[132]{#frac{5}{2}^{+}}")
    ax1.ChangeLabel(7,-1,-1,-1,-1,-1," ")
#    ax1.ChangeLabel(8,-1,-1,-1,-1,-1,"#font[132]{#frac{1}{2}^{+}}")
#    ax1.ChangeLabel(9,-1,-1,-1,-1,-1,"#font[132]{#frac{3}{2}^{+}}")
#    ax1.ChangeLabel(10,-1,-1,-1,-1,-1," ")

    c1 = ROOT.TCanvas("c1", "c1", 800, 600)
    h1.Draw()
    h1.GetYaxis().SetRangeUser(2200,3500)

    l1 = ROOT.TLine(0,2594,7,2594)
    l1.SetLineStyle(2)
    l1.SetLineWidth(2)
    l1.SetLineColorAlpha(ROOT.kBlue,0.5)
    l1.Draw("SAME")
    l2 = ROOT.TLine(0,2802,7,2802)
    l2.SetLineStyle(2)
    l2.SetLineWidth(2)
    l2.SetLineColorAlpha(ROOT.kRed,0.5)
    l2.Draw("SAME")
    l3 = ROOT.TLine(0,2834,7,2834)
    l3.SetLineStyle(2)
    l3.SetLineWidth(2)
    l3.SetLineColorAlpha(ROOT.kBlue,0.5)
    l3.Draw("SAME")
    l4 = ROOT.TLine(0,2945,7,2945)
    l4.SetLineStyle(2)
    l4.SetLineWidth(2)
    l4.SetLineColorAlpha(ROOT.kBlue,0.5)
    l4.Draw("SAME")
    DpLat = ROOT.TLatex()
    DpLat.SetTextFont(12)
    DpLat.SetTextColorAlpha(ROOT.kRed,0.5)
    DpLat.DrawLatex(6.3,2802-70,"D^{#font[132]{0}}p")
    ScPipLat = ROOT.TLatex()
    ScPipLat.SetTextFont(12)
    ScPipLat.SetTextColorAlpha(ROOT.kBlue,0.5)
    ScPipLat.DrawLatex(6.3,2594+20,"\Sigma_{c}^{#font[132]{0}}\pi^{+}")
    LcEtaLat = ROOT.TLatex()
    LcEtaLat.SetTextFont(12)
    LcEtaLat.SetTextColorAlpha(ROOT.kBlue,0.5)
    LcEtaLat.DrawLatex(6.3,2834+20,"\Lambda_{c}^{+}\eta")
    DstpLat = ROOT.TLatex()
    DstpLat.SetTextFont(12)
    DstpLat.SetTextColorAlpha(ROOT.kBlue,0.5)
    DstpLat.DrawLatex(6.3,2945+20,"D^{#font[132]{*0}}p")
    LcLat = ROOT.TLatex()
    LcLat.SetTextFont(132)
    LcLat.SetTextSize(0.15)
    LcLat.SetTextColorAlpha(ROOT.kGreen+3,0.2)
    LcLat.DrawLatex(6,2300,"#Lambda_{c}")

    hLcTheory = []
    hLcFound = []
    for i in range(0,len(df)):
        row = df.iloc[i]
        if row["Name"] == "Sc":
            continue

        DrawnL(row)
        hLcTheory.append(oneLcTheory(row))
        hLcTheory[i].Draw("same,][")
        hLcTheory[i].SetLineWidth(2)
        hLcTheory[i].SetLineColor(ROOT.kBlack)

        hLcFound.append(oneLcFound(row))
        hLcFound[i].Draw("same")
        hLcFound[i].SetLineColor(ROOT.kRed)
        hLcFound[i].SetLineWidth(1)
        hLcFound[i].SetFillStyle(0)

        DrawName(row)
    
    boxLcone_1m = DrawLcone_1m()
    boxLcone_3p = DrawLcone_3p()
    boxLctwo_3m = DrawLctwo_3m()
    boxLcthr_1m = DrawLcthr_1m()
    boxLcthr_1p = DrawLcthr_1p()
    boxLcone_1m.Draw("same")
    boxLcone_3p.Draw("same")
    boxLctwo_3m.Draw("same")
    boxLcthr_1m.Draw("same")
    boxLcthr_1p.Draw("same")

    leg1 = ROOT.TLegend(0.75,0.75,0.9,0.9)
    leg1.AddEntry(boxLcone_1m, "#font[12]{#Lambda_{c}#it{-1}}","f")
    leg1.AddEntry(boxLctwo_3m, "#font[12]{#Lambda_{c}#it{-2}}","f")
    leg1.AddEntry(boxLcthr_1m, "#font[12]{#Lambda_{c}#it{-3}}","f")
    leg1.Draw("same")

    c1.SaveAs("LcConclusion.pdf")

def DrawSc():
    df = pd.read_csv("./LcSc.csv")
    h1 = ROOT.TH1D("h1", "h1", 11,0,11)
    h1.SetStats(0)
    h1.GetYaxis().SetTitle("Mass / MeV")
    ROOT.gStyle.SetOptTitle(0)
    ax1 = h1.GetXaxis()
    ax1.CenterLabels(ROOT.kTRUE)
    ax1.SetNdivisions(-11)
    ax1.SetLabelOffset(0.02)
    ax1.ChangeLabel(1,-1,-1,-1,-1,-1,"#font[12]{J^{P}} ")
    ax1.ChangeLabel(2,-1,-1,-1,-1,-1,"#font[132]{#frac{1}{2}^{+}}")
    ax1.ChangeLabel(3,-1,-1,-1,-1,-1,"#font[132]{#frac{3}{2}^{+}}")
    ax1.ChangeLabel(4,-1,-1,-1,-1,-1,"#font[132]{#frac{1}{2}^{#minus}}")
    ax1.ChangeLabel(5,-1,-1,-1,-1,-1,"#font[132]{#frac{3}{2}^{#minus}}")
    ax1.ChangeLabel(6,-1,-1,-1,-1,-1,"#font[132]{#frac{5}{2}^{#minus}}")
    ax1.ChangeLabel(7,-1,-1,-1,-1,-1,"#font[132]{#frac{1}{2}^{+}}")
    ax1.ChangeLabel(8,-1,-1,-1,-1,-1,"#font[132]{#frac{3}{2}^{+}}")
    ax1.ChangeLabel(9,-1,-1,-1,-1,-1,"#font[132]{#frac{5}{2}^{+}}")
    ax1.ChangeLabel(10,-1,-1,-1,-1,-1,"#font[132]{#frac{7}{2}^{+}}")
    ax1.ChangeLabel(11,-1,-1,-1,-1,-1," ")

    c1 = ROOT.TCanvas("c1", "c1", 800, 600)
    h1.Draw()
    h1.GetYaxis().SetRangeUser(2350,3550)

    l1 = ROOT.TLine(0,2594,11,2594)
    l1.SetLineStyle(2)
    l1.SetLineWidth(2)
    l1.SetLineColorAlpha(ROOT.kBlue,0.5)
    l1.Draw("SAME")
    l2 = ROOT.TLine(0,2802,11,2802)
    l2.SetLineStyle(2)
    l2.SetLineWidth(2)
    l2.SetLineColorAlpha(ROOT.kRed,0.5)
    l2.Draw("SAME")
    l3 = ROOT.TLine(0,2834,11,2834)
    l3.SetLineStyle(2)
    l3.SetLineWidth(2)
    l3.SetLineColorAlpha(ROOT.kBlue,0.5)
    l3.Draw("SAME")
    l4 = ROOT.TLine(0,2945,11,2945)
    l4.SetLineStyle(2)
    l4.SetLineWidth(2)
    l4.SetLineColorAlpha(ROOT.kBlue,0.5)
    l4.Draw("SAME")

    DpLat = ROOT.TLatex()
    DpLat.SetTextFont(12)
    DpLat.SetTextColorAlpha(ROOT.kRed,0.5)
    DpLat.DrawLatex(10,2802-70,"D^{#font[132]{0}}p")
    ScPipLat = ROOT.TLatex()
    ScPipLat.SetTextFont(12)
    ScPipLat.SetTextColorAlpha(ROOT.kBlue,0.5)
    ScPipLat.DrawLatex(10,2594+20,"\Sigma_{c}^{#font[132]{0}}\pi^{+}")
    LcEtaLat = ROOT.TLatex()
    LcEtaLat.SetTextFont(12)
    LcEtaLat.SetTextColorAlpha(ROOT.kBlue,0.5)
    LcEtaLat.DrawLatex(10,2834+20,"\Lambda_{c}^{+}\eta")
    DstpLat = ROOT.TLatex()
    DstpLat.SetTextFont(12)
    DstpLat.SetTextColorAlpha(ROOT.kBlue,0.5)
    DstpLat.DrawLatex(10,2945+20,"D^{#font[132]{*0}}p")

    LcLat = ROOT.TLatex()
    LcLat.SetTextFont(132)
    LcLat.SetTextSize(0.15)
    LcLat.SetTextColorAlpha(ROOT.kGreen+3,0.2)
    LcLat.DrawLatex(9.5,2450,"#Sigma_{c}")

    hLcTheory = []
    hLcFound = []
    for i in range(0,len(df)):
        row = df.iloc[i]
        if row["Name"] == "Lc":
            hLcTheory.append(oneLcTheory(row))
            hLcFound.append(oneLcFound(row))
            continue

        DrawnL(row)

        hLcTheory.append(oneLcTheory(row))
        hLcTheory[i].Draw("same,][")
        hLcTheory[i].SetLineWidth(2)
        hLcTheory[i].SetLineColor(ROOT.kBlack)
        
        #if i < len(df):
        #    hLcFound.append(oneLcFound(row))
        #else:
        #    hLcFound.append(DrawSc2800())
        hLcFound.append(oneLcFound(row))
        hLcFound[i].Draw("same")
        hLcFound[i].SetLineColor(ROOT.kRed)
        hLcFound[i].SetLineWidth(1)
        hLcFound[i].SetFillStyle(0)

        DrawName(row)
    
    box2800 = DrawSc2800()
    box2800.Draw("same")
    box2800.SetLineColor(ROOT.kRed)
    box2800.SetLineWidth(1)
    box2800.SetFillStyle(0)
    
    boxScone_1m = DrawScone_1m()
    boxScone_3p_1 = DrawScone_3p_1()
    boxScone_3p_2 = DrawScone_3p_2()
    boxSctwo_3m = DrawSctwo_3m()
    boxScthr_1m = DrawScthr_1m()
    boxScthr_1p_1 = DrawScthr_1p_1()
    boxScthr_1p_2 = DrawScthr_1p_2()
    boxScone_1m.Draw("same")
    boxScone_3p_1.Draw("same")
    boxScone_3p_2.Draw("same")
    boxSctwo_3m.Draw("same")
    boxScthr_1m.Draw("same")
    boxScthr_1p_1.Draw("same")
    boxScthr_1p_2.Draw("same")

    leg1 = ROOT.TLegend(0.75,0.75,0.9,0.9)
    leg1.AddEntry(boxScone_1m, "#font[12]{#Lambda_{c}#it{-1}}","f")
    leg1.AddEntry(boxSctwo_3m, "#font[12]{#Lambda_{c}#it{-2}}","f")
    leg1.AddEntry(boxScthr_1m, "#font[12]{#Lambda_{c}#it{-3}}","f")
    leg1.Draw("same")

    c1.SaveAs("ScConclusion.pdf")




if __name__ == "__main__":
    DrawLc()
    DrawSc()
