#include "RooDalitz/RooDalitzAmplitude.h"
#include "RooDalitz/Resonances.h"
#include "TList.h"
#include "RooRealVar.h"
#include "RooArgList.h"
#include "TSystem.h"
#include "RooFormulaVar.h"
#include "RooDataSet.h"
#include "TFile.h"
#include "RooFitResult.h"
#include "TDatime.h"
#include "RooRealConstant.h"
#include "TRandom2.h"
//#include "TRandom3.h"
#include "RooMinimizer.h"
#include "RooAddition.h"
#include "RooDalitz/RooFGaussian.h"
#include "RooMsgService.h"
#include <TMatrixD.h>
//#include "nvToolsExt.h"
#define NFit 1
//#define ZNR
#define BW
//#define Z21P

using namespace RooFit;
//int DEVICE_NUM_ext;
//int sDevice_ext;

TRandom2 *g_rnd;


RooRealVar *fbg = new RooRealVar("fbg", "bkg fraction", 0.0548);
//RooRealVar *fSP = new RooRealVar("fSP", "SimgacPi fraction", 0.01,0,1);
RooRealVar *fSP = new RooRealVar("fSP", "SimgacPi fraction", 0.5);
//RooRealVar *fSP = new RooRealVar("fSP", "SimgacPi fraction",1);

#include "RooDalitz/Setup.h"

int main(int argc, char **argv)
{
    TDatime BeginTime;
    int seed = 1+atoi( argv[1] );
    g_rnd = new TRandom2(); 
    g_rnd->SetSeed(seed);
    std::cout <<"Seed " << seed << std::endl;
    //read config file. (how many GPUs are used, start from which.) 
    //std::ifstream gpu_fin("config");
    //gpu_fin >> DEVICE_NUM_ext >> sDevice_ext;

    std::cout << "Time(begin)  " << BeginTime.GetYear() << "." << BeginTime.GetMonth() << "." << BeginTime.GetDay() << "    " << BeginTime.GetHour() << ":" << BeginTime.GetMinute() << ":" << BeginTime.GetSecond() << std::endl;
  
    //============================
    //============================
    // resonance setup
    //============================
    //============================
#include "RooDalitz/Res.h"
#include "RooDalitz/MODEL/model0.h"
    //==================
    //==================
    //create dataset
    //==================
    //==================
    RooRealVar mppbar("mppbar", "mppbar", 0);
    RooRealVar mD0p("mD0p", "mD0p", 0);
    RooRealVar mD0pbar("mD0pbar", "mD0ppbar", 0);
    RooRealVar cosTheta_X("cosTheta_X", "cosTheta_X", -1, 1);
    RooRealVar cosTheta_L("cosTheta_L", "cosTheta_L", -1, 1);
    RooRealVar cosTheta_p("cosTheta_p", "cosTheta_p", -1, 1);
    RooRealVar cosTheta_pbar("cosTheta_pbar", "cosTheta_pbar", -1, 1);
    RooArgList *obs = new RooArgList();

    obs->add(mppbar);
    obs->add(mD0p);
    obs->add(mD0pbar);
    obs->add(cosTheta_X);
    obs->add(cosTheta_L);
    obs->add(cosTheta_p);
    obs->add(cosTheta_pbar);
    
    RooArgList *obs2 = new RooArgList();
    obs2->add(*obs);

    
    TString bootfile;
    bootfile.Form("/disk401/lhcb/qinning/b2dpp_farm10/qnFit/bootstrapping/samples/dataweff_%s.root", argv[2]);
    TChain *datree = new TChain("mytree");
    //datree->Add("datafit.root");
    datree->Add(bootfile);
    TTree *datree1 = (TTree *)datree->CopyTree("abs(B_mass_con-5279.65)<20");

    RooDataSet *datars = new RooDataSet("datars", "", datree1, *obs2);
    datree->Delete();
    datree1->Delete();


    RooRealVar *index1 = new RooRealVar("index", "index", 0);
    RooDataSet *IND1 = new RooDataSet("IND", "", RooArgSet(*index1));

    double nev1 = datars->numEntries();
    std::cout << "==================================================== " << std::endl;
    std::cout << "the number of B+ part numEntries:" << nev1 << std::endl;
    std::cout << "==================================================== " << std::endl;

    for (int i = 0; i < nev1; ++i)
    {
        *index1 = i;
        IND1->add(RooArgSet(*index1));
    }
    datars->merge(IND1);
    RooDataSet *data_fit;
    data_fit = datars;
    data_fit->Print("V");

    obs->add(*index1);

    
    //width_2765.setVal(0.824653); width_2765.setConstant(1);
    //J1835.setVal(0); P1835.setVal(-1);



    std::cout << "Begin " << std::endl;
    RooDalitzAmplitude *sig1 = new RooDalitzAmplitude("sig1", "", *obs, list, listX, "mcsw.root", *data_fit, *fbg, *fSP);
    RooArgSet *setdlz1 = sig1->getParameters(*data_fit);
    setdlz1->readFromFile("fit0.func");

/*    ar_H0_1835.setVal(1.); ar_H0_1835.setConstant(0);
    ai_H0_1835.setVal(0.); ai_H0_1835.setConstant(0);    
    ar_H1_1835.setVal(0.); ar_H1_1835.setConstant(1);
    ai_H1_1835.setVal(0.); ai_H1_1835.setConstant(1);    
    ar_H1_2150.setVal(0.); ar_H1_2150.setConstant(1); 
    ai_H1_2150.setVal(0.); ai_H1_2150.setConstant(1);
    ar_H1_f_2150.setVal(0.); ar_H1_f_2150.setConstant(1); 
    ai_H1_f_2150.setVal(0.); ai_H1_f_2150.setConstant(1);    
    ar_H1_f_1950.setVal(0.); ar_H1_f_1950.setConstant(1); 
    ai_H1_f_1950.setVal(0.); ai_H1_f_1950.setConstant(1);    
    */
    
//Setup Random number

    //  setdlz->Print("V");
#if 0
//    TFile *ff = new TFile(".tmp.root","recreate");
    TFile *fnew = new TFile("fit0.root");
    RooFitResult *r = (RooFitResult*)fnew->Get("nll");
    
    RooArgList parlist = r->floatParsFinal();
    for(int i=0; i<parlist.getSize(); ++i) {
      // double value =     ((RooRealVar*)(parlist.at(i)))->getVal();
      RooRealVar* newvar = (RooRealVar*)(setdlz1->find(((RooRealVar*)(parlist.at(i)))->GetName()));
      if(newvar) {
	      TString str = ((RooRealVar*)(parlist.at(i)))->GetName();
	      if(str.Contains("m0")|str.Contains("width")) continue;
	      double value = newvar->getVal() ;
	      double err = newvar->getError();
	      //std::cout << str << " " << value << "+/-" << err << std::endl;
        newvar->setVal(value+err*2.5*g_rnd->Gaus());
      }
    }
#endif
  //  exit(0);
    ar_H0_2150.setVal(1.); ar_H0_2150.setConstant(1); 
    ai_H0_2150.setVal(0.); ai_H0_2150.setConstant(1);    
   // sig1->genToy("mcsw.root","Toy");
    sig1->Delete();
    int status;
    setdlz1->Print("V");
    RooArgSet *fitvar = new RooArgSet();
    //fitvar->add(ar_H0_f_1950);
    //fitvar->add(ai_H0_f_1950);
    //fitvar->add(width_NR1p);
    double NLL;
    //NLL = doFit(obs, list, listX, data_fit, status, true, "", fitvar,true);
    NLL = doFit(obs, list, listX, data_fit, status, true, "", fitvar,false);
    TString outfunc;
    outfunc.Form("/disk401/lhcb/qinning/b2dpp_farm10/uncertainties/fixed_states/qnFit/bootstrapping/funcs/%s.func", argv[3]);
    setdlz1->writeToFile(outfunc);
    //=================================
    //=================================
    //    config fit parameters part
    //=================================
    //=================================

    RooDalitzAmplitude *sigflat = new RooDalitzAmplitude("sigflat","", *obs, list, listX, "MCFlatAcc.root", *data_fit, *fbg, *fSP);
  //RooArgSet* setdlzflat = sigflat->getParameters(*data_fit);


    //===Calculate Fit Fraction===
    int nres = listX->GetSize();//+listZ->GetSize();
    double Dsum[100];
    sigflat->getInt(Dsum, true);
    char resname[100];
    double sum(0);
    std::cout << "======Fit Fraction======" << std::endl;
    for(int i=0; i<nres; ++i) {
      printf("%10s %6.2f\n", (listX->At(i))->GetName(),Dsum[i]*100.0 );
      sum += Dsum[i];
    }
    for(int i=0; i<list->GetSize(); ++i) {
        printf("%10s %6.2f\n", (list->At(i))->GetName(),Dsum[i+nres]*100.0 );
        sum += Dsum[i+nres];    
    }
    printf("Total  %6.2f\n", sum*100.0 );  

    obs->Delete();
    obs2->Delete();
    setdlz1->Delete();
        
    std::cout << "FCN, Total FF " << NLL << " " << sum*100.0 << std::endl;

    std::cout << "AmplitudeFit() successfully completed!" << std::endl;
    TDatime FinishTime;
    std::cout << "Time(finish)  " << FinishTime.GetYear() << "." << FinishTime.GetMonth() << "." << FinishTime.GetDay() << "    " << FinishTime.GetHour() << ":" << FinishTime.GetMinute() << ":" << FinishTime.GetSecond() << std::endl;
    std::cout << "Done" << std::endl;
    std::cout << NLL <<std::endl;
    return 0;
}
