import os
from multiprocessing import Pool
from sys import argv

#pattern = argv[1]
pattern = "pattern"

#vars = ["mDs", "mkst", "mpsi", 'mZc', "mdst", "mZcs", "mDK", "mDpK"]
#vars += ["cosTheta_Ds_Kst", "cosTheta_Kst_Ds", "phiPi_Ds_Kst"]
#vars += ["cosTheta_Ds_Dst", "cosTheta_Dst_Ds", "phiPi_Ds_Dst"]
#vars += ["cosTheta_Psi_Kst", "cosTheta_Kst_Psi", "phi_Psi_Kst"]
#vars += ["cosTheta_Zc_Dst", "cosTheta_Dst_Zc", "phiPi_Zc_Dst"]
#vars += ["cosTheta_Zc_Psi", "cosTheta_Psi_Zc", "phiDm_Zc_Psi"]
#vars += ["cosTheta_Zcs_Kst", "cosTheta_Kst_Zcs", "phiPi_Zcs_Kst"]

vars = ["mppbar", "mD0p", "mD0pbar", "cosTheta_X", "cosTheta_L","mD0pZoomIn"]

ncpus = len(vars)

pool = Pool(processes=ncpus)
err_logs = []

key = ""
#key = "cos_kst_g_m05_l_05"
imgdir = "imgs_" + pattern 
cut = ""
#cut = "abs(cosTheta_Kst_Ds)<0.5"


if not os.path.exists(imgdir):
    os.system("mkdir "+imgdir)

def plot_all():
    for var in vars:
        if var == "mD0pZoomIn":
            cmd = "root -l -q -b './plot_general.C(\"mD0p\", \"ZoomIn\", \"pattern\", \"\", 1)'"
        else:
            cmd = "root -l -q -b './plot_general.C(\"{}\", \"{}\", \"{}\", \"{}\")'".format(var, key, pattern, cut, 1)
        args = cmd,
        pool.apply_async(os.system, args, error_callback=err_logs.append)
    print("Pool join... ")
    pool.close()
    pool.join()
    print("Pool finsh... ")
    if err_logs:
        print("Errors:", err_logs)

plot_all()

