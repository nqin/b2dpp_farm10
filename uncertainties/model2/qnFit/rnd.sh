#!/bin/bash 
#source ../gpu10.sh
id=${1}
ngpu=${2}
state=${3}

ifit=0
#str="testKM4Pc-fullPHSP-fineBinning_ExtLst_openCoupleChannel-4440${Pn1}${J1}-4457${Pn2}${J2}-4312${Pn3}${J3}-4600${Pn4}${J4}_ImMass_wlt500"
njobs=100
echo "total number of jobs: ${njobs}"
while [ "$ifit" -lt "$njobs" ]; do
   echo "Number = $id"
   nid=`printf  "%04d" $id`
   echo $nid
   ../bin/FitRnd${ngpu}_${state}_farm10 $id >& FitRnd/logs/fit-$nid.log 
   #../bin/FitRnd${ngpu} $id >& logrnd${ngpu}/fit-$nid.log 
   #cp logrnd/fit-$nid.log logrnd${ngpu}/fit-$nid.log
  id=$((id + 1))  
  ifit=$((ifit + 1))  
done

wait
python ./send_email.py $id $ngpu $njobs

echo all jobs are done!
