# Calculating systematic uncertainties

[TOC]

In this folder, different types of uncertainties are checked.

## fixed_states

To check the fixed parameters in the base fit model. Vary the fixed mass and width with $1\sigma$, do 100 random fits?

### The log file index

| states | mass | width |
| --- | --- | --- |
| f22150 | 0-199 | 200-399 |
| f21950 | 400-599 | 600-799 |
| Lc2860 | 800-999 | 1000-1199 |
| Lc2880 | 1200-1399 | 1400-1599 |
| rho2150| 1600-1799 | 1800-1999 |

## spin-parity

Take the JP assumptions with significance less than $5\sigma$. Do 100 random fit each, take the largest difference.

| states | JP | index |
| --- | --- | --- |
| Lc1 | 1/2+ | 0-99 |
| Lc1 | 3/2+ | 100-199 |
| Lc3 | 1/2+ | 200-299 |
| Lc3 | 3/2+ | 300-399 |
| Lc3 | 3/2- | 400-499 |
| Lc3 | 5/2+ | 500-699 |
| Lc2765 | 3/2+ | 700-799 |

## Blatt-Weisskopf

- [x] model4

Change $d=3$ to $d=[1.5,4.5]$, do 100 random fit each, take the largest difference.
|BW|index|
|---|---|
|1.5|0-99|
|4.5|100-199|

## background_fraction

- [x] model4

|beta|index|
|---|---|
|plus|0-99|
|minus|100-199|

## Lc2765 Flatté

- [x] model4

|$\Gamma_{0}$|$\Gamma_{\text{pole}}$|$M_{\text{pole}}$|
|---|---|---|
|0.5|50|2.7739|
|0.7|49|2.7746|
|0.9|48|2.7750|
|0.3|52|2.7721|
|0.1|55|2.7628|
|0.05|46|2.7540|
|0.025|26|2.7456|


|item|index|ngpu|
|--|--|--|
|Lc2765 mass|0-199|0,1|
|0.025|200-299|0|

## state significance

|state|index|
|---|--|
|Lc2900|0-99|
|Lc3130|100-199|
|Lc3189|200-299|
|Lc2765|300-399|
|Lc2860|400-499|
|Lc2880|500-599|
|Lc2940|600-699|
|after removing Lc2940|new index|
|Lc2900|1000-1099|
|Lc3130|1100-1199|
|Lc3189|1200-1299|
|Lc2765|1300-1399|
|Lc2860|1400-1499|
|Lc2880|1500-1599|

## SplinePhase 

|spline_dat|index|nGPU|
|--|--|--|
|00|0-99|0|
|01|100-199|1|
|02|200-299|2|
|03|300-399|0|
|04|400-499|1|
|05|500-599|2|

## SplinePoint

|spline_point_number|index|nGPU|
|--|--|--|
|12|2000-2199||
|13_1|3000-3099||

|spline interval|index|tag|
|--|--|--|
|original|2000-2199||
|2.1 -> 2.05|4000-4099|12_1|
|1.9 -> 1.91|4100-4199|12_2|
|1.925 -> 1.935|4200-4299|12_3|
|1.95 -> 1.96|4300-4399|12_4|

## AddLcScStates

Add $\Lambda_c(2910),\Lambda_c(2940),\Sigma_c(2800)$ to check significance.

|state|JP|index|
|--|--|--|
|Lc2910|3/2+|0-99|
|Lc2910|3/2-|100-199|
|Sc2800|1/2+|200-299|
|Sc2800|1/2-|300-399|
|Sc2800|3/2+|400-499|
|Sc2800|3/2-|500-599|
|Sc2800|5/2+|8000-8099|
|Sc2800|5/2-|8000-8199|
|Lc2910|1/2+|600-699|
|Lc2910|1/2-|700-799|
|Lc2910_fix|1/2+|800-899|
|Lc2910_fix|1/2-|900-999|
|Lc2910_fix|3/2+|9000-9099|
|Lc2910_fix|3/2-|9100-9199|

|state_significance|JP|index|
|--|--|--|
|Lc2910_Lc1|3/2+|1000-1099|
|Lc2910_Lc2|3/2+|1100-1199|
|Lc2910_Lc3|3/2+|1200-1299|
|Sc2800_Lc1|1/2-|2000-2099|
|Sc2800_Lc2|1/2-|2100-2199|
|Sc2800_Lc3|1/2-|2200-2299|
|Sc2800_Lc1|3/2+|3000-3099|
|Sc2800_Lc2|3/2+|3100-3199|
|Sc2800_Lc3|3/2+|3200-3299|
|Lc2910_Lc1|3/2-|4000-4099|
|Lc2910_Lc2|3/2-|4100-4199|
|Lc2910_Lc3|3/2-|4200-4299|
|Lc2910_Lc1|1/2+|5000-5099|
|Lc2910_Lc2|1/2+|5100-5199|
|Lc2910_Lc3|1/2+|5200-5299|
|Lc2910_Lc1|1/2-|6000-6099|
|Lc2910_Lc2|1/2-|6100-6199|
|Lc2910_Lc3|1/2-|6200-6299|
|Lc2910_Lc1_fix|1/2+|5300-5399|
|Lc2910_Lc2_fix|1/2+|5400-5499|
|Lc2910_Lc3_fix|1/2+|5500-5599|
|Lc2910_Lc1_fix|1/2-|6300-6399|
|Lc2910_Lc2_fix|1/2-|6400-6499|
|Lc2910_Lc3_fix|1/2-|6500-6599|


<!----
## model2

Change Lc-1 to $\Sigma_c(2800)$, different JP.

|state|JP|index|
|--|--|--|
|Lc2800|1/2+|0-99|
|Lc2800|1/2-|100-199|
|Lc2800|3/2+|200-299|
|Lc2800|3/2-|300-399|
|Lc2800|5/2+|400-499|
|Lc2800|5/2-|500-599|
|Lc2800, mass +0.014 over the threshold|all|+1000|

## model3

Change Lc-1 to a Flatte model with $\Lambda_c\ \eta$ channel.

|index|log|
|--|--|
|0-299|the normal model3|
|300-399|use fSP2 to do the model0 flatte, check whether it is well written|
|1000-1099|2900_JP = 1/2+|
|1100-1199|2900_JP = 1/2-|
|1200-1299|2900_JP = 3/2+|
|1300-1399|2900_JP = 3/2-|
|1400-1499|2900_JP = 5/2+|
|1500-1599|2900_JP = 5/2-|

after fixing the unreal momentum

|index|2900_JP|2765_JP|
|--|--|--|
|2000-2199|1/2-|3/2+|
|2200-2399|1/2-|1/2+|

after change the pRSigmac0 to pR0

|index|2900_JP|2765_JP|
|--|--|--|
|3000-3099|1/2-|3/2+|
|3200-3299|1/2-|3/2+|
|3100-3199|1/2-|1/2+|
|3300-3399|1/2-|1/2+|

after change const double to double

|index|2900_JP|2765_JP|log|
|--|--|--|--|
|4000-4099|1/2-|3/2+||
|4100-4199|1/2-|1/2+||
|4200-4299|1/2-|3/2+|remain const double but fSP2 fix to 0.5|
|4300-4399|1/2-|1/2+|remain const double but fSP2 fix to 0.5|

|index|fSP2|log|
|--|--|--|
|5000-5099|0.0||
|5100-5199|0.1||
|5200-5299|0.2||
|5300-5399|0.3||
|5400-5499|1.0||
|5500-5599|0.9||
|5600-5699|0.8||
|5700-5799|0.7||

## model4

Float Lc2860 mass and width. Choose Lc2765 JP = 1/2+.

|indexx|logs|
|--|--|
|0-99|fix 2860|
|100-299|float 2860|
-------->




