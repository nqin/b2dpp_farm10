//set how many SL of K* to float
#if 0
void SetKPar(RooArgList *argli, int maxind = 4, bool dornd = false, bool setzero = false)
{
    auto size = argli->getSize();
    int spin = ((RooAbsReal &)(*argli)[size - 2]).getVal();
    int parity = ((RooAbsReal &)(*argli)[size - 1]).getVal();

    std::cout << "JP " << argli->GetName() << " = " << spin << " " << parity << std::endl;

    int maxi = maxind;
    if (spin == 0 && parity > 0)
    {
        std::cout << "wrong JP for K*, shouldn't be 0+" << std::endl;
        maxi = 0;
    }
    if (spin == 0 && parity < 0)
    {
        maxi = 1;
    }
    if (spin >= 1)
        maxi = TMath::Min(maxind, 4);
    if ((pow(-1, spin) * parity > 0) && maxi >= 4)
    {
        maxi = 3;
    }

    int mini = 0;

    for (int ind = mini; ind < maxi; ++ind)
    {
        RooRealVar *var = (RooRealVar *)(argli->at(2 * ind));
        if (setzero)
            var->setVal(0.01);
        if (dornd)
            var->setVal(g_rnd->Gaus() * var->getError() * 5 + var->getVal());
        var->setError(0.1);
        var->setConstant(0);
        RooRealVar *var1 = (RooRealVar *)(argli->at(2 * ind + 1));
        if (setzero)
            var1->setVal(0.01);
        if (dornd)
        {
            var1->setVal(g_rnd->Gaus() * var1->getError() * 5 + var1->getVal());
            //      if(g_rnd->Uniform()>0.5) {var->setVal(-1.*var->getVal()); var1->setVal(-1.*var1->getVal()); }
        }
        var1->setError(0.1);
        var1->setConstant(0);
    }
    for (int ind = maxi; ind < 4; ++ind)
    {
        RooRealVar *var = (RooRealVar *)(argli->at(2 * ind));
        var->setVal(0.);
        var->setConstant(1);
        RooRealVar *var1 = (RooRealVar *)(argli->at(2 * ind + 1));
        var1->setVal(0.);
        var1->setConstant(1);
    }
}

void ResetKPar(RooArgList *argli, bool flmw, int ifix = -1, float fixvalue = -1.)
{
    auto size = argli->getSize();
    //float mass and width
    if (flmw && !(TString(argli->GetName()).Contains("NR")))
    {
        //    RooRealVar *var = (RooRealVar*)(argli->at(size-5));
        RooRealVar *var = dynamic_cast<RooRealVar *>(argli->at(size - 5));
        if (var != NULL)
        {
            std::cout << TString(argli->GetName()) << " set mass range" << std::endl;
            var->setRange(1.513, 2.182);
            var->setError(0.01);
            var->setConstant(0);
        }
        if (TString(argli->GetName()).Contains("Z"))
            var->setRange(3.8, 4.3);
        RooRealVar *var1 = (RooRealVar *)(argli->at(size - 4));
        var1->setRange(0.1, 2.0);
        var1->setError(0.01);
        var1->setConstant(0);
        if (TString(argli->GetName()).Contains("Z"))
            var1->setRange(0.01, 0.5);
    }

    if (ifix >= 0 && ifix < 4)
    {
        RooRealVar *var = (RooRealVar *)(argli->at(ifix * 2));
        var->setVal(fixvalue);
        var->setConstant(1);
        RooRealVar *var1 = (RooRealVar *)(argli->at(ifix * 2 + 1));
        var1->setVal(0.);
        var1->setConstant(1);
    }
}

void ResetXPar(RooArgList *argli, bool flmw, int ifix = -1, float fixvalue = -1.)
{
    auto size = argli->getSize();
    //float mass and width
    if (flmw && !(TString(argli->GetName()).Contains("NR")))
    {
        RooRealVar *var = (RooRealVar *)(argli->at(size - 5));
        var->setRange(4.117, 4.785);
        var->setError(0.01);
        var->setConstant(0);
        RooRealVar *var1 = (RooRealVar *)(argli->at(size - 4));
        var1->setRange(0.01, 0.25);
        var1->setError(0.01);
        var1->setConstant(0);
        if (TString(argli->GetName()).Contains("4500"))
            var1->setRange(0.03, 0.25);
    }

    if (ifix >= 0 && ifix < 5)
    {
        RooRealVar *var = (RooRealVar *)(argli->at(ifix * 2));
        var->setVal(fixvalue);
        var->setConstant(1);
        RooRealVar *var1 = (RooRealVar *)(argli->at(ifix * 2 + 1));
        var1->setVal(0.);
        var1->setConstant(1);
    }
}

//set how many SL of X to float
void SetXPar(RooArgList *argli, int maxind = 5, bool dornd = false, bool setzero = false)
{
    int size = argli->getSize();
    //  if(dornd&&!(TString(argli->GetName()).Contains("4140")||TString(argli->GetName()).Contains("4274"))) {
    int spin = ((RooAbsReal &)(*argli)[size - 2]).getVal();
    int parity = ((RooAbsReal &)(*argli)[size - 1]).getVal();
    std::cout << "JP " << argli->GetName() << " = " << spin << " " << parity << std::endl;
    int maxi = 5;
    if (spin == 0 && parity > 0)
        maxi = 2;
    if (spin == 0 && parity < 0)
        maxi = 1;
    if (spin == 1 && parity > 0)
        maxi = 3;
    if (spin >= 1 && parity < 0)
        maxi = 4;

    maxi = TMath::Min(maxind, maxi);

    int mini = 0;

    for (int ind = mini; ind < maxi; ++ind)
    {
        RooRealVar *var = (RooRealVar *)(argli->at(2 * ind));
        if (dornd)
            var->setVal(g_rnd->Gaus() * var->getError() * 5 + var->getVal());
        if (setzero)
            var->setVal(0.001);
        var->setError(0.1);
        var->setConstant(0);
        RooRealVar *var1 = (RooRealVar *)(argli->at(2 * ind + 1));
        if (dornd)
        {
            var1->setVal(g_rnd->Gaus() * var1->getError() * 5 + var1->getVal());
            //      if(g_rnd->Uniform()>0.5) {var->setVal(-1.*var->getVal()); var1->setVal(-1.*var1->getVal());   }
        }
        if (setzero)
            var1->setVal(0.001);
        var1->setError(0.1);
        var1->setConstant(0);
    }
    for (int ind = maxi; ind < 5; ++ind)
    {
        RooRealVar *var = (RooRealVar *)(argli->at(2 * ind));
        var->setVal(0.);
        var->setConstant(1);
        RooRealVar *var1 = (RooRealVar *)(argli->at(2 * ind + 1));
        var1->setVal(0.);
        var1->setConstant(1);
    }
#if 0
  //random mass & width
  if (dornd && !(TString(argli->GetName()).Contains("NR"))) {
    RooRealVar* var = (RooRealVar*)(argli->at(size - 5));
    if (!(var->isConstant())) {
      float m0_rnd = 0;
      do {
        m0_rnd = g_rnd->Gaus() * var->getError() * 10 + var->getVal();
      } while (!(m0_rnd > 4.117 && m0_rnd < 4.785));
      var->setVal(m0_rnd);
      var->setRange(4.117, 4.785); var->setError(0.01); var->setConstant(0);
    }
    RooRealVar* var1 = (RooRealVar*)(argli->at(size - 4));
    if (!(var1->isConstant())) {
      float w0_rnd = 0;
      do {
        w0_rnd = g_rnd->Gaus() * var1->getError() * 10 + var1->getVal();
      } while (!(w0_rnd > 0.03 && w0_rnd < 1.0));
      var1->setVal(w0_rnd);
      var1->setRange(0.03, 1.0); var1->setError(0.01); var1->setConstant(0);
      if (TString(argli->GetName()).Contains("4500")) {
        if (w0_rnd > 0.25) var1->setVal(0.2);
        var1->setRange(0.03, 0.25);
      }
    }
  }
#endif
}
#endif

double doFit(RooArgList *obs, TList *list, TList *listX, RooDataSet *data_fit, int &status, bool dofit = true, TString toy = "", RooArgSet *minosList = NULL, bool save = true)
{

    printf("================================================\n");
    printf("================do fit begin ===================\n");
    printf("================================================\n");
    RooDalitzAmplitude *sig = new RooDalitzAmplitude("sig", "", *obs, list, listX, "mcsw.root", *data_fit, *fbg, *fSP);
    int mc_massBins1 = 20;
    int mc_ybins1 = 20;
    std::cout << "MCeff INIT" << std::endl;
    sig->eff_init(mc_massBins1, mc_ybins1, "mcsw.root", 0, 0);
    std::cout << "BKG INIT" << std::endl;
    sig->eff_init(mc_massBins1, mc_ybins1, "databg.root", 1, 1);
    sig->CachBkgPDF(*data_fit);
    //  sig->ConvCoupling();
    //begin of chenchen
    RooArgSet *setdlz = sig->getParameters(*data_fit);
    setdlz->Print("V");
    RooDataSet *One_Data = new RooDataSet("One data", "", *obs);
    One_Data -> add(*(data_fit->get(0)));
    RooAbsReal *log_sum = sig->createSUM(*One_Data);
    RooFormulaVar *nll = new RooFormulaVar("nll", "@0-@1", RooArgSet(*log_sum, RooRealConstant::value(NLL_SHIFT)));
    //end of chenchen

    RooMinimizer m(*nll);

    m.setStrategy(0);
    m.setPrintLevel(2);
    m.setEps(3.0);
    //m.setVerbose(1);
    //m.setProfile(1);
    //m.setLogFile("log_minuit");

    if(dofit) {
      m.migrad();
    //do 2 more fits until converge
    if (((m.save())->status()) != 0)
        m.migrad();
    if (((m.save())->status()) != 0)
        m.migrad();
    if (((m.save())->status()) != 0)
        m.migrad();
    if (((m.save())->status()) != 0)
        m.migrad();
    //m.hesse();
    status = (m.save())->status();
    }
    if(dofit&&save) {
    RooFitResult* r = m.save();
    r->SetName("nll");
    r->SaveAs("fit_result.root");
    }
    double LLsig = (nll->getVal());   // * 2.0;

    printf("B+  FCN: %10.6e\n", LLsig);

    if (toy != "")
    {
        //sig->genToy("mcsw.root", "Fit" + toy, true);
        sig->genToy("mcsw.root", "Fit" + toy, 2);
    }

    if(dofit&&(minosList!=NULL)) m.minos(*minosList);

    return LLsig;
}
