#ifndef RESONANCES
#define RESONANCES
#define NUMSP 12
#define NUMSPY 12
//#define NUMSP 15
const int nset  = 6;
const double FFalpha = 0;
enum{
  MASS = 0, WIDTH, SPIN, LRESON, TYPE, PARITY
};
enum {
  BREITWIGNER = 1,
  FLATTE,  
  MODELINDEPENDENT,
  NonResonance,
  CHEBYCHEV,
  MODIFIEDFLATTE,
  CUSP,
  TRIANGLE,
  PARTIALWAVE = 120,
  KMATRIX = 240,
  REVISEDKMATRIX = 360
};


#endif //RESONANCES
