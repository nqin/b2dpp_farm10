#include "RooRealVar.h"
#include "RooDataSet.h"
#include "TChain.h"
#include "TTree.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TH1F.h"
#include "TLine.h"
#include "TLegend.h"
#include "TROOT.h"

void CombineRun12(){
    TChain *toy_weighted = new TChain("tree");
    toy_weighted->Add("../run1/toy_weighted.root");
    toy_weighted->Add("../run2/toy_weighted.root");
    toy_weighted->SetBranchStatus("*",1);

//    double sw, w_nTracks;
//    toy_weighted->SetBranchAddress("sw",&sw);
//    toy_weighted->SetBranchAddress("w_nTracks",&w_nTracks);
//
//    double sw_new[toy_weighted->GetEntries()];
//    double sw_old[toy_weighted->GetEntries()];
//    for(int i=0; i<toy_weighted->GetEntries(); ++i) {
//        toy_weighted->GetEntry(i);
//        sw_old[i] = sw;
//        sw_new[i] = sw*w_nTracks;
//    }
//
//    TFile *file2 = new TFile("./toyFitmemeda.root","recreate");
//    toy_weighted->SetBranchStatus("*",1);
//    toy_weighted->SetBranchStatus("sw",0);
//    TTree *newtree = toy_weighted->CloneTree();
//    newtree->AutoSave();
//    file2->Close();
//    //delete toy_weighted;
//
//    TFile *file3 = new TFile("./toyFitmemeda.root","update");
//    TTree *tree3 = (TTree*)file3->Get("tree");
//    double sw_new_temp;
//    double sw_old_temp;
//    TBranch *bsw_old = tree3->Branch("sw_Run12",&sw_old_temp,"sw_Run12/D");
//    TBranch *bsw = tree3->Branch("sw",&sw_new_temp,"sw/D");
//
//    for(int i=0; i<tree3->GetEntries(); ++i) {
//        tree3->GetEntry(i);
//        sw_new_temp = sw_new[i];
//        sw_old_temp = sw_old[i];
//        bsw_old->Fill();
//        bsw->Fill();
//    }
//    tree3->Write();

    TFile *file4 = new TFile("./mcsw.root","recreate");
    toy_weighted->SetBranchStatus("*",1);
    toy_weighted->SetBranchStatus("wX*",0);
    toy_weighted->SetBranchStatus("wY*",0);
    toy_weighted->SetBranchStatus("w",0);
    toy_weighted->SetBranchStatus("wL",0);
    toy_weighted->SetBranchStatus("wX",0);
    toy_weighted->SetBranchStatus("wbkg",0);

    TTree *tree4 = toy_weighted->CloneTree();
    tree4->SetName("mytree");
    tree4->AutoSave();
    file4->Close();
}