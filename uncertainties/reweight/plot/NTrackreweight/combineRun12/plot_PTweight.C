#include "RooRealVar.h"
#include "RooDataSet.h"
#include "TChain.h"
#include "TTree.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TH1F.h"
#include "TLine.h"
#include "TLegend.h"
#include "TROOT.h"
using namespace std;

double fbg = 0.058087679;
void setStyle(TH1F &histos, const char* histname, TLegend & leg)
{
#include "style.h"
}

void plot_PTweight(TString var){
    gROOT->ProcessLine(".x ./lhcbStyle.C");
    RooRealVar B0_PT("B0_PT","B0_PT",0);
    RooRealVar nTracks("nTracks","nTracks",0);

    RooArgList *obs =new RooArgList(B0_PT,nTracks);
    RooArgList* obs1 = (RooArgList*)obs->Clone("");

    TChain *datree = new TChain("mytree");
    datree->Add("../run2/data_run2.root");
    //datree->Add("../run1/data_run1.root");
    TTree *datree1 = (TTree *)datree->CopyTree("abs(B_mass_con-5279.65)<20");

    RooDataSet *data_fit = new RooDataSet("data_fit","",datree1,*obs1,0,"");
    data_fit->Print("V");
    
    TString toy_file = "../run2/toy_weighted.root";
    //TString toy_file = "../run1/toy_weighted.root";
    TFile *toy = new TFile(toy_file,"READ");
    TTree *toytree = (TTree*)toy->Get("tree");

    TObjArray* listb = toytree->GetListOfBranches();

    std::vector<TString> NameList;
    TString sname;
    for(int i=0; i<listb->GetSize(); ++i) {
        TBranch *ibr = (TBranch*)listb->At(i);
        if(ibr!=NULL) {
            const char* name = ibr->GetName();
            sname = TString(name);
            if(sname.Contains("wX_") || sname.Contains("wY_") || sname.Contains("wbkg")) {
                NameList.push_back(sname);
                std::cout << sname << std::endl;
            }
        }
    }

    double border0[2], border1[2];
    int bins0, bins1;
    TString xtitle, ytitle;
    if(var == "B0_PT"){
        border0[0] = 5000; border0[1]=20000; bins0=15;
        border1[0] = 5000; border1[1]=20000; bins1=15;
        xtitle = "B0_PT";
        ytitle = "Events";
    }
    if(var == "nTracks"){
        border0[0] = 0; border0[1]=500; bins0=50;
        border1[0] = 0; border1[1]=500; bins1=50;
        xtitle = "nTracks";
        ytitle = "Events";
    }

    std::cout << "name list size " << NameList.size() << std::endl;
    TH1F *h100 = new TH1F("h100","",bins0, border0[0], border0[1]);
    TH1F *h200 = new TH1F("h200","",bins0, border0[0], border0[1]);
    TH1F *h200_noweight = new TH1F("h200_noweight","",bins0, border0[0], border0[1]);
    TH1F *h300 = new TH1F("h300","",bins0, border0[0], border0[1]);    
    h100->Sumw2(); h200->Sumw2(); 
    data_fit->fillHistogram(h100,*(obs1->find(var)));
    toytree->Project("h200",var,"w*w_nTracks");//(draw run2 but not run1)
    toytree->Project("h200_noweight",var);

    toytree->Project("h300",var,"wbkg");    
    h100->SetMinimum(0);
    h200->SetMarkerColor(2);
    h200->SetLineColor(2);
    h200->SetMarkerStyle(24);

    cout<<"h101->Integral():"<<h100->Integral()<<endl;
    cout<<"h201->Integral():"<<h200->Integral()<<endl;
    cout<<"h101/h201->Integral():"<<h100->Integral()/h200->Integral()<<endl;

    double scale =  (1.-fbg)*h100->Integral()/h200->Integral() ;
    double scale_noweight =  (1.-fbg)*h100->Integral()/h200_noweight->Integral() ;
    double scbg = fbg*h100->Integral()/h300->Integral();
    std::cout <<"scale: "<< scale << std::endl;
    std::cout <<"scale_noweight: "<< scale_noweight << std::endl;
    std::cout <<"scbg: "<< scbg << std::endl;

    TCanvas *histosPrint = new TCanvas("hist", "",0,0,700,600);

    TH1F *h101 = new TH1F("h101","",bins1, border1[0], border1[1]);
    TH1F *h201 = new TH1F("h201","",bins1, border1[0], border1[1]);
    TH1F *h201_noweight = new TH1F("h201_noweight","",bins0, border0[0], border0[1]);
    TH1F *h302 = new TH1F("h302","",bins1, border1[0], border1[1]);    
    h101->Sumw2(); h201->Sumw2(); h201_noweight->Sumw2();
    data_fit->fillHistogram(h101, *(obs1->find(var)));
    toytree->Project("h201",var,"w*w_nTracks");//(draw run1 but not run2)
    toytree->Project("h201_noweight",var);
    toytree->Project("h302",var,"wbkg");
 
    h201->SetMinimum(0);
    h201->SetMarkerColor(2);
    h201->SetLineColor(2); 
    h201->SetMarkerSize(0.7);
    h201->Scale(scale);
    h201->Add(h302,scbg);
    h201_noweight->SetMarkerColor(kBlue);
    h201_noweight->SetLineColor(kBlue); 
    h201_noweight->SetMarkerSize(0.7);
    h201_noweight->SetMarkerStyle(24);
    h201_noweight->Scale(scale_noweight);
    h201_noweight->Add(h302,scbg);
    h201->SetMaximum(TMath::Max(h101->GetMaximum(), h201->GetMaximum())*1.3);
    h101->SetTitle(var);

    h101->SetMarkerStyle(21);
    h201->SetXTitle(xtitle);
    h201->SetYTitle(ytitle);
    TCanvas *c1 = new TCanvas("c1", "",0,0,700,600);
    h201->Draw("hsame");
    h201_noweight->Draw("hsame");
    h101->Draw("same");

    c1->SetRightMargin(0.05);
    c1->SetTopMargin(0.05);
    h201->GetYaxis()->SetTitleSize(0.09*0.8);
    h201->GetYaxis()->SetTitleOffset(0.75);
    h201->GetYaxis()->SetLabelSize(0.07*0.6);

    double mLegScale=2.0;
    double dx = 0;
    double dy = 0;
    double ymax = 0.93;
    double y_one = 0.2/8;
    double nhists = 2 + NameList.size();
    double ymin = 0.91-y_one*nhists;

    TLegend *legtotal = new TLegend(0.65+dx,0.75,0.95+dx,0.93,NULL,"brNDC");
    legtotal->SetBorderSize(0);
    legtotal->SetLineColor(1);
    legtotal->SetTextFont(132);
    legtotal->SetLineWidth(3);
    legtotal->SetFillColor(0);
    legtotal->SetFillStyle(0);  
    legtotal->AddEntry(h101,"Data");
    legtotal->AddEntry(h201,"MC weighted");
    legtotal->AddEntry(h201_noweight,"MC no weight");

    //leg->Draw();
    //leg2->Draw();
    legtotal->Draw();

    c1->Update();   
    TString outpath = "./";
    c1->SaveAs(var + "_compare.png");
    c1->SaveAs(var + "_compare.pdf");
}