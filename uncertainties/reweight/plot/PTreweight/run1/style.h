double gScale=1.3;
double gmarker_size = gScale*0.7;

TString T_histoname = TString(histname);

//////////////////////////////////////////
// ppbar X-chain
//////////////////////////////////////////

if(T_histoname.Contains("wX_NR0m")) {
    Int_t color = kGreen;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 34;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{0^{#minus}} pp-NR}";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}  

else if(T_histoname.Contains("wX_NR0p")) {
    Int_t color = kGreen;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 34;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{0^{+}} pp-NR}";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}  

else if(T_histoname.Contains("wX_X_1835")) {
    Int_t color = kOrange;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 34;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{1^{#minus}} X#it{(1835)} }";

    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}  

else if(T_histoname.Contains("wX_a_1950")) {
    Int_t color = kCyan;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 24;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{0^{+}} a#it{(1950)} }";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}  

else if(T_histoname.Contains("wX_f_2020")) {
    Int_t color = kYellow+2;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 34;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{0^{+}} f_{#it{0}}#it{(2020)} }";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}  

else if(T_histoname.Contains("wX_f_2100")) {
    Int_t color = kYellow+2;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 26;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{0^{+}} f_{#it{0}}#it{(2100)} }";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}  

else if(T_histoname.Contains("wX_f_2200")) {
    Int_t color = kRed+2;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 34;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{0^{+}} f_{#it{0}}#it{(2200)} }";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}  

else if(T_histoname.Contains("wX_eta_2225")) {
    Int_t color = kRed+2;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 34;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{0^{#minus}} #eta #it{(2225)} }";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}  

else if(T_histoname.Contains("wX_f_2330")) {
    Int_t color = kRed+2;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 34;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{0^{+}} f_{#it{0}}#it{(2330)} }";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}  

else if(T_histoname.Contains("wX_rho_1900")) {
    Int_t color = kGreen+2;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 36;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{1^{#minus}} #rho#it{(1900)} }";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}  


else if(T_histoname.Contains("wX_rho_2150")) {
    Int_t color = kGreen+2;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 24;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{1^{#minus}} #rho#it{(2150)} }";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}  

else if(T_histoname.Contains("wX_phi_2170")) {
    Int_t color = kGreen+4;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 34;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{1^{#minus}} #phi#it{(2170)} }";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}  

else if(T_histoname.Contains("wX_f2_1810")) {
    Int_t color = kYellow+2;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 34;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{2^{+}} f_{#it{2}}#it{(1810)} }";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}  

else if(T_histoname.Contains("wX_f2_1910")) {
    Int_t color = kYellow+2;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 34;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{2^{+}} f_{#it{2}}#it{(1910)} }";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}  

else if(T_histoname.Contains("wX_f2_1950")) {
    Int_t color = kYellow+2;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 24;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{2^{+}} f_{#it{2}}#it{(1950)} }";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}  

else if(T_histoname.Contains("wX_f2_2010")) {
    Int_t color = kYellow+2;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 34;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{2^{+}} f_{#it{2}}#it{(2010)} }";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}  

else if(T_histoname.Contains("wX_f2_2150")) {
    Int_t color = kRed+2;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 34;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{2^{+}} f_{#it{2}}#it{(2150)} }";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}  

else if(T_histoname.Contains("wX_f2_2300")) {
    Int_t color = kRed+2;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 34;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{2^{+}} f_{#it{2}}#it{(2300)} }";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}  

else if(T_histoname.Contains("wX_f2_2340")) {
    Int_t color = kRed+2;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 34;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{2^{+}} f_{#it{2}}#it{(2340)} }";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}  

else if(T_histoname.Contains("wX_eta2_1870")) {
    Int_t color = kRed+2;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 34;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{2^{+}} #eta{2}#it{(1870)} }";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}  

else if(T_histoname.Contains("wX_pi2_1880")) {
    Int_t color = kRed+2;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 34;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{2^{+}} #pi_{#it{2}}#it{(1880)} }";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}  

else if(T_histoname.Contains("wX_pi2_2005")) {
    Int_t color = kRed+2;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 34;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{2^{+}} #pi_{#it{2}}#it{(2005)} }";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}  


else if(T_histoname.Contains("wX_pi2_2100")) {
    Int_t color = kRed+2;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 34;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{2^{+}} #pi_{#it{2}}#it{(2100)} }";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}  

else if(T_histoname.Contains("wX_Fwave")) {
    Int_t color = kYellow+2;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 34;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{0^{+}} #it{Spline}} ";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}  

//////////////////////////////////////////
// D0p Y-chain
//////////////////////////////////////////

else if(T_histoname.Contains("wY_Y1mwave")) {
    Int_t color = kCyan-1;
    Int_t line_width = 1;
    Int_t line_style = 2;
    Int_t marker_style = 26;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{1/2^{#minus}} #it{Spline}} ";
    histos.SetFillColorAlpha(color,0.5);
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    //histos.SetLineStyle(line_style);
    //histos.SetMarkerColor(color);
    //histos.SetMarkerStyle(marker_style);
    //histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend, "f");
    histos.DrawCopy("same,hist", histname);
}  

else if(T_histoname.Contains("wY_Y1pwave")) {
    Int_t color = kCyan;
    Int_t line_width = 1;
    Int_t line_style = 2;
    Int_t marker_style = 26;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{1/2^{+}} #it{Spline}} ";
    histos.SetFillColorAlpha(color,0.5);
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    //histos.SetLineStyle(line_style);
    //histos.SetMarkerColor(color);
    //histos.SetMarkerStyle(marker_style);
    //histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend, "f");
    histos.DrawCopy("same,hist", histname);
}  
else if(T_histoname.Contains("wY_Y3pwave")) {
    Int_t color = kCyan;
    Int_t line_width = 1;
    Int_t line_style = 2;
    Int_t marker_style = 26;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{3/2^{+}} #it{Spline}} ";
    histos.SetFillColorAlpha(color,0.5);
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    //histos.SetLineStyle(line_style);
    //histos.SetMarkerColor(color);
    //histos.SetMarkerStyle(marker_style);
    //histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend, "f");
    histos.DrawCopy("same,hist", histname);
}

else if(T_histoname.Contains("wY_NR1m")) {
    Int_t color = kGreen;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 24;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{1/2^{#minus}} Dp-NR  }";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}

else if(T_histoname.Contains("wY_NR1p")) {
    Int_t color = kGreen;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 26;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{1/2^{+}} Dp-NR  }";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}

else if(T_histoname.Contains("wY_NR3m")) {
    Int_t color = kGreen;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 34;
    double marker_size = marker_size;
    TString legend = "#font[12]{#it{3/2^{#minus}} Dp-NR  }";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}

else if(T_histoname.Contains("wY_NR3p")) {
    Int_t color = kGreen;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 36;
    double marker_size = marker_size;
    TString legend = "#font[12]{#it{3/2^{+}} Dp-NR  }";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}

else if(T_histoname.Contains("wY_L_2765")) {
//else if(T_histoname.Contains("wY_S_1P_1")) {
    Int_t color = kRed;
    Int_t line_width = 1;
    Int_t line_style = 2;
    Int_t marker_style = 24;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{1/2^{+}} #Lambda_{c}#it{(2765)} #it{2S} }";
    histos.SetFillColorAlpha(color,0.5);
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    //histos.SetLineStyle(line_style);
    //histos.SetMarkerColor(color);
    //histos.SetMarkerStyle(marker_style);
    //histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend, "f");
    histos.DrawCopy("same,hist", histname);
}  

else if(T_histoname.Contains("wY_L_2940")) {
    Int_t color = kCyan-1;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 34;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{3/2^{#minus}} #Lambda_{c}#it{(2940)} #it{2P} }";
    //TString legend = "#font[12]{ #it{3/2+} #Lambda_{c}#it{(2940)} #it{2P} }";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}  

else if(T_histoname.Contains("wY_L_2900")) {
    Int_t color = kCyan-1;
    Int_t line_width = 1;
    Int_t line_style = 2;
    Int_t marker_style = 26;
    double marker_size = gmarker_size;
    //TString legend = "#font[12]{#it{1/2^{#minus}} #Lambda_{c}#it{(2900)} #it{2P} }";
    //TString legend = "#font[12]{#it{1/2^{#minus}} #Lambda_{c}#it{(2P)}/#Sigma_{c}#it{(1P)}}";
    TString legend = "#font[12]{#it{1/2^{#minus}} #Lambda_{c}#it{(2P)(2900)}}";
    histos.SetFillColorAlpha(color,0.5);
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    //histos.SetLineStyle(line_style);
    //histos.SetMarkerColor(color);
    //histos.SetMarkerStyle(marker_style);
    //histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend, "f");
    histos.DrawCopy("same,hist", histname);
}  



else if(T_histoname.Contains("wY_L_2860")) {
    Int_t color = kAzure+1;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 34;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{3/2^{+}} #Lambda_{c}#it{(2860)} #it{1D} }";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}  

else if(T_histoname.Contains("wY_L_2880")) {
    Int_t color = kAzure+1;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 26;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{5/2^{+}} #Lambda_{c}#it{(2880)} #it{1D} }";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}  

else if(T_histoname.Contains("wY_L_3130")) {
    Int_t color = kOrange;
    Int_t line_width = 1;
    Int_t line_style = 2;
    Int_t marker_style = 26;
    double marker_size = gmarker_size;
    //TString legend = "#font[12]{#it{1/2^{+}} #Lambda_{c}#it{(3130)} #it{3S} }";
    //TString legend = "#font[12]{#it{1/2^{+}} #Lambda_{c}#it{(3S)(3130)} }";
    TString legend = "#font[12]{#it{3/2^{#minus}} #Lambda_{c}#it{(3S)(3130)} }";
    histos.SetFillColorAlpha(color,0.5);
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    //histos.SetLineStyle(line_style);
    //histos.SetMarkerColor(color);
    //histos.SetMarkerStyle(marker_style);
    //histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend, "f");
    histos.DrawCopy("same,hist", histname);
}  

else if(T_histoname.Contains("wY_L_2850")) {
    Int_t color = kCyan;
    Int_t line_width = 1;
    Int_t line_style = 2;
    Int_t marker_style = 26;
    double marker_size = gmarker_size;
    //TString legend = "#font[12]{#it{1/2^{+}} #Lambda_{c}#it{(2850)} #it{3S} }";
    TString legend = "#font[12]{#it{1/2^{+}} #Lambda_{c}#it{(2850)} }";
    histos.SetFillColorAlpha(color,0.5);
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    //histos.SetLineStyle(line_style);
    //histos.SetMarkerColor(color);
    //histos.SetMarkerStyle(marker_style);
    //histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend, "f");
    histos.DrawCopy("same,hist", histname);
}

else if(T_histoname.Contains("wY_L_3150")) {
    Int_t color = kRed;
    Int_t line_width = 1;
    Int_t line_style = 2;
    Int_t marker_style = 26;
    double marker_size = gmarker_size;
    //TString legend = "#font[12]{#it{1/2^{+}} #Lambda_{c}#it{(3130)} #it{3S} }";
    TString legend = "#font[12]{#it{1/2^{#minus}} #Lambda_{c}#it{(3150)} }";
    histos.SetFillColorAlpha(color,0.5);
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    //histos.SetLineStyle(line_style);
    //histos.SetMarkerColor(color);
    //histos.SetMarkerStyle(marker_style);
    //histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend, "f");
    histos.DrawCopy("same,hist", histname);
}

else if(T_histoname.Contains("wY_L_3189")) {
    Int_t color = kBlue;
    Int_t line_width = 1;
    Int_t line_style = 2;
    Int_t marker_style = 34;
    double marker_size = gmarker_size;
    //TString legend = "#font[12]{#it{3/2^{+}} #Lambda_{c}/#Sigma_{c}#it{(J^{P})}}";
    //TString legend = "#font[12]{#it{3/2^{+}} #Lambda_{c}#it{(2D)}/#Sigma_{c}#it{(2S)}/#Sigma_{c}#it{(1D)}}";
    TString legend = "#font[12]{#it{3/2^{+}} #Lambda_{c}#it{(2D)(3189)}}";
    histos.SetFillColorAlpha(color,0.1);
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    //histos.SetLineStyle(line_style);
    //histos.SetMarkerColor(color);
    //histos.SetMarkerStyle(marker_style);
    //histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend, "f");
    histos.DrawCopy("same,hist", histname);
}  

else if(T_histoname.Contains("wY_L_3209")) {
    Int_t color = kBlue;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 26;
    double marker_size = gmarker_size;
    //TString legend = "#font[12]{#it{5/2^{+}} #Lambda_{c}#it{(3209)} #it{2D} }";
    TString legend = "#font[12]{#it{5/2^{#minus}} #Lambda_{c}#it{(3209)}  }";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}  

else if(T_histoname.Contains("wY_S_2901")) {
    Int_t color = kBlue-7;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 24;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{1/2^{+}} #Sigma_{c}#it{(2901)} #it{2S} }";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}

else if(T_histoname.Contains("wY_S_2936")) {
    Int_t color = kBlue-7;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 26;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{3/2^{+}} #Sigma_{c}#it{(2936)} #it{2S}}";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}

else if(T_histoname.Contains("wY_S_3271")) {
    Int_t color = kOrange+10;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 24;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{1/2^{+}} #Sigma_{c}#it{(3271)} #it{2S}}";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}

else if(T_histoname.Contains("wY_S_3293")) {
    Int_t color = kOrange+10;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 26;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{#it{3/2^{+}} #Sigma_{c}#it{(3293)} #it{2S}}";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}

else if(T_histoname.Contains("wDp_NR")) {
    Int_t color = kGreen-5;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 34;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{Dp-NR}";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}


//////////////////////////////////////////////
// Bkg components
//////////////////////////////////////////////

else if(T_histoname.Contains("wbkg")) {
    Int_t color = kGray;
    Int_t line_width = 3;
    Int_t line_style = 2;
    Int_t marker_style = 34;
    double marker_size = gmarker_size;
    TString legend = "#font[12]{background} ";
    histos.SetLineColor(color);
    histos.SetLineWidth(line_width);
    histos.SetLineStyle(line_style);
    histos.SetMarkerColor(color);
    histos.SetMarkerStyle(marker_style);
    histos.SetMarkerSize(marker_size);
    leg.AddEntry(&histos, legend);
    histos.DrawCopy("same", histname);
}  

else{
    cout<<"Style not defined for "<< T_histoname <<endl;
    Error("style.h", "Histogram name not found");
}
