import numpy as np
from hep_ml.reweight import GBReweighter
import uproot

reweighter = GBReweighter(max_depth=2)
f_data = uproot.open("./data_run2.root")
t_data = f_data["mytree"]
arr_data_nTracks = t_data["nTracks"].array()
arr_data_p = t_data["log_B0_P"].array()


f_MC = uproot.open("./toyFitmemeda_run2.root")
t_MC = f_MC["tree"]
arr_MC_nTracks = t_MC["nTracks"].array()
arr_MC_p = t_MC["log_B0_P"].array()

arr_MC_w = t_MC["w"].array()
arr_MC_sw = t_MC["sw"].array()

reweighter.fit(original=arr_MC, target=arr_data)
MC_weights = reweighter.predict_weights(arr_MC)