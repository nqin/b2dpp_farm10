#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TString.h"
/**
 * @brief Add the logarithm of var for plotting
 * 
 */
void AddLogVar(){
    TString filename = "./toyFitmemeda_run2.root";
    TString treename = "tree";
    //TString filename = "data.root";
    //TString treename = "mytree";
    TFile *file1  = new TFile(filename,"update");
    TTree *tree1 = (TTree*)file1->Get(treename);
    double B0_P, B0_PT, B0_BDDTF_P, B0_BDDTF_PT, B0_BDTF_P, B0_BDTF_PT, B0_DDTF_P, B0_DDTF_PT;
    double log_B0_P, log_B0_PT, log_B0_BDDTF_P, log_B0_BDDTF_PT, log_B0_BDTF_P, log_B0_BDTF_PT, log_B0_DDTF_P, log_B0_DDTF_PT;

    tree1->SetBranchAddress("B0_P",&B0_P);
    tree1->SetBranchAddress("B0_PT",&B0_PT);
    tree1->SetBranchAddress("B0_BDDTF_P",&B0_BDDTF_P);
    tree1->SetBranchAddress("B0_BDDTF_PT",&B0_BDDTF_PT);
    tree1->SetBranchAddress("B0_BDTF_P",&B0_BDTF_P);
    tree1->SetBranchAddress("B0_BDTF_PT",&B0_BDTF_PT);
    tree1->SetBranchAddress("B0_DDTF_P",&B0_DDTF_P);
    tree1->SetBranchAddress("B0_DDTF_PT",&B0_DDTF_PT);

    TBranch *b_log_B0_P = tree1->Branch("log_B0_P",&log_B0_P,"log_B0_P/D");
    TBranch *b_log_B0_PT = tree1->Branch("log_B0_PT",&log_B0_PT,"log_B0_PT/D");
    TBranch *b_log_B0_BDDTF_P = tree1->Branch("log_B0_BDDTF_P",&log_B0_BDDTF_P,"log_B0_BDDTF_P/D");
    TBranch *b_log_B0_BDDTF_PT = tree1->Branch("log_B0_BDDTF_PT",&log_B0_BDDTF_PT,"log_B0_BDDTF_PT/D");
    TBranch *b_log_B0_BDTF_P = tree1->Branch("log_B0_BDTF_P",&log_B0_BDTF_P,"log_B0_BDTF_P/D");
    TBranch *b_log_B0_BDTF_PT = tree1->Branch("log_B0_BDTF_PT",&log_B0_BDTF_PT,"log_B0_BDTF_PT/D");
    TBranch *b_log_B0_DDTF_P = tree1->Branch("log_B0_DDTF_P",&log_B0_DDTF_P,"log_B0_DDTF_P/D");
    TBranch *b_log_B0_DDTF_PT = tree1->Branch("log_B0_DDTF_PT",&log_B0_DDTF_PT,"log_B0_DDTF_PT/D");

    for(int i=0; i<tree1->GetEntries(); i++){
        tree1->GetEntry(i);
        log_B0_P = log(B0_P);
        log_B0_PT = log(B0_PT);
        log_B0_BDDTF_P = log(B0_BDDTF_P);
        log_B0_BDDTF_PT = log(B0_BDDTF_PT);
        log_B0_BDTF_P = log(B0_BDTF_P);
        log_B0_BDTF_PT = log(B0_BDTF_PT);
        log_B0_DDTF_P = log(B0_DDTF_P);
        log_B0_DDTF_PT = log(B0_DDTF_PT);
        
        b_log_B0_P->Fill();
        b_log_B0_PT->Fill();
        b_log_B0_BDDTF_P->Fill();
        b_log_B0_BDDTF_PT->Fill();
        b_log_B0_BDTF_P->Fill();
        b_log_B0_BDTF_PT->Fill();
        b_log_B0_DDTF_P->Fill();
        b_log_B0_DDTF_PT->Fill();
    }
    tree1->Write();
    file1->Close();
}