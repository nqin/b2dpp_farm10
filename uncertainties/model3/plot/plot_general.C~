using namespace RooFit;
void setStyle(TH1F &histos, const char* histname, TLegend & leg)
{
#include "style.h"
}


TCanvas* draw(TH1F* h140,  TH1F* h440)
{
    TH1F *h540 = (TH1F*)h440->Clone("hchi");
    h540->GetSumw2()->Set(0);
    h540->SetYTitle("");


    TCanvas* c = new TCanvas(h440->GetTitle(),"",700,700) ;
    c->Divide(1,2,0.0,0.0);

    c->cd(1);
    TVirtualPad* c_1 = c->GetPad(1);
    c_1->SetPad(0,0.3,1,1);
    c_1->SetRightMargin(0.05);
    c_1->SetTopMargin(0.05);
    h140->SetMinimum(0);
    h140->Draw("h");
    h440->Draw("Same");
    TH1F *hpdf = (TH1F*)h140->Clone("hpdf");
    hpdf->Sumw2();

    double chi2mpp(0);
    int ndf(0);
    for(int i=1; i<=h440->GetNbinsX(); ++i) {
        double CHI = 0;//
        double err;
        if((err = pow(hpdf->GetBinError(i),2)+pow(h440->GetBinError(i),2))>0) { 
            ndf++;
            if(fabs(h440->GetBinError(i))<1e-10&&fabs(h440->GetBinContent(i))<1e-10) err += 1.0;
            CHI =(h440->GetBinContent(i)-hpdf->GetBinContent(i))/sqrt(err);
        }
        //    cout << CHI << endl;
        chi2mpp += CHI*CHI;
        //    cout << i  << " " << CHI << endl;
        h540->SetBinContent(i,CHI);
    }

    cout << "===1D chi2 with for " << h440->GetTitle() << " with " << ndf << " bins =" << chi2mpp << endl;


    TVirtualPad* c_2 = c->GetPad(2);
    c->cd(2);
    c_2->SetPad(0,0.0,1,0.3);
    c_2->SetRightMargin(0.05);
    c_2->SetBottomMargin(0.5);
    h540->GetXaxis()->SetLabelFont(133);
    h540->GetYaxis()->SetLabelFont(133);
    h440->GetYaxis()->SetLabelSize(0.07*0.8);
    h540->GetXaxis()->SetLabelSize(18*1.8*0.8); // labels will be 14 pixels
    h540->GetYaxis()->SetLabelSize(18*1.8*0.8); // labels will be 14 pixels
    h540->GetYaxis()->SetNdivisions(503); // labels will be 14 pixels
    h540->GetXaxis()->SetTitleSize(0.24*0.8);
    h440->GetYaxis()->SetTitleSize(0.09*0.8);
    h440->GetYaxis()->SetTitleOffset(0.75*0.8);
    h540->SetMinimum(-5);
    h540->SetMaximum(5);
    // TCanvas *c2 = new TCanvas("c2","");
    h540->SetFillColor(21);
    h540->Draw();
    double xxmin=h540->GetXaxis()->GetXmin();
    double xxmax=h540->GetXaxis()->GetXmax();
    TLine *l1 = new TLine(xxmin,0,xxmax,0);
    l1->Draw();
    TLine *l2 = new TLine(xxmin,-3,xxmax,-3);
    l2->SetLineStyle(3);
    l2->Draw();
    TLine *l3 = new TLine(xxmin,3,xxmax,3);
    l3->SetLineStyle(3);
    l3->Draw();
    c->Update();
    return c;
}


void plot_general(TString var, TString out_key="-Ds-0", TString pattern="", TString cut="1", int flag = 0)
{
    int NoCPU(1);
    //TString cut = "1";
    if(cut.IsNull())
        cut = "1";
    gROOT->ProcessLine(".x ./lhcbStyle.C");

    bool UseCut = true;
    double m_lb = 5.6195, m_k = 0.13957, m_p = 0.938272046, m_jpsi = 3.096916;

    double m0_kst = 0.892;
    double w_kst = 0.1;


    RooRealVar mppbar("mppbar","mppbar",0);
    RooRealVar mD0p("mD0p","mD0p",0);
    RooRealVar mD0pbar("mD0pbar","mD0pbar",0);
    RooRealVar cosTheta_X("cosTheta_X","cosTheta_X",0);
    RooRealVar cosTheta_L("cosTheta_L","cosTheta_L",0);
    RooRealVar Sweight_sig("Sweight_sig","Sweight_sig",0);

    int bins_cos = 10;
    int bins_phi = 10;

    //RooRealVar Var;
    double border0[2], border1[2];
    double h_scale = 1.3;
    int bins0, bins1;
    TString xtitle, ytitle;
    bool draw_leg = false;

    if (var == "mppbar"){
        border0[0] = 1.82; border0[1]=3.44; bins0=54;
        border1[0] = 1.82; border1[1]=3.44; bins1=54;
        xtitle = "#it{m_{p#bar{p}}} [GeV]";
        ytitle = "Events/ (30 MeV)";
        draw_leg = true;
    }

    if (var == "mD0p"){
        draw_leg = true;
        xtitle = "#it{m_{D^{0}p}} [GeV]";
        border0[0] = 2.75; border0[1]=4.4; bins0=55;
        border1[0] = 2.75; border1[1]=4.4; bins1=55;
        ytitle = "Events/ (30 MeV)";
        if(flag == 1)
        {
            border0[0] = 2.75; border0[1]=3.1; bins0=60;
            border1[0] = 2.75; border1[1]=3.1; bins1=60;
            ytitle = "Events/ (5.8 MeV)";
        }
    }

    if (var == "mD0pbar"){
        border0[0] = 2.75; border0[1]=4.4; bins0=55;
        border1[0] = 2.75; border1[1]=4.4; bins1=55;
        xtitle = "#it{m_{D^{0}#bar{p}}} [GeV]";
        ytitle = "Events/ (30 MeV)";
        draw_leg = true;
    }

    if (var == "cosTheta_X"){
        border0[0] = -1; border0[1]=1; bins0=32;
        border1[0] = -1; border1[1]=1; bins1=32;
        xtitle = "cos#theta_{X} [GeV]";
        ytitle = "Events/ (0.0625)";
        //draw_leg = true;
    }

    if (var == "cosTheta_L"){
        border0[0] = -1; border0[1]=1; bins0=32;
        border1[0] = -1; border1[1]=1; bins1=32;
        xtitle = "cos#theta_{L} [GeV]";
        ytitle = "Events/ (0.0625)";
        //draw_leg = true;
    }
  //  RooArgList *obs =new RooArgList(mDs, mZc, mdst, mkst, mpsi);
  //  obs->add(cosTheta_Ds_Kst); obs->add(cosTheta_Kst_Ds); obs->add(phiPi_Ds_Kst);
  //  obs->add(cosTheta_Ds_Dst); obs->add(cosTheta_Dst_Ds); obs->add(phiPi_Ds_Dst);
  //  obs->add(cosTheta_Zc_Dst); obs->add(cosTheta_Dst_Zc); obs->add(phiPi_Zc_Dst);
  //  obs->add(cosTheta_Zc_Psi); obs->add(cosTheta_Psi_Zc); obs->add(phiDm_Zc_Psi);
  //  obs->add(cosTheta_Psi_Kst); obs->add(cosTheta_Kst_Psi); obs->add(phi_Psi_Kst);
  //  obs->add(cosTheta_Zcs_Kst); obs->add(cosTheta_Kst_Zcs); obs->add(phiPi_Zcs_Kst);
  //  obs->add(mZcs); obs->add(mDK); obs->add(mDpK);

    RooArgList *obs =new RooArgList(mppbar, mD0p, mD0pbar, cosTheta_X, cosTheta_L, Sweight_sig);
//    RooArgList *obs =new RooArgList(mppbar);
    RooArgList* obs1 = (RooArgList*)obs->Clone("");
//    RooRealVar sw("sw","sw",0);
//    obs1->add(sw);

//    TFile *fdata = new TFile("splot_data.root");
    TChain *datree = new TChain("mytree");
    datree->Add("./data_mytree.root");
    TTree *datree1 = (TTree *)datree->CopyTree("abs(B_mass_con-5279.65)<20");

    RooDataSet *datars = new RooDataSet("datars","",datree1,*obs1,0,"");

//    RooDataSet * data_fit = new  RooDataSet(TString(datars->GetName())+TString("new"),datars->GetTitle(),datars,*datars->get(),0,"weights");
    RooDataSet * data_fit = new  RooDataSet(TString(datars->GetName())+TString("new"),datars->GetTitle(),datars,*datars->get(),0,"Sweight_sig");
    data_fit->Print("V");  

//    TString toy_file = "weighted_mc/mc_"+pattern+".root";
//    TString toy_file = "~/workdir/B2Dpp/disk101/Work/qnFit/toyFitmemeda.root";
    TString toy_file = "../qnFit/toyFitmemeda.root";
    TFile *toy = new TFile(toy_file);
    TTree *toytree = (TTree*)toy->Get("tree");
    TObjArray* listb = toytree->GetListOfBranches();
    //std::vector<const char*> NameList;

    std::vector<TString> NameList;
    TString sname;
    for(int i=0; i<listb->GetSize(); ++i) {
        TBranch *ibr = (TBranch*)listb->At(i);
        if(ibr!=NULL) {
            const char* name = ibr->GetName();
            sname = TString(name);
            //if(sname.Contains("wL_") || sname.Contains("wS_") || sname.Contains("wX_") || sname.Contains("wrho_") || sname.Contains("wf2_") || sname.Contains("wbkg")) {
            //if(sname.Contains("wL_") || sname.Contains("wDp_NR") ||sname.Contains("wS_") || sname.Contains("wX_") || sname.Contains("wrho_") || sname.Contains("wf2_") || sname.Contains("wbkg")) {
            if(sname.Contains("wX_") || sname.Contains("wY_") || sname.Contains("wbkg")) {
            //if( sname.Contains("wY_") || sname.Contains("wbkg")) {
                //NameList.push_back(name);
                NameList.push_back(sname);
                std::cout << sname << std::endl;
            }
        }
    }


    std::cout << "name list size " << NameList.size() << std::endl;
    TH1F *h100 = new TH1F("h100","",bins0, border0[0], border0[1]);
    TH1F *h200 = new TH1F("h200","",bins0, border0[0], border0[1]);
    h100->Sumw2(); h200->Sumw2(); 
    data_fit->fillHistogram(h100,*(obs1->find(var)));
    toytree->Project("h200",var,"w");
    h100->SetMinimum(0);
    h200->SetMarkerColor(2);
    h200->SetLineColor(2);
    h200->SetMarkerStyle(24);

    cout<<"h101->Integral():"<<h100->Integral()<<endl;
    cout<<"h201->Integral():"<<h200->Integral()<<endl;
    cout<<"h101/h201->Integral():"<<h100->Integral()/h200->Integral()<<endl;

    double scale =  h100->Integral()/h200->Integral() ;
    std::cout <<"scale"<< scale << std::endl;


    TCanvas *histosPrint = new TCanvas("hist", "",0,0,700,600);

    TH1F *h101 = new TH1F("h101","",bins1, border1[0], border1[1]);
    TH1F *h201 = new TH1F("h201","",bins1, border1[0], border1[1]);
    h101->Sumw2(); h201->Sumw2(); 
    if (UseCut){
        cout << "###### " << "Cut used: "<< cut << " for h101 &h201" <<endl;
        data_fit->fillHistogram(h101, *(obs1->find(var)), cut);
        toytree->Project("h201",var,"w*("+cut+")");
    }
    else {
        data_fit->fillHistogram(h101, *(obs1->find(var)));
        toytree->Project("h201",var,"w");
    }


    h101->SetMinimum(0);
    h201->SetMarkerColor(2);
    h201->SetLineColor(2); 
    h201->SetMarkerSize(0.7);
    h201->Scale(scale);
    h201->SetMaximum(TMath::Max(h101->GetMaximum(), h201->GetMaximum())*1.3);
    h101->SetTitle(var);

    h101->SetMarkerStyle(21);
    h101->SetXTitle(xtitle);
    h201->SetYTitle(ytitle);
    TCanvas *c1 = draw(h201,h101);  

    double mLegScale=2.0;
    double dx = 0;
    //double dx = -0.3;
    double dy = 0;
    double ymax = 0.93;
    double y_one = 0.2/8;
    double nhists = 2 + NameList.size();
    double ymin = 0.91-y_one*nhists;
    TLegend *leg = new TLegend(0.65+dx,ymin+dy-3*y_one,0.95+dx,ymax+dy,NULL,"brNDC");
    leg->SetBorderSize(0);
    leg->SetLineColor(1);
    leg->SetTextFont(132);
    leg->SetLineWidth(3);
    leg->SetFillColor(0);
    leg->SetFillStyle(0);  
    leg->AddEntry(h101,"Data");
    leg->AddEntry(h201,"Fit");

    TLegend *leg2 = new TLegend(0.65+dx,ymin+dy,0.95+dx,ymax+dy,NULL,"brNDC");
    leg2->SetBorderSize(0);
    leg2->SetLineColor(1);
    leg2->SetTextFont(132);
    leg2->SetLineWidth(3);
    leg2->SetFillColor(0);
    leg2->SetFillStyle(0);  
//    leg2->AddEntry(h101,"Data");
//    leg2->AddEntry(h201,"Fit");


    c1->cd(1);
    for(int i=0; i<NameList.size(); ++i) {
        TH1F *h301 = new TH1F("h301","",bins1, border1[0], border1[1]);
        h301->Sumw2();
        if(  NameList[i].Contains("wbkg"))
        {
            double fbg = 0.058087679;
//            TH1F *h300 = new TH1F("h300","",85,2.7,4.4);  
//            h300->Sumw2();
            toytree->Project("h301",var, NameList[i]); 
            double scbg = h201->Integral()/h301->Integral()*fbg/(1.-fbg); 
            h301->Scale(scbg);
        }
        else{
            if (UseCut){
                cout << "###### " << "Cut used: "<< cut << " for h301" <<endl;
                toytree->Project("h301", var, NameList[i] + "*("+cut+")");
            }
            else
                toytree->Project("h301", var, NameList[i]);
        }


        h301->Scale(scale);
        h301->SetTitle(NameList[i]);
        h301->SetName(NameList[i]);
        if( NameList[i].Contains("wX_") )
        {
            setStyle(*h301,NameList[i],*leg);
        }
        if( NameList[i].Contains("wY_") )
        {
            setStyle(*h301,NameList[i],*leg2);
        }
    }
    h201->Draw("hsame");
    h101->Draw("same");
    if(var == "mppbar")
    {
        if (draw_leg) leg->Draw();
    }
    else if(var == "mD0p")
    {
        if(draw_leg) leg2->Draw();
    }

    TString outpath = "./";
    TString out_pattern = pattern;
    c1->SaveAs(outpath + "imgs_"+out_pattern+"/AmAn_" + var+"_" +pattern+out_key+".pdf");
    c1->SaveAs(outpath + "imgs_"+out_pattern+"/AmAn_" + var+"_" +pattern+out_key+".png");
    
    /*
    if (draw_leg){
        TCanvas* c2 = new TCanvas();
        leg->Draw();
        c2->SaveAs(outpath + "imgs_"+pattern+"/AmAn_" + "leg"+"_" +pattern+".png");
        c2->SaveAs(outpath + "imgs_"+pattern+"/AmAn_" + "leg"+"_" +pattern+".pdf");
    }
    */
}

//void getFCN(){
//
//}
