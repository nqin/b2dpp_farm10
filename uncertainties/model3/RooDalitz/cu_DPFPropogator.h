#ifndef CU_DPF_PROPOGATOR_HH
#define CU_DPF_PROPOGATOR_HH

#include <cuda_runtime.h>
#include "../RooDalitz/Resonances.h"

__device__ double getp(double M, double m1, double m2);
__device__ double F_BW(double r, int L, double p, double p0);
__device__ double BTerm(int J, double mpp, double m_r, int iType);
__device__ double2 breit_wigner(double m, double gamma, double e2) ;
__device__ double2 BW_FLATTE(double m_r, double gamma_r, double mpp, int LR, double br1);
__device__ double2 BW_FLATTE2(double m_r, double gamma_r, double mpp, int LR, double br1);
__device__ double2 BW_AMP(double m_r, double gamma_r, double mpp, int LR, int iType);
__device__ double2 BW_NR(double alpha, double beta, double mpp, int LR, int iType);
__device__ double2 ModelIndependentWave3(const double *fPoly, double x, int iType);
//__device__ double2 ModelIndependentWave3(const double *fPoly, double x);
#endif
