int typeR(360);
int typeKM(360);
int typeFlatte = FLATTE;
int typeFlatte2 = FLATTE2;
int typeNonResonance = NonResonance;
typeR = BREITWIGNER;
typeKM = BREITWIGNER;

/////////////////////////////////////////
// ppbar,X-chain 
/////////////////////////////////////////
TList *listX = new TList();


/////////////////////////////
//***** Non-resonance *****//
/////////////////////////////

// JP=0- spin-0, only has 1 helicity coupling, 
RooRealVar m0_NR0m("m0_NR0m","m0",-1);
//RooRealVar m0_NR0m("m0_NR0m","m0",0.,-100,100);
RooRealVar width_NR0m("width_NR0m","width",0.,-10,10);
//RooRealVar width_NR0m("width_NR0m","width",2.046);
RooRealVar ar_H0_NR0m("ar_H0_NR0m","",0,-10,10);
RooRealVar ai_H0_NR0m("ai_H0_NR0m","",0.,-10,10);
RooRealVar ar_H1_NR0m("ar_H1_NR0m","",0.);
RooRealVar ai_H1_NR0m("ai_H1_NR0m","",0.);  
RooArgList* X_NR0m = new RooArgList(ar_H0_NR0m,ai_H0_NR0m,
      			      ar_H1_NR0m,ai_H1_NR0m,"X_NR0m");
X_NR0m->add(m0_NR0m);
X_NR0m->add(width_NR0m);
//X_NR0m->add(RooRealConstant::value(typeKM));
X_NR0m->add(RooRealConstant::value(typeNonResonance));
X_NR0m->add(RooRealConstant::value(0));//spin
X_NR0m->add(RooRealConstant::value(-1)); //parity

// JP=1-, rho(2150)
RooRealVar m0_2150("m0_2150","m0",2.034);//2.034\pm0.016
RooRealVar width_2150("width_2150","width",0.234);//0.234\pm0.040
RooRealVar ar_H0_2150("ar_H0_2150","",0.,-10,10);
RooRealVar ai_H0_2150("ai_H0_2150","",0.,-10,10);
RooRealVar ar_H1_2150("ar_H1_2150","",0.,-10,10);
RooRealVar ai_H1_2150("ai_H1_2150","",0.,-10,10);  
RooArgList* X_rho_2150 = new RooArgList(ar_H0_2150,ai_H0_2150,
      			      ar_H1_2150,ai_H1_2150,"X_rho_2150");
X_rho_2150->add(m0_2150);
X_rho_2150->add(width_2150);
X_rho_2150->add(RooRealConstant::value(typeKM));
X_rho_2150->add(RooRealConstant::value(1));//spin
X_rho_2150->add(RooRealConstant::value(-1)); //parity

// JP=2+ f2(1950)
RooRealVar m0_f_1950("m0_f_1950","m0",1.936);//1.936\pm0.012
RooRealVar width_f_1950("width_f_1950","width",0.464);//0.464\pm0.024
RooRealVar ar_H0_f_1950("ar_H0_f_1950","",0.,-10,10);
RooRealVar ai_H0_f_1950("ai_H0_f_1950","",0.,-10,10);
RooRealVar ar_H1_f_1950("ar_H1_f_1950","",0.,-10,10);
RooRealVar ai_H1_f_1950("ai_H1_f_1950","",0.,-10,10);  
RooArgList* X_f2_1950 = new RooArgList(ar_H0_f_1950,ai_H0_f_1950,
      			      ar_H1_f_1950,ai_H1_f_1950,"X_f2_1950");
X_f2_1950->add(m0_f_1950);
X_f2_1950->add(width_f_1950);
X_f2_1950->add(RooRealConstant::value(typeKM));
X_f2_1950->add(RooRealConstant::value(2));//spin
X_f2_1950->add(RooRealConstant::value(1)); //parity

// JP=2+ f2(2150)
RooRealVar m0_f_2150("m0_f_2150","m0",2.157);//2.157\pm0.012
RooRealVar width_f_2150("width_f_2150","width",0.152);//0.152\pm0.030
RooRealVar ar_H0_f_2150("ar_H0_f_2150","",0.,-100,100);
RooRealVar ai_H0_f_2150("ai_H0_f_2150","",0.,-100,100);
RooRealVar ar_H1_f_2150("ar_H1_f_2150","",0.,-100,100);
RooRealVar ai_H1_f_2150("ai_H1_f_2150","",0.,-100,100);  
RooArgList* X_f2_2150 = new RooArgList(ar_H0_f_2150,ai_H0_f_2150,
      			      ar_H1_f_2150,ai_H1_f_2150,"X_f2_2150");
X_f2_2150->add(m0_f_2150);
X_f2_2150->add(width_f_2150);
X_f2_2150->add(RooRealConstant::value(typeKM));
X_f2_2150->add(RooRealConstant::value(2));//spin
X_f2_2150->add(RooRealConstant::value(1)); //parity


/////////////////////////////////////////
// D0p,Y-chain 
/////////////////////////////////////////
TList *list = new TList();



////////////////////////////////////
//***** Lambda_c Spectrum    *****//
////////////////////////////////////

/*
 * The spectrum is \lambda-mode for sigle charmed baryon.
 * S-wave: singlet.  JP = (1/2)+
 * P-wave: doublet. JP = (1/2)+, (3/2)+
 * D-wave: doublet. JP = (3/2)+, (5/2)+
*/

double wid_ini = 0.150;
double wid_min = 0.001;
double wid_max = 0.350;
//************//
//** S-wave **//
//************//

// 1S Lc(2286), under threshold.


// 2S Lc(2765), JP = (1/2)+.
// Could be Sc(2765).
// Mass and width is statiscally significant.
// Quantum numbers is not known for sure.
RooRealVar m0_2765("m0_2765","m0",2.7666);
//RooRealVar m0_2765("m0_2765","m0",2.7666,2,3.0);
RooRealVar width_2765("width_2765","width",0.5);
//RooRealVar width_2765("width_2765","width",wid_ini,wid_min,2.0);
RooRealVar ar_H0_2765("ar_H0_2765","",0.,-10,10);
RooRealVar ai_H0_2765("ai_H0_2765","",0.,-10,10);
RooRealVar ar_H1_2765("ar_H1_2765","",0.,-10,10);
RooRealVar ai_H1_2765("ai_H1_2765","",0.,-10,10);  
RooArgList* Y_L_2765 = new RooArgList(ar_H0_2765,ai_H0_2765,
      			      ar_H1_2765,ai_H1_2765,"Y_L_2765");
Y_L_2765->add(m0_2765);
Y_L_2765->add(width_2765);
//RooRealVar T2765("T2765","type of 2765",typeR);
RooRealVar T2765("T2765","type of 2765",typeFlatte);
RooRealVar J2765("J2765","",1);
RooRealVar P2765("P2765","",1);
Y_L_2765->add(T2765);
Y_L_2765->add(J2765);
Y_L_2765->add(P2765);

// 3S
// Predicted Lc(3130), JP = (1/2)+
RooRealVar m0_3130("m0_3130","m0",3.13,3.03,3.33);
//RooRealVar m0_3130("m0_3130","m0",3.255,3.255-0.007*3,3.255+0.009*3);
//RooRealVar m0_3130("m0_3130","m0",3.23);
RooRealVar width_3130("width_3130","width",wid_ini,wid_min,wid_max);
//RooRealVar width_3130("width_3130","width",0.206,0.206-0.015*3,0.206+0.016*3);
RooRealVar ar_H0_3130("ar_H0_3130","",0.,-10,10);
RooRealVar ai_H0_3130("ai_H0_3130","",0.,-10,10);
RooRealVar ar_H1_3130("ar_H1_3130","",0.,-10,10);
RooRealVar ai_H1_3130("ai_H1_3130","",0.,-10,10);  
RooArgList* Y_L_3130 = new RooArgList(ar_H0_3130,ai_H0_3130,
      			      ar_H1_3130,ai_H1_3130,"Y_L_3130");
Y_L_3130->add(m0_3130);
Y_L_3130->add(width_3130);
RooRealVar T3130("T3130","type of 3130",typeR);
RooRealVar J3130("J3130","",3);//ini:3-
RooRealVar P3130("P3130","",-1);
Y_L_3130->add(T3130);
Y_L_3130->add(J3130);
Y_L_3130->add(P3130);

// 2P Lc(2940) JP = (3/2)-
RooRealVar m0_2940("m0_2940","m0",2.9396);//2.9396\p0.0013\m0.0015
RooRealVar width_2940("width_2940","width",0.020);//0.020\p0.006\m0.005
RooRealVar ar_H0_2940("ar_H0_2940","",0.,-10,10);
RooRealVar ai_H0_2940("ai_H0_2940","",0.,-10,10);
RooRealVar ar_H1_2940("ar_H1_2940","",0.,-10,10);
RooRealVar ai_H1_2940("ai_H1_2940","",0.,-10,10);  
RooArgList* Y_L_2940 = new RooArgList(ar_H0_2940,ai_H0_2940,
      			      ar_H1_2940,ai_H1_2940,"Y_L_2940");
Y_L_2940->add(m0_2940);
Y_L_2940->add(width_2940);
RooRealVar T2940("T2940","type of 2940",typeR);
RooRealVar J2940("J2940","",3);
RooRealVar P2940("P2940","",-1);
Y_L_2940->add(T2940);
Y_L_2940->add(J2940);
Y_L_2940->add(P2940);

// 2P
// Predicted Lc(3005), JP = (1/2)-
// The doublet of Lc(2940)
//RooRealVar m0_2900("m0_2900","m0",2.85,2.805,2.9);
//RooRealVar m0_2900("m0_2900","m0",2.817,2.817-0.005*3,2.817+0.007*3);
////RooRealVar width_2900("width_2900","width",wid_ini,wid_min,wid_max);
//RooRealVar width_2900("width_2900","width",0.037,0.037-0.009*3,0.037+0.017*3);
//RooRealVar ar_H0_2900("ar_H0_2900","",0.,-10,10);
//RooRealVar ai_H0_2900("ai_H0_2900","",0.,-10,10);
//RooRealVar ar_H1_2900("ar_H1_2900","",0.,-10,10);
//RooRealVar ai_H1_2900("ai_H1_2900","",0.,-10,10);  
//RooArgList* Y_L_2900 = new RooArgList(ar_H0_2900,ai_H0_2900,
//      			      ar_H1_2900,ai_H1_2900,"Y_L_2900");
//Y_L_2900->add(m0_2900);
//Y_L_2900->add(width_2900);
//RooRealVar T2900("T2900","type of 2900",typeR);
//RooRealVar J2900("J2900","",1);
//RooRealVar P2900("P2900","",-1);
//Y_L_2900->add(T2900);
//Y_L_2900->add(J2900);
//Y_L_2900->add(P2900);

RooRealVar m0_2900("m0_2900","m0",2.85,2.805,2.9);
//RooRealVar m0_2900("m0_2900","m0",2.817,2.817-0.005*3,2.817+0.007*3);
RooRealVar width_2900("width_2900","width",wid_ini,wid_min,wid_max);
//RooRealVar width_2900("width_2900","width",0.037,0.037-0.009*3,0.037+0.017*3);
RooRealVar ar_H0_2900("ar_H0_2900","",0.,-10,10);
RooRealVar ai_H0_2900("ai_H0_2900","",0.,-10,10);
RooRealVar ar_H1_2900("ar_H1_2900","",0.,-10,10);
RooRealVar ai_H1_2900("ai_H1_2900","",0.,-10,10);  
RooArgList* Y_L_2900 = new RooArgList(ar_H0_2900,ai_H0_2900,
      			      ar_H1_2900,ai_H1_2900,"Y_L_2900");
Y_L_2900->add(m0_2900);
Y_L_2900->add(width_2900);
RooRealVar T2900("T2900","type of 2900",typeR);
RooRealVar J2900("J2900","",1);
RooRealVar P2900("P2900","",-1);
Y_L_2900->add(T2900);
Y_L_2900->add(J2900);
Y_L_2900->add(P2900);

RooRealVar m0_2800("m0_2800","m0",2.792+0.014);
//RooRealVar width_2800("width_2800","width",wid_ini,wid_min,wid_max);
RooRealVar width_2800("width_2800","width",62,10,150);
RooRealVar ar_H0_2800("ar_H0_2800","",0.,-10,10);
RooRealVar ai_H0_2800("ai_H0_2800","",0.,-10,10);
RooRealVar ar_H1_2800("ar_H1_2800","",0.,-10,10);
RooRealVar ai_H1_2800("ai_H1_2800","",0.,-10,10);  
RooArgList* Y_L_2800 = new RooArgList(ar_H0_2800,ai_H0_2800,
      			      ar_H1_2800,ai_H1_2800,"Y_L_2800");
Y_L_2800->add(m0_2800);
Y_L_2800->add(width_2800);
RooRealVar T2800("T2800","type of 2800",typeR);
RooRealVar J2800("J2800","",5);
RooRealVar P2800("P2800","",-1);
Y_L_2800->add(T2800);
Y_L_2800->add(J2800);
Y_L_2800->add(P2800);


RooRealVar m0_2835("m0_2835","m0",2.835,2.825,2.845);
RooRealVar width_2835("width_2835","width",wid_ini,wid_min,wid_max);
RooRealVar ar_H0_2835("ar_H0_2835","",0.,-10,10);
RooRealVar ai_H0_2835("ai_H0_2835","",0.,-10,10);
RooRealVar ar_H1_2835("ar_H1_2835","",0.,-10,10);
RooRealVar ai_H1_2835("ai_H1_2835","",0.,-10,10);  
RooArgList* Y_L_2835 = new RooArgList(ar_H0_2835,ai_H0_2835,
      			      ar_H1_2835,ai_H1_2835,"Y_L_2835");
Y_L_2835->add(m0_2835);
Y_L_2835->add(width_2835);
RooRealVar T2835("T2835","type of 2835",typeR);
RooRealVar J2835("J2835","",1);
RooRealVar P2835("P2835","",-1);
Y_L_2835->add(T2835);
Y_L_2835->add(J2835);
Y_L_2835->add(P2835);

//************//
//** D-wave **//
//************//

// 1D Lc(2860) JP = (3/2)+
RooRealVar m0_2860("m0_2860","m0",2.8561,2.805,2.900);//2.856\p0.002\m0.006
RooRealVar width_2860("width_2860","width",0.068,0,0.4);//0.068\p0.012\m0.022
RooRealVar ar_H0_2860("ar_H0_2860","",0.,-10,10);
RooRealVar ai_H0_2860("ai_H0_2860","",0.,-10,10);
RooRealVar ar_H1_2860("ar_H1_2860","",0.,-10,10);
RooRealVar ai_H1_2860("ai_H1_2860","",0.,-10,10);  
RooArgList* Y_L_2860 = new RooArgList(ar_H0_2860,ai_H0_2860,
      			      ar_H1_2860,ai_H1_2860,"Y_L_2860");
Y_L_2860->add(m0_2860);
Y_L_2860->add(width_2860);
RooRealVar T2860("T2860","type of 2860",typeFlatte2);
RooRealVar J2860("J2860","",3);
RooRealVar P2860("P2860","",1);
Y_L_2860->add(T2860);
Y_L_2860->add(J2860);
Y_L_2860->add(P2860);

// 1D Lc(2880) JP = (5/2)+
// doublet of Lc(2860)
RooRealVar m0_2880("m0_2880","m0",2.881);//2.8816\pm0.0002
RooRealVar width_2880("width_2880","width",0.0056);//0.0056\p0.0008\m0.0006
RooRealVar ar_H0_2880("ar_H0_2880","",0.,-10,10);
RooRealVar ai_H0_2880("ai_H0_2880","",0.,-10,10);
RooRealVar ar_H1_2880("ar_H1_2880","",0.,-10,10);
RooRealVar ai_H1_2880("ai_H1_2880","",0.,-10,10);  
RooArgList* Y_L_2880 = new RooArgList(ar_H0_2880,ai_H0_2880,
      			      ar_H1_2880,ai_H1_2880,"Y_L_2880");
Y_L_2880->add(m0_2880);
Y_L_2880->add(width_2880);
RooRealVar T2880("T2880","type of 2880",typeR);
RooRealVar J2880("J2880","",5);
RooRealVar P2880("P2880","",1);
Y_L_2880->add(T2880);
Y_L_2880->add(J2880);
Y_L_2880->add(P2880);

// 2D 
// Predicted Lc(3189) JP = (3/2)+
RooRealVar m0_3189("m0_3189","m0",2.85,2.8,3.3);
//RooRealVar m0_3189("m0_3189","m0",2.969,2.969-0.010*3,2.969+0.006*3);
RooRealVar width_3189("width_3189","width",wid_ini,wid_min,wid_max);
//RooRealVar width_3189("width_3189","width",0.138,0.138-0.010*3,0.138+0.008*3);
RooRealVar ar_H0_3189("ar_H0_3189","",0.,-10,10);
RooRealVar ai_H0_3189("ai_H0_3189","",0.,-10,10);
RooRealVar ar_H1_3189("ar_H1_3189","",0.,-10,10);
RooRealVar ai_H1_3189("ai_H1_3189","",0.,-10,10);  
RooArgList* Y_L_3189 = new RooArgList(ar_H0_3189,ai_H0_3189,
      			      ar_H1_3189,ai_H1_3189,"Y_L_3189");
Y_L_3189->add(m0_3189);
Y_L_3189->add(width_3189);
RooRealVar T3189("T3189","type of 3189",typeR);
RooRealVar J3189("J3189","",1);
RooRealVar P3189("P3189","",-1);
Y_L_3189->add(T3189);
Y_L_3189->add(J3189);
Y_L_3189->add(P3189);


//************//
//** Spline **//
//************//
// JP=0+, f0(2100)
static int Nsp=30;
char Wname[20];
RooRealVar *SMass[Nsp], *SRAm[Nsp], *SIAm[Nsp];
RooArgList *FWave = new RooArgList("FWave");
double xm[Nsp], sr0[Nsp], sp0[Nsp];
std::ifstream inswav;
inswav.open("/disk401/lhcb/qinning/b2dpp_farm10/qnFit/splinedat/toy_X.dat");
int iswav=0;
while(1) {
	inswav >> xm[iswav] >> sr0[iswav] >> sp0[iswav];
	if(!inswav.good()) break;
	std::cout << xm[iswav] << " " << sr0[iswav] << " " << sp0[iswav] << std::endl;
	iswav++;
} 
inswav.close();
int Npoint = iswav;
std::cout << "point of FWave " << Npoint << std::endl;
if(Npoint!=NUMSP) {
	std::cout <<  "SPline number is wrong " << std::endl;
	assert(0);
}
//  double sr0[Nsp],  = {};
//  RooDalitzRAmlitude dz;
for(int i=0; i<Npoint; ++i) {
	double fac(1.0);
	if(i>0) {
		fac = 1.0;
		std::cout << fac << std::endl;
	}
	sprintf(Wname,"SR_%02d",i);
	SRAm[i] = new RooRealVar(Wname,Wname,sr0[i]*fac);
	SRAm[i]->setConstant(0);
	FWave->add(*SRAm[i]);
	sprintf(Wname,"SI_%02d",i);
	SIAm[i] = new RooRealVar(Wname,Wname,sp0[i]*fac);
	SIAm[i]->setConstant(0);
	FWave->add(*SIAm[i]);
	sprintf(Wname,"m_%02d",i);
	SMass[i] = new RooRealVar(Wname,Wname,xm[i]);
	FWave->add(*SMass[i]);    
}
SRAm[3]->setConstant(1);
SIAm[3]->setConstant(1);  

RooRealVar m0_Fwave("m0_Fwave","m0",2.086);
RooRealVar width_Fwave("width_Fwave","width",0.286);
RooRealVar ar_H0_Fwave("ar_H0_Fwave","",1.,-10,10);
RooRealVar ai_H0_Fwave("ai_H0_Fwave","",0.,-10,10);
RooRealVar ar_H1_Fwave("ar_H1_Fwave","",0.);
RooRealVar ai_H1_Fwave("ai_H1_Fwave","",0.);  
RooArgList* X_Fwave = new RooArgList(ar_H0_Fwave,ai_H0_Fwave,
      			      ar_H1_Fwave,ai_H1_Fwave,"X_Fwave");
  X_Fwave->add(*FWave);
X_Fwave->add(m0_Fwave);
X_Fwave->add(width_Fwave);
X_Fwave->add(RooRealConstant::value(MODELINDEPENDENT));
X_Fwave->add(RooRealConstant::value(0));//spin
X_Fwave->add(RooRealConstant::value(1)); //parity
