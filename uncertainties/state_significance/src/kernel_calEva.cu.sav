#include <cuda_runtime.h>
#include <thrust/reduce.h>
#include <thrust/device_ptr.h>
#include "../RooDalitz/cuComplex.h"
#include <iostream>
#include <vector>
#include <fstream>
#include <math.h>
#include "../RooDalitz/cu_DPFPropogator.h"
#include "../RooDalitz/kernel_calEva.h"
#include <assert.h>
#include <vector>
#include "../RooDalitz/MultDevice.h"
#include "../RooDalitz/cu_Jpsihh_dlz.h"
#include "nvToolsExt.h"
using namespace std;
//extern int DEVICE_NUM_ext;
//extern int iDevice_ext;

//将任何cuda函数作为CUDA_CALL的参数，能够显示返回的错误，并定位错误。 
#define CUDA_CALL(x) {const cudaError_t a=(x); if(a != cudaSuccess) {printf("\nerror in line:%d CUDAError:%s(err_num=%d)\n",__LINE__,cudaGetErrorString(a),a); cudaDeviceReset(); assert(0); }}
//block_size 的设定要考虑shared memory 的大小
//shared memory per block ：48k bytes
//每个block所使用的共享空间大小 ：paraList+vampList+paraSplineList)*sizeof(dobule) + 337×sizeof(double)xBLOCK_SIZE  
//控制BLOCK_SIZE 使所使用的shared memory 不可高于上限
#define BLOCK_SIZE 256

double *d_fx[DEVICE_NUM];     
double *d_nll[DEVICE_NUM];
double *d_paraList[DEVICE_NUM];
double *SP[DEVICE_NUM];
//double SP;
int Ns[DEVICE_NUM+1];
//bool once_flag[DEVICE_NUM];
bool once_flag_config = 1;
double * intermediate_BW[DEVICE_NUM];    //complex number, the length is needed to be twice.
double * intermediate_Bterm[DEVICE_NUM]; //real number 

//GetAmp2s是在gpu中运行的一个子程序
__device__ double GetAmp2s(const double * pp, const double * d_paraList, int para_size, int vamp_size, int ip, int NR, int NX, int NZ, int N_size, double* intermediate_BW, double * intermediate_Bterm) 
    ////return square of complex amplitude
{
    double2 H[2][2];// = complex_para;
    for(int i=0;i<=1; ++i) {
        for(int j=0; j<=1; ++j) {
            H[i][j] = make_cuDoubleComplex(0.0f, 0.0f); 
        }
    }

    double2 ampCoff, Atmp2;
    double2 BW;
    double Bterm;
    int S;

    int ir=0;
    int ampcount = para_size;
    int ampcount_save = para_size;
    int iT=0;

    while( ir<NR+NX+NZ ) {
        if(ir==NX) {
            iT = 1;
        }

        S = (int) (d_paraList[ir*nset+SPIN]);

        Bterm = intermediate_Bterm[ip+ir*N_size];
        BW = make_cuDoubleComplex(intermediate_BW[ip + 2*ir*N_size], intermediate_BW[ip + (2*ir+1)*N_size]);    
        BW = cuCmuldc( Bterm, BW );
        for(int i=0; i<=1; ++i) { //helicity of proton
            for(int j=0; j<=1; ++j) { //helicity of antiproton
                if(iT==0) {
                    ampCoff = make_cuDoubleComplex(d_paraList[ampcount++],d_paraList[ampcount++]);    
                    if(cuCabs(ampCoff)<1e-10f) continue;
                    Atmp2 = cuCmuldc((pp[ip + (11+iT*NumS*2*2+S*2*2+i*2+j)*N_size ] ),ampCoff);
                } 
                else if(iT==1) {
                    ampcount = ampcount_save;
                    Atmp2 = make_cuDoubleComplex(0.0f, 0.0f); 
                    for(int ii=0; ii<=1; ++ii) {
                        for(int jj=0; jj<=1; ++jj) {
                            ampCoff = make_cuDoubleComplex(d_paraList[ampcount++],d_paraList[ampcount++]);
                            if(cuCabs(ampCoff)<1e-10f) continue;
                            ampCoff = cuCmuldc( (pp[ip + (3+2*ii + i)*N_size ]  
                                * pp[ip + (7+2*jj+j)*N_size ]
                                * pp[ip + (11+iT*NumS*2*2+(S-1)*2+ii*2+jj)*N_size ] ) ,ampCoff);
                            Atmp2 = cuCadd(Atmp2,ampCoff);
                        }
                    }
                } 
                H[i][j] = cuCadd(H[i][j],cuCmul(Atmp2,BW));
            }    
        }
        ampcount_save = ampcount;
        ir++;
    }
    double totalA = cuCabs2(H[0][0]) + cuCabs2(H[0][1])
                    + cuCabs2(H[1][0]) + cuCabs2(H[1][1]);
//    printf("GPU mppb, mDp, PDF: %10.4f %10.4f %10.4f\n", pp[ip], pp[ip+1*N_size], totalA);                    
    return totalA;  
}

__global__ void kernel_store_fx(const double * double_pp,const double *d_paraList, int para_size, int vamp_size, 
    int paraSpline_size, double * d_fx,int end,int begin, int NUMR, int NUMX, int NUMZ, int N_size, double* intermediate_BW, double* intermediate_Bterm)
{
    int index = blockDim.x * blockIdx.x + threadIdx.x;
    if(index<end-begin && index>= 0)
    {
        d_fx[index] = GetAmp2s(double_pp, d_paraList, para_size, vamp_size, index, NUMR, NUMX, NUMZ, N_size, intermediate_BW, intermediate_Bterm) * double_pp[index+2*N_size];
    }
}

__global__ void kernel_store_BW_AMP(const double *pp, const double *d_paraList, int end,int begin, int N_size, double* intermediate_BW, int ir, int iT, const double *SP)
{
    int index = blockDim.x * blockIdx.x + threadIdx.x;
    if(index<end-begin && index>= 0)
    {
        double m0 = d_paraList[ir*nset+MASS];
        double g0 = d_paraList[ir*nset+WIDTH];
        int LR = (int)(d_paraList[ir*nset+LRESON]);
        int type = (int)(d_paraList[ir*nset+TYPE]);//Breit-Wigner or Flatte
        double mhh;
        if (iT ==0){
            mhh = pp[index]; //mX
        }
        else if(iT == 1){
            mhh = pp[index+1*N_size]; //mDp
        }
        double2 BW;
        if(type == BREITWIGNER){
            //printf("kernel_store_BW_AMP: BREITWIGNER\n");
            BW = BW_AMP(m0,g0,mhh,LR,iT);
        }
        else if(type == FLATTE){
            //printf("kernel_store_BW_AMP: FLATTE\n");
            BW = BW_AMP(m0,g0,mhh,LR,SP[0]);
            //printf("!!!!!!!!!!!!!!!! qnSP %10.2f \n",SP[0]);
        }
        intermediate_BW[index + 2*ir*N_size] = cuCreal(BW);
        intermediate_BW[index + (2*ir+1)*N_size] = cuCimag(BW);
    }
}

__global__ void kernel_store_ModelIndependentWave(const double *pp, const double *d_paraList, int end,int begin, int N_size, double* intermediate_BW, int ir, int iT, int splcount){
    int index = blockDim.x * blockIdx.x + threadIdx.x;
    if(index<end-begin && index>= 0)
    {
        //double m0 = d_paraList[ir*nset+MASS];
        //double g0 = d_paraList[ir*nset+WIDTH];
        int LR = (int)(d_paraList[ir*nset+LRESON]);
        double mhh;
        if (iT ==0){
            mhh = pp[index]; //mX
        }
        else if(iT == 1){
            mhh = pp[index+1*N_size]; //mDp
        }
        double2 BW;
        BW = ModelIndependentWave3(&d_paraList[splcount], mhh, iT);
        //if(iT == 0){
        //    BW = ModelIndependentWave3(&d_paraList[splcount], mhh, iT);
        //    splcount += 9*NUMSP;
        //}
        //if(iT == 1){
        //    BW = ModelIndependentWave3(&d_paraList[splcount], mhh, iT);
        //    splcount += 9*NUMSPY;
        //}
        //printf("iType: %d\n",iT);
        //printf("splcount in GPU GetAmp2S: %d\n",splcount);
        intermediate_BW[index + 2*ir*N_size] = cuCreal(BW);
        intermediate_BW[index + (2*ir+1)*N_size] = cuCimag(BW);
    }
}


void Get_BW( const double* pp,  const double * h_paraList, const double * d_paraList, int para_size, int vamp_size, int paraSpline_size, int end , int begin, int NR, int NX, int NZ, int N_size, double* intermediate_BW, int blocksPerGrid, int threadsPerBlock, const double *SP){

    int ir=0;
    int splcount = para_size+vamp_size;

    int iT=0;
    //int RNs(0), RNd(NR);
    while( ir<NR+NX+NZ ) {
        if(ir==NX) {
            iT = 1;
        }
        int type = (int)(h_paraList[ir*nset+TYPE]);
        switch (type) {
            case BREITWIGNER:
                {    
                    kernel_store_BW_AMP<<<blocksPerGrid, threadsPerBlock>>>(pp, d_paraList, end, begin, N_size, intermediate_BW, ir, iT, SP);
                    break;
                }
            case MODELINDEPENDENT:
                { 
                    //for X only
                    kernel_store_ModelIndependentWave<<<blocksPerGrid, threadsPerBlock>>>(pp, d_paraList, end, begin, N_size, intermediate_BW, ir, iT, splcount);
                    splcount += 9*NUMSP; 
                    break;
                } 
            case FLATTE:
                {
                    kernel_store_BW_AMP<<<blocksPerGrid, threadsPerBlock>>>(pp, d_paraList, end, begin, N_size, intermediate_BW, ir, iT, SP);
                }

        }
        ir++;
    }
}

__device__ void Get_Bterm(const double * pp, const double * d_paraList, int para_size, int vamp_size, int ip, int NR, int NX, int NZ, int N_size, double* intermediate_Bterm)
{
    int S,S2,type;
    double m0;

    int ir=0;
    int iT=0;
    double mhh = pp[ip]; // mX
    while( ir<NR+NX+NZ ) {
        if(ir==NX) {
            iT = 1;
            mhh = pp[ip+1*N_size]; //pp.mDp
        }
        m0 = d_paraList[ir*nset+MASS];
        S = (int) (d_paraList[ir*nset+SPIN]); 
        S2 = (iT==0) ? 2*S : S;
        type = (int)(d_paraList[ir*nset+TYPE]);
        switch (type) {
            case MODELINDEPENDENT:
                {
                    m0 = -0.5f;//4.1163770;  
                    break;
                }        
        }
        intermediate_Bterm[ip + ir*N_size] = BTerm(S2, mhh, m0,  iT); 
        ir++;
    }
    /*
    if (ip<10){
        printf("%f\n", intermediate_Bterm[ip + 1*N_size]);
    }*/
}

__global__ void kernel_store_Bterm(const double * double_pp,const double *d_paraList, int para_size, int vamp_size, int paraSpline_size, int end,int begin, int NUMR, int NUMX, int NUMZ, int N_size, double* intermediate_Bterm){

    int index = blockDim.x * blockIdx.x + threadIdx.x;
    if(index<end-begin && index>= 0)
    {
        Get_Bterm(double_pp, d_paraList, para_size, vamp_size, index, NUMR, NUMX, NUMZ, N_size, intermediate_Bterm);
    }
}

// begin of chenchen
__device__ double GetNll(double fx, double bg, double beta, double inte_over_sumbkg) {
    double f_pdf =  fx * (1.0 - beta) + beta*inte_over_sumbkg*bg;
    return (f_pdf>0) ? log(f_pdf) : -1e12;
}
__global__ void kernel_store_nll(const double *fx, const double *bg, double *d_nll, double beta, double inte_over_sumbkg, int end, int begin)
{
    int index = blockDim.x * blockIdx.x + threadIdx.x;
    if(index<end-begin && index>= 0)
    {
        d_nll[index] = GetNll(fx[index], bg[index], beta, inte_over_sumbkg);
//        printf("index:%i; d_nll:%f; fx:%f; bg:%f; beta:%f; inte_over_sumbkg:%f \n",index,d_nll[index],fx[index],bg[index],beta,inte_over_sumbkg);
    }
}
//end of chenchen


double host_store_fx(std::vector<double*> h_double_pp, std::vector<double*> d_double_pp, std::vector<double*> d_double_bg,  double *h_paraList, int para_size, int vamp_size, int paraSpline_size, int end, int begin, const int Nda, int NUMR, int NUMX, int NUMZ, double beta, double h_SP, double SumBkg, double SumW, double &mynll)
{
//    printf("begin for host\n");
    int total_para_size = para_size+vamp_size+paraSpline_size;
    int size_paraList = total_para_size*sizeof(double);
    
    int number_of_resonance = NUMR + NUMX + NUMZ;
    int intermediate_BW_size_per_evt = number_of_resonance*2*sizeof(double);
    int intermediate_Bterm_size_per_evt = number_of_resonance*sizeof(double);

    if (once_flag_config==1){
        //Ns为分段数组，第i个gpu所处理的线程序号范围为:[ Ns[i] , Ns[i+1] ) 
        //int Ns[DEVICE_NUM+1];
        Ns[0]=0;
        for(int i=1;i<DEVICE_NUM;i++)
        {
            Ns[i]=Ns[i-1]+end/DEVICE_NUM;
//            printf("Ns[%i] %i\n",i, Ns[i]);
        }
        Ns[DEVICE_NUM]=end;

        //for(int i=0;i<DEVICE_NUM;i++){
        //    once_flag[i] = 1;
        //}
        for(int i=0;i<DEVICE_NUM;i++)
        {
            CUDA_CALL( cudaSetDevice(i+iDevice) );
            int N_thread=Ns[i+1]-Ns[i];
            nvtxRangePushA("malloc");
//            printf("malloc begin\n");
            CUDA_CALL(cudaMalloc((void **)&(d_fx[i]),N_thread * sizeof(double)));
            //CUDA_CALL(cudaMalloc((void **)&(SP[i],sizeof(double)));
            CUDA_CALL(cudaMalloc((void **)&(d_paraList[i]),size_paraList));//为待拟合参数在GPU上开辟空间
            CUDA_CALL(cudaMalloc((void **)&(SP[i]),sizeof(double)));//为待拟合参数在GPU上开辟空间
            CUDA_CALL(cudaMalloc((void **)&(intermediate_BW[i]),    (N_thread/32+1)*32*intermediate_BW_size_per_evt));   //times 32 for coalseced problem.
            CUDA_CALL(cudaMalloc((void **)&(intermediate_Bterm[i]), (N_thread/32+1)*32*intermediate_Bterm_size_per_evt));   //times 32 for coalseced problem.
            //Only malloc memory of size of data
            //N_thread = min(Ns[i+1]-Ns[i], Nda-Ns[i]);
            CUDA_CALL(cudaMalloc((void **)&(d_nll[i]),N_thread * sizeof(double)));
//            printf("malloc end\n");            
            nvtxRangePop();
        }
        once_flag_config = 0;
    }

    //memcpy d_paraList
    for(int i=0;i<DEVICE_NUM;i++)
    {
        CUDA_CALL(cudaSetDevice(i+iDevice) );
        //使用异步函数.
        CUDA_CALL(cudaMemcpyAsync(d_paraList[i] , h_paraList, size_paraList, cudaMemcpyHostToDevice));
        CUDA_CALL(cudaMemcpyAsync(SP[i] , &h_SP, sizeof(double), cudaMemcpyHostToDevice));
    }

    int threadsPerBlock = BLOCK_SIZE;
    //SP = h_SP;
    //printf("!!!!!!!!!!!!!!!! qnSP %10.2f \n",SP);
    
    for(int i=0;i<DEVICE_NUM;i++)
    {
        CUDA_CALL(cudaSetDevice(i+iDevice) );
        int N_thread=Ns[i+1]-Ns[i];
        int blocksPerGrid =(N_thread + threadsPerBlock - 1) / threadsPerBlock;
        //      std::cout << "blocksPerGrid, threadsPerBlock,size_paraList " << blocksPerGrid << " " << threadsPerBlock << " " << size_paraList << std::endl;
        //==========================================================================================
        Get_BW(d_double_pp[i], h_paraList, d_paraList[i], para_size, vamp_size, paraSpline_size, Ns[i+1],Ns[i], NUMR, NUMX, NUMZ, (N_thread/32+1)*32 , intermediate_BW[i], blocksPerGrid, threadsPerBlock, SP[i]);
        /*
        kernel_store_BW<<<blocksPerGrid, threadsPerBlock,size_paraList>>>(d_double_pp[i], d_paraList[i],para_size,vamp_size, paraSpline_size, Ns[i+1],Ns[i], NUMR, NUMX, NUMZ, (N_thread/32+1)*32 , intermediate_BW[i]); 
        */
        kernel_store_Bterm<<<blocksPerGrid, threadsPerBlock,size_paraList>>>(d_double_pp[i], d_paraList[i],para_size,vamp_size, paraSpline_size, Ns[i+1],Ns[i], NUMR, NUMX, NUMZ, (N_thread/32+1)*32 , intermediate_Bterm[i]); 
//        printf("Done BTerm\n");
        kernel_store_fx<<<blocksPerGrid, threadsPerBlock,size_paraList>>>(d_double_pp[i], d_paraList[i],para_size,vamp_size, paraSpline_size, d_fx[i],Ns[i+1],Ns[i], NUMR, NUMX, NUMZ, (N_thread/32+1)*32 , intermediate_BW[i], intermediate_Bterm[i]); 
//        printf("Done combination\n");
    }

    double sumint(0.);
    for(int i=0;i<DEVICE_NUM;i++)
    {
        if(Ns[i+1]<=Nda) continue;
        CUDA_CALL(cudaSetDevice(i+iDevice) );
        int N_thread=Ns[i+1]-Ns[i];
        thrust::device_ptr<double> dev_ptr(d_fx[i]);
        nvtxRangePushA("sum MC");
        sumint = thrust::reduce(dev_ptr+max(Nda-Ns[i],0), dev_ptr + N_thread, sumint, thrust::plus<double>());
        nvtxRangePop();
        //    
    }

    //begin of chenchen 
    // nll store
    double inte_over_sumbkg = sumint/SumW/SumBkg;
    for(int i=0;i<DEVICE_NUM;i++)
    {
        if(Ns[i]>=Nda) continue;
        CUDA_CALL(cudaSetDevice(i+iDevice) );
        int N_thread=min(Ns[i+1]-Ns[i], Nda-Ns[i]);
        if(!N_thread) continue; // no event to process
        int blocksPerGrid =(N_thread + threadsPerBlock - 1) / threadsPerBlock;
        nvtxRangePushA("kernel_store_nll");
        kernel_store_nll<<<blocksPerGrid, threadsPerBlock>>>(d_fx[i], d_double_bg[i], d_nll[i], beta, inte_over_sumbkg, Ns[i]+N_thread, Ns[i]); 
        nvtxRangePop();
    }

    // nll sum of data 
    double inttt = sumint/SumW;
    double nll_shift = NLL_SHIFT;
    nll_shift += Nda*log(inttt);
    double nll_data = 0; 
    for(int i=0;i<DEVICE_NUM;i++)
    {
        if(Ns[i]>=Nda) continue;
        CUDA_CALL(cudaSetDevice(i+iDevice) );
        int N_thread=min(Ns[i+1]-Ns[i], Nda-Ns[i]);
        thrust::device_ptr<double> dev_ptr(d_nll[i]);
        nvtxRangePushA("log sum of data");
        nll_data = thrust::reduce(dev_ptr, dev_ptr + N_thread, nll_data, thrust::plus<double>());
        nvtxRangePop();
    }

    mynll = nll_shift - nll_data;
    // end of chenchen
    return sumint;
}
/*
//在gpu中为pwa_paras开辟空间
void cu_malloc_h_pp(double *h_double_pp,double *&d_double_pp,int length,int device)
{
    //const int DEVICE_NUM = DEVICE_NUM_ext;
    //const int iDevice = iDevice_ext;
    //Ns为分段数组，第i个gpu所处理的线程序号范围为:[ Ns[i] , Ns[i+1] ) 
    int Ns[DEVICE_NUM+1];
    Ns[0]=0;
    for(int i=1;i<DEVICE_NUM;i++)
    {
        Ns[i]=Ns[i-1]+length/DEVICE_NUM;
    }
    Ns[DEVICE_NUM]=length;
    int deviceCount;
    CUDA_CALL( cudaGetDevice(&deviceCount) );
    printf("Num GPU %i %i\n",deviceCount, device);
    CUDA_CALL( cudaSetDevice(device+iDevice) );
    //  CUDA_CALL( cudaDeviceSynchronize() );
    //  CUDA_CALL( cudaThreadSynchronize() );  
    int N_size = Ns[device+1]-Ns[device];

    int array_num = sizeof(cu_Jpsihh_dlz) / sizeof(double);
    size_t mem_size = array_num*sizeof(double);


    //size_t mem_size = (int(sizeof(cu_Jpsihh_dlz)/32)+1)*32;// * N_size;
    //int array_size = (int(sizeof(cu_Jpsihh_dlz)/32)+1)*32/sizeof(double);
    printf("size to locate %d %d\n", mem_size, N_size);
    CUDA_CALL(cudaMalloc((void **)&d_double_pp, mem_size * N_size));
    CUDA_CALL(cudaMemcpy(d_double_pp , &h_double_pp[array_size*Ns[device]], mem_size * N_size, cudaMemcpyHostToDevice));
}
*/
void cu_malloc_h_pp(double *h_double_pp,double *&d_double_pp,int mem_length, int device){
    //int deviceCount;
    //CUDA_CALL( cudaGetDevice(&deviceCount) );
    //printf("Num GPU %i %i\n",deviceCount, device);
    CUDA_CALL( cudaSetDevice(device+iDevice) );
    printf("size to locate device: %d\n", device);

    CUDA_CALL(cudaMalloc((void **)&d_double_pp, mem_length));
    CUDA_CALL(cudaMemcpy(d_double_pp , &h_double_pp[0], mem_length, cudaMemcpyHostToDevice));
}

void cu_Free_pp(std::vector<double *>d_double_pp)
{
    //const int DEVICE_NUM = DEVICE_NUM_ext;
    //const int iDevice = iDevice_ext;
    if(d_double_pp.size()!=DEVICE_NUM) return;
    for(int i=0;i<DEVICE_NUM;i++)
    {
        CUDA_CALL(cudaSetDevice(i+iDevice) );
        if(d_double_pp[i]) CUDA_CALL(cudaFree(d_double_pp[i]));
        CUDA_CALL(cudaSetDevice(i+iDevice) );
        CUDA_CALL(cudaFree(d_fx[i]));
        CUDA_CALL(cudaFree(d_paraList[i]));
        CUDA_CALL(cudaFree(SP[i]));
    }
}
