/*************************************************************************
	> File Name: kernel_calEva.h
	> Author:
	> Mail:
	> Created Time: 2017年03月21日 星期二 18时20分53秒
 ************************************************************************/

#ifndef _KERNEL_CALEVA
#define _KERNEL_CALEVA
#include <vector>
//double *d_double_pp=NULL;
//#include "cu_PWA_PARAS.h"
//int initialize_data(std::vector<PWA_PARAS>&, DataPointers&); // 把vector数据和指针数据对应起来，并copy到gpu里面去
//int data_distribution(DataPointers&, CudaDataPointers&); // 把vector数据和指针数据对应起来，并copy到gpu里面去
//double calEva(const PWA_PARAS &pp, int idp);
//double kernel_calEva(const PWA_PARAS &pp,int idp);
//void func(DataPointers& cpu_data_pointers);
void cu_malloc_h_pp(double *, double *&, int, int);
void cu_Free_pp(std::vector<double *> d_double_pp);
//double host_store_fx(std::vector<double *> d_double_pp, double *h_paraList, int para_size, int vamp_size, int, double *h_fx, int end, int begin, int Nda, int NUMR, int NUMX, int NUMZ);
//chenchen
double host_store_fx(std::vector<double*> h_double_pp, std::vector<double*> d_double_pp, std::vector<double*> d_double_bg,  double *h_paraList, int para_size, int vamp_size, int paraSpline_size, int end, int begin, const int Nda, int NUMR, int NUMX, int NUMZ, double beta, double SP, double SumBkg, double SumW, double &mynll);
#endif
