import numpy as np
import ROOT

NLL_base = -11130.4
NLL_2910 = [-11137.2,-11137.8,-11136.5,-11137.6]#JP=1/2+,1/2-,3/2+,3/2-
NLL_2800 = [-11138.0,-11151.7,-11148.4,-11134.4,-11137.3,-11134.4]#JP=1/2+,1/2-,3/2+,3/2-,5/2+,5/2-
ndof = 6

for i in range(len(NLL_2910)):
    sig = np.sqrt(2)*ROOT.TMath.ErfcInverse(ROOT.TMath.Prob(2*(NLL_base-NLL_2910[i]),2*ndof))
    print(i,'{:.2f}'.format(sig))
for i in range(len(NLL_2800)):
    sig = np.sqrt(2)*ROOT.TMath.ErfcInverse(ROOT.TMath.Prob(2*(NLL_base-NLL_2800[i]),2*ndof))
    print(i,'{:.2f}'.format(sig))

NLL_2910_1p = [-11105.8,-11101.1,-11108.7]
#NLL_2910_1m = [-11104.3,-11104.9,-11112.2]
NLL_2910_1m = [-11104.3,-11104.9,-11111.2]
NLL_2910_3p = [-11090.3,-11048.3,-11091.1]
NLL_2910_3m = [-11086.1,-11056.3,-11078.1]

NLL_2800_1m = [-11130.4,-10974.7,-11039.3]
NLL_2800_3p = [-11130.2,-10996.6,-11051.3]

print("2910_1p")
for i in range(len(NLL_2910_1p)):
    sig = np.sqrt(2)*ROOT.TMath.ErfcInverse(ROOT.TMath.Prob(2*(NLL_2910_1p[i]-NLL_2910[0]),2*ndof))
    print(i,'{:.2f}'.format(sig))
print("2910_1m")
for i in range(len(NLL_2910_1m)):
    sig = np.sqrt(2)*ROOT.TMath.ErfcInverse(ROOT.TMath.Prob(2*(NLL_2910_1m[i]-NLL_2910[1]),2*ndof))
    print(i,'{:.2f}'.format(sig))
print("2910_3p")
for i in range(len(NLL_2910_3p)):
    sig = np.sqrt(2)*ROOT.TMath.ErfcInverse(ROOT.TMath.Prob(2*(NLL_2910_3p[i]-NLL_2910[2]),2*ndof))
    print(i,'{:.2f}'.format(sig))
print("2910_3m")
for i in range(len(NLL_2910_3m)):
    sig = np.sqrt(2)*ROOT.TMath.ErfcInverse(ROOT.TMath.Prob(2*(NLL_2910_3m[i]-NLL_2910[3]),2*ndof))
    print(i,'{:.2f}'.format(sig))

print("2800_1m")
for i in range(len(NLL_2800_1m)):
    sig = np.sqrt(2)*ROOT.TMath.ErfcInverse(ROOT.TMath.Prob(2*(NLL_2800_1m[i]-NLL_2800[1]),2*ndof))
    print(i,'{:.2f}'.format(sig))
print("2800_3p")
for i in range(len(NLL_2800_3p)):
    sig = np.sqrt(2)*ROOT.TMath.ErfcInverse(ROOT.TMath.Prob(2*(NLL_2800_3p[i]-NLL_2800[2]),2*ndof))
    print(i,'{:.2f}'.format(sig))

ndof = 4
NLL_2910_fix = [-11137.4,-11132.8]#1/2+,1/2-
NLL_2910_fix_1p = [-11090.3,-11058.2,-11091.3]
NLL_2910_fix_1m = [-11088.3,-11074.9,-11084.8]
print("2910_fix_1p")
for i in range(len(NLL_2910_fix_1p)):
    sig = np.sqrt(2)*ROOT.TMath.ErfcInverse(ROOT.TMath.Prob(2*(NLL_2910_fix_1p[i]-NLL_2910_fix[0]),2*ndof))
    print(i,'{:.2f}'.format(sig))
print("2910_fix_1m")
for i in range(len(NLL_2910_fix_1m)):
    sig = np.sqrt(2)*ROOT.TMath.ErfcInverse(ROOT.TMath.Prob(2*(NLL_2910_fix_1m[i]-NLL_2910_fix[1]),2*ndof))
    print(i,'{:.2f}'.format(sig))

