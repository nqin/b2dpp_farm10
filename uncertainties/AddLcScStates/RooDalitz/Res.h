int typeR(360);
int typeKM(360);
int typeFlatte = FLATTE;
int typeNonResonance = NonResonance;
typeR = BREITWIGNER;
typeKM = BREITWIGNER;

/////////////////////////////////////////
// ppbar,X-chain 
/////////////////////////////////////////
TList *listX = new TList();


/////////////////////////////
//***** Non-resonance *****//
/////////////////////////////

// JP=0- spin-0, only has 1 helicity coupling, 
RooRealVar m0_NR0m("m0_NR0m","m0",-1);
//RooRealVar m0_NR0m("m0_NR0m","m0",0.,-100,100);
RooRealVar width_NR0m("width_NR0m","width",0.,-10,10);
//RooRealVar width_NR0m("width_NR0m","width",2.046);
RooRealVar ar_H0_NR0m("ar_H0_NR0m","",0,-10,10);
RooRealVar ai_H0_NR0m("ai_H0_NR0m","",0.,-10,10);
RooRealVar ar_H1_NR0m("ar_H1_NR0m","",0.);
RooRealVar ai_H1_NR0m("ai_H1_NR0m","",0.);  
RooArgList* X_NR0m = new RooArgList(ar_H0_NR0m,ai_H0_NR0m,
      			      ar_H1_NR0m,ai_H1_NR0m,"X_NR0m");
X_NR0m->add(m0_NR0m);
X_NR0m->add(width_NR0m);
//X_NR0m->add(RooRealConstant::value(typeKM));
X_NR0m->add(RooRealConstant::value(typeNonResonance));
X_NR0m->add(RooRealConstant::value(0));//spin
X_NR0m->add(RooRealConstant::value(-1)); //parity

// JP=0+ spin-0, only has 1 helicity coupling, 
RooRealVar m0_NR0p("m0_NR0p","m0",-1);
RooRealVar width_NR0p("width_NR0p","width",0.,-10,10);
RooRealVar ar_H0_NR0p("ar_H0_NR0p","",0,-10,10);
RooRealVar ai_H0_NR0p("ai_H0_NR0p","",0.,-10,10);
RooRealVar ar_H1_NR0p("ar_H1_NR0p","",0.);
RooRealVar ai_H1_NR0p("ai_H1_NR0p","",0.);  
RooArgList* X_NR0p = new RooArgList(ar_H0_NR0p,ai_H0_NR0p,
      			      ar_H1_NR0p,ai_H1_NR0p,"X_NR0p");
X_NR0p->add(m0_NR0p);
X_NR0p->add(width_NR0p);
X_NR0p->add(RooRealConstant::value(typeKM));
X_NR0p->add(RooRealConstant::value(0));//spin
X_NR0p->add(RooRealConstant::value(1)); //parity

// JP=1-
RooRealVar m0_NR1m("m0_NR1m","m0",-1);
RooRealVar width_NR1m("width_NR1m","width",0.,-10,10);
RooRealVar ar_H0_NR1m("ar_H0_NR1m","",0,-10,10);
RooRealVar ai_H0_NR1m("ai_H0_NR1m","",0.,-10,10);
RooRealVar ar_H1_NR1m("ar_H1_NR1m","",0.,-10,10);
RooRealVar ai_H1_NR1m("ai_H1_NR1m","",0.,-10,10);  
RooArgList* X_NR1m = new RooArgList(ar_H0_NR1m,ai_H0_NR1m,
      			      ar_H1_NR1m,ai_H1_NR1m,"X_NR1m");
X_NR1m->add(m0_NR1m);
X_NR1m->add(width_NR1m);
X_NR1m->add(RooRealConstant::value(typeKM));
X_NR1m->add(RooRealConstant::value(1));//spin
X_NR1m->add(RooRealConstant::value(-1)); //parity

// JP=1+
RooRealVar m0_NR1p("m0_NR1p","m0",-1);
RooRealVar width_NR1p("width_NR1p","width",0,-10,10);
RooRealVar ar_H0_NR1p("ar_H0_NR1p","",0,-10,10);
RooRealVar ai_H0_NR1p("ai_H0_NR1p","",0.,-10,10);
RooRealVar ar_H1_NR1p("ar_H1_NR1p","",0.,-10,10);
RooRealVar ai_H1_NR1p("ai_H1_NR1p","",0.,-10,10);  
RooArgList* X_NR1p = new RooArgList(ar_H0_NR1p,ai_H0_NR1p,
      			      ar_H1_NR1p,ai_H1_NR1p,"X_NR1p");
X_NR1p->add(m0_NR1p);
X_NR1p->add(width_NR1p);
X_NR1p->add(RooRealConstant::value(typeKM));
X_NR1p->add(RooRealConstant::value(1));//spin
X_NR1p->add(RooRealConstant::value(1)); //parity

// JP=2+
RooRealVar m0_NR2("m0_NR2","m0",-1);
RooRealVar width_NR2("width_NR2","width",0.);
RooRealVar ar_H0_NR2("ar_H0_NR2","",0,-10,10);
RooRealVar ai_H0_NR2("ai_H0_NR2","",0.,-10,10);
RooRealVar ar_H1_NR2("ar_H1_NR2","",0.,-10,10);
RooRealVar ai_H1_NR2("ai_H1_NR2","",0.,-10,10);  
RooArgList* X_NR2 = new RooArgList(ar_H0_NR2,ai_H0_NR2,
      			      ar_H1_NR2,ai_H1_NR2,"X_NR2");
X_NR2->add(m0_NR2);
X_NR2->add(width_NR2);
X_NR2->add(RooRealConstant::value(typeKM));
X_NR2->add(RooRealConstant::value(2));//spin
X_NR2->add(RooRealConstant::value(1)); //parity
 
/////////////////////////////
//***** Spin-0        *****//
/////////////////////////////

// JP=0-, X(1835)
//RooRealVar m0_1835("m0_1835","m0",1.8265);
//RooRealVar width_1835("width_1835","width",0.242);
//RooRealVar m0_1835("m0_1835","m0",1.832,1.812,1.852);
RooRealVar m0_1835("m0_1835","m0",1.832);
RooRealVar width_1835("width_1835","width",0.1,0,0.442);
RooRealVar ar_H0_1835("ar_H0_1835","",0.,-10,10);
RooRealVar ai_H0_1835("ai_H0_1835","",0.,-10,10);
RooRealVar ar_H1_1835("ar_H1_1835","",0.);
RooRealVar ai_H1_1835("ai_H1_1835","",0.);  
RooArgList* X_X_1835 = new RooArgList(ar_H0_1835,ai_H0_1835,
      			      ar_H1_1835,ai_H1_1835,"X_X_1835");
X_X_1835->add(m0_1835);
X_X_1835->add(width_1835);
X_X_1835->add(RooRealConstant::value(typeKM));
RooRealVar J1835("J1835","",1);
RooRealVar P1835("P1835","",-1);  
X_X_1835->add(J1835);//spin
X_X_1835->add(P1835); //parity

// JP=0+, a0(1950)
RooRealVar m0_a_1950("m0_a_1950","m0",1.931);
RooRealVar width_a_1950("width_a_1950","width",0.271);
RooRealVar ar_H0_a_1950("ar_H0_a_1950","",0.,-10,10);
RooRealVar ai_H0_a_1950("ai_H0_a_1950","",0.,-10,10);
RooRealVar ar_H1_a_1950("ar_H1_a_1950","",0.);
RooRealVar ai_H1_a_1950("ai_H1_a_1950","",0.);  
RooArgList* X_a_1950 = new RooArgList(ar_H0_a_1950,ai_H0_a_1950,
      			      ar_H1_a_1950,ai_H1_a_1950,"X_a_1950");
X_a_1950->add(m0_a_1950);
X_a_1950->add(width_a_1950);
X_a_1950->add(RooRealConstant::value(typeKM));
X_a_1950->add(RooRealConstant::value(0));//spin
X_a_1950->add(RooRealConstant::value(1)); //parity

// JP=0+, f0(2020)
RooRealVar m0_f_2020("m0_f_2020","m0",1.992);
RooRealVar width_f_2020("width_f_2020","width",0.442);
RooRealVar ar_H0_f_2020("ar_H0_f_2020","",0.,-10,10);
RooRealVar ai_H0_f_2020("ai_H0_f_2020","",0.,-10,10);
RooRealVar ar_H1_f_2020("ar_H1_f_2020","",0.);
RooRealVar ai_H1_f_2020("ai_H1_f_2020","",0.);  
RooArgList* X_f_2020 = new RooArgList(ar_H0_f_2020,ai_H0_f_2020,
      			      ar_H1_f_2020,ai_H1_f_2020,"X_f_2020");
X_f_2020->add(m0_f_2020);
X_f_2020->add(width_f_2020);
X_f_2020->add(RooRealConstant::value(typeKM));
X_f_2020->add(RooRealConstant::value(0));//spin
X_f_2020->add(RooRealConstant::value(1)); //parity
 
// JP=0+, f0(2100)
RooRealVar m0_f_2100("m0_f_2100","m0",2.095);
RooRealVar width_f_2100("width_f_2100","width",0.287);
RooRealVar ar_H0_f_2100("ar_H0_f_2100","",0.,-10,10);
RooRealVar ai_H0_f_2100("ai_H0_f_2100","",0.,-10,10);
RooRealVar ar_H1_f_2100("ar_H1_f_2100","",0.);
RooRealVar ai_H1_f_2100("ai_H1_f_2100","",0.);  
RooArgList* X_f_2100 = new RooArgList(ar_H0_f_2100,ai_H0_f_2100,
      			      ar_H1_f_2100,ai_H1_f_2100,"X_f_2100");
X_f_2100->add(m0_f_2100);
X_f_2100->add(width_f_2100);
X_f_2100->add(RooRealConstant::value(typeKM));
X_f_2100->add(RooRealConstant::value(0));//spin
X_f_2100->add(RooRealConstant::value(1)); //parity
 
// JP=0+, f0(2200)
RooRealVar m0_f_2200("m0_f_2200","m0",2.187);
RooRealVar width_f_2200("width_f_2200","width",0.207);
RooRealVar ar_H0_f_2200("ar_H0_f_2200","",0.,-10,10);
RooRealVar ai_H0_f_2200("ai_H0_f_2200","",0.,-10,10);
RooRealVar ar_H1_f_2200("ar_H1_f_2200","",0.);
RooRealVar ai_H1_f_2200("ai_H1_f_2200","",0.);  
RooArgList* X_f_2200 = new RooArgList(ar_H0_f_2200,ai_H0_f_2200,
      			      ar_H1_f_2200,ai_H1_f_2200,"X_f_2200");
X_f_2200->add(m0_f_2200);
X_f_2200->add(width_f_2200);
X_f_2200->add(RooRealConstant::value(typeKM));
X_f_2200->add(RooRealConstant::value(0));//spin
X_f_2200->add(RooRealConstant::value(1)); //parity
 
// JP=0-, eta(2225)
RooRealVar m0_eta_2225("m0_eta_2225","m0",2.187);
RooRealVar width_eta_2225("width_eta_2225","width",0.207);
RooRealVar ar_H0_eta_2225("ar_H0_eta_2225","",0.,-10,10);
RooRealVar ai_H0_eta_2225("ai_H0_eta_2225","",0.,-10,10);
RooRealVar ar_H1_eta_2225("ar_H1_eta_2225","",0.);
RooRealVar ai_H1_eta_2225("ai_H1_eta_2225","",0.);  
RooArgList* X_eta_2225 = new RooArgList(ar_H0_eta_2225,ai_H0_eta_2225,
      			      ar_H1_eta_2225,ai_H1_eta_2225,"X_eta_2225");
X_eta_2225->add(m0_eta_2225);
X_eta_2225->add(width_eta_2225);
X_eta_2225->add(RooRealConstant::value(typeKM));
X_eta_2225->add(RooRealConstant::value(0));//spin
X_eta_2225->add(RooRealConstant::value(-1)); //parity
 
// JP=0+, f0(2330)
RooRealVar m0_f_2330("m0_f_2330","m0",2.330);
RooRealVar width_f_2330("width_f_2330","width",0.207);
RooRealVar ar_H0_f_2330("ar_H0_f_2330","",0.,-10,10);
RooRealVar ai_H0_f_2330("ai_H0_f_2330","",0.,-10,10);
RooRealVar ar_H1_f_2330("ar_H1_f_2330","",0.);
RooRealVar ai_H1_f_2330("ai_H1_f_2330","",0.);  
RooArgList* X_f_2330 = new RooArgList(ar_H0_f_2330,ai_H0_f_2330,
      			      ar_H1_f_2330,ai_H1_f_2330,"X_f_2330");
X_f_2330->add(m0_f_2330);
X_f_2330->add(width_f_2330);
X_f_2330->add(RooRealConstant::value(typeKM));
X_f_2330->add(RooRealConstant::value(0));//spin
X_f_2330->add(RooRealConstant::value(1)); //parity
 
/////////////////////////////
//***** Spin-1        *****//
/////////////////////////////

// JP=1-, rho(1900)
RooRealVar m0_1900("m0_1900","m0",1.90,1.85,1.92);
RooRealVar width_1900("width_1900","width",0.05,0.01,0.1);
RooRealVar ar_H0_1900("ar_H0_1900","",0.,-10,10);
RooRealVar ai_H0_1900("ai_H0_1900","",0.,-10,10);
RooRealVar ar_H1_1900("ar_H1_1900","",0.,-10,10);
RooRealVar ai_H1_1900("ai_H1_1900","",0.,-10,10);  
RooArgList* X_rho_1900 = new RooArgList(ar_H0_1900,ai_H0_1900,
      			      ar_H1_1900,ai_H1_1900,"X_rho_1900");
X_rho_1900->add(m0_1900);
X_rho_1900->add(width_1900);
X_rho_1900->add(RooRealConstant::value(typeKM));
X_rho_1900->add(RooRealConstant::value(1));//spin
X_rho_1900->add(RooRealConstant::value(-1)); //parity

// JP=1-, rho(2150)
RooRealVar m0_2150("m0_2150","m0",2.034);
RooRealVar width_2150("width_2150","width",0.234);
RooRealVar ar_H0_2150("ar_H0_2150","",0.,-10,10);
RooRealVar ai_H0_2150("ai_H0_2150","",0.,-10,10);
RooRealVar ar_H1_2150("ar_H1_2150","",0.,-10,10);
RooRealVar ai_H1_2150("ai_H1_2150","",0.,-10,10);  
RooArgList* X_rho_2150 = new RooArgList(ar_H0_2150,ai_H0_2150,
      			      ar_H1_2150,ai_H1_2150,"X_rho_2150");
X_rho_2150->add(m0_2150);
X_rho_2150->add(width_2150);
X_rho_2150->add(RooRealConstant::value(typeKM));
X_rho_2150->add(RooRealConstant::value(1));//spin
X_rho_2150->add(RooRealConstant::value(-1)); //parity

// JP=1-, phi(2170)
RooRealVar m0_phi_2170("m0_phi_2170","m0",2.162);
RooRealVar width_phi_2170("width_phi_2170","width",0.100);
RooRealVar ar_H0_phi_2170("ar_H0_phi_2170","",0.,-10,10);
RooRealVar ai_H0_phi_2170("ai_H0_phi_2170","",0.,-10,10);
RooRealVar ar_H1_phi_2170("ar_H1_phi_2170","",0.,-10,10);
RooRealVar ai_H1_phi_2170("ai_H1_phi_2170","",0.,-10,10);  
RooArgList* X_phi_2170 = new RooArgList(ar_H0_phi_2170,ai_H0_phi_2170,
      			      ar_H1_phi_2170,ai_H1_phi_2170,"X_phi_2170");
X_phi_2170->add(m0_phi_2170);
X_phi_2170->add(width_phi_2170);
X_phi_2170->add(RooRealConstant::value(typeKM));
X_phi_2170->add(RooRealConstant::value(1));//spin
X_phi_2170->add(RooRealConstant::value(-1)); //parity

/////////////////////////////
//***** Spin-2        *****//
/////////////////////////////

// JP=2+ f2(1810)
RooRealVar m0_f_1810("m0_f_1810","m0",1.815);
RooRealVar width_f_1810("width_f_1810","width",0.197);
RooRealVar ar_H0_f_1810("ar_H0_f_1810","",0.,-100,100);
RooRealVar ai_H0_f_1810("ai_H0_f_1810","",0.,-100,100);
RooRealVar ar_H1_f_1810("ar_H1_f_1810","",0.,-100,100);
RooRealVar ai_H1_f_1810("ai_H1_f_1810","",0.,-100,100);  
RooArgList* X_f2_1810 = new RooArgList(ar_H0_f_1810,ai_H0_f_1810,
      			      ar_H1_f_1810,ai_H1_f_1810,"X_f2_1810");
X_f2_1810->add(m0_f_1810);
X_f2_1810->add(width_f_1810);
X_f2_1810->add(RooRealConstant::value(typeKM));
X_f2_1810->add(RooRealConstant::value(2));//spin
X_f2_1810->add(RooRealConstant::value(1)); //parity

// JP=2+ f2(1910)
RooRealVar m0_f_1910("m0_f_1910","m0",1.900);
RooRealVar width_f_1910("width_f_1910","width",0.167);
//RooRealVar m0_f_1910("m0_f_1910","m0",1.934);
//RooRealVar width_f_1910("width_f_1910","width",0.141);
RooRealVar ar_H0_f_1910("ar_H0_f_1910","",0.,-100,100);
RooRealVar ai_H0_f_1910("ai_H0_f_1910","",0.,-100,100);
RooRealVar ar_H1_f_1910("ar_H1_f_1910","",0.,-100,100);
RooRealVar ai_H1_f_1910("ai_H1_f_1910","",0.,-100,100);  
RooArgList* X_f2_1910 = new RooArgList(ar_H0_f_1910,ai_H0_f_1910,
      			      ar_H1_f_1910,ai_H1_f_1910,"X_f2_1910");
X_f2_1910->add(m0_f_1910);
X_f2_1910->add(width_f_1910);
X_f2_1910->add(RooRealConstant::value(typeKM));
X_f2_1910->add(RooRealConstant::value(2));//spin
X_f2_1910->add(RooRealConstant::value(1)); //parity

// JP=2+ f2(1950)
RooRealVar m0_f_1950("m0_f_1950","m0",1.936);//1.936\pm0.012
RooRealVar width_f_1950("width_f_1950","width",0.464);//0.464\pm0.024
RooRealVar ar_H0_f_1950("ar_H0_f_1950","",0.,-10,10);
RooRealVar ai_H0_f_1950("ai_H0_f_1950","",0.,-10,10);
RooRealVar ar_H1_f_1950("ar_H1_f_1950","",0.,-10,10);
RooRealVar ai_H1_f_1950("ai_H1_f_1950","",0.,-10,10);  
RooArgList* X_f2_1950 = new RooArgList(ar_H0_f_1950,ai_H0_f_1950,
      			      ar_H1_f_1950,ai_H1_f_1950,"X_f2_1950");
X_f2_1950->add(m0_f_1950);
X_f2_1950->add(width_f_1950);
X_f2_1950->add(RooRealConstant::value(typeKM));
X_f2_1950->add(RooRealConstant::value(2));//spin
X_f2_1950->add(RooRealConstant::value(1)); //parity

// JP=2+ f2(2010)
RooRealVar m0_f_2010("m0_f_2010","m0",2.011);
RooRealVar width_f_2010("width_f_2010","width",0.202);
RooRealVar ar_H0_f_2010("ar_H0_f_2010","",0.,-10,10);
RooRealVar ai_H0_f_2010("ai_H0_f_2010","",0.,-10,10);
RooRealVar ar_H1_f_2010("ar_H1_f_2010","",0.,-10,10);
RooRealVar ai_H1_f_2010("ai_H1_f_2010","",0.,-10,10);  
RooArgList* X_f2_2010 = new RooArgList(ar_H0_f_2010,ai_H0_f_2010,
      			      ar_H1_f_2010,ai_H1_f_2010,"X_f2_2010");
X_f2_2010->add(m0_f_2010);
X_f2_2010->add(width_f_2010);
X_f2_2010->add(RooRealConstant::value(typeKM));
X_f2_2010->add(RooRealConstant::value(2));//spin
X_f2_2010->add(RooRealConstant::value(1)); //parity

// JP=2+ f2(2150)
RooRealVar m0_f_2150("m0_f_2150","m0",2.157);//2.157\pm0.012
RooRealVar width_f_2150("width_f_2150","width",0.152);//0.152\pm0.030
RooRealVar ar_H0_f_2150("ar_H0_f_2150","",0.,-100,100);
RooRealVar ai_H0_f_2150("ai_H0_f_2150","",0.,-100,100);
RooRealVar ar_H1_f_2150("ar_H1_f_2150","",0.,-100,100);
RooRealVar ai_H1_f_2150("ai_H1_f_2150","",0.,-100,100);  
RooArgList* X_f2_2150 = new RooArgList(ar_H0_f_2150,ai_H0_f_2150,
      			      ar_H1_f_2150,ai_H1_f_2150,"X_f2_2150");
X_f2_2150->add(m0_f_2150);
X_f2_2150->add(width_f_2150);
X_f2_2150->add(RooRealConstant::value(typeKM));
X_f2_2150->add(RooRealConstant::value(2));//spin
X_f2_2150->add(RooRealConstant::value(1)); //parity

// JP=2+ f2(2300)
RooRealVar m0_f_2300("m0_f_2300","m0",2.297);
RooRealVar width_f_2300("width_f_2300","width",0.149);
RooRealVar ar_H0_f_2300("ar_H0_f_2300","",0.,-100,100);
RooRealVar ai_H0_f_2300("ai_H0_f_2300","",0.,-100,100);
RooRealVar ar_H1_f_2300("ar_H1_f_2300","",0.,-100,100);
RooRealVar ai_H1_f_2300("ai_H1_f_2300","",0.,-100,100);  
RooArgList* X_f2_2300 = new RooArgList(ar_H0_f_2300,ai_H0_f_2300,
      			      ar_H1_f_2300,ai_H1_f_2300,"X_f2_2300");
X_f2_2300->add(m0_f_2300);
X_f2_2300->add(width_f_2300);
X_f2_2300->add(RooRealConstant::value(typeKM));
X_f2_2300->add(RooRealConstant::value(2));//spin
X_f2_2300->add(RooRealConstant::value(1)); //parity

// JP=2+ f2(2340)
RooRealVar m0_f_2340("m0_f_2340","m0",2.345);
RooRealVar width_f_2340("width_f_2340","width",0.322);
RooRealVar ar_H0_f_2340("ar_H0_f_2340","",0.,-100,100);
RooRealVar ai_H0_f_2340("ai_H0_f_2340","",0.,-100,100);
RooRealVar ar_H1_f_2340("ar_H1_f_2340","",0.,-100,100);
RooRealVar ai_H1_f_2340("ai_H1_f_2340","",0.,-100,100);  
RooArgList* X_f2_2340 = new RooArgList(ar_H0_f_2340,ai_H0_f_2340,
      			      ar_H1_f_2340,ai_H1_f_2340,"X_f2_2340");
X_f2_2340->add(m0_f_2340);
X_f2_2340->add(width_f_2340);
X_f2_2340->add(RooRealConstant::value(typeKM));
X_f2_2340->add(RooRealConstant::value(2));//spin
X_f2_2340->add(RooRealConstant::value(1)); //parity

// JP=2- eta2(1870)
RooRealVar m0_eta2_1870("m0_eta2_1870","m0",2.157);
RooRealVar width_eta2_1870("width_eta2_1870","width",0.152);
RooRealVar ar_H0_eta2_1870("ar_H0_eta2_1870","",0.,-100,100);
RooRealVar ai_H0_eta2_1870("ai_H0_eta2_1870","",0.,-100,100);
RooRealVar ar_H1_eta2_1870("ar_H1_eta2_1870","",0.,-100,100);
RooRealVar ai_H1_eta2_1870("ai_H1_eta2_1870","",0.,-100,100);  
RooArgList* X_eta2_1870 = new RooArgList(ar_H0_eta2_1870,ai_H0_eta2_1870,
      			      ar_H1_eta2_1870,ai_H1_eta2_1870,"X_eta2_1870");
X_eta2_1870->add(m0_eta2_1870);
X_eta2_1870->add(width_eta2_1870);
X_eta2_1870->add(RooRealConstant::value(typeKM));
X_eta2_1870->add(RooRealConstant::value(2));//spin
X_eta2_1870->add(RooRealConstant::value(1)); //parity

// JP=2- pi2(1880)
RooRealVar m0_pi2_1880("m0_pi2_1880","m0",1.874);
RooRealVar width_pi2_1880("width_pi2_1880","width",0.237);
RooRealVar ar_H0_pi2_1880("ar_H0_pi2_1880","",0.,-100,100);
RooRealVar ai_H0_pi2_1880("ai_H0_pi2_1880","",0.,-100,100);
RooRealVar ar_H1_pi2_1880("ar_H1_pi2_1880","",0.,-100,100);
RooRealVar ai_H1_pi2_1880("ai_H1_pi2_1880","",0.,-100,100);  
RooArgList* X_pi2_1880 = new RooArgList(ar_H0_pi2_1880,ai_H0_pi2_1880,
      			      ar_H1_pi2_1880,ai_H1_pi2_1880,"X_pi2_1880");
X_pi2_1880->add(m0_pi2_1880);
X_pi2_1880->add(width_pi2_1880);
X_pi2_1880->add(RooRealConstant::value(typeKM));
X_pi2_1880->add(RooRealConstant::value(2));//spin
X_pi2_1880->add(RooRealConstant::value(-1)); //parity

// JP=2- pi2(2005)
RooRealVar m0_pi2_2005("m0_pi2_2005","m0",1.963);
RooRealVar width_pi2_2005("width_pi2_2005","width",0.370);
RooRealVar ar_H0_pi2_2005("ar_H0_pi2_2005","",0.,-100,100);
RooRealVar ai_H0_pi2_2005("ai_H0_pi2_2005","",0.,-100,100);
RooRealVar ar_H1_pi2_2005("ar_H1_pi2_2005","",0.,-100,100);
RooRealVar ai_H1_pi2_2005("ai_H1_pi2_2005","",0.,-100,100);  
RooArgList* X_pi2_2005 = new RooArgList(ar_H0_pi2_2005,ai_H0_pi2_2005,
      			      ar_H1_pi2_2005,ai_H1_pi2_2005,"X_pi2_2005");
X_pi2_2005->add(m0_pi2_2005);
X_pi2_2005->add(width_pi2_2005);
X_pi2_2005->add(RooRealConstant::value(typeKM));
X_pi2_2005->add(RooRealConstant::value(2));//spin
X_pi2_2005->add(RooRealConstant::value(-1)); //parity

// JP=2- pi2(2100)
RooRealVar m0_pi2_2100("m0_pi2_2100","m0",2.090);
RooRealVar width_pi2_2100("width_pi2_2100","width",0.625);
RooRealVar ar_H0_pi2_2100("ar_H0_pi2_2100","",0.,-100,100);
RooRealVar ai_H0_pi2_2100("ai_H0_pi2_2100","",0.,-100,100);
RooRealVar ar_H1_pi2_2100("ar_H1_pi2_2100","",0.,-100,100);
RooRealVar ai_H1_pi2_2100("ai_H1_pi2_2100","",0.,-100,100);  
RooArgList* X_pi2_2100 = new RooArgList(ar_H0_pi2_2100,ai_H0_pi2_2100,
      			      ar_H1_pi2_2100,ai_H1_pi2_2100,"X_pi2_2100");
X_pi2_2100->add(m0_pi2_2100);
X_pi2_2100->add(width_pi2_2100);
X_pi2_2100->add(RooRealConstant::value(typeKM));
X_pi2_2100->add(RooRealConstant::value(2));//spin
X_pi2_2100->add(RooRealConstant::value(-1)); //parity



/////////////////////////////////////////
// D0p,Y-chain 
/////////////////////////////////////////
TList *list = new TList();


/////////////////////////////
//***** Non-resonance *****//
/////////////////////////////

// JP=0.5- 
RooRealVar m0_Y_NR1m("m0_Y_NR1m","m0",-1);
//RooRealVar width_Y_NR1m("width_Y_NR1m","width_Y_NR1m",0.,-10,10);
RooRealVar width_Y_NR1m("width_Y_NR1m","width_Y_NR1m",0.);
RooRealVar ar_H0_Y_NR1m("ar_H0_Y_NR1m","",0,-10,10);
RooRealVar ai_H0_Y_NR1m("ai_H0_Y_NR1m","",0.,-10,10);
RooRealVar ar_H1_Y_NR1m("ar_H1_Y_NR1m","",0.);
RooRealVar ai_H1_Y_NR1m("ai_H1_Y_NR1m","",0.);  
RooArgList* Y_NR1m = new RooArgList(ar_H0_Y_NR1m,ai_H0_Y_NR1m,
      			      ar_H1_Y_NR1m,ai_H1_Y_NR1m,"Y_NR1m");
Y_NR1m->add(m0_Y_NR1m);
Y_NR1m->add(width_Y_NR1m);
Y_NR1m->add(RooRealConstant::value(typeKM));
Y_NR1m->add(RooRealConstant::value(1));//spin
Y_NR1m->add(RooRealConstant::value(-1)); //parity

// JP=0.5- 
//RooRealVar m0_Y_NR1m("m0_Y_NR1m","m0",-1);
//RooRealVar width_Y_NR1m("width_Y_NR1m","width_Y_NR1m",0.,-10,10);
//RooRealVar a0_Y_NR1m("a0_Y_NR1m","a0_Y_NR1m",0.,-1000,1000);
//RooRealVar a1_Y_NR1m("a1_Y_NR1m","a1_Y_NR1m",0.,-10,10);
//RooRealVar a2_Y_NR1m("a2_Y_NR1m","a2_Y_NR1m",0.,-10,10);
//RooRealVar ar_H0_Y_NR1m("ar_H0_Y_NR1m","",0,-10,10);
//RooRealVar ai_H0_Y_NR1m("ai_H0_Y_NR1m","",0.,-10,10);
//RooRealVar ar_H1_Y_NR1m("ar_H1_Y_NR1m","",0.);
//RooRealVar ai_H1_Y_NR1m("ai_H1_Y_NR1m","",0.);  
//RooArgList* Y_NR1m = new RooArgList(ar_H0_Y_NR1m,ai_H0_Y_NR1m,
//      			      ar_H1_Y_NR1m,ai_H1_Y_NR1m,"Y_NR1m");
//Y_NR1m->add(m0_Y_NR1m);
//Y_NR1m->add(width_Y_NR1m);
//Y_NR1m->add(RooRealConstant::value(typePoly));
//Y_NR1m->add(RooRealConstant::value(1));//spin
//Y_NR1m->add(RooRealConstant::value(-1)); //parity
//Y_NR1m->add(a0_Y_NR1m);
//Y_NR1m->add(a1_Y_NR1m);
//Y_NR1m->add(a2_Y_NR1m);

// JP=0.5+ 
RooRealVar m0_Y_NR1p("m0_Y_NR1p","m0",-1);
RooRealVar width_Y_NR1p("width_Y_NR1p","width_Y_NR1p",0.,-10,10);
//RooRealVar width_Y_NR1p("width_Y_NR1p","width_Y_NR1p",0.);
RooRealVar ar_H0_Y_NR1p("ar_H0_Y_NR1p","",0,-10,10);
RooRealVar ai_H0_Y_NR1p("ai_H0_Y_NR1p","",0.,-10,10);
RooRealVar ar_H1_Y_NR1p("ar_H1_Y_NR1p","",0.);
RooRealVar ai_H1_Y_NR1p("ai_H1_Y_NR1p","",0.);  
RooArgList* Y_NR1p = new RooArgList(ar_H0_Y_NR1p,ai_H0_Y_NR1p,
      			      ar_H1_Y_NR1p,ai_H1_Y_NR1p,"Y_NR1p");
Y_NR1p->add(m0_Y_NR1p);
Y_NR1p->add(width_Y_NR1p);
Y_NR1p->add(RooRealConstant::value(typeKM));
Y_NR1p->add(RooRealConstant::value(1));//spin
Y_NR1p->add(RooRealConstant::value(1)); //parity

// JP=1.5- 
RooRealVar m0_Y_NR3m("m0_Y_NR3m","m0",-1);
RooRealVar width_Y_NR3m("width_Y_NR3m","width_Y_NR3m",0.,-10,10);
RooRealVar ar_H0_Y_NR3m("ar_H0_Y_NR3m","",0,-10,10);
RooRealVar ai_H0_Y_NR3m("ai_H0_Y_NR3m","",0.,-10,10);
RooRealVar ar_H1_Y_NR3m("ar_H1_Y_NR3m","",0.);
RooRealVar ai_H1_Y_NR3m("ai_H1_Y_NR3m","",0.);  
RooArgList* Y_NR3m = new RooArgList(ar_H0_Y_NR3m,ai_H0_Y_NR3m,
      			      ar_H1_Y_NR3m,ai_H1_Y_NR3m,"Y_NR3m");
Y_NR3m->add(m0_Y_NR3m);
Y_NR3m->add(width_Y_NR3m);
Y_NR3m->add(RooRealConstant::value(typeKM));
Y_NR3m->add(RooRealConstant::value(3));//spin
Y_NR3m->add(RooRealConstant::value(-1)); //parity

// JP=1.5+ 
RooRealVar m0_Y_NR3p("m0_Y_NR3p","m0",-1);
RooRealVar width_Y_NR3p("width_Y_NR3p","width_Y_NR3p",0.,-10,10);
RooRealVar ar_H0_Y_NR3p("ar_H0_Y_NR3p","",0,-10,10);
RooRealVar ai_H0_Y_NR3p("ai_H0_Y_NR3p","",0.,-10,10);
RooRealVar ar_H1_Y_NR3p("ar_H1_Y_NR3p","",0.);
RooRealVar ai_H1_Y_NR3p("ai_H1_Y_NR3p","",0.);  
RooArgList* Y_NR3p = new RooArgList(ar_H0_Y_NR3p,ai_H0_Y_NR3p,
      			      ar_H1_Y_NR3p,ai_H1_Y_NR3p,"Y_NR3p");
Y_NR3p->add(m0_Y_NR3p);
Y_NR3p->add(width_Y_NR3p);
Y_NR3p->add(RooRealConstant::value(typeKM));
Y_NR3p->add(RooRealConstant::value(3));//spin
Y_NR3p->add(RooRealConstant::value(1)); //parity





////////////////////////////////////
//***** Lambda_c Spectrum    *****//
////////////////////////////////////

/*
 * The spectrum is \lambda-mode for sigle charmed baryon.
 * S-wave: singlet.  JP = (1/2)+
 * P-wave: doublet. JP = (1/2)+, (3/2)+
 * D-wave: doublet. JP = (3/2)+, (5/2)+
*/


double wid_ini = 0.150;
double wid_min = 0.001;
double wid_max = 0.350;
//************//
//** S-wave **//
//************//

// 1S Lc(2286), under threshold.


// 2S Lc(2765), JP = (1/2)+.
// Could be Sc(2765).
// Mass and width is statiscally significant.
// Quantum numbers is not known for sure.
RooRealVar m0_2765("m0_2765","m0",2.7666);
//RooRealVar m0_2765("m0_2765","m0",2.7666,2,3.0);
RooRealVar width_2765("width_2765","width",0.5);
//RooRealVar width_2765("width_2765","width",wid_ini,wid_min,2.0);
RooRealVar ar_H0_2765("ar_H0_2765","",0.,-10,10);
RooRealVar ai_H0_2765("ai_H0_2765","",0.,-10,10);
RooRealVar ar_H1_2765("ar_H1_2765","",0.,-10,10);
RooRealVar ai_H1_2765("ai_H1_2765","",0.,-10,10);  
RooArgList* Y_L_2765 = new RooArgList(ar_H0_2765,ai_H0_2765,
      			      ar_H1_2765,ai_H1_2765,"Y_L_2765");
Y_L_2765->add(m0_2765);
Y_L_2765->add(width_2765);
//RooRealVar T2765("T2765","type of 2765",typeR);
RooRealVar T2765("T2765","type of 2765",typeFlatte);
RooRealVar J2765("J2765","",1);//ini:1+
RooRealVar P2765("P2765","",1);
Y_L_2765->add(T2765);
Y_L_2765->add(J2765);
Y_L_2765->add(P2765);


//RooRealVar m0_2765("m0_2765","m0",2.85,2.82,3.0);
////RooRealVar width_2765("width_2765","width",0.05,0.001,0.300);
//RooRealVar width_2765("width_2765","width",wid_ini,wid_min,0.45);
//RooRealVar ar_H0_2765("ar_H0_2765","",0.,-10,10);
//RooRealVar ai_H0_2765("ai_H0_2765","",0.,-10,10);
//RooRealVar ar_H1_2765("ar_H1_2765","",0.,-10,10);
//RooRealVar ai_H1_2765("ai_H1_2765","",0.,-10,10);  
//RooArgList* Y_L_2765 = new RooArgList(ar_H0_2765,ai_H0_2765,
//      			      ar_H1_2765,ai_H1_2765,"Y_L_2765");
//Y_L_2765->add(m0_2765);
//Y_L_2765->add(width_2765);
//RooRealVar T2765("T2765","type of 2765",typeR);
//RooRealVar J2765("J2765","",1);
//RooRealVar P2765("P2765","",1);
//Y_L_2765->add(T2765);
//Y_L_2765->add(J2765);
//Y_L_2765->add(P2765);
//

// 3S
// Predicted Lc(3130), JP = (1/2)+
//RooRealVar m0_3130("m0_3130","m0",3.13,3.03,3.33);
//RooRealVar width_3130("width_3130","width",wid_ini,wid_min,wid_max);
RooRealVar m0_3130("m0_3130","m0",3.265,3.265-0.009*3,3.265+0.009*3);
RooRealVar width_3130("width_3130","width",0.20179,0.20179-0.016*3,0.20179+0.016*3);
RooRealVar ar_H0_3130("ar_H0_3130","",0.,-10,10);
RooRealVar ai_H0_3130("ai_H0_3130","",0.,-10,10);
RooRealVar ar_H1_3130("ar_H1_3130","",0.,-10,10);
RooRealVar ai_H1_3130("ai_H1_3130","",0.,-10,10);  
RooArgList* Y_L_3130 = new RooArgList(ar_H0_3130,ai_H0_3130,
      			      ar_H1_3130,ai_H1_3130,"Y_L_3130");
Y_L_3130->add(m0_3130);
Y_L_3130->add(width_3130);
RooRealVar T3130("T3130","type of 3130",typeR);
RooRealVar J3130("J3130","",3);//ini:3-
RooRealVar P3130("P3130","",-1);
Y_L_3130->add(T3130);
Y_L_3130->add(J3130);
Y_L_3130->add(P3130);

// 3S
RooRealVar m0_2850("m0_2850","m0",2.85,2.8,3);
RooRealVar width_2850("width_2850","width",wid_ini,wid_min,wid_max);
RooRealVar ar_H0_2850("ar_H0_2850","",0.,-10,10);
RooRealVar ai_H0_2850("ai_H0_2850","",0.,-10,10);
RooRealVar ar_H1_2850("ar_H1_2850","",0.,-10,10);
RooRealVar ai_H1_2850("ai_H1_2850","",0.,-10,10);  
RooArgList* Y_L_2850 = new RooArgList(ar_H0_2850,ai_H0_2850,
      			      ar_H1_2850,ai_H1_2850,"Y_L_2850");
Y_L_2850->add(m0_2850);
Y_L_2850->add(width_2850);
RooRealVar T2850("T2850","type of 2850",typeR);
RooRealVar J2850("J2850","",1);
RooRealVar P2850("P2850","",1);
Y_L_2850->add(T2850);
Y_L_2850->add(J2850);
Y_L_2850->add(P2850);



RooRealVar m0_3150("m0_3150","m0",3.3,3.2,3.5);
//RooRealVar m0_3150("m0_3150","m0",3.23);
RooRealVar width_3150("width_3150","width",wid_ini,wid_min,wid_max);
RooRealVar ar_H0_3150("ar_H0_3150","",0.,-10,10);
RooRealVar ai_H0_3150("ai_H0_3150","",0.,-10,10);
RooRealVar ar_H1_3150("ar_H1_3150","",0.,-10,10);
RooRealVar ai_H1_3150("ai_H1_3150","",0.,-10,10);  
RooArgList* Y_L_3150 = new RooArgList(ar_H0_3150,ai_H0_3150,
      			      ar_H1_3150,ai_H1_3150,"Y_L_3150");
Y_L_3150->add(m0_3150);
Y_L_3150->add(width_3150);
RooRealVar T3150("T3150","type of 3150",typeR);
RooRealVar J3150("J3150","",1);
RooRealVar P3150("P3150","",-1);
Y_L_3150->add(T3150);
Y_L_3150->add(J3150);
Y_L_3150->add(P3150);


// 4S
// Predicted Lc(3437), JP = (1/2)+
RooRealVar m0_3437("m0_3437","m0",3.43,3.33,3.53);
RooRealVar width_3437("width_3437","width",wid_ini,wid_min,wid_max);
RooRealVar ar_H0_3437("ar_H0_3437","",0.,-10,10);
RooRealVar ai_H0_3437("ai_H0_3437","",0.,-10,10);
RooRealVar ar_H1_3437("ar_H1_3437","",0.,-10,10);
RooRealVar ai_H1_3437("ai_H1_3437","",0.,-10,10);  
RooArgList* Y_L_3437 = new RooArgList(ar_H0_3437,ai_H0_3437,
      			      ar_H1_3437,ai_H1_3437,"Y_L_3437");
Y_L_3437->add(m0_3437);
Y_L_3437->add(width_3437);
RooRealVar T3437("T3437","type of 3437",typeR);
RooRealVar J3437("J3437","",1);
RooRealVar P3437("P3437","",1);
Y_L_3437->add(T3437);
Y_L_3437->add(J3437);
Y_L_3437->add(P3437);


//************//
//** P-wave **//
//************//

// 1P Lc(2595) & Lc(2625), under threshold

// 2P Lc(2940) JP = (3/2)-
RooRealVar m0_2940("m0_2940","m0",2.9396);//2.9396\p0.0013\m0.0015
RooRealVar width_2940("width_2940","width",0.020);//0.020\p0.006\m0.005
RooRealVar ar_H0_2940("ar_H0_2940","",0.,-10,10);
RooRealVar ai_H0_2940("ai_H0_2940","",0.,-10,10);
RooRealVar ar_H1_2940("ar_H1_2940","",0.,-10,10);
RooRealVar ai_H1_2940("ai_H1_2940","",0.,-10,10);  
RooArgList* Y_L_2940 = new RooArgList(ar_H0_2940,ai_H0_2940,
      			      ar_H1_2940,ai_H1_2940,"Y_L_2940");
Y_L_2940->add(m0_2940);
Y_L_2940->add(width_2940);
RooRealVar T2940("T2940","type of 2940",typeR);
RooRealVar J2940("J2940","",3);
RooRealVar P2940("P2940","",-1);
Y_L_2940->add(T2940);
Y_L_2940->add(J2940);
Y_L_2940->add(P2940);

//RooRealVar m0_2910("m0_2910","m0",2.9138,2.9138-3*0.0067,2.9138+3*0.0067);//2.9138\pm0.0067
RooRealVar m0_2910("m0_2910","m0",2.9138);//2.9138\pm0.0067
//RooRealVar width_2910("width_2910","width",0.052,0,0.052+3*0.022);//0.052\pm0.022
RooRealVar width_2910("width_2910","width",0.052);//0.052\pm0.022
RooRealVar ar_H0_2910("ar_H0_2910","",0.,-10,10);
RooRealVar ai_H0_2910("ai_H0_2910","",0.,-10,10);
RooRealVar ar_H1_2910("ar_H1_2910","",0.,-10,10);
RooRealVar ai_H1_2910("ai_H1_2910","",0.,-10,10);  
RooArgList* Y_L_2910 = new RooArgList(ar_H0_2910,ai_H0_2910,
      			      ar_H1_2910,ai_H1_2910,"Y_L_2910");
Y_L_2910->add(m0_2910);
Y_L_2910->add(width_2910);
RooRealVar T2910("T2910","type of 2910",typeR);
RooRealVar J2910("J2910","",3);
RooRealVar P2910("P2910","",-1);
Y_L_2910->add(T2910);
Y_L_2910->add(J2910);
Y_L_2910->add(P2910);

RooRealVar m0_2800("m0_2800","m0",2.792,2.792-0.005*3,2.792+0.014*3);//2.792\p0.014\m0.005
RooRealVar width_2800("width_2800","width",0.062,0,0.150);//0.062\p0.060\m0.040
RooRealVar ar_H0_2800("ar_H0_2800","",0.,-10,10);
RooRealVar ai_H0_2800("ai_H0_2800","",0.,-10,10);
RooRealVar ar_H1_2800("ar_H1_2800","",0.,-10,10);
RooRealVar ai_H1_2800("ai_H1_2800","",0.,-10,10);  
RooArgList* Y_L_2800 = new RooArgList(ar_H0_2800,ai_H0_2800,
      			      ar_H1_2800,ai_H1_2800,"Y_L_2800");
Y_L_2800->add(m0_2800);
Y_L_2800->add(width_2800);
RooRealVar T2800("T2800","type of 2800",typeR);
RooRealVar J2800("J2800","",5);
RooRealVar P2800("P2800","",-1);
Y_L_2800->add(T2800);
Y_L_2800->add(J2800);
Y_L_2800->add(P2800);
// 2P
// Predicted Lc(3005), JP = (1/2)-
// The doublet of Lc(2940)
//RooRealVar m0_2900("m0_2900","m0",2.85,2.805,2.9);
//RooRealVar width_2900("width_2900","width",wid_ini,wid_min,wid_max);
RooRealVar m0_2900("m0_2900","m0",2.811,2.805,2.811+0.007*3);
RooRealVar width_2900("width_2900","width",0.031,0.031-0.009*3,0.031+0.017*3);
RooRealVar ar_H0_2900("ar_H0_2900","",0.,-10,10);
RooRealVar ai_H0_2900("ai_H0_2900","",0.,-10,10);
RooRealVar ar_H1_2900("ar_H1_2900","",0.,-10,10);
RooRealVar ai_H1_2900("ai_H1_2900","",0.,-10,10);  
RooArgList* Y_L_2900 = new RooArgList(ar_H0_2900,ai_H0_2900,
      			      ar_H1_2900,ai_H1_2900,"Y_L_2900");
Y_L_2900->add(m0_2900);
Y_L_2900->add(width_2900);
RooRealVar T2900("T2900","type of 2900",typeR);
RooRealVar J2900("J2900","",1);//ini:1-
RooRealVar P2900("P2900","",-1);
Y_L_2900->add(T2900);
Y_L_2900->add(J2900);
Y_L_2900->add(P2900);

//************//
//** D-wave **//
//************//

// 1D Lc(2860) JP = (3/2)+
RooRealVar m0_2860("m0_2860","m0",2.8561);//2.856\p0.002\m0.006
RooRealVar width_2860("width_2860","width",0.068);//0.068\p0.012\m0.022
//RooRealVar m0_2860("m0_2860","m0",2.8561,2.805,2.900);//2.856\p0.002\m0.006
//RooRealVar width_2860("width_2860","width",0.068,0,0.4);//0.068\p0.012\m0.022
RooRealVar ar_H0_2860("ar_H0_2860","",0.,-10,10);
RooRealVar ai_H0_2860("ai_H0_2860","",0.,-10,10);
RooRealVar ar_H1_2860("ar_H1_2860","",0.,-10,10);
RooRealVar ai_H1_2860("ai_H1_2860","",0.,-10,10);  
RooArgList* Y_L_2860 = new RooArgList(ar_H0_2860,ai_H0_2860,
      			      ar_H1_2860,ai_H1_2860,"Y_L_2860");
Y_L_2860->add(m0_2860);
Y_L_2860->add(width_2860);
RooRealVar T2860("T2860","type of 2860",typeR);
RooRealVar J2860("J2860","",3);
RooRealVar P2860("P2860","",1);
Y_L_2860->add(T2860);
Y_L_2860->add(J2860);
Y_L_2860->add(P2860);

// 1D Lc(2880) JP = (5/2)+
// doublet of Lc(2860)
RooRealVar m0_2880("m0_2880","m0",2.8816);//2.8816\pm0.0002
RooRealVar width_2880("width_2880","width",0.0056);//0.0056\p0.0008\m0.0006
RooRealVar ar_H0_2880("ar_H0_2880","",0.,-10,10);
RooRealVar ai_H0_2880("ai_H0_2880","",0.,-10,10);
RooRealVar ar_H1_2880("ar_H1_2880","",0.,-10,10);
RooRealVar ai_H1_2880("ai_H1_2880","",0.,-10,10);  
RooArgList* Y_L_2880 = new RooArgList(ar_H0_2880,ai_H0_2880,
      			      ar_H1_2880,ai_H1_2880,"Y_L_2880");
Y_L_2880->add(m0_2880);
Y_L_2880->add(width_2880);
RooRealVar T2880("T2880","type of 2880",typeR);
RooRealVar J2880("J2880","",5);
RooRealVar P2880("P2880","",1);
Y_L_2880->add(T2880);
Y_L_2880->add(J2880);
Y_L_2880->add(P2880);

// 2D 
// Predicted Lc(3189) JP = (3/2)+
//RooRealVar m0_3189("m0_3189","m0",2.85,2.8,3.3);
//RooRealVar width_3189("width_3189","width",wid_ini,wid_min,wid_max);
RooRealVar m0_3189("m0_3189","m0",2.9767,2.9767-0.01*3,2.9767+0.01*3);
RooRealVar width_3189("width_3189","width",0.14191,0.14191-0.01*3,0.14191+0.01*3);
RooRealVar ar_H0_3189("ar_H0_3189","",0.,-10,10);
RooRealVar ai_H0_3189("ai_H0_3189","",0.,-10,10);
RooRealVar ar_H1_3189("ar_H1_3189","",0.,-10,10);
RooRealVar ai_H1_3189("ai_H1_3189","",0.,-10,10);  
RooArgList* Y_L_3189 = new RooArgList(ar_H0_3189,ai_H0_3189,
      			      ar_H1_3189,ai_H1_3189,"Y_L_3189");
Y_L_3189->add(m0_3189);
Y_L_3189->add(width_3189);
RooRealVar T3189("T3189","type of 3189",typeR);
RooRealVar J3189("J3189","",1);//ini:1-
RooRealVar P3189("P3189","",-1);
Y_L_3189->add(T3189);
Y_L_3189->add(J3189);
Y_L_3189->add(P3189);

// 2D 
// Predicted Lc(3209) JP = (5/2)+
//RooRealVar m0_3209("m0_3209","m0",3.309,2.0,3.509);
RooRealVar m0_3209("m0_3209","m0",3.009,2.9,3.509);
RooRealVar width_3209("width_3209","width",wid_ini,wid_min,wid_max);
RooRealVar ar_H0_3209("ar_H0_3209","",0.,-10,10);
RooRealVar ai_H0_3209("ai_H0_3209","",0.,-10,10);
RooRealVar ar_H1_3209("ar_H1_3209","",0.,-10,10);
RooRealVar ai_H1_3209("ai_H1_3209","",0.,-10,10);  
RooArgList* Y_L_3209 = new RooArgList(ar_H0_3209,ai_H0_3209,
      			      ar_H1_3209,ai_H1_3209,"Y_L_3209");
Y_L_3209->add(m0_3209);
Y_L_3209->add(width_3209);
RooRealVar T3209("T3209","type of 3209",typeR);
RooRealVar J3209("J3209","",5);
RooRealVar P3209("P3209","",-1);
Y_L_3209->add(T3209);
Y_L_3209->add(J3209);
Y_L_3209->add(P3209);

// 3D 
// Predicted Lc(3480) JP = (3/2)+
RooRealVar m0_3480("m0_3480","m0",3.480,3.380,3.580);
RooRealVar width_3480("width_3480","width",wid_ini,wid_min,wid_max);
RooRealVar ar_H0_3480("ar_H0_3480","",0.,-10,10);
RooRealVar ai_H0_3480("ai_H0_3480","",0.,-10,10);
RooRealVar ar_H1_3480("ar_H1_3480","",0.,-10,10);
RooRealVar ai_H1_3480("ai_H1_3480","",0.,-10,10);  
RooArgList* Y_L_3480 = new RooArgList(ar_H0_3480,ai_H0_3480,
      			      ar_H1_3480,ai_H1_3480,"Y_L_3480");
Y_L_3480->add(m0_3480);
Y_L_3480->add(width_3480);
RooRealVar T3480("T3480","type of 3480",typeR);
RooRealVar J3480("J3480","",3);
RooRealVar P3480("P3480","",1);
Y_L_3480->add(T3480);
Y_L_3480->add(J3480);
Y_L_3480->add(P3480);

// 3D 
// Predicted Lc(3500) JP = (5/2)+
RooRealVar m0_3500("m0_3500","m0",3.400,3.500,3.600);
RooRealVar width_3500("width_3500","width",wid_ini,wid_min,wid_max);
RooRealVar ar_H0_3500("ar_H0_3500","",0.,-10,10);
RooRealVar ai_H0_3500("ai_H0_3500","",0.,-10,10);
RooRealVar ar_H1_3500("ar_H1_3500","",0.,-10,10);
RooRealVar ai_H1_3500("ai_H1_3500","",0.,-10,10);  
RooArgList* Y_L_3500 = new RooArgList(ar_H0_3500,ai_H0_3500,
      			      ar_H1_3500,ai_H1_3500,"Y_L_3500");
Y_L_3500->add(m0_3500);
Y_L_3500->add(width_3500);
RooRealVar T3500("T3500","type of 3500",typeR);
RooRealVar J3500("J3500","",5);
RooRealVar P3500("P3500","",1);
Y_L_3500->add(T3500);
Y_L_3500->add(J3500);
Y_L_3500->add(P3500);




////////////////////////////////////
//***** Sigma_c Spectrum    *****//
////////////////////////////////////

/*
 * The spectrum is \lambda-mode for sigle charmed baryon.
 * S-wave: doublet. JP = (1/2)+, (3/2)+
 * P-wave: 5-let. JP = (1/2)-(two), (3/2)-(two), (5/2)-
 * D-wave: 6-let. JP = (1/2)+, (3/2)+(two), (5/2)+(two), (7/2)+
*/

//************//
//** S-wave **//
//************//

// 1S Sc(2452), Sc(2517) under thereshold


// 2S 
// Predicted Sc(2901) 1/2+ 
RooRealVar m0_2901("m0_2901","m0",2.85,2.801,3.001);
RooRealVar width_2901("width_2901","width",wid_ini,wid_min,wid_max);
RooRealVar ar_H0_2901("ar_H0_2901","",0.,-10,10);
RooRealVar ai_H0_2901("ai_H0_2901","",0.,-10,10);
RooRealVar ar_H1_2901("ar_H1_2901","",0.,-10,10);
RooRealVar ai_H1_2901("ai_H1_2901","",0.,-10,10);  
RooArgList* Y_S_2901 = new RooArgList(ar_H0_2901,ai_H0_2901,
      			      ar_H1_2901,ai_H1_2901,"Y_S_2901");
Y_S_2901->add(m0_2901);
Y_S_2901->add(width_2901);
RooRealVar T2901("T2901","type of 2901",typeR);
RooRealVar J2901("J2901","",1);
RooRealVar P2901("P2901","",1);
Y_S_2901->add(T2901);
Y_S_2901->add(J2901);
Y_S_2901->add(P2901);

// Predicted Sc(2936) 3/2+
RooRealVar m0_2936("m0_2936","m0",2.936,2.836,3.036);
RooRealVar width_2936("width_2936","width",wid_ini,wid_min,wid_max);
RooRealVar ar_H0_2936("ar_H0_2936","",0.,-10,10);
RooRealVar ai_H0_2936("ai_H0_2936","",0.,-10,10);
RooRealVar ar_H1_2936("ar_H1_2936","",0.,-10,10);
RooRealVar ai_H1_2936("ai_H1_2936","",0.,-10,10);  
RooArgList* Y_S_2936 = new RooArgList(ar_H0_2936,ai_H0_2936,
      			      ar_H1_2936,ai_H1_2936,"Y_S_2936");
Y_S_2936->add(m0_2936);
Y_S_2936->add(width_2936);
RooRealVar T2936("T2936","type of 2936",typeR);
RooRealVar J2936("J2936","",3);
RooRealVar P2936("P2936","",1);
Y_S_2936->add(T2936);
Y_S_2936->add(J2936);
Y_S_2936->add(P2936);

// 3S
// Predicted Sc(3271) 1/2+
RooRealVar m0_3271("m0_3271","m0",3.271,3.171,3.371);
RooRealVar width_3271("width_3271","width",wid_ini,wid_min,wid_max);
RooRealVar ar_H0_3271("ar_H0_3271","",0.,-10,10);
RooRealVar ai_H0_3271("ai_H0_3271","",0.,-10,10);
RooRealVar ar_H1_3271("ar_H1_3271","",0.,-10,10);
RooRealVar ai_H1_3271("ai_H1_3271","",0.,-10,10);  
RooArgList* Y_S_3271 = new RooArgList(ar_H0_3271,ai_H0_3271,
      			      ar_H1_3271,ai_H1_3271,"Y_S_3271");
Y_S_3271->add(m0_3271);
Y_S_3271->add(width_3271);
RooRealVar T3271("T3271","type of 3271",typeR);
RooRealVar J3271("J3271","",1);
RooRealVar P3271("P3271","",1);
Y_S_3271->add(T3271);
Y_S_3271->add(J3271);
Y_S_3271->add(P3271);

// Predicted Sc(3293) 3/2+
RooRealVar m0_3293("m0_3293","m0",3.293,3.193,3.493);
RooRealVar width_3293("width_3293","width",wid_ini,wid_min,wid_max);
RooRealVar ar_H0_3293("ar_H0_3293","",0.,-10,10);
RooRealVar ai_H0_3293("ai_H0_3293","",0.,-10,10);
RooRealVar ar_H1_3293("ar_H1_3293","",0.,-10,10);
RooRealVar ai_H1_3293("ai_H1_3293","",0.,-10,10);  
RooArgList* Y_S_3293 = new RooArgList(ar_H0_3293,ai_H0_3293,
      			      ar_H1_3293,ai_H1_3293,"Y_S_3293");
Y_S_3293->add(m0_3293);
Y_S_3293->add(width_3293);
RooRealVar T3293("T3293","type of 3293",typeR);
RooRealVar J3293("J3293","",3);
RooRealVar P3293("P3293","",1);
Y_S_3293->add(T3293);
Y_S_3293->add(J3293);
Y_S_3293->add(P3293);

//************//
//** P-wave **//
//************//

// 1P JP = (1/2)-
RooRealVar m0_1P_1("m0_1P_1","m0",2.8,2.5,3.0);
RooRealVar width_1P_1("width_1P_1","width",wid_ini,wid_min,wid_max);
RooRealVar ar_H0_1P_1("ar_H0_1P_1","",0.,-10,10);
RooRealVar ai_H0_1P_1("ai_H0_1P_1","",0.,-10,10);
RooRealVar ar_H1_1P_1("ar_H1_1P_1","",0.,-10,10);
RooRealVar ai_H1_1P_1("ai_H1_1P_1","",0.,-10,10);  
RooArgList* Y_S_1P_1 = new RooArgList(ar_H0_1P_1,ai_H0_1P_1,
      			      ar_H1_1P_1,ai_H1_1P_1,"Y_S_1P_1");
Y_S_1P_1->add(m0_1P_1);
Y_S_1P_1->add(width_1P_1);
RooRealVar T1P_1("T1P_1","type of 1P_1",typeR);
RooRealVar J1P_1("J1P_1","",1);
RooRealVar P1P_1("P1P_1","",-1);
Y_S_1P_1->add(T1P_1);
Y_S_1P_1->add(J1P_1);
Y_S_1P_1->add(P1P_1);



//************//
//** D-wave **//
//************//

// 2D JP = (1/2)+
RooRealVar m0_2D_1("m0_2D_1","m0",3.262,3.2,3.5);
RooRealVar width_2D_1("width_2D_1","width",0.10);
RooRealVar ar_H0_2D_1("ar_H0_2D_1","",0.,-10,10);
RooRealVar ai_H0_2D_1("ai_H0_2D_1","",0.,-10,10);
RooRealVar ar_H1_2D_1("ar_H1_2D_1","",0.,-10,10);
RooRealVar ai_H1_2D_1("ai_H1_2D_1","",0.,-10,10);  
RooArgList* Y_S_2D_1 = new RooArgList(ar_H0_2D_1,ai_H0_2D_1,
      			      ar_H1_2D_1,ai_H1_2D_1,"Y_S_2D_1");
Y_S_2D_1->add(m0_2D_1);
Y_S_2D_1->add(width_2D_1);
RooRealVar T2D_1("T2D_1","type of 2D_1",typeR);
RooRealVar J2D_1("J2D_1","",1);
RooRealVar P2D_1("P2D_1","",1);
Y_S_2D_1->add(T2D_1);
Y_S_2D_1->add(J2D_1);
Y_S_2D_1->add(P2D_1);

// 2D JP = (3/2)+
RooRealVar m0_2D_3("m0_2D_3","m0",3.262,3.2,3.5);
RooRealVar width_2D_3("width_2D_3","width",0.10);
RooRealVar ar_H0_2D_3("ar_H0_2D_3","",0.,-10,10);
RooRealVar ai_H0_2D_3("ai_H0_2D_3","",0.,-10,10);
RooRealVar ar_H1_2D_3("ar_H1_2D_3","",0.,-10,10);
RooRealVar ai_H1_2D_3("ai_H1_2D_3","",0.,-10,10);  
RooArgList* Y_S_2D_3 = new RooArgList(ar_H0_2D_3,ai_H0_2D_3,
      			      ar_H1_2D_3,ai_H1_2D_3,"Y_S_2D_3");
Y_S_2D_3->add(m0_2D_3);
Y_S_2D_3->add(width_2D_3);
RooRealVar T2D_3("T2D_3","type of 2D_3",typeR);
RooRealVar J2D_3("J2D_3","",3);
RooRealVar P2D_3("P2D_3","",1);
Y_S_2D_3->add(T2D_3);
Y_S_2D_3->add(J2D_3);
Y_S_2D_3->add(P2D_3);

// 2D JP = (5/2)+
RooRealVar m0_2D_5("m0_2D_5","m0",3.262,3.2,3.5);
RooRealVar width_2D_5("width_2D_5","width",0.10);
RooRealVar ar_H0_2D_5("ar_H0_2D_5","",0.,-10,10);
RooRealVar ai_H0_2D_5("ai_H0_2D_5","",0.,-10,10);
RooRealVar ar_H1_2D_5("ar_H1_2D_5","",0.,-10,10);
RooRealVar ai_H1_2D_5("ai_H1_2D_5","",0.,-10,10);  
RooArgList* Y_S_2D_5 = new RooArgList(ar_H0_2D_5,ai_H0_2D_5,
      			      ar_H1_2D_5,ai_H1_2D_5,"Y_S_2D_5");
Y_S_2D_5->add(m0_2D_5);
Y_S_2D_5->add(width_2D_5);
RooRealVar T2D_5("T2D_5","type of 2D_5",typeR);
RooRealVar J2D_5("J2D_5","",5);
RooRealVar P2D_5("P2D_5","",1);
Y_S_2D_5->add(T2D_5);
Y_S_2D_5->add(J2D_5);
Y_S_2D_5->add(P2D_5);

// 2D JP = (7/2)+
RooRealVar m0_2D_7("m0_2D_7","m0",3.262,3.2,3.5);
RooRealVar width_2D_7("width_2D_7","width",0.10);
RooRealVar ar_H0_2D_7("ar_H0_2D_7","",0.,-10,10);
RooRealVar ai_H0_2D_7("ai_H0_2D_7","",0.,-10,10);
RooRealVar ar_H1_2D_7("ar_H1_2D_7","",0.,-10,10);
RooRealVar ai_H1_2D_7("ai_H1_2D_7","",0.,-10,10);  
RooArgList* Y_S_2D_7 = new RooArgList(ar_H0_2D_7,ai_H0_2D_7,
      			      ar_H1_2D_7,ai_H1_2D_7,"Y_S_2D_7");
Y_S_2D_7->add(m0_2D_7);
Y_S_2D_7->add(width_2D_7);
RooRealVar T2D_7("T2D_7","type of 2D_7",typeR);
RooRealVar J2D_7("J2D_7","",1);
RooRealVar P2D_7("P2D_7","",1);
Y_S_2D_7->add(T2D_7);
Y_S_2D_7->add(J2D_7);
Y_S_2D_7->add(P2D_7);


//// 1+
//RooRealVar m0_3262("m0_3262","m0",3.262,3.2,3.5);
//RooRealVar width_3262("width_3262","width",0.10);
//RooRealVar ar_H0_3262("ar_H0_3262","",0.,-10,10);
//RooRealVar ai_H0_3262("ai_H0_3262","",0.,-10,10);
//RooRealVar ar_H1_3262("ar_H1_3262","",0.,-10,10);
//RooRealVar ai_H1_3262("ai_H1_3262","",0.,-10,10);  
//RooArgList* Y_S_3262 = new RooArgList(ar_H0_3262,ai_H0_3262,
//      			      ar_H1_3262,ai_H1_3262,"Y_S_3262");
//Y_S_3262->add(m0_3262);
//Y_S_3262->add(width_3262);
//RooRealVar T3262("T3262","type of 3262",typeR);
//RooRealVar J3262("J3262","",3);
//RooRealVar P3262("P3262","",1);
//Y_S_3262->add(T3262);
//Y_S_3262->add(J3262);
//Y_S_3262->add(P3262);
//
//RooRealVar m0_3268("m0_3268","m0",3.268,3.2,3.5);
//RooRealVar width_3268("width_3268","width",0.02);
//RooRealVar ar_H0_3268("ar_H0_3268","",0.,-10,10);
//RooRealVar ai_H0_3268("ai_H0_3268","",0.,-10,10);
//RooRealVar ar_H1_3268("ar_H1_3268","",0.,-10,10);
//RooRealVar ai_H1_3268("ai_H1_3268","",0.,-10,10);  
//
//RooArgList* Y_S_3268 = new RooArgList(ar_H0_3268,ai_H0_3268,
//      			      ar_H1_3268,ai_H1_3268,"Y_S_3268");
//Y_S_3268->add(m0_3268);
//Y_S_3268->add(width_3268);
//RooRealVar T3268("T3268","type of 3268",typeR);
//RooRealVar J3268("J3268","",5);
//RooRealVar P3268("P3268","",1);
//Y_S_3268->add(T3268);
//Y_S_3268->add(J3268);
//Y_S_3268->add(P3268);

// 1+
RooRealVar m0_3700("m0_3700","m0",3.6);
RooRealVar width_3700("width_3700","width",0.20);
RooRealVar ar_H0_3700("ar_H0_3700","",0.,-10,10);
RooRealVar ai_H0_3700("ai_H0_3700","",0.,-10,10);
RooRealVar ar_H1_3700("ar_H1_3700","",0.,-10,10);
RooRealVar ai_H1_3700("ai_H1_3700","",0.,-10,10);  
RooArgList* Y_S_3700 = new RooArgList(ar_H0_3700,ai_H0_3700,
      			      ar_H1_3700,ai_H1_3700,"Y_S_3700");
Y_S_3700->add(m0_3700);
Y_S_3700->add(width_3700);
RooRealVar T3700("T3700","type of 3700",typeR);
RooRealVar J3700("J3700","",3);
RooRealVar P3700("P3700","",1);
Y_S_3700->add(T3700);
Y_S_3700->add(J3700);
Y_S_3700->add(P3700);

// 1+
RooRealVar m0_4000("m0_4000","m0",4);
RooRealVar width_4000("width_4000","width",0.20);
RooRealVar ar_H0_4000("ar_H0_4000","",0.,-10,10);
RooRealVar ai_H0_4000("ai_H0_4000","",0.,-10,10);
RooRealVar ar_H1_4000("ar_H1_4000","",0.,-10,10);
RooRealVar ai_H1_4000("ai_H1_4000","",0.,-10,10);  

RooArgList* Y_S_4000 = new RooArgList(ar_H0_4000,ai_H0_4000,
      			      ar_H1_4000,ai_H1_4000,"Y_S_4000");
Y_S_4000->add(m0_4000);
Y_S_4000->add(width_4000);
RooRealVar T4000("T4000","type of 4000",typeR);
RooRealVar J4000("J4000","",3);
RooRealVar P4000("P4000","",1);
Y_S_4000->add(T4000);
Y_S_4000->add(J4000);
Y_S_4000->add(P4000);


//************//
//** Spline **//
//************//
// JP=0+, f0(2100)
static int Nsp=30;
char Wname[20];
RooRealVar *SMass[Nsp], *SRAm[Nsp], *SIAm[Nsp];
RooArgList *FWave = new RooArgList("FWave");
double xm[Nsp], sr0[Nsp], sp0[Nsp];
std::ifstream inswav;
inswav.open("/disk401/lhcb/qinning/b2dpp_farm10/qnFit/splinedat/toy_X.dat");
int iswav=0;
while(1) {
	inswav >> xm[iswav] >> sr0[iswav] >> sp0[iswav];
	if(!inswav.good()) break;
	std::cout << xm[iswav] << " " << sr0[iswav] << " " << sp0[iswav] << std::endl;
	iswav++;
} 
inswav.close();
int Npoint = iswav;
std::cout << "point of FWave " << Npoint << std::endl;
if(Npoint!=NUMSP) {
	std::cout <<  "SPline number is wrong " << std::endl;
	assert(0);
}
//  double sr0[Nsp],  = {};
//  RooDalitzRAmlitude dz;
for(int i=0; i<Npoint; ++i) {
	double fac(1.0);
	if(i>0) {
		fac = 1.0;
		std::cout << fac << std::endl;
	}
	sprintf(Wname,"SR_%02d",i);
	SRAm[i] = new RooRealVar(Wname,Wname,sr0[i]*fac);
	SRAm[i]->setConstant(0);
	FWave->add(*SRAm[i]);
	sprintf(Wname,"SI_%02d",i);
	SIAm[i] = new RooRealVar(Wname,Wname,sp0[i]*fac);
	SIAm[i]->setConstant(0);
	FWave->add(*SIAm[i]);
	sprintf(Wname,"m_%02d",i);
	SMass[i] = new RooRealVar(Wname,Wname,xm[i]);
	FWave->add(*SMass[i]);    
}
SRAm[3]->setConstant(1);
SIAm[3]->setConstant(1);  

RooRealVar m0_Fwave("m0_Fwave","m0",2.086);
RooRealVar width_Fwave("width_Fwave","width",0.286);
RooRealVar ar_H0_Fwave("ar_H0_Fwave","",1.,-10,10);
RooRealVar ai_H0_Fwave("ai_H0_Fwave","",0.,-10,10);
RooRealVar ar_H1_Fwave("ar_H1_Fwave","",0.);
RooRealVar ai_H1_Fwave("ai_H1_Fwave","",0.);  
RooArgList* X_Fwave = new RooArgList(ar_H0_Fwave,ai_H0_Fwave,
      			      ar_H1_Fwave,ai_H1_Fwave,"X_Fwave");
  X_Fwave->add(*FWave);
X_Fwave->add(m0_Fwave);
X_Fwave->add(width_Fwave);
X_Fwave->add(RooRealConstant::value(MODELINDEPENDENT));
X_Fwave->add(RooRealConstant::value(0));//spin
X_Fwave->add(RooRealConstant::value(1)); //parity



char Wname_Y1m[20];
RooRealVar *SMass_Y1m[Nsp], *SRAm_Y1m[Nsp], *SIAm_Y1m[Nsp];
RooArgList *FWave_Y1m = new RooArgList("FWave_Y1m");
double xm_Y1m[Nsp], sr0_Y1m[Nsp], sp0_Y1m[Nsp];
std::ifstream inswav_Y1m;
inswav_Y1m.open("/disk401/lhcb/qinning/b2dpp_farm10/qnFit/splinedat/toy_Y1m.dat");
int iswav_Y1m=0;
while(1) {
	inswav_Y1m >> xm_Y1m[iswav_Y1m] >> sr0_Y1m[iswav_Y1m] >> sp0_Y1m[iswav_Y1m];
	if(!inswav_Y1m.good()) break;
	std::cout << xm_Y1m[iswav_Y1m] << " " << sr0_Y1m[iswav_Y1m] << " " << sp0_Y1m[iswav_Y1m] << std::endl;
	iswav_Y1m++;
} 
inswav_Y1m.close();
int Npoint_Y1m = iswav_Y1m;
std::cout << "point of Y spline Wave " << Npoint << std::endl;
if(Npoint_Y1m!=NUMSPY) {
	std::cout <<  "SPline number is wrong " << std::endl;
	assert(0);
}
for(int i=0; i<Npoint_Y1m; ++i) {
	double fac(1.0);
	if(i>0) {
		fac = 1.0;
		std::cout << fac << std::endl;
	}
	sprintf(Wname_Y1m,"SR_Y1m_%02d",i);
	SRAm_Y1m[i] = new RooRealVar(Wname_Y1m,Wname_Y1m,sr0_Y1m[i]*fac);
	SRAm_Y1m[i]->setConstant(0);
	FWave_Y1m->add(*SRAm_Y1m[i]);
	sprintf(Wname_Y1m,"SI_Y1m_%02d",i);
	SIAm_Y1m[i] = new RooRealVar(Wname_Y1m,Wname_Y1m,sp0_Y1m[i]*fac);
	SIAm_Y1m[i]->setConstant(0);
	FWave_Y1m->add(*SIAm_Y1m[i]);
	sprintf(Wname_Y1m,"m_Y1m_%02d",i);
	SMass_Y1m[i] = new RooRealVar(Wname_Y1m,Wname_Y1m,xm_Y1m[i]);
	FWave_Y1m->add(*SMass_Y1m[i]);    
}
//SRAm_Y1m[3]->setConstant(1);
//SIAm_Y1m[3]->setConstant(1);  

RooRealVar m0_Y1mwave("m0_Y1mwave","m0",2.900);
RooRealVar width_Y1mwave("width_Y1mwave","width",0.286);
RooRealVar ar_H0_Y1mwave("ar_H0_Y1mwave","",1.,-10,10);
RooRealVar ai_H0_Y1mwave("ai_H0_Y1mwave","",0.,-10,10);
RooRealVar ar_H1_Y1mwave("ar_H1_Y1mwave","",0.);
RooRealVar ai_H1_Y1mwave("ai_H1_Y1mwave","",0.);  
RooArgList* Y_Y1mwave = new RooArgList(ar_H0_Y1mwave,ai_H0_Y1mwave,
      			      ar_H1_Y1mwave,ai_H1_Y1mwave,"Y_Y1mwave");
Y_Y1mwave->add(*FWave_Y1m);
Y_Y1mwave->add(m0_Y1mwave);
Y_Y1mwave->add(width_Y1mwave);
Y_Y1mwave->add(RooRealConstant::value(MODELINDEPENDENT));
Y_Y1mwave->add(RooRealConstant::value(1));//spin
Y_Y1mwave->add(RooRealConstant::value(-1)); //parity


char Wname_Y1p[20];
RooRealVar *SMass_Y1p[Nsp], *SRAm_Y1p[Nsp], *SIAm_Y1p[Nsp];
RooArgList *FWave_Y1p = new RooArgList("FWave_Y1p");
double xm_Y1p[Nsp], sr0_Y1p[Nsp], sp0_Y1p[Nsp];
std::ifstream inswav_Y1p;
inswav_Y1p.open("/disk401/lhcb/qinning/b2dpp_farm10/qnFit/splinedat/toy_Y1p.dat");
int iswav_Y1p=0;
while(1) {
	inswav_Y1p >> xm_Y1p[iswav_Y1p] >> sr0_Y1p[iswav_Y1p] >> sp0_Y1p[iswav_Y1p];
	if(!inswav_Y1p.good()) break;
	std::cout << xm_Y1p[iswav_Y1p] << " " << sr0_Y1p[iswav_Y1p] << " " << sp0_Y1p[iswav_Y1p] << std::endl;
	iswav_Y1p++;
} 
inswav_Y1p.close();
int Npoint_Y1p = iswav_Y1p;
std::cout << "point of Y spline Wave " << Npoint << std::endl;
if(Npoint_Y1p!=NUMSPY) {
	std::cout <<  "SPline number is wrong " << std::endl;
	assert(0);
}
for(int i=0; i<Npoint_Y1p; ++i) {
	double fac(1.0);
	if(i>0) {
		fac = 1.0;
		std::cout << fac << std::endl;
	}
	sprintf(Wname_Y1p,"SR_Y1p_%02d",i);
	SRAm_Y1p[i] = new RooRealVar(Wname_Y1p,Wname_Y1p,sr0_Y1p[i]*fac);
	SRAm_Y1p[i]->setConstant(0);
	FWave_Y1p->add(*SRAm_Y1p[i]);
	sprintf(Wname_Y1p,"SI_Y1p_%02d",i);
	SIAm_Y1p[i] = new RooRealVar(Wname_Y1p,Wname_Y1p,sp0_Y1p[i]*fac);
	SIAm_Y1p[i]->setConstant(0);
	FWave_Y1p->add(*SIAm_Y1p[i]);
	sprintf(Wname_Y1p,"m_Y1p_%02d",i);
	SMass_Y1p[i] = new RooRealVar(Wname_Y1p,Wname_Y1p,xm_Y1p[i]);
	FWave_Y1p->add(*SMass_Y1p[i]);    
}
//SRAm_Y1p[3]->setConstant(1);
//SIAm_Y1p[3]->setConstant(1);  
for(int i=0; i<Npoint_Y1p; ++i) {
	if(xm_Y1p[i]>3.15){
		SRAm_Y1p[i]->setConstant(1);
		SIAm_Y1p[i]->setConstant(1);
	}
}

RooRealVar m0_Y1pwave("m0_Y1pwave","m0",2.900);
RooRealVar width_Y1pwave("width_Y1pwave","width",0.286);
RooRealVar ar_H0_Y1pwave("ar_H0_Y1pwave","",1.,-10,10);
RooRealVar ai_H0_Y1pwave("ai_H0_Y1pwave","",0.,-10,10);
RooRealVar ar_H1_Y1pwave("ar_H1_Y1pwave","",0.);
RooRealVar ai_H1_Y1pwave("ai_H1_Y1pwave","",0.);  
RooArgList* Y_Y1pwave = new RooArgList(ar_H0_Y1pwave,ai_H0_Y1pwave,
      			      ar_H1_Y1pwave,ai_H1_Y1pwave,"Y_Y1pwave");
Y_Y1pwave->add(*FWave_Y1p);
Y_Y1pwave->add(m0_Y1pwave);
Y_Y1pwave->add(width_Y1pwave);
Y_Y1pwave->add(RooRealConstant::value(MODELINDEPENDENT));
Y_Y1pwave->add(RooRealConstant::value(1));//spin
Y_Y1pwave->add(RooRealConstant::value(1)); //parity


char Wname_Y3p[20];
RooRealVar *SMass_Y3p[Nsp], *SRAm_Y3p[Nsp], *SIAm_Y3p[Nsp];
RooArgList *FWave_Y3p = new RooArgList("FWave_Y3p");
double xm_Y3p[Nsp], sr0_Y3p[Nsp], sp0_Y3p[Nsp];
std::ifstream inswav_Y3p;
inswav_Y3p.open("/disk401/lhcb/qinning/b2dpp_farm10/qnFit/splinedat/toy_Y3p.dat");
int iswav_Y3p=0;
while(1) {
	inswav_Y3p >> xm_Y3p[iswav_Y3p] >> sr0_Y3p[iswav_Y3p] >> sp0_Y3p[iswav_Y3p];
	if(!inswav_Y3p.good()) break;
	std::cout << xm_Y3p[iswav_Y3p] << " " << sr0_Y3p[iswav_Y3p] << " " << sp0_Y3p[iswav_Y3p] << std::endl;
	iswav_Y3p++;
} 
inswav_Y3p.close();
int Npoint_Y3p = iswav_Y3p;
std::cout << "point of Y spline Wave " << Npoint << std::endl;
if(Npoint_Y3p!=NUMSPY) {
	std::cout <<  "SPline number is wrong " << std::endl;
	assert(0);
}
for(int i=0; i<Npoint_Y3p; ++i) {
	double fac(1.0);
	if(i>0) {
		fac = 1.0;
		std::cout << fac << std::endl;
	}
	sprintf(Wname_Y3p,"SR_Y3p_%02d",i);
	SRAm_Y3p[i] = new RooRealVar(Wname_Y3p,Wname_Y3p,sr0_Y3p[i]*fac);
	SRAm_Y3p[i]->setConstant(0);
	FWave_Y3p->add(*SRAm_Y3p[i]);
	sprintf(Wname_Y3p,"SI_Y3p_%02d",i);
	SIAm_Y3p[i] = new RooRealVar(Wname_Y3p,Wname_Y3p,sp0_Y3p[i]*fac);
	SIAm_Y3p[i]->setConstant(0);
	FWave_Y3p->add(*SIAm_Y3p[i]);
	sprintf(Wname_Y3p,"m_Y3p_%02d",i);
	SMass_Y3p[i] = new RooRealVar(Wname_Y3p,Wname_Y3p,xm_Y3p[i]);
	FWave_Y3p->add(*SMass_Y3p[i]);    
}
//SRAm_Y3p[3]->setConstant(1);
//SIAm_Y3p[3]->setConstant(1);  
for(int i=0; i<Npoint_Y3p; ++i) {
	if(xm_Y3p[i]>3.3){
		SRAm_Y3p[i]->setConstant(1);
		SIAm_Y3p[i]->setConstant(1);
	}
}

RooRealVar m0_Y3pwave("m0_Y3pwave","m0",2.900);
RooRealVar width_Y3pwave("width_Y3pwave","width",0.286);
RooRealVar ar_H0_Y3pwave("ar_H0_Y3pwave","",1.,-10,10);
RooRealVar ai_H0_Y3pwave("ai_H0_Y3pwave","",0.,-10,10);
RooRealVar ar_H1_Y3pwave("ar_H1_Y3pwave","",0.);
RooRealVar ai_H1_Y3pwave("ai_H1_Y3pwave","",0.);  
RooArgList* Y_Y3pwave = new RooArgList(ar_H0_Y3pwave,ai_H0_Y3pwave,
      			      ar_H1_Y3pwave,ai_H1_Y3pwave,"Y_Y3pwave");
Y_Y3pwave->add(*FWave_Y3p);
Y_Y3pwave->add(m0_Y3pwave);
Y_Y3pwave->add(width_Y3pwave);
Y_Y3pwave->add(RooRealConstant::value(MODELINDEPENDENT));
Y_Y3pwave->add(RooRealConstant::value(3));//spin
Y_Y3pwave->add(RooRealConstant::value(1)); //parity
