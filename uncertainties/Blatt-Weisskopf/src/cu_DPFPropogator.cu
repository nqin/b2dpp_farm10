#include <math.h>
#include <cuda_runtime.h>
#include "../RooDalitz/cuComplex.h"
#include "../RooDalitz/cu_DPFPropogator.h"
#include <iostream>
//#include "../RooDalitz/Resonances.h"

const double m_b0 = 5.27965;
const double m_d0 = 1.86484;
const double m_pr = 0.938272081;
const double _FFR = 4.5;
//const double NN = 10000.;
//const double _mres0 = -0.134064 ;
//const double _mres1 =  0.0599014;
//const double _mres2 = -0.00655841;

__device__ double getp(double M, double m1, double m2)
{
    double M2 = M*M;
    return sqrt(fabs((M2-pow(m1+m2,2))*(M2-pow(m1-m2,2))))/2.0/M;    
}

__device__ double F_BW(double r, int L, double p, double p0)
{
    //The orbital angular momentum barrier factors, Blatt_Weisskopf functions
    double z = pow(r*p,2);
    double z0 = pow(r*p0,2);
    double c = 1.0;
    switch (L) {
        case 1: 
            c = sqrt((1.0+z0)/(1.0+z));
            break;

        case 2:
            c = sqrt((z0*z0+3*z0+9.0)/(z*z+3*z+9.0));
            break;

        case 3:
            c = sqrt((z0*z0*z0+6*z0*z0+45*z0+225)/(z*z*z+6*z*z+45*z+225));
            break;

        case 4:
            c = sqrt((pow(z0,4)+10*pow(z0,3)+135*z0*z0+1575*z0+11025)
                    /(pow(z,4)+10*pow(z,3)+135*z*z+1575*z+11025));
            break;

        case 5:
            c = sqrt((pow(z0,5)+15*pow(z0,4)+315*pow(z0,3)+6300*pow(z0,2)+99225*z0+893025)
                    /(pow(z,5)+15*pow(z,4)+315*pow(z,3)+6300*pow(z,2)+99225*z+893025));
            break;
    }
    return c;
}

__device__ double BTerm(int J2, double mpp, double m_r, int iType)
{

    int LB = (iType == 0) ? J2 : abs(J2 - 1);
    LB = LB/2;
    //double m1,m2,m3;
    double m3;
    //m1, m2, m3, 1+2=R, 3 is other B daughters,
    double m1_0,m2_0,m3_0;
    switch(iType) {
        case 0: //X
            //m1 = mkk;
            //m2 = m_k;
        m3 = m_d0;
        m1_0 = m_pr;
        m2_0 = m_pr;
        m3_0 = m_d0;
            break;
        case 1:  //Dp
            //m1 = mkk;
            //m2 = m_jpsi;
        m3 = m_pr;
        m1_0 = m_d0;
        m2_0 = m_pr;
        m3_0 = m_pr;
            break;
    }

    double mreff = m_r;
    //for NR changed here to use middle mass;
    if(m_r<0.0) {
        mreff = (m2_0+m1_0+m_b0-m3_0)/2.0;
    } else if(m_r+m3_0>m_b0) {
        double mmin = m2_0+m1_0;
        double mmax = m_b0-m3_0;
        mreff = mmin+0.5*(mmax-mmin)*(1.0+tanh((m_r-(mmin+mmax)/2.0)/(mmax-mmin)));
    }

    double pB = getp(m_b0, m3, mpp);  
    double pB0 = getp(m_b0, m3_0, mreff);
    return F_BW(_FFR,LB,pB,pB0) * pow(pB/pB0, LB); 
}

__device__ double2 breit_wigner(double m, double gamma, double e2) 
{
    double de2 = m*m-e2;
    double gamma_m = gamma*m;
    double d = (de2*de2+gamma_m*gamma_m);
    return make_cuDoubleComplex(de2/d, gamma_m/d);
}

//For Flatte 
__device__ double2 BW_AMP(double m_r, double gamma_r, double mpp, int LR, double br1)
{
    double m2pp = mpp*mpp;
    double m1,m2,m3;
    double m1_0,m2_0,m3_0;
    m1 = m_d0;
    m2 = m_pr;
    m3 = m_pr;
    m1_0 = m_d0;
    m2_0 = m_pr;
    m3_0 = m_pr;
    double mSigmac = 2.45397;
    double mPi = 0.13957039;
    //double mSigmac = 2.28646;// m Lambda_c
    //double mPi = 2*0.13957039;// m pipi
    double pR0; // The momentum of Resonance->D0+p, m_r is the mass of resonance.
    if(m_r > m1_0+m2_0){
        pR0 = getp(m_r,m1_0,m2_0); 
    }
    else if(m_r > 0){
        pR0 = getp(m_r,mSigmac,mPi);
    }
    double pR = getp(mpp, m1, m2); // The momentum of (D0p)->D0+p, mpp is the invariant mass of D0p of this candidate.
    double FR = F_BW(_FFR,LR,pR,pR0);
    double gamma = gamma_r;
    gamma = gamma_r*pow((pR/pR0),2*LR+1)*(m_r/mpp)*FR*FR*br1;

    double pRSigmacPi = getp(mpp,mSigmac,mPi);
    double pRSigmacPi0 = getp(m_r,mSigmac,mPi);
//    double pRSigmacPi = pRSigmacPi0;


    double FRSigmacPi = F_BW(_FFR,LR,pRSigmacPi,pRSigmacPi0);
    double gammaSigmacPi = gamma_r*pow((pRSigmacPi/pRSigmacPi0),2*LR+1)*(m_r/mpp)*FRSigmacPi*FRSigmacPi*(1-br1);
    double de2 = m_r*m_r - m2pp;
    double gamma_m = gamma*m_r;
    if(mpp<mSigmac+mPi){
        de2 += gammaSigmacPi * m_r;
    }
    else{
        gamma_m += gammaSigmacPi * m_r;
    }
    double d = (de2*de2+gamma_m*gamma_m);

    double2 bw = make_cuDoubleComplex(de2/d, gamma_m/d);
    double kr = sqrt(m_r*m_r+gamma_r*gamma_r);
    double ksqrt = 0.948850 * m_r* sqrt(gamma_r*kr/sqrt(m_r*(m_r+kr))); 
    double factor = FR*pow(pR/pR0,LR)*ksqrt;  

    return cuCmuldc(factor,bw);
}

//Another method for Flatte
__device__ double2 Flatte(double m_r, double gamma_r, double mpp, int LR, double br1)
{
    double m2pp = mpp*mpp;
    double m1,m2,m3;
    double m1_0,m2_0,m3_0;
    m1 = m_d0;
    m2 = m_pr;
    m3 = m_pr;
    m1_0 = m_d0;
    m2_0 = m_pr;
    m3_0 = m_pr;

    double pR0 = 1.0;
    if(m_r > m1_0+m2_0)
        pR0 = getp(m_r,m1_0,m2_0); // The momentum of Resonance->D0+p, m_r is the mass of resonance.
    double pR = getp(mpp, m1, m2); // The momentum of (D0p)->D0+p, mpp is the invariant mass of D0p of this candidate.
    double FR = 1.0;

    double gamma = gamma_r;
    double FF2=exp(-2.0*pR*pR*2);


    double mSigmac = 2.45397;
    double mPi = 0.13957039;
    double pRSigmacPi = getp(mpp,mSigmac,mPi);
    double pRSigmacPi0 = getp(m_r,mSigmac,mPi);
    double gammaSigmacPi = gamma_r*pow((pRSigmacPi/pRSigmacPi0),2*LR+1)*(m_r/mpp)*FR*FR*(1-br1);
    gamma = gamma_r*pow((pR/pR0),2*LR+1)*(m_r/mpp)*FR*FR*br1*FF2;
    double de2 = m_r*m_r - m2pp;
    double gamma_m = gamma*m_r;
    if(mpp<mSigmac+mPi){
        de2 += gammaSigmacPi * m_r;
    }
    else{
        gamma_m += gammaSigmacPi * m_r;
    }
    double d = (de2*de2+gamma_m*gamma_m);

    double2 bw = make_cuDoubleComplex(de2/d, gamma_m/d);
    double kr = sqrt(m_r*m_r+gamma_r*gamma_r);
    double ksqrt = 0.948850 * m_r* sqrt(gamma_r*kr/sqrt(m_r*(m_r+kr))); 
    double factor = FR*pow(pR/pR0,LR)*ksqrt;  

    return cuCmuldc(factor,bw);
}

__device__ double2 BW_AMP(double m_r, double gamma_r, double mpp, int LR, int iType)
{
    double m2pp = mpp*mpp;
    double m1,m2,m3;
    double m1_0,m2_0,m3_0;
    switch(iType) {
        case 0: //X
        m1 = m_pr;
        m2 = m_pr;
        m3 = m_d0;
        m1_0 = m_pr;
        m2_0 = m_pr;
        m3_0 = m_d0;
            break;
        case 1:  //Dp
        m1 = m_d0;
        m2 = m_pr;
        m3 = m_pr;
        m1_0 = m_d0;
        m2_0 = m_pr;
        m3_0 = m_pr;
            break;
    }

    double mmin = m1_0+m2_0;
    double mmax = m_b0-m3_0;  
    double pR = getp(mpp, m1, m2);
    double pR0, mreff;
    if(m_r>m1_0+m2_0) {
        mreff = m_r;
    } else if(m_r>=0.0) {
        mreff = mmin+0.5*(mmax-mmin)*(1.0+tanh((m_r-(mmin+mmax)/2.0)/(mmax-mmin)));
    } else { //NR
        mreff = (m1_0+m2_0+m_b0-m3)/2.0;
    }
    pR0 = getp(mreff, m1_0, m2_0);

    double FR = F_BW(_FFR,LR,pR,pR0);
    if(m_r<0.0) {
   //Exp NR
          //return make_cuDoubleComplex(pow(pR/pR0, LR)*FR*exp(-(mpp-(mmax*mmax+mmin*mmin)*0.5)*gamma_r), 0); 
          return make_cuDoubleComplex(pow(pR/pR0, LR)*FR*exp(-(m2pp-(mmax*mmax+mmin*mmin)*0.5)*gamma_r), 0); 
          //return make_cuDoubleComplex(pow(pR/pR0, LR)*FR, 0); 
    }

    double gamma = gamma_r;
    if(m_r>m1_0+m2_0)  gamma = gamma_r*pow((pR/pR0),2*LR+1)*(m_r/mpp)*FR*FR;
    if((iType==0&&m_r<1.9) | (iType == 1 && m_r < 2.83)) gamma = gamma_r;
    double kr = sqrt(m_r*m_r+gamma_r*gamma_r);
    double factor = FR*pow((pR/pR0),LR) * 0.948850 *m_r* sqrt(gamma_r*kr/sqrt(m_r*(m_r+kr))); 
    double2 bw = breit_wigner(m_r, gamma, m2pp);
    return cuCmuldc(factor, bw );
}

__device__ double2 BW_NR(double alpha, double beta, double mpp, int LR, int iType)
{
    double m2pp = mpp*mpp;
    double m1,m2,m3;
    double m1_0,m2_0,m3_0;
    switch(iType) {
        case 0: //X
        m1 = m_pr;
        m2 = m_pr;
        m3 = m_d0;
        m1_0 = m_pr;
        m2_0 = m_pr;
        m3_0 = m_d0;
            break;
        case 1:  //Dp
        m1 = m_d0;
        m2 = m_pr;
        m3 = m_pr;
        m1_0 = m_d0;
        m2_0 = m_pr;
        m3_0 = m_pr;
            break;
    }

    double mmin = m1_0+m2_0;
    double mmax = m_b0-m3_0;  
    double pR = getp(mpp, m1, m2);
    double pR0, mreff;
    mreff = (m1_0+m2_0+m_b0-m3)/2.0;
    pR0 = getp(mreff, m1_0, m2_0);

    double FR = F_BW(_FFR,LR,pR,pR0);
    //return make_cuDoubleComplex(pow(pR/pR0, LR)*FR*exp(-(m2pp-(mmax*mmax+mmin*mmin)*0.5)*alpha)*cos(-beta*(m2pp-(mmax*mmax+mmin*mmin)*0.5)), pow(pR/pR0, LR)*FR*sin(-beta*(m2pp-(mmax*mmax+mmin*mmin)*0.5))); 
    return make_cuDoubleComplex(pow(pR/pR0, LR)*FR*exp(-(mpp-(mmax*mmax+mmin*mmin)*0.5)*alpha)*cos(-beta*(mpp-(mmax*mmax+mmin*mmin)*0.5)), pow(pR/pR0, LR)*FR*sin(-beta*(mpp-(mmax*mmax+mmin*mmin)*0.5))); 
}
//implemented from TSpline3
__device__ double2 ModelIndependentWave3(const double *fPoly, double x, int iType)
//__device__ double2 ModelIndependentWave3(const double *fPoly, double x)
{
    double sumre(0.0);
    double sumim(0.0);

    //ms – input mass i.e. m(J/psi p) in this case
    //Rea,Ima – real and imaginary parts of the amplitude

    // get binning and number of interpolation points

    int fNp;
    double fXmin;
    double fXmax;
    //fNp = NUMSP;
    //fXmin = m_pr+m_pr-0.002;
    //fXmax = m_b0-m_d0+0.002;
    if(iType == 0){
        fNp = NUMSP;
        fXmin = m_pr+m_pr-0.002;
        fXmax = m_b0-m_d0+0.002;
    }
    else if(iType == 1){
        fNp = NUMSPY;
        fXmin = m_d0+m_pr-0.002;
        fXmax = m_b0-m_pr+0.002;
    }


    if( fNp <=0 )return make_cuDoubleComplex(0,0);
    if( x<fXmin )return make_cuDoubleComplex(0,0);
    if( x>fXmax )return make_cuDoubleComplex(0,0);


    //The above is the spline interpolation of the magnitude (called sumamp), real and imaginary parts.

    //spline3 from root
    int klow=0, khig=fNp-1;
    //
    // If out of boundaries, extrapolate
    // It may be badly wrong
    if(x<=fXmin) klow=0;
    else if(x>=fXmax) klow=khig;
    else {
        int khalf;
        //
        // Non equidistant knots, binary search
        while(khig-klow>1)
            if(x>fPoly[khalf=(klow+khig)/2])
                klow=khalf;
            else
                khig=khalf;
        //
    }
    //this is the number to be used klow;  
    // Evaluate now
    double dx=x-fPoly[klow];
    int ist = 4*klow+fNp;
    sumre = (fPoly[ist]+dx*(fPoly[ist+1]+dx*(fPoly[ist+2]+dx*fPoly[ist+3])));
    ist = ist + 4*fNp;
    sumim = (fPoly[ist]+dx*(fPoly[ist+1]+dx*(fPoly[ist+2]+dx*fPoly[ist+3])));
    return make_cuDoubleComplex(sumre, sumim);
}

