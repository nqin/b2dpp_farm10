#ifndef CU_JPSIHH_DLZ_H
#define CU_JPSIHH_DLZ_H

#include "../RooDalitz/NDefine.h"
/* define data struct for an event, including varibles used for PWA calcuations, 
and these intermediate varaibles are not changed
*/

struct cu_Jpsihh_dlz
{
  double mppb;
  double mDp;
  double w;

  //d-function of resonance decay part first index[3] is for K*,X,Z, last [3][3] for lambda of psi and phi
  double dRp[2][2],dRb[2][2];//Rotation dfunction for proton and antiproton  
  double DL[2][NumS][2][2]; //For dfunc of X/Dp of J=0...3; //caution, only up to J=NumS;

};
#endif
