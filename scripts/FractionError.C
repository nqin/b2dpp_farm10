#include "RooDalitz/RooDalitzAmplitude.h"
#include "RooDalitz/Resonances.h"
#include "TList.h"
#include "RooRealVar.h"
#include "RooArgList.h"
#include "TSystem.h"
#include "RooFormulaVar.h"
#include "RooDataSet.h"
#include "TFile.h"
#include "RooFitResult.h"
#include "TDatime.h"
#include "RooRealConstant.h"
#include "TRandom2.h"
#include "TRandom3.h"
#include "RooMinimizer.h"
#include "RooAddition.h"
#include "RooDalitz/RooFGaussian.h"
#include "RooMsgService.h"
#include "TStyle.h"
#include <TMatrixD.h>
#include "TH2F.h"
#include "TFile.h"
#define NFit 1
#define BW

#include "RooMultiVarGaussian.h"
#include "RooRandom.h"
#include <iostream>
using namespace RooFit;
TRandom3 *g_rnd;
RooRealVar *fbg = new RooRealVar("fbg", "bkg fraction", 0.0548);
RooRealVar *fSP = new RooRealVar("fSP", "SimgacPi fraction",0.5);

#include "RooDalitz/Setup.h"

void resetVal(RooArgSet* setdlz, const RooArgList parlist){
    for(int i=0; i<parlist.getSize(); ++i) {
        double value = ((RooRealVar*)parlist.at(i))->getVal();
        ((RooRealVar*)(setdlz->find(((RooRealVar*)(parlist.at(i)))->GetName())))->setVal(value);
        std::cout<<((RooRealVar*)(parlist.at(i)))->GetName()<<" "<<value<<std::endl;
    }
    ((RooRealVar*)(setdlz->find("ar_H0_2150")))->setVal(1.);
    ((RooRealVar*)(setdlz->find("ai_H0_2150")))->setVal(0.);
    ((RooRealVar*)(setdlz->find("ar_H0_2150")))->setConstant(1);
    ((RooRealVar*)(setdlz->find("ai_H0_2150")))->setConstant(1);
}

int main(int argc, char **argv){
#include "RooDalitz/Res.h"
//#include "RooDalitz/MODEL/model_FracErrorCal.h"
#include "RooDalitz/MODEL/model0.h"
    listX->Add(X_f2_2010);

    RooRealVar mppbar("mppbar", "mppbar", 0);
    RooRealVar mD0p("mD0p", "mD0p", 0);
    RooRealVar mD0pbar("mD0pbar", "mD0ppbar", 0);
    RooRealVar cosTheta_X("cosTheta_X", "cosTheta_X", -1, 1);
    RooRealVar cosTheta_L("cosTheta_L", "cosTheta_L", -1, 1);
    RooRealVar cosTheta_p("cosTheta_p", "cosTheta_p", -1, 1);
    RooRealVar cosTheta_pbar("cosTheta_pbar", "cosTheta_pbar", -1, 1);
    RooArgList *obs = new RooArgList();
    obs->add(mppbar);
    obs->add(mD0p);
    obs->add(mD0pbar);
    obs->add(cosTheta_X);
    obs->add(cosTheta_L);
    obs->add(cosTheta_p);
    obs->add(cosTheta_pbar);
    RooArgList *obs2 = new RooArgList();
    obs2->add(*obs);
    TChain *datree = new TChain("mytree");
    datree->Add("datafit.root");
    TTree *datree1 = (TTree *)datree->CopyTree("abs(B_mass_con-5279.65)<20");
    RooDataSet *datars = new RooDataSet("datars", "", datree1, *obs2);
    datree->Delete();
    datree1->Delete();
    RooRealVar *index1 = new RooRealVar("index", "index", 0);
    RooDataSet *IND1 = new RooDataSet("IND", "", RooArgSet(*index1));
    double nev1 = datars->numEntries();
    std::cout << "==================================================== " << std::endl;
    std::cout << "the number of B+ part numEntries:" << nev1 << std::endl;
    std::cout << "==================================================== " << std::endl;
    for (int i = 0; i < nev1; ++i)
    {
        *index1 = i;
        IND1->add(RooArgSet(*index1));
    }
    datars->merge(IND1);
    RooDataSet *data_fit;
    data_fit = datars;
    obs->add(*index1);
    //RooDalitzAmplitude *sigflat = new RooDalitzAmplitude("sigflat","",*obs,list,listX,"MCFlatAcc.root",*data_fit,*fbg,*fSP);

    RooDalitzAmplitude *sig1 = new RooDalitzAmplitude("sig1", "", *obs, list, listX, "mcsw.root", *data_fit, *fbg, *fSP);
    RooArgSet *setdlz1 = sig1->getParameters(*data_fit);
//    setdlz1->readFromFile("/disk302/lhcb/qinning/b2dpp_double/qnFit/func/HESSEOK.func");
    setdlz1->readFromFile("/disk401/lhcb/qinning/b2dpp_farm10/qnFit/fitmemeda.func");
    sig1->Delete();



//    TFile fresult("/disk302/lhcb/qinning/b2dpp_double/qnFit/fit_result.root");
    TFile fresult("/disk401/lhcb/qinning/b2dpp_farm10/qnFit/fit_result.root");
    RooFitResult *result = (RooFitResult *)fresult.Get("nll");
    const RooArgList paralist = result->floatParsFinal();
    RooArgList veclist;
    for(int i=0; i<paralist.getSize(); ++i){
        veclist.add(paralist[i]);
    }

    RooMultiVarGaussian mg("mg", "", paralist, *result);
    const int Nset = 20;
    RooRandom::randomGenerator()->SetSeed(52770);
    RooDataSet *new_set = mg.generate(veclist,Nset);
    RooArgSet *row1;
    row1 = (RooArgSet *) new_set->get(1);

    char fileName1[100]="./ResultValue.csv";
    std::ofstream fout1(fileName1);

    for(int i=0; i<paralist.getSize(); i++){
        fout1 << ((RooRealVar*)paralist.at(i))->GetName() << ",";
    }
    fout1<<"\n";

    double resultval;
    double resultval2;
    for(int j=0; j<Nset; j++){
        row1 = (RooArgSet *) new_set->get(j);
        for(int i=0; i<paralist.getSize(); i++){
            resultval = ((RooRealVar*)row1->find(((RooRealVar*)paralist.at(i))->GetName()))->getVal();
            resultval2 = row1->getRealValue(((RooRealVar*)(paralist.at(i)))->GetName()); 
            fout1 << resultval2 << ",";
        }
        fout1<<"\n";
    }

    
    RooDalitzAmplitude *sigflat = new RooDalitzAmplitude("sigflat","", *obs, list, listX, "MCFlatAcc.root", *data_fit, *fbg, *fSP);
    RooArgSet *row;
    int nres = listX->GetSize();//+listZ->GetSize();
    double Dsum[Nset][100];
    int printseq = 1;
    for(int iset=0; iset<Nset; iset++){
        if (iset%printseq==0){
            std::cout<<"processed: "<< iset<<"/"<< Nset<< " | "<<std::endl;
        }
        row = (RooArgSet*)new_set->get(iset);
        sigflat->getInt(Dsum[iset],true);
        resetVal(setdlz1,*row);
    }
//    for(int iset=0; iset<Nset; iset++){
//        std::cout << "======Fit Fraction======" << std::endl;
//        for(int i=0; i<nres; ++i){
//            printf("%10s %6.2f\n", (listX->At(i))->GetName(),Dsum[iset][i]*100.0 );
//        }
//        for(int i=0; i<list->GetSize(); ++i) {
//            printf("%10s %6.2f\n", (list->At(i))->GetName(),Dsum[iset][i+nres]*100.0 );
//        }
//    }
    char fileName[100]="./FractionError.txt";
    std::ofstream fout(fileName);
    for(int i=0; i<nres; ++i){
        fout << (listX->At(i))->GetName() << ",";
    }
    for(int i=0; i<list->GetSize(); ++i) {
        fout << (list->At(i))->GetName() << ",";
    }
    fout<<"\n";

    for(int i=0; i<Nset; i++){
        for(int j=0; j<(list->GetSize()+listX->GetSize()); j++){
            fout << Dsum[i][j] << ",";
        }
        fout<<"\n";
    }
    return 0;
}
