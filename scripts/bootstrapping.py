from pandas import DataFrame
import numpy as np
from ROOT import RDataFrame as rdf
import ROOT as rt

tree_ori = 'mytree'
file_ori = '/data2/lhcb/shenzh/B2JpsiPhiKs/AMAN/fast_new/data/mcsw.root'
df_origin = rdf(tree_ori, file_ori).AsNumpy()
df_origin = DataFrame(df_origin)


for i in range(200):
    print(i)
    #m = len(df_origin)
    m = rt.gRandom.Poisson(len(df_origin))
    print(len(df_origin),m)
    ind = np.random.choice(df_origin.index, m, replace=True)
    new_df = df_origin.loc[ind]
    new_df_dict = {key: new_df[key].values for key in df_origin.columns.values}
    #print(new_df_dict)
    df_target = rt.RDF.MakeNumpyDataFrame(new_df_dict)
    df_target.Snapshot(tree_ori, "./mcsw/mcsw_{0}.root".format(i+1) )