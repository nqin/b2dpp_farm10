//#include "DataBin.h"
//#include "DataPoint.h"
//#define SMCInfo_cxx
//#define DrawClass_cxx
//#include "CGD/SMCInfo.h"
//#include "DrawClass.h"
#include <vector>
#include <algorithm>
#include <iostream>
#include "TStyle.h"
#include "TSystem.h"
#include "TH1D.h"
#include "TH2.h"
#include "TLatex.h"
#include "TF1.h"
#include "TString.h"
#include "RooArgSet.h"
#include "RooDataSet.h"
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TObject.h>
#include <TNtuple.h>
#include <TDirectory.h>
#include <TCanvas.h>
#include <TH2Poly.h>
#include <TColor.h>
#include "TMath.h"

#include <cmath>
#include "TBox.h"
#include "RooRealVar.h"
using namespace std;

//#define MINPOINTS 20
#define PI 3.14159265

class DataPoint
{
public:
	DataPoint() { mpp = mDp = mDpbar = cosTheta_X = cosTheta_L = cosTheta_p = cosTheta_pbar = weight = 0; }
	DataPoint(double, double, double, double, double, double, double, double);
	void SetValues(double, double, double, double, double, double, double, double);
	double Getmpp() const { return mpp; }
	double GetmDp() const { return mDp; }
	double GetmDpbar() const { return mDpbar; }
	double GetcosTheta_X() const { return cosTheta_X; }
	double GetcosTheta_L() const { return cosTheta_L; }
	double GetcosTheta_p() const { return cosTheta_p; }
	double GetcosTheta_pbar() const { return cosTheta_pbar; }
	double Getweight() const { return weight; }

private:
	double mpp;
	double mDp;
	double mDpbar;
	double cosTheta_X;
	double cosTheta_L;
	double cosTheta_p;
	double cosTheta_pbar;
	double weight;
	double minpoints;
};

DataPoint::DataPoint(double mpp_in, double mDp_in, double mDpbar_in, double cosTheta_X_in, double cosTheta_L_in, double cosTheta_p_in, double cosTheta_pbar_in, double weight_in)
{
	mpp = mpp_in;
	mDp = mDp_in;
	mDpbar = mDpbar_in;
	cosTheta_X = cosTheta_X_in;
	cosTheta_L = cosTheta_L_in;
	cosTheta_p = cosTheta_p_in;
	cosTheta_pbar = cosTheta_pbar_in;
	weight = weight_in;
}

void DataPoint::SetValues(double mpp_in, double mDp_in, double mDpbar_in, double cosTheta_X_in, double cosTheta_L_in, double cosTheta_p_in, double cosTheta_pbar_in, double weight_in)
{
	mpp = mpp_in;
	mDp = mDp_in;
	mDpbar = mDpbar_in;
	cosTheta_X = cosTheta_X_in;
	cosTheta_L = cosTheta_L_in;
	cosTheta_p = cosTheta_p_in;
	cosTheta_pbar = cosTheta_pbar_in;
	weight = weight_in;
}

typedef std::vector<DataPoint> DataPoints;
bool Comparempp(DataPoint a, DataPoint b)
{
  return a.Getmpp() < b.Getmpp();
}
bool ComparemDp(DataPoint a, DataPoint b)
{
  return a.GetmDp() < b.GetmDp();
}
bool ComparecosTheta_X(DataPoint a, DataPoint b)
{
  return a.GetcosTheta_X() < b.GetcosTheta_X();
}
bool ComparecosTheta_L(DataPoint a, DataPoint b)
{
  return a.GetcosTheta_L() < b.GetcosTheta_L();
}
bool ComparecosTheta_p(DataPoint a, DataPoint b)
{
  return a.GetcosTheta_p() < b.GetcosTheta_p();
}
bool ComparecosTheta_pbar(DataPoint a, DataPoint b)
{
  return a.GetcosTheta_pbar() < b.GetcosTheta_pbar();
}



class DataBin
{
public:
	DataBin(double minpoints) {
		pmin = DataPoint(0, 0, 0, 0, 0, 0, 0, 0);
		pmax = DataPoint(0, 0, 0, 0, 0, 0, 0, 0);
		MINPOINTS = minpoints;
	}
	DataBin(DataPoint& a, DataPoint& b, double minpoints){
		pmin = a; pmax = b; MINPOINTS = minpoints; 
	}
	double GetNumber() const {
		double total = 0;
		for (DataPoints::const_iterator ip = points.begin();ip != points.end();ip++) {
			total += (*ip).Getweight();
		}
		return total;
	}
	double GetBkgNumber() const {
		return bkg_points.size();
	}
	double GetNumErr() const {
		double total = 0;
		for (DataPoints::const_iterator ip = points.begin();ip != points.end();ip++) {
			total += (*ip).Getweight() * (*ip).Getweight();
		}
		return sqrt(total);
	}
	bool ZeroArea() const;
	void SetMin(DataPoint& a) { pmin = a; }
	void SetMax(DataPoint& b) { pmax = b; }
	DataPoint GetMin() const { return pmin; }
	DataPoint GetMax() const { return pmax; }
  	void DrawDalitz() const;
  	bool Contains(DataPoint&) const;
  	void AddPoint(DataPoint& a) { points.push_back(a); }
  	void AddBkgPoint(DataPoint& a) { bkg_points.push_back(a); } //NEWADD
  	DataPoints GetPoints() const { return points; }
  	std::vector<DataBin> Dividempp();
  	std::vector<DataBin> DividemDp();
  	std::vector<DataBin> DividecosTheta_X();
  	std::vector<DataBin> DividecosTheta_L();
  	std::vector<DataBin> DividecosTheta_p();
  	std::vector<DataBin> DividecosTheta_pbar();
private:
	DataPoint pmin;
  	DataPoint pmax;
  	DataPoints points;
  	DataPoints bkg_points; //NEWADD
  	double MINPOINTS;
};

bool DataBin::ZeroArea() const
{
	return (abs(pmin.Getmpp() - pmax.Getmpp()) < 0.0000001)
	|| (abs(pmin.GetmDp() - pmax.GetmDp()) < 0.0000001)
	|| (abs(pmin.GetcosTheta_X() - pmax.GetcosTheta_X()) < 0.0000001)
	|| (abs(pmin.GetcosTheta_L() - pmax.GetcosTheta_L()) < 0.0000001)
	|| (abs(pmin.GetcosTheta_p() - pmax.GetcosTheta_p()) < 0.0000001)
	|| (abs(pmin.GetcosTheta_pbar() - pmax.GetcosTheta_pbar()) < 0.0000001);
}


void DataBin::DrawDalitz() const
{
  	TBox b;
  	b.SetFillStyle(0);
  	b.DrawBox(pmin.Getmpp(), pmin.GetmDp(), pmax.Getmpp(), pmax.GetmDp());
}


bool DataBin::Contains(DataPoint& a) const
{
    bool passmpp = (a.Getmpp() >= pmin.Getmpp() && a.Getmpp() <= pmax.Getmpp());
    bool passmDp = (a.GetmDp() >= pmin.GetmDp() && a.GetmDp() <= pmax.GetmDp());
    bool passcostheta_X = (a.GetcosTheta_X() >= pmin.GetcosTheta_X() && a.GetcosTheta_X() <= pmax.GetcosTheta_X());
    bool passcostheta_L = (a.GetcosTheta_L() >= pmin.GetcosTheta_L() && a.GetcosTheta_L() <= pmax.GetcosTheta_L());
    bool passcostheta_p = (a.GetcosTheta_p() >= pmin.GetcosTheta_p() && a.GetcosTheta_p() <= pmax.GetcosTheta_p());
    bool passcostheta_pbar = (a.GetcosTheta_pbar() >= pmin.GetcosTheta_pbar() && a.GetcosTheta_pbar() <= pmax.GetcosTheta_pbar());
	return passmpp && passmDp && passcostheta_X && passcostheta_L && passcostheta_p && passcostheta_pbar;
}

std::vector<DataBin> DataBin::Dividempp()
{
	std::vector<DataBin>  newbins;
	int npoints = points.size();
	int bkg_npoints = bkg_points.size();
	double sumW = GetNumber();
	double sig_points = sumW;
	bool isCFit = false;
	if (int(sumW) == npoints) {
		isCFit = true;
		if (bkg_npoints != 0) { sig_points = sumW - bkg_npoints * ((bkg_points[0]).Getweight()); }
		else { sig_points = sumW; }
	}

  //printf("npoints: %i, bkg_npoints: %i, sumW: %f, sig_points: %f\n",npoints, bkg_npoints, sumW, sig_points);


  	if (sig_points < MINPOINTS) {
    	newbins.push_back(*this);//dont split
	}
	else {
		sort(points.begin(), points.end(), Comparempp);
		if (bkg_npoints != 0)sort(bkg_points.begin(), bkg_points.end(), Comparempp);
    	double critical_point = sumW / 2;
    	double i_sumW = 0;
    	int split_point = 0;
    	while (i_sumW < critical_point) {
    	  i_sumW += points[split_point].Getweight();
    	  split_point += 1;
    	}
		DataPoint midpoint = points[split_point - 1];
    	DataPoint nexpoint = points[split_point];
    	double midmpp = (midpoint.Getmpp() + nexpoint.Getmpp()) / 2.;

    	int bkg_split_point = 0;

    	if (isCFit && bkg_npoints != 0) {
			while (bkg_points[bkg_split_point].Getmpp() < midmpp && bkg_split_point < bkg_npoints)
			{bkg_split_point++;}
    	}
    	//  printf("bkg split point: %i\n", bkg_split_point);

    	DataPoint minmpp(midmpp, pmin.GetmDp(), pmin.GetmDpbar(), pmin.GetcosTheta_X(), pmin.GetcosTheta_L(), pmin.GetcosTheta_p(), pmin.GetcosTheta_pbar(), 0);
    	DataPoint maxmpp(midmpp, pmax.GetmDp(), pmax.GetmDpbar(), pmax.GetcosTheta_X(), pmax.GetcosTheta_L(), pmax.GetcosTheta_p(), pmax.GetcosTheta_pbar(), 0);

    	DataBin lowmpp(pmin, maxmpp, MINPOINTS);
    	for (int i = 0;i < split_point;i++) {
			lowmpp.AddPoint(points[i]);
    	}

    	if (isCFit && bkg_npoints != 0) {
		for (int i = 0;i < bkg_split_point;i++) {
			lowmpp.AddBkgPoint(bkg_points[i]);
		}
    	}
    	if (lowmpp.GetNumber() < critical_point / 2) {
    	  	printf("Somewhat low points\n");
    	  	//newbins.push_back( *this );
    	  	//return newbins;
    	}

    	newbins.push_back(lowmpp);

    	DataBin upmpp(minmpp, pmax, MINPOINTS);
    	for (int i = split_point;i < npoints;i++) {
			upmpp.AddPoint(points[i]);
    	}

    	if (isCFit && bkg_npoints != 0) {
			for (int i = bkg_split_point;i < bkg_npoints;i++) {
				upmpp.AddBkgPoint(bkg_points[i]);
			}
    	}

    	if (upmpp.GetNumber() < critical_point / 2) {
    	  	std::cout << " low content " << std::endl;
    	}
    	newbins.push_back(upmpp);
	}
	return newbins;
}

std::vector<DataBin> DataBin::DividemDp()
{
	std::vector<DataBin>  newbins;

	int npoints = points.size();
	int bkg_npoints = bkg_points.size();
	double sumW = GetNumber();
	double sig_points = sumW;
	bool isCFit = false;
	if (int(sumW) == npoints) {
		isCFit = true;
		if (bkg_npoints != 0) { sig_points = sumW - bkg_npoints * ((bkg_points[0]).Getweight()); }
		else { sig_points = sumW; }
	}
  //printf("npoints: %i, bkg_npoints: %i, sumW: %f, sig_points: %f\n",npoints, bkg_npoints, sumW, sig_points);


  	if (sig_points < MINPOINTS) {
    	newbins.push_back(*this);//dont split
	}
	else {
		sort(points.begin(), points.end(), ComparemDp);
		if (bkg_npoints != 0)sort(bkg_points.begin(), bkg_points.end(), ComparemDp);
    	double critical_point = sumW / 2;
    	double i_sumW = 0;
    	int split_point = 0;
    	while (i_sumW < critical_point) {
    	  i_sumW += points[split_point].Getweight();
    	  split_point += 1;
    	}
		DataPoint midpoint = points[split_point - 1];
    	DataPoint nexpoint = points[split_point];
    	double midmDp = (midpoint.GetmDp() + nexpoint.GetmDp()) / 2.;

    	int bkg_split_point = 0;

    	if (isCFit && bkg_npoints != 0) {
			while (bkg_points[bkg_split_point].GetmDp() < midmDp && bkg_split_point < bkg_npoints)
			{bkg_split_point++;}
    	}
    	//  printf("bkg split point: %i\n", bkg_split_point);

    	DataPoint minmDp(pmin.Getmpp(), midmDp, pmin.GetmDpbar(), pmin.GetcosTheta_X(), pmin.GetcosTheta_L(), pmin.GetcosTheta_p(), pmin.GetcosTheta_pbar(), 0);
    	DataPoint maxmDp(pmax.Getmpp(), midmDp, pmax.GetmDpbar(), pmax.GetcosTheta_X(), pmax.GetcosTheta_L(), pmax.GetcosTheta_p(), pmax.GetcosTheta_pbar(), 0);

    	DataBin lowmDp(pmin, maxmDp, MINPOINTS);
    	for (int i = 0;i < split_point;i++) {
			lowmDp.AddPoint(points[i]);
    	}

    	if (isCFit && bkg_npoints != 0) {
			for (int i = 0;i < bkg_split_point;i++) {
				lowmDp.AddBkgPoint(bkg_points[i]);
			}
    	}
    	if (lowmDp.GetNumber() < critical_point / 2) {
    	  	printf("Somewhat low points\n");
    	  	//newbins.push_back( *this );
    	  	//return newbins;
    	}

    	newbins.push_back(lowmDp);

    	DataBin upmDp(minmDp, pmax, MINPOINTS);
    	for (int i = split_point;i < npoints;i++) {
			upmDp.AddPoint(points[i]);
    	}

    	if (isCFit && bkg_npoints != 0) {
			for (int i = bkg_split_point;i < bkg_npoints;i++) {
				upmDp.AddBkgPoint(bkg_points[i]);
			}
    	}

    	if (upmDp.GetNumber() < critical_point / 2) {
    	  	std::cout << " low content " << std::endl;
    	}
    	newbins.push_back(upmDp);
	}
	return newbins;
}

std::vector<DataBin> DataBin::DividecosTheta_X()
{
	std::vector<DataBin>  newbins;

	int npoints = points.size();
	int bkg_npoints = bkg_points.size();
	double sumW = GetNumber();
	double sig_points = sumW;
	bool isCFit = false;
	if (int(sumW) == npoints) {
		isCFit = true;
		if (bkg_npoints != 0) { sig_points = sumW - bkg_npoints * ((bkg_points[0]).Getweight()); }
		else { sig_points = sumW; }
	}

  //printf("npoints: %i, bkg_npoints: %i, sumW: %f, sig_points: %f\n",npoints, bkg_npoints, sumW, sig_points);


  	if (sig_points < MINPOINTS) {
    	newbins.push_back(*this);//dont split
	}
	else {
		sort(points.begin(), points.end(), ComparecosTheta_X);
		if (bkg_npoints != 0)sort(bkg_points.begin(), bkg_points.end(), ComparecosTheta_X);
    	double critical_point = sumW / 2;
    	double i_sumW = 0;
    	int split_point = 0;
    	while (i_sumW < critical_point) {
    	  i_sumW += points[split_point].Getweight();
    	  split_point += 1;
    	}
		DataPoint midpoint = points[split_point - 1];
    	DataPoint nexpoint = points[split_point];
    	double midcosTheta_X = (midpoint.GetcosTheta_X() + nexpoint.GetcosTheta_X()) / 2.;

    	int bkg_split_point = 0;

    	if (isCFit && bkg_npoints != 0) {
			while (bkg_points[bkg_split_point].GetcosTheta_X() < midcosTheta_X && bkg_split_point < bkg_npoints)
			{bkg_split_point++;}
    	}
    	//  printf("bkg split point: %i\n", bkg_split_point);

    	DataPoint mincosTheta_X(midcosTheta_X, pmin.GetmDp(), pmin.GetmDpbar(), pmin.GetcosTheta_X(), pmin.GetcosTheta_L(), pmin.GetcosTheta_p(), pmin.GetcosTheta_pbar(), 0);
    	DataPoint maxcosTheta_X(midcosTheta_X, pmax.GetmDp(), pmax.GetmDpbar(), pmax.GetcosTheta_X(), pmax.GetcosTheta_L(), pmax.GetcosTheta_p(), pmax.GetcosTheta_pbar(), 0);

    	DataBin lowcosTheta_X(pmin, maxcosTheta_X, MINPOINTS);
    	for (int i = 0;i < split_point;i++) {
			lowcosTheta_X.AddPoint(points[i]);
    	}

    	if (isCFit && bkg_npoints != 0) {
			for (int i = 0;i < bkg_split_point;i++) {
				lowcosTheta_X.AddBkgPoint(bkg_points[i]);
			}
    	}
    	if (lowcosTheta_X.GetNumber() < critical_point / 2) {
    	  	printf("Somewhat low points\n");
    	  	//newbins.push_back( *this );
    	  	//return newbins;
    	}

    	newbins.push_back(lowcosTheta_X);

    	DataBin upcosTheta_X(mincosTheta_X, pmax, MINPOINTS);
    	for (int i = split_point;i < npoints;i++) {
			upcosTheta_X.AddPoint(points[i]);
    	}

    	if (isCFit && bkg_npoints != 0) {
			for (int i = bkg_split_point;i < bkg_npoints;i++) {
				upcosTheta_X.AddBkgPoint(bkg_points[i]);
			}
    	}

    	if (upcosTheta_X.GetNumber() < critical_point / 2) {
    	  	std::cout << " low content " << std::endl;
    	}
    	newbins.push_back(upcosTheta_X);
	}
	return newbins;
}
std::vector<DataBin> DataBin::DividecosTheta_L()
{
	std::vector<DataBin>  newbins;

	int npoints = points.size();
	int bkg_npoints = bkg_points.size();
	double sumW = GetNumber();
	double sig_points = sumW;
	bool isCFit = false;
	if (int(sumW) == npoints) {
		isCFit = true;
		if (bkg_npoints != 0) { sig_points = sumW - bkg_npoints * ((bkg_points[0]).Getweight()); }
		else { sig_points = sumW; }
	}

  //printf("npoints: %i, bkg_npoints: %i, sumW: %f, sig_points: %f\n",npoints, bkg_npoints, sumW, sig_points);


  	if (sig_points < MINPOINTS) {
    	newbins.push_back(*this);//dont split
	}
	else {
		sort(points.begin(), points.end(), ComparecosTheta_L);
		if (bkg_npoints != 0)sort(bkg_points.begin(), bkg_points.end(), ComparecosTheta_L);
    	double critical_point = sumW / 2;
    	double i_sumW = 0;
    	int split_point = 0;
    	while (i_sumW < critical_point) {
    	  i_sumW += points[split_point].Getweight();
    	  split_point += 1;
    	}
		DataPoint midpoint = points[split_point - 1];
    	DataPoint nexpoint = points[split_point];
    	double midcosTheta_L = (midpoint.GetcosTheta_L() + nexpoint.GetcosTheta_L()) / 2.;

    	int bkg_split_point = 0;

    	if (isCFit && bkg_npoints != 0) {
			while (bkg_points[bkg_split_point].GetcosTheta_L() < midcosTheta_L && bkg_split_point < bkg_npoints)
			{bkg_split_point++;}
    	}
    	//  printf("bkg split point: %i\n", bkg_split_point);

    	DataPoint mincosTheta_L(midcosTheta_L, pmin.GetmDp(), pmin.GetmDpbar(), pmin.GetcosTheta_L(), pmin.GetcosTheta_L(), pmin.GetcosTheta_p(), pmin.GetcosTheta_pbar(), 0);
    	DataPoint maxcosTheta_L(midcosTheta_L, pmax.GetmDp(), pmax.GetmDpbar(), pmax.GetcosTheta_L(), pmax.GetcosTheta_L(), pmax.GetcosTheta_p(), pmax.GetcosTheta_pbar(), 0);

    	DataBin lowcosTheta_L(pmin, maxcosTheta_L, MINPOINTS);
    	for (int i = 0;i < split_point;i++) {
			lowcosTheta_L.AddPoint(points[i]);
    	}

    	if (isCFit && bkg_npoints != 0) {
			for (int i = 0;i < bkg_split_point;i++) {
				lowcosTheta_L.AddBkgPoint(bkg_points[i]);
			}
    	}
    	if (lowcosTheta_L.GetNumber() < critical_point / 2) {
    	  	printf("Somewhat low points\n");
    	  	//newbins.push_back( *this );
    	  	//return newbins;
    	}

    	newbins.push_back(lowcosTheta_L);

    	DataBin upcosTheta_L(mincosTheta_L, pmax, MINPOINTS);
    	for (int i = split_point;i < npoints;i++) {
			upcosTheta_L.AddPoint(points[i]);
    	}

    	if (isCFit && bkg_npoints != 0) {
			for (int i = bkg_split_point;i < bkg_npoints;i++) {
				upcosTheta_L.AddBkgPoint(bkg_points[i]);
			}
    	}

    	if (upcosTheta_L.GetNumber() < critical_point / 2) {
    	  	std::cout << " low content " << std::endl;
    	}
    	newbins.push_back(upcosTheta_L);
	}
	return newbins;
}
std::vector<DataBin> DataBin::DividecosTheta_p()
{
	std::vector<DataBin>  newbins;

	int npoints = points.size();
	int bkg_npoints = bkg_points.size();
	double sumW = GetNumber();
	double sig_points = sumW;
	bool isCFit = false;
	if (int(sumW) == npoints) {
		isCFit = true;
		if (bkg_npoints != 0) { sig_points = sumW - bkg_npoints * ((bkg_points[0]).Getweight()); }
		else { sig_points = sumW; }
	}

  //printf("npoints: %i, bkg_npoints: %i, sumW: %f, sig_points: %f\n",npoints, bkg_npoints, sumW, sig_points);


  	if (sig_points < MINPOINTS) {
    	newbins.push_back(*this);//dont split
	}
	else {
		sort(points.begin(), points.end(), ComparecosTheta_p);
		if (bkg_npoints != 0)sort(bkg_points.begin(), bkg_points.end(), ComparecosTheta_p);
    	double critical_point = sumW / 2;
    	double i_sumW = 0;
    	int split_point = 0;
    	while (i_sumW < critical_point) {
    	  i_sumW += points[split_point].Getweight();
    	  split_point += 1;
    	}
		DataPoint midpoint = points[split_point - 1];
    	DataPoint nexpoint = points[split_point];
    	double midcosTheta_p = (midpoint.GetcosTheta_p() + nexpoint.GetcosTheta_p()) / 2.;

    	int bkg_split_point = 0;

    	if (isCFit && bkg_npoints != 0) {
			while (bkg_points[bkg_split_point].GetcosTheta_p() < midcosTheta_p && bkg_split_point < bkg_npoints)
			{bkg_split_point++;}
    	}
    	//  printf("bkg split point: %i\n", bkg_split_point);

    	DataPoint mincosTheta_p(midcosTheta_p, pmin.GetmDp(), pmin.GetmDpbar(), pmin.GetcosTheta_p(), pmin.GetcosTheta_L(), pmin.GetcosTheta_p(), pmin.GetcosTheta_pbar(), 0);
    	DataPoint maxcosTheta_p(midcosTheta_p, pmax.GetmDp(), pmax.GetmDpbar(), pmax.GetcosTheta_p(), pmax.GetcosTheta_L(), pmax.GetcosTheta_p(), pmax.GetcosTheta_pbar(), 0);

    	DataBin lowcosTheta_p(pmin, maxcosTheta_p, MINPOINTS);
    	for (int i = 0;i < split_point;i++) {
			lowcosTheta_p.AddPoint(points[i]);
    	}

    	if (isCFit && bkg_npoints != 0) {
			for (int i = 0;i < bkg_split_point;i++) {
				lowcosTheta_p.AddBkgPoint(bkg_points[i]);
			}
    	}
    	if (lowcosTheta_p.GetNumber() < critical_point / 2) {
    	  	printf("Somewhat low points\n");
    	  	//newbins.push_back( *this );
    	  	//return newbins;
    	}

    	newbins.push_back(lowcosTheta_p);

    	DataBin upcosTheta_p(mincosTheta_p, pmax, MINPOINTS);
    	for (int i = split_point;i < npoints;i++) {
			upcosTheta_p.AddPoint(points[i]);
    	}

    	if (isCFit && bkg_npoints != 0) {
			for (int i = bkg_split_point;i < bkg_npoints;i++) {
				upcosTheta_p.AddBkgPoint(bkg_points[i]);
			}
    	}

    	if (upcosTheta_p.GetNumber() < critical_point / 2) {
    	  	std::cout << " low content " << std::endl;
    	}
    	newbins.push_back(upcosTheta_p);
	}
	return newbins;
}
std::vector<DataBin> DataBin::DividecosTheta_pbar()
{
	std::vector<DataBin>  newbins;
	int npoints = points.size();
	int bkg_npoints = bkg_points.size();
	double sumW = GetNumber();
	double sig_points = sumW;
	bool isCFit = false;
	if (int(sumW) == npoints) {
		isCFit = true;
		if (bkg_npoints != 0) { sig_points = sumW - bkg_npoints * ((bkg_points[0]).Getweight()); }
		else { sig_points = sumW; }
	}

  //printf("npoints: %i, bkg_npoints: %i, sumW: %f, sig_points: %f\n",npoints, bkg_npoints, sumW, sig_points);


  	if (sig_points < MINPOINTS) {
    	newbins.push_back(*this);//dont split
	}
	else {
		sort(points.begin(), points.end(), ComparecosTheta_pbar);
		if (bkg_npoints != 0)sort(bkg_points.begin(), bkg_points.end(), ComparecosTheta_pbar);
    	double critical_point = sumW / 2;
    	double i_sumW = 0;
    	int split_point = 0;
    	while (i_sumW < critical_point) {
    	  i_sumW += points[split_point].Getweight();
    	  split_point += 1;
    	}
		DataPoint midpoint = points[split_point - 1];
    	DataPoint nexpoint = points[split_point];
    	double midcosTheta_pbar = (midpoint.GetcosTheta_pbar() + nexpoint.GetcosTheta_pbar()) / 2.;

    	int bkg_split_point = 0;

    	if (isCFit && bkg_npoints != 0) {
			while (bkg_points[bkg_split_point].GetcosTheta_pbar() < midcosTheta_pbar && bkg_split_point < bkg_npoints)
			{bkg_split_point++;}
    	}
    	//  printf("bkg split point: %i\n", bkg_split_point);

    	DataPoint mincosTheta_pbar(midcosTheta_pbar, pmin.GetmDp(), pmin.GetmDpbar(), pmin.GetcosTheta_pbar(), pmin.GetcosTheta_L(), pmin.GetcosTheta_pbar(), pmin.GetcosTheta_pbar(), 0);
    	DataPoint maxcosTheta_pbar(midcosTheta_pbar, pmax.GetmDp(), pmax.GetmDpbar(), pmax.GetcosTheta_pbar(), pmax.GetcosTheta_L(), pmax.GetcosTheta_pbar(), pmax.GetcosTheta_pbar(), 0);

    	DataBin lowcosTheta_pbar(pmin, maxcosTheta_pbar, MINPOINTS);
    	for (int i = 0;i < split_point;i++) {
			lowcosTheta_pbar.AddPoint(points[i]);
    	}

    	if (isCFit && bkg_npoints != 0) {
			for (int i = 0;i < bkg_split_point;i++) {
				lowcosTheta_pbar.AddBkgPoint(bkg_points[i]);
			}
    	}
    	if (lowcosTheta_pbar.GetNumber() < critical_point / 2) {
    	  	printf("Somewhat low points\n");
    	  	//newbins.push_back( *this );
    	  	//return newbins;
    	}

    	newbins.push_back(lowcosTheta_pbar);

    	DataBin upcosTheta_pbar(mincosTheta_pbar, pmax, MINPOINTS);
    	for (int i = split_point;i < npoints;i++) {
			upcosTheta_pbar.AddPoint(points[i]);
    	}

    	if (isCFit && bkg_npoints != 0) {
			for (int i = bkg_split_point;i < bkg_npoints;i++) {
				upcosTheta_pbar.AddBkgPoint(bkg_points[i]);
			}
    	}

    	if (upcosTheta_pbar.GetNumber() < critical_point / 2) {
    	  	std::cout << " low content " << std::endl;
    	}
    	newbins.push_back(upcosTheta_pbar);
	}
	return newbins;
}

typedef std::vector<DataBin> DataBins;

DataBins Dividempp(DataBins&);
DataBins DividemDp(DataBins&);
DataBins DividecosTheta_X(DataBins&);
DataBins DividecosTheta_L(DataBins&);
DataBins DividecosTheta_p(DataBins&);
DataBins DividecosTheta_pbar(DataBins&);
double foldPhiAngle(double phiAngle);




void MassChi2(RooDataSet* data, TTree* sideband_tree, TString MCloc, double f_bkg, TString label, 
				int nmpp, int nmDp, int ncosTheta_X, int ncosTheta_L, int ncosTheta_p, int ncosTheta_pbar, double kine_min(double*, double*), double kine_max(double*, double*), 
				double minimalPoints, bool foldPhi)
{
	double m_b0 = 5.27965;
	double m_d0 = 1.86484;
	double m_pr = 0.938272081;

	double mpp_min = pow(m_pr+m_pr,2);
	double mpp_max = pow(m_b0-m_d0,2);
	double mDp_min = pow(m_d0+m_pr,2);
	double mDp_max = pow(m_b0-m_pr,2);
	double mDpbar_min = pow(m_d0+m_pr,2);
	double mDpbar_max = pow(m_b0-m_pr,2);

	gROOT->SetStyle("Plain");
	gStyle->SetOptStat(0);
	gStyle->SetOptTitle(0);
	gStyle->SetOptFit(0111);

  	DataPoint pmin((mpp_min), (mDp_min), (mDpbar_min), 0, 0, 0, 0, 0);
  	DataPoint pmax((mpp_max), (mDp_max), (mDpbar_max), 1, 1, 1, 1, 0);
  	DataBin StartBin(pmin, pmax, minimalPoints);

  	TH2D* hist = new TH2D("da_mppvsmDp", "da_mppvsmDp", 10, mpp_min, mpp_max, 10, mDp_min, mDp_max);
  	TH2D* histmc = new TH2D("mc_mppvsmDp", "mc_mppvsmDp", 10, mpp_min, mpp_max, 10, mDp_min, mDp_max);
  	TH2D* hist_b(0);
  	if (true)hist_b = new TH2D("b_mppvsmDp", "b_mppvsmDp", 10, mpp_min, mpp_max, 10, mDp_min, mDp_max);
	
	double D_mpp, D_mDp, D_mDpbar, D_cosTheta_X, D_cosTheta_L, D_cosTheta_p, D_cosTheta_pbar, D_sig_sw;
  	double data_sumWeight = 0;
  	int data_npoints = data->numEntries();
  	RooArgSet* row;
  	for (int i = 0;i < data_npoints;i++) {
    	row = (RooArgSet*)data->get(i);
    	//row->Print("V");
    	D_mpp = ((RooAbsReal*)row->find("mppbar"))->getVal();
    	D_mDp = ((RooAbsReal*)row->find("mD0p"))->getVal();
    	D_mDpbar = ((RooAbsReal*)row->find("mD0pbar"))->getVal();
    	D_cosTheta_X = ((RooAbsReal*)row->find("cosTheta_X"))->getVal();
    	D_cosTheta_L = ((RooAbsReal*)row->find("cosTheta_L"))->getVal();
    	D_cosTheta_p = ((RooAbsReal*)row->find("cosTheta_p"))->getVal();
    	D_cosTheta_pbar = ((RooAbsReal*)row->find("cosTheta_pbar"))->getVal();
    	//D_sig_sw = ((RooAbsReal*)row->find("Sweight_sig"))->getVal();
		D_sig_sw = 1.0;
    	data_sumWeight += D_sig_sw;

    	if (D_mpp * D_mpp < mpp_min) continue;
    	if (D_mpp * D_mpp > mpp_max) continue;
    	if (D_mDp * D_mDp < mDp_min) continue;
    	if (D_mDp * D_mDp > mDp_max) continue;

    	hist->Fill((D_mpp) * (D_mpp), (D_mDp) * (D_mDp), D_sig_sw);
    	//DataPoint dp(D_mpp * D_mpp, D_mDp * D_mDp, fabs(D_cosTheta_L), fabs(D_cosTheta_Lb), fabs(D_cosTheta_Jpsi), Df_phiMu)
    	DataPoint dp(D_mpp*D_mpp, D_mDp*D_mDp, D_mDpbar*D_mDpbar, fabs(D_cosTheta_X), fabs(D_cosTheta_L), fabs(D_cosTheta_p), fabs(D_cosTheta_pbar), D_sig_sw);
    	StartBin.AddPoint(dp);
	}
	cout << "Data sum SW " << data_sumWeight << endl;
	
	double scale_factor = 0;
	if (sideband_tree) {
		double B_mpp, B_mDp, B_mDpbar, B_cosTheta_X, B_cosTheta_L, B_cosTheta_p, B_cosTheta_pbar;
    	sideband_tree->SetBranchAddress("mppbar", &B_mpp);
    	sideband_tree->SetBranchAddress("mD0p", &B_mDp);
    	sideband_tree->SetBranchAddress("mD0pbar", &B_mDpbar);
    	sideband_tree->SetBranchAddress("cosTheta_X", &B_cosTheta_X);
    	sideband_tree->SetBranchAddress("cosTheta_L", &B_cosTheta_L);
    	sideband_tree->SetBranchAddress("cosTheta_p", &B_cosTheta_p);
    	sideband_tree->SetBranchAddress("cosTheta_pbar", &B_cosTheta_pbar);

    	double sideband_entries = sideband_tree->GetEntries();
    	scale_factor = (data_npoints / sideband_entries) * f_bkg;

    	std::cout << " bkg entries " << sideband_entries << std::endl;
    	std::cout << "and number of data points is: " << data_npoints << ", with frac bkg: " << f_bkg << std::endl;
    	std::cout << "So scaling factor is: " << scale_factor << std::endl;

    	double Bf_phiK, Bf_phiMu;

    	for (int i = 0;i < sideband_entries;i++) {
			sideband_tree->GetEntry(i);
			if (B_mpp * B_mpp < mpp_min) continue;
			if (B_mpp * B_mpp > mpp_max) continue;
			if (B_mDp * B_mDp < mDp_min) continue;
			if (B_mDp * B_mDp > mDp_max) continue;
			hist_b->Fill((B_mpp) * (B_mpp), (B_mDp) * (B_mDp), scale_factor);
			DataPoint dp(B_mpp*B_mpp, B_mDp*B_mDp, B_mDpbar*B_mDpbar, fabs(B_cosTheta_X), fabs(B_cosTheta_L), fabs(B_cosTheta_p), fabs(B_cosTheta_pbar), scale_factor);
			StartBin.AddBkgPoint(dp);
		}
	}


	bool plot((ncosTheta_X == 0) && (ncosTheta_L == 0) && (ncosTheta_p == 0) && (ncosTheta_pbar == 0));
	std::cout << " nmpp:" << nmpp << " nmDp:" << nmDp <<  " ncosTheta_X:" << ncosTheta_X << " ncosTheta_L:" << ncosTheta_L << " ncosTheta_p:" << ncosTheta_p << " ncosTheta_pbar:" << ncosTheta_pbar << std::endl;

	DataBins oldbins;
	DataBins newbins;
	oldbins.reserve(pow(2, nmpp+nmDp+ncosTheta_X+ncosTheta_L+ncosTheta_p+ncosTheta_pbar));
	newbins.reserve(pow(2, nmpp+nmDp+ncosTheta_X+ncosTheta_L+ncosTheta_p+ncosTheta_pbar));
	//oldbins.reserve(pow(nmpp+nmDp+ncosTheta_X+ncosTheta_L+ncosTheta_p+ncosTheta_pbar, 2));
	//newbins.reserve(pow(nmpp+nmDp+ncosTheta_X+ncosTheta_L+ncosTheta_p+ncosTheta_pbar, 2));
	oldbins.push_back(StartBin);
	
	for (int i = 0;i < 4;i++) {
		if (plot) {
      		if (nmpp > 0) {
        		std::cout << " divde mpp oldbins size= " << oldbins.size() << std::endl; 
        		newbins = Dividempp(oldbins);
        		std::cout << " divde mpp newbins size= " << newbins.size() << std::endl; 
        		oldbins = newbins;
        		nmpp--;
				cout<<"nmpp: "<<i<<": "<<nmpp<<endl;
	  		}
			if (nmDp > 0) {
        		std::cout << " divde nmDp oldbins size= " << oldbins.size() << std::endl; 
        		newbins = DividemDp(oldbins);
        		std::cout << " divde nmDp newbins size= " << newbins.size() << std::endl; 
        		oldbins = newbins;
        		nmDp--;
				cout<<"nmDp: "<<i<<": "<<nmDp<<endl;
			}
		}
		else {
			if (nmpp > 0) {
				//std::cout << " divde mL oldbins size= " << oldbins.size() << std::endl; 
				newbins = Dividempp(oldbins);
				oldbins = newbins;
				//std::cout << " divde mL newbins size= " << newbins.size() << std::endl; 
				nmpp--;
			}
			if (nmDp > 0) {
				//std::cout << " divde mJpsip oldbins size= " << oldbins.size() << std::endl; 
				newbins = DividemDp(oldbins);
				oldbins = newbins;
				//std::cout << " divde mJpsip newbins size= " << newbins.size() << std::endl; 
				nmDp--;
			}
			if (ncosTheta_X > 0) {
        		//std::cout << " divde cosRes oldbins size= " << oldbins.size() << std::endl; 
        		newbins = DividecosTheta_X(oldbins);
        		//std::cout << " divde cosRes newbins size= " << newbins.size() << std::endl; 
        		oldbins = newbins;
        		ncosTheta_X--;
			}
			if (ncosTheta_L > 0) {
        		//std::cout << " divde cosRes oldbins size= " << oldbins.size() << std::endl; 
        		newbins = DividecosTheta_L(oldbins);
        		//std::cout << " divde cosRes newbins size= " << newbins.size() << std::endl; 
        		oldbins = newbins;
        		ncosTheta_L--;
			}
			if (ncosTheta_p > 0) {
        		//std::cout << " divde cosRes oldbins size= " << oldbins.size() << std::endl; 
        		newbins = DividecosTheta_p(oldbins);
        		//std::cout << " divde cosRes newbins size= " << newbins.size() << std::endl; 
        		oldbins = newbins;
        		ncosTheta_p--;
			}
			if (ncosTheta_pbar > 0) {
        		//std::cout << " divde cosRes oldbins size= " << oldbins.size() << std::endl; 
        		newbins = DividecosTheta_pbar(oldbins);
        		//std::cout << " divde cosRes newbins size= " << newbins.size() << std::endl; 
        		oldbins = newbins;
        		ncosTheta_pbar--;
			}
		}
	}
	std::cout << "hey!" << std::endl;
	TH2Poly* hpoly = new TH2Poly("dev_mppvsmDp", "dev_mppvsmDp", (mpp_min - 0.005), (mpp_max + 0.005), (mDp_min - 0.005), (mDp_max + 0.005));
	TH2Poly* hpoly_dat = new TH2Poly("dat_mppvsmDp", "dat_mppvsmDp", (mpp_min - 0.005), (mpp_max + 0.005), (mDp_min - 0.005), (mDp_max + 0.005));
	TH2Poly* hpoly_fit = new TH2Poly("fit_mppvsmDp", "fit_mppvsmDp", (mpp_min - 0.005), (mpp_max + 0.005), (mDp_min - 0.005), (mDp_max + 0.005));
	hpoly->Sumw2();
	hpoly_dat->Sumw2();
	hpoly_fit->Sumw2();

  	//double bwmpp = ((mpp_max + 0.005) - ((mpp_min + 0.005))) / 100.0;
  	//double bwmDp = ((mDp_max + 0.005) - ((mDp_min + 0.005))) / 100.0;
	for (DataBins::iterator ibin = oldbins.begin();ibin != oldbins.end();ibin++) {
		hpoly->AddBin((*ibin).GetMin().Getmpp(), (*ibin).GetMin().GetmDp(), (*ibin).GetMax().Getmpp(), (*ibin).GetMax().GetmDp());
		hpoly_dat->AddBin((*ibin).GetMin().Getmpp(), (*ibin).GetMin().GetmDp(), (*ibin).GetMax().Getmpp(), (*ibin).GetMax().GetmDp());
		hpoly_fit->AddBin((*ibin).GetMin().Getmpp(), (*ibin).GetMin().GetmDp(), (*ibin).GetMax().Getmpp(), (*ibin).GetMax().GetmDp());
		//cout<<"mppmin: "<<(*ibin).GetMin().Getmpp()<<" mDpmin: "<<(*ibin).GetMin().GetmDp()<<" mppmax: "<<(*ibin).GetMax().Getmpp()<<" mDpmax: "<<(*ibin).GetMax().GetmDp()<<endl;
	}

   	DataPoints mcpoints;
   	std::cout << "Import MC from: " << MCloc << std::endl;
	double MC_mpp, MC_mDp, MC_mDpbar, MC_cosTheta_X, MC_cosTheta_L, MC_cosTheta_p, MC_cosTheta_pbar, MC_sig_sw, Amp;

  	TChain* smcdata = new TChain("tree");
	smcdata->Add(MCloc);
    smcdata->SetBranchAddress("mppbar", &MC_mpp);
    smcdata->SetBranchAddress("mD0p", &MC_mDp);
    smcdata->SetBranchAddress("mD0pbar", &MC_mDpbar);
    smcdata->SetBranchAddress("cosTheta_X", &MC_cosTheta_X);
    smcdata->SetBranchAddress("cosTheta_L", &MC_cosTheta_L);
    smcdata->SetBranchAddress("cosTheta_p", &MC_cosTheta_p);
    smcdata->SetBranchAddress("cosTheta_pbar", &MC_cosTheta_pbar);
	smcdata->SetBranchAddress("w", &MC_sig_sw);
	
	Amp = 1.0;
	cout << "MC Entries: " << smcdata->GetEntries() << endl;

	double suminte = 0.0;
	for (int i = 0;i < smcdata->GetEntries();i++) {
		smcdata->GetEntry(i);
		if (MC_mpp * MC_mpp < mpp_min) continue;
		if (MC_mpp * MC_mpp > mpp_max) continue;
		if (MC_mDp * MC_mDp < mDp_min) continue;
		if (MC_mDp * MC_mDp > mDp_max) continue;
		suminte += MC_sig_sw * Amp;
	}
	
	for (int i = 0;i < smcdata->GetEntries();i++) {
		smcdata->GetEntry(i);
		if (MC_mpp * MC_mpp < mpp_min) continue;
		if (MC_mpp * MC_mpp > mpp_max) continue;
		if (MC_mDp * MC_mDp < mDp_min) continue;
		if (MC_mDp * MC_mDp > mDp_max) continue;
		
		histmc->Fill((MC_mpp) * (MC_mpp), (MC_mDp) * (MC_mDp), MC_sig_sw * Amp * data_sumWeight / suminte);
		DataPoint dp(MC_mpp*MC_mpp, MC_mDp*MC_mDp, MC_mDpbar*MC_mDpbar, fabs(MC_cosTheta_X), fabs(MC_cosTheta_L), fabs(MC_cosTheta_p), fabs(MC_cosTheta_pbar), MC_sig_sw * Amp * data_sumWeight / suminte);
		mcpoints.push_back(dp);
	}


  	///check split everything OK
  	int final_nBin = oldbins.size();
  	double total_sig = 0; double total_bkg = 0;
  	for (int ib = 0; ib < final_nBin; ib++) {
    	total_sig += oldbins[ib].GetNumber();
    	total_bkg += oldbins[ib].GetBkgNumber();
    	//printf("Bin %i: sig %f, bkg %f \n",ib, oldbins[ib].GetNumber(), oldbins[ib].GetBkgNumber());
  	}
  	printf("total sig %f, total bkg %f \n", total_sig, total_bkg);

	cout << "plots" << endl;
  	if (plot) {
		TCanvas* cd = new TCanvas("cd");
    	gPad->SetLogz(1);
    	gStyle->SetPalette(1);
		hist->GetXaxis()->SetTitle("m^{2}(p#bar{p}) (GeV^{2})");
		hist->GetYaxis()->SetTitle("m^{2}(Dp) (GeV^{2})");
    	hist->DrawCopy("colz");
    	//cd->SaveAs(TString("data_").Append(label).Append(TString(".C")));
    	//cd->SaveAs(TString("data_").Append(label).Append(TString(".pdf")));
    	delete cd;
    	delete hist;
		TCanvas* cdmc = new TCanvas("cdmc");
    	gPad->SetLogz(1);
    	gStyle->SetPalette(1);
		histmc->GetXaxis()->SetTitle("m^{2}(p#bar{p}) (GeV^{2})");
		histmc->GetYaxis()->SetTitle("m^{2}(Dp) (GeV^{2})");
    	histmc->DrawCopy("colz");
    	//cdmc->SaveAs(TString("mc_").Append(label).Append(TString(".C")));
    	//cdmc->SaveAs(TString("mc_").Append(label).Append(TString(".pdf")));

    	delete histmc;
    	delete cdmc;

    	if (hist_b) {
      		TCanvas* cdbkg = new TCanvas("cdbkg");
      		gPad->SetLogz(1);
      		gStyle->SetPalette(1);
			hist_b->GetXaxis()->SetTitle("m^{2}(p#bar{p}) (GeV^{2})");
			hist_b->GetYaxis()->SetTitle("m^{2}(Dp) (GeV^{2})");
      		hist_b->DrawCopy("colz");
      		//cdbkg->SaveAs(TString("bkg_").Append(label).Append(TString(".C")));
      		//cdbkg->SaveAs(TString("bkg_").Append(label).Append(TString(".pdf")));
      		delete cdbkg;
      		delete hist_b;
		}
	}

	double realnum[10000], mcnum[10000], bnum[10000];
	double realerr[10000], mcerr[10000], berr[10000];
  	for (int i = 0;i < 10000;i++) {
    	realnum[i] = mcnum[i] = bnum[i] = 0;
    	realerr[i] = mcerr[i] = berr[i] = 0;
	}

  	int index = 0;
  	for (DataBins::iterator ibin = oldbins.begin();ibin != oldbins.end();ibin++) {
		realnum[index] = (*ibin).GetNumber();
    	realerr[index] = (*ibin).GetNumErr();
		for (DataPoints::iterator ip = mcpoints.begin();ip != mcpoints.end();ip++) {
			if ((*ibin).Contains((*ip))) {
				mcnum[index] += (*ip).Getweight();
				mcerr[index] += (*ip).Getweight() * (*ip).Getweight();
			}
		}
		mcerr[index] = sqrt(mcerr[index]);
		if (sideband_tree) {
			bnum[index] = (*ibin).GetBkgNumber() * scale_factor;
      		berr[index] = sqrt(bnum[index]);
    	}
		index++;
  	}

  	TH1D* reslist = new TH1D("reslist", "reslist", oldbins.size(), 0.0, oldbins.size());
  	TH1D* datlist = new TH1D("datlist", "datlist", oldbins.size(), 0.0, oldbins.size());
  	TH1D* fitlist = new TH1D("fitlist", "fitlist", oldbins.size(), 0.0, oldbins.size());
  	reslist->Sumw2();
  	datlist->Sumw2();
  	fitlist->Sumw2();

  	TH1D* hgauss = new TH1D("hgauss", "", 1000, -10, 10);
  	double chi2 = 0;
  	double chi21 = 0;
  	double ndof = oldbins.size();
  	for (int i = 0;i < oldbins.size();i++) {
		double dev = (realnum[i] - mcnum[i]) / sqrt(realerr[i] * realerr[i] + mcerr[i] * mcerr[i] + berr[i] * berr[i]);
		DataBin& bi = oldbins[i];
		#if 0    
    	if (fabs(dev) > 3.0) {
			cout << realnum[i] << " +/- " << realerr[i] << " ; mc " << mcnum[i] << " +/- " << mcerr[i] << " berr " << berr[i] << " sig " << dev;
			cout << " mpp " << bi.GetMin().Getmpp() << "  " << bi.GetMax().Getmpp() << " mDp " << bi.GetMin().GetmDp() << " " << bi.GetMax().GetmDp() << endl;
			if (!plot) {
				cout << " cosTheta_X " << bi.GetMin().GetcosTheta_X() << "  " << bi.GetMax().GetcosTheta_X() << " cosTheta_L " << bi.GetMin().GetcosTheta_L() << "  " << bi.GetMax().GetcosTheta_L() << " cosTheta_p " << bi.GetMin().GetcosTheta_p() << "  " << bi.GetMax().GetcosTheta_p() << " cosTheta_pbar " << bi.GetMin().GetcosTheta_pbar() << " " << bi.GetMax().GetcosTheta_pbar() << endl;
			}
			cout << endl;
		}
		#endif
    	chi2 += dev * dev;
    	hgauss->Fill(dev);
		hpoly->SetBinContent(i + 1, dev);

    	double mppbw = (-bi.GetMin().Getmpp() + bi.GetMax().Getmpp());
    	double mDpbw = (-bi.GetMin().GetmDp() + bi.GetMax().GetmDp());
    	double area = mppbw * mDpbw;
    	hpoly_dat->SetBinContent(i + 1, realnum[i] / area);
    	hpoly_dat->SetBinError(i + 1, sqrt(pow(realerr[i], 2) + pow(berr[i], 2)) / area);
    	hpoly_fit->SetBinContent(i + 1, mcnum[i] / area);
    	hpoly_fit->SetBinError(i + 1, mcerr[i] / area);
		if ((realnum[i] > 0.0) && (mcnum[i] > 0.0))
			chi21 += -2. * realnum[i] * log(mcnum[i] / realnum[i]);
		reslist->SetBinContent(i + 1, realnum[i] - mcnum[i]);
		reslist->SetBinError(i + 1, sqrt(realerr[i] * realerr[i] + mcerr[i] * mcerr[i] + berr[i] * berr[i]));
    	datlist->SetBinContent(i + 1, realnum[i]);
   		datlist->SetBinError(i + 1, sqrt(realerr[i] * realerr[i] + berr[i] * berr[i]));
   		fitlist->SetBinContent(i + 1, mcnum[i]);
    	fitlist->SetBinError(i + 1, mcerr[i]);
	}
	cout << " chi2/ndof = " << chi2 << " / " << ndof << " other " << chi21 << endl;


	TCanvas* c1n = new TCanvas("c1n");
  	hgauss->Draw();
  	hgauss->Fit("gaus", "wwL");
 	c1n->SaveAs(TString("residue_").Append(label).Append(TString(".C")));
  	c1n->SaveAs(TString("residue_").Append(label).Append(TString(".pdf")));
	delete c1n;

    {
		TCanvas* c2reslist = new TCanvas("c2reslist");
      	c2reslist->cd(0);
      	reslist->Draw();
      	//c2reslist->SaveAs(TString("reslist_").Append(label).Append(TString(".C")));
      	//c2reslist->SaveAs(TString("reslist_").Append(label).Append(TString(".pdf")));
      	delete c2reslist;
	}
    {
      	TCanvas* c2datlist = new TCanvas("c2datlist");
      	c2datlist->cd(0);
      	datlist->Draw();
      	//c2datlist->SaveAs(TString("datlist_").Append(label).Append(TString(".C")));
      	//c2datlist->SaveAs(TString("datlist_").Append(label).Append(TString(".pdf")));
      	delete c2datlist;
    }
    {
      	TCanvas* c2fitlist = new TCanvas("c2fitlist");
      	c2fitlist->cd(0);
      	fitlist->Draw();
      	//c2fitlist->SaveAs(TString("fitlist_").Append(label).Append(TString(".C")));
      	//c2fitlist->SaveAs(TString("fitlist_").Append(label).Append(TString(".pdf")));
      	delete c2fitlist;
    }
	TCanvas* c2 = new TCanvas("c2");
	c2->cd(0);


    hpoly->GetXaxis()->SetTitle("m^{2}(p#bar{p}) (GeV^{2})");
    hpoly->GetYaxis()->SetTitle("m^{2}(Dp) (GeV^{2})");

    //color, 51-100,range, -3,3
    hpoly->SetMinimum(-4);
    hpoly->SetMaximum(4);
    double hmin = hpoly->GetMinimum();
    double hmax = hpoly->GetMaximum();
    gStyle->SetPalette(1);
    //  cout<<"orginal max, min"<<hmin<<" "<<hmax<<endl;
    // cout<<"a, b"<<49./(hmax-hmin)<<" "<<(51*hmax-100*hmin)/(hmax-hmin)<<endl;
    int cmin = int(7.91 * hmin + 79.7);
    int cmax = int(7.91 * hmax + 79.7);

    int NewPalette[50];
    for (int i = cmin; i < cmax;i++) {
      	NewPalette[i - cmin] = i;
    }
    //  gStyle->SetPalette(cmax-cmin,NewPalette);

    hpoly->Draw("colz");
	TLatex *lat1 = new TLatex();
	lat1->SetTextFont(42);
	lat1->DrawLatex(8.5,17.5,Form("#chi^{2} / ndof = %4.1f / %1.f", chi2, ndof));
	//lat1->DrawLatex(10.5,17,"chi2");

	
    if (kine_max != 0) {
      	TF1* fmax = new TF1("fmax", kine_max, (mpp_min - 0.0005), (mpp_max + 0.0005), 0);
      	TF1* fmin = new TF1("fmin", kine_min, (mpp_min - 0.0005), (mpp_max + 0.0005), 0);
      	fmin->SetLineColor(0);
      	fmax->SetLineColor(0);
      	fmin->SetFillColor(0);
      	fmax->SetFillColor(0);
      	fmin->Draw("same");
      	fmax->Draw("same");
    }

    //c2->SaveAs(TString("resmap_").Append(label).Append(TString(".C")));
    c2->SaveAs(TString("resmap_").Append(label).Append(TString(".pdf")));
    delete c2;
	
	{
      	TCanvas* c2 = new TCanvas("c2dat");
      	c2->cd(0);
		hpoly_dat->GetXaxis()->SetTitle("m^{2}(p#bar{p}) (GeV^{2})");
		hpoly_dat->GetYaxis()->SetTitle("m^{2}(Dp) (GeV^{2})");
      	hpoly_dat->Draw("colz");
		if (kine_max != 0) {
        	TF1* fmax = new TF1("fmax", kine_max, (mpp_min - 0.005), (mpp_max + 0.005), 0);
        	TF1* fmin = new TF1("fmin", kine_min, (mpp_min - 0.005), (mpp_max + 0.005), 0);
        	fmin->SetLineColor(0);
        	fmax->SetLineColor(0);
        	fmin->SetFillColor(0);
        	fmax->SetFillColor(0);
        	fmin->Draw("same");
        	fmax->Draw("same");
		}
		//c2->SaveAs(TString("mapdat_").Append(label).Append(TString(".C")));
		c2->SaveAs(TString("mapdat_").Append(label).Append(TString(".pdf")));
      	delete c2;
	}
    {
		TCanvas* c2 = new TCanvas("c2fit");
      	c2->cd(0);

		hpoly_fit->GetXaxis()->SetTitle("m^{2}(p#bar{p}) (GeV^{2})");
		hpoly_fit->GetYaxis()->SetTitle("m^{2}(Dp) (GeV^{2})");
      	hpoly_fit->Draw("colz");
		if (kine_max != 0) {
        	TF1* fmax = new TF1("fmax", kine_max, (mpp_min - 0.005), (mpp_max + 0.005), 0);
        	TF1* fmin = new TF1("fmin", kine_min, (mpp_min - 0.005), (mpp_max + 0.005), 0);
        	fmin->SetLineColor(0);
        	fmax->SetLineColor(0);
        	fmin->SetFillColor(0);
        	fmax->SetFillColor(0);
        	fmin->Draw("same");
        	fmax->Draw("same");
		}
		//c2->SaveAs(TString("mapfit_").Append(label).Append(TString(".C")));
		c2->SaveAs(TString("mapfit_").Append(label).Append(TString(".pdf")));
      	delete c2;
	}

}

DataBins Dividempp(DataBins& oldbins)
{
	DataBins newbins;
	for (DataBins::iterator ibin = oldbins.begin(); ibin != oldbins.end(); ibin++) {
		DataBins temp = (*ibin).Dividempp();
		//cout<<"temp.size() "<<temp.size()<<endl;
		if (temp.size() == 2) {
			if (temp[0].GetBkgNumber() + temp[1].GetBkgNumber() != (*ibin).GetBkgNumber()) {
				std::cout << "Bad bkg split in dividmkp" << std::endl;
				std::cout << temp[0].GetBkgNumber() << ", " << temp[1].GetBkgNumber() << ", " << (*ibin).GetBkgNumber() << std::endl;
			}
			if (TMath::Nint(temp[0].GetNumber() + temp[1].GetNumber()) != TMath::Nint((*ibin).GetNumber())) {
				std::cout << "Bad signal split in dividmkp" << std::endl;
			}
		}
		for (DataBins::iterator itmp = temp.begin(); itmp != temp.end(); itmp++) {
			newbins.push_back((*itmp));
		}
	}
	return newbins;
}

DataBins DividemDp(DataBins& oldbins)
{
	DataBins newbins;
	for (DataBins::iterator ibin = oldbins.begin(); ibin != oldbins.end(); ibin++) {
		DataBins temp = (*ibin).DividemDp();
		if (temp.size() == 2) {
			if (temp[0].GetBkgNumber() + temp[1].GetBkgNumber() != (*ibin).GetBkgNumber()) {
				std::cout << "Bad bkg split in dividmkp" << std::endl;
				std::cout << temp[0].GetBkgNumber() << ", " << temp[1].GetBkgNumber() << ", " << (*ibin).GetBkgNumber() << std::endl;
			}
			if (TMath::Nint(temp[0].GetNumber() + temp[1].GetNumber()) != TMath::Nint((*ibin).GetNumber())) {
				std::cout << "Bad signal split in dividmkp" << std::endl;
			}
		}
		for (DataBins::iterator itmp = temp.begin(); itmp != temp.end(); itmp++) {
			newbins.push_back((*itmp));
		}
	}
	return newbins;
}

DataBins DividecosTheta_X(DataBins& oldbins)
{
	DataBins newbins;
	for (DataBins::iterator ibin = oldbins.begin(); ibin != oldbins.end(); ibin++) {
		DataBins temp = (*ibin).DividecosTheta_X();
		if (temp.size() == 2) {
			if (temp[0].GetBkgNumber() + temp[1].GetBkgNumber() != (*ibin).GetBkgNumber()) {
				std::cout << "Bad bkg split in dividmkp" << std::endl;
				std::cout << temp[0].GetBkgNumber() << ", " << temp[1].GetBkgNumber() << ", " << (*ibin).GetBkgNumber() << std::endl;
			}
			if (TMath::Nint(temp[0].GetNumber() + temp[1].GetNumber()) != TMath::Nint((*ibin).GetNumber())) {
				std::cout << "Bad signal split in dividmkp" << std::endl;
			}
		}
		for (DataBins::iterator itmp = temp.begin(); itmp != temp.end(); itmp++) {
			newbins.push_back((*itmp));
		}
	}
	return newbins;
}

DataBins DividecosTheta_L(DataBins& oldbins)
{
	DataBins newbins;
	for (DataBins::iterator ibin = oldbins.begin(); ibin != oldbins.end(); ibin++) {
		DataBins temp = (*ibin).DividecosTheta_L();
		if (temp.size() == 2) {
			if (temp[0].GetBkgNumber() + temp[1].GetBkgNumber() != (*ibin).GetBkgNumber()) {
				std::cout << "Bad bkg split in dividmkp" << std::endl;
				std::cout << temp[0].GetBkgNumber() << ", " << temp[1].GetBkgNumber() << ", " << (*ibin).GetBkgNumber() << std::endl;
			}
			if (TMath::Nint(temp[0].GetNumber() + temp[1].GetNumber()) != TMath::Nint((*ibin).GetNumber())) {
				std::cout << "Bad signal split in dividmkp" << std::endl;
			}
		}
		for (DataBins::iterator itmp = temp.begin(); itmp != temp.end(); itmp++) {
			newbins.push_back((*itmp));
		}
	}
	return newbins;
}

DataBins DividecosTheta_p(DataBins& oldbins)
{
	DataBins newbins;
	for (DataBins::iterator ibin = oldbins.begin(); ibin != oldbins.end(); ibin++) {
		DataBins temp = (*ibin).DividecosTheta_p();
		if (temp.size() == 2) {
			if (temp[0].GetBkgNumber() + temp[1].GetBkgNumber() != (*ibin).GetBkgNumber()) {
				std::cout << "Bad bkg split in dividmkp" << std::endl;
				std::cout << temp[0].GetBkgNumber() << ", " << temp[1].GetBkgNumber() << ", " << (*ibin).GetBkgNumber() << std::endl;
			}
			if (TMath::Nint(temp[0].GetNumber() + temp[1].GetNumber()) != TMath::Nint((*ibin).GetNumber())) {
				std::cout << "Bad signal split in dividmkp" << std::endl;
			}
		}
		for (DataBins::iterator itmp = temp.begin(); itmp != temp.end(); itmp++) {
			newbins.push_back((*itmp));
		}
	}
	return newbins;
}

DataBins DividecosTheta_pbar(DataBins& oldbins)
{
	DataBins newbins;
	for (DataBins::iterator ibin = oldbins.begin(); ibin != oldbins.end(); ibin++) {
		DataBins temp = (*ibin).DividecosTheta_pbar();
		if (temp.size() == 2) {
			if (temp[0].GetBkgNumber() + temp[1].GetBkgNumber() != (*ibin).GetBkgNumber()) {
				std::cout << "Bad bkg split in dividmkp" << std::endl;
				std::cout << temp[0].GetBkgNumber() << ", " << temp[1].GetBkgNumber() << ", " << (*ibin).GetBkgNumber() << std::endl;
			}
			if (TMath::Nint(temp[0].GetNumber() + temp[1].GetNumber()) != TMath::Nint((*ibin).GetNumber())) {
				std::cout << "Bad signal split in dividmkp" << std::endl;
			}
		}
		for (DataBins::iterator itmp = temp.begin(); itmp != temp.end(); itmp++) {
			newbins.push_back((*itmp));
		}
	}
	return newbins;
}

double foldPhiAngle(double phiAngle) {
	double foldedAngle;
	if (phiAngle > 0) {
		if (phiAngle < PI / 2) {
			foldedAngle = phiAngle;
		}
		else {
			foldedAngle = PI - phiAngle; //D_phiX_Res - 2*(D_phiX_Res - PI/2);
		}
	}
  	else {
    	if (phiAngle > -PI / 2) {
      		foldedAngle = -phiAngle;
    	}
    	else {
      		foldedAngle = PI + phiAngle;
    	}
  	}
	return foldedAngle;
}
