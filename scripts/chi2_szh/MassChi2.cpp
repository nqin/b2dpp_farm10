//#include "DataBin.h"
//#include "DataPoint.h"
//#define SMCInfo_cxx
//#define DrawClass_cxx
//#include "CGD/SMCInfo.h"
//#include "DrawClass.h"
#include <vector>
#include <algorithm>
#include <iostream>
#include "TStyle.h"
#include "TSystem.h"
#include "TH1D.h"
#include "TH2.h"
#include "TF1.h"
#include "TString.h"
#include "RooArgSet.h"
#include "RooDataSet.h"
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TObject.h>
#include <TNtuple.h>
#include <TDirectory.h>
#include <TCanvas.h>
#include <TH2Poly.h>
#include <TColor.h>
#include "TMath.h"

#include <cmath>
#include "TBox.h"
#include "RooRealVar.h"
using namespace std;

//#define MINPOINTS 20
#define PI 3.14159265

class DataPoint
{
public:
  DataPoint() { mKp = mJpsip = CosThL = CosThLb = CosThJpsi = phiMu = phiK = weight = 0; };
  DataPoint(double, double, double, double, double, double, double, double);
  void SetValues(double, double, double, double, double, double, double, double);
  double GetmKp() const { return mKp; }
  double GetmJpsip() const { return mJpsip; }
  double GetCosThL() const { return CosThL; }
  double GetCosThLb() const { return CosThLb; }
  double GetCosThJpsi() const { return CosThJpsi; }
  double GetphiMu() const { return phiMu; }
  double GetphiK() const { return phiK; }
  double Getweight() const { return weight; }

private:

  double mKp;
  double mJpsip;
  double CosThL;
  double CosThLb;
  double CosThJpsi;
  double phiMu;
  double phiK;
  double weight;
  double minpoints;



};

void DataPoint::SetValues(double a, double b, double c, double d, double e, double f, double g, double h)
{
  mKp = a;
  mJpsip = b;
  CosThL = c;
  CosThLb = d;
  CosThJpsi = e;
  phiMu = f;
  phiK = g;
  weight = h;
}

DataPoint::DataPoint(double a, double b, double c, double d, double e, double f, double g, double h)
{
  mKp = a;
  mJpsip = b;
  CosThL = c;
  CosThLb = d;
  CosThJpsi = e;
  phiMu = f;
  phiK = g;
  weight = h;
}

typedef std::vector<DataPoint> DataPoints;


bool ComparemKp(DataPoint a, DataPoint b)
{
  return a.GetmKp() < b.GetmKp();
}
bool ComparemJpsip(DataPoint a, DataPoint b)
{
  return a.GetmJpsip() < b.GetmJpsip();
}
bool CompareCosThL(DataPoint a, DataPoint b)
{
  return a.GetCosThL() < b.GetCosThL();
}
bool CompareCosThLb(DataPoint a, DataPoint b)
{
  return a.GetCosThLb() < b.GetCosThLb();
}
bool CompareCosThJpsi(DataPoint a, DataPoint b)
{
  return a.GetCosThJpsi() < b.GetCosThJpsi();
}
bool ComparephiMu(DataPoint a, DataPoint b)
{
  return a.GetphiMu() < b.GetphiMu();
}
bool ComparephiK(DataPoint a, DataPoint b)
{
  return a.GetphiK() < b.GetphiK();
}



class DataBin
{
public:
  DataBin(double minpoints) {
    pmin = DataPoint(0, 0, 0, 0, 0, 0, 0, 0);
    pmax = DataPoint(0, 0, 0, 0, 0, 0, 0, 0);
    MINPOINTS = minpoints;
  }
  DataBin(DataPoint& a, DataPoint& b, double minpoints) { pmin = a; pmax = b; MINPOINTS = minpoints; }
  double GetNumber() const {
    double total = 0;
    for (DataPoints::const_iterator ip = points.begin();ip != points.end();ip++) {
      total += (*ip).Getweight();
    }
    return total;
  }
  double GetBkgNumber() const {
    return bkg_points.size();
  }
  double GetNumErr() const {
    double total = 0;
    for (DataPoints::const_iterator ip = points.begin();ip != points.end();ip++) {
      total += (*ip).Getweight() * (*ip).Getweight();
    }
    return sqrt(total);
  }
  bool ZeroArea() const;
  void SetMin(DataPoint& a) { pmin = a; }
  void SetMax(DataPoint& b) { pmax = b; }
  DataPoint GetMin() const { return pmin; }
  DataPoint GetMax() const { return pmax; }
  void DrawDalitz() const;
  bool Contains(DataPoint&) const;
  void AddPoint(DataPoint& a) { points.push_back(a); }
  void AddBkgPoint(DataPoint& a) { bkg_points.push_back(a); } //NEWADD
  DataPoints GetPoints() const { return points; }
  std::vector<DataBin> DividemKp();
  std::vector<DataBin> DividemJpsip();
  std::vector<DataBin> DivideCosThL();
  std::vector<DataBin> DivideCosThJpsi();
  std::vector<DataBin> DivideCosThLb();
  std::vector<DataBin> DividephiMu();
  std::vector<DataBin> DividephiK();

private:
  DataPoint pmin;
  DataPoint pmax;
  DataPoints points;
  DataPoints bkg_points; //NEWADD
  double MINPOINTS;
};

bool DataBin::ZeroArea() const
{
  return (abs(pmin.GetmKp() - pmax.GetmKp()) < 0.0000001)
    || (abs(pmin.GetmJpsip() - pmax.GetmJpsip()) < 0.0000001)
    || (abs(pmin.GetCosThL() - pmax.GetCosThL()) < 0.0000001)
    || (abs(pmin.GetCosThLb() - pmax.GetCosThLb()) < 0.0000001)
    || (abs(pmin.GetCosThJpsi() - pmax.GetCosThJpsi()) < 0.0000001)
    || (abs(pmin.GetphiMu() - pmax.GetphiMu()) < 0.0000001)
    || (abs(pmin.GetphiK() - pmax.GetphiK()) < 0.0000001);
}


void DataBin::DrawDalitz() const
{
  TBox b;
  b.SetFillStyle(0);
  b.DrawBox(pmin.GetmKp(), pmin.GetmJpsip(), pmax.GetmKp(), pmax.GetmJpsip());
}


bool DataBin::Contains(DataPoint& a) const
{
  bool passmKp = (a.GetmKp() >= pmin.GetmKp() && a.GetmKp() <= pmax.GetmKp());
  bool passmJpsip = (a.GetmJpsip() >= pmin.GetmJpsip() && a.GetmJpsip() <= pmax.GetmJpsip());
  bool passCosThL = (a.GetCosThL() >= pmin.GetCosThL() && a.GetCosThL() <= pmax.GetCosThL());
  bool passCosThLb = (a.GetCosThLb() >= pmin.GetCosThLb() && a.GetCosThLb() <= pmax.GetCosThLb());
  bool passCosThJpsi = (a.GetCosThJpsi() >= pmin.GetCosThJpsi() && a.GetCosThJpsi() <= pmax.GetCosThJpsi());
  bool passphiMu = (a.GetphiMu() >= pmin.GetphiMu() && a.GetphiMu() <= pmax.GetphiMu());
  bool passphiK = (a.GetphiK() >= pmin.GetphiK() && a.GetphiK() <= pmax.GetphiK());
  return (passmKp && passmJpsip && passCosThL && passCosThLb && passCosThJpsi && passphiMu && passphiK);
  //*a.Getweight();
}

std::vector<DataBin> DataBin::DividemKp()
{

  std::vector<DataBin>  newbins;

  int npoints = points.size();
  int bkg_npoints = bkg_points.size();
  double sumW = GetNumber();
  double sig_points = sumW;
  bool isCFit = false;
  if (int(sumW) == npoints) {
    isCFit = true;
    if (bkg_npoints != 0) { sig_points = sumW - bkg_npoints * ((bkg_points[0]).Getweight()); }
    else { sig_points = sumW; }
  }

  //printf("npoints: %i, bkg_npoints: %i, sumW: %f, sig_points: %f\n",npoints, bkg_npoints, sumW, sig_points);


  if (sig_points < MINPOINTS) {
    newbins.push_back(*this);//dont split
  }
  else {
    sort(points.begin(), points.end(), ComparemKp);
    if (bkg_npoints != 0)sort(bkg_points.begin(), bkg_points.end(), ComparemKp);

    double critical_point = sumW / 2;
    double i_sumW = 0;
    int split_point = 0;
    while (i_sumW < critical_point) {
      i_sumW += points[split_point].Getweight();
      split_point += 1;
    }
    //  printf("split point: %i\n", split_point);


    DataPoint midpoint = points[split_point - 1];
    DataPoint nexpoint = points[split_point];
    double midmKp = (midpoint.GetmKp() + nexpoint.GetmKp()) / 2.;

    int bkg_split_point = 0;

    if (isCFit && bkg_npoints != 0) {
      while (bkg_points[bkg_split_point].GetmKp() < midmKp && bkg_split_point < bkg_npoints) {
        bkg_split_point++;
      }
    }

    //  printf("bkg split point: %i\n", bkg_split_point);

    DataPoint minmKp(midmKp, pmin.GetmJpsip(), pmin.GetCosThL(), pmin.GetCosThLb(), pmin.GetCosThJpsi(), pmin.GetphiMu(), pmin.GetphiK(), 0);
    DataPoint maxmKp(midmKp, pmax.GetmJpsip(), pmax.GetCosThL(), pmax.GetCosThLb(), pmax.GetCosThJpsi(), pmax.GetphiMu(), pmax.GetphiK(), 0);

    DataBin lowmKp(pmin, maxmKp, MINPOINTS);
    for (int i = 0;i < split_point;i++) {
      lowmKp.AddPoint(points[i]);
    }

    if (isCFit && bkg_npoints != 0) {
      for (int i = 0;i < bkg_split_point;i++) {
        lowmKp.AddBkgPoint(bkg_points[i]);
      }
    }


    if (lowmKp.GetNumber() < critical_point / 2) {
      printf("Somewhat low points\n");
      //newbins.push_back( *this );
      //return newbins;
    }
    newbins.push_back(lowmKp);
    DataBin upmKp(minmKp, pmax, MINPOINTS);
    for (int i = split_point;i < npoints;i++) {
      upmKp.AddPoint(points[i]);
    }

    if (isCFit && bkg_npoints != 0) {
      for (int i = bkg_split_point;i < bkg_npoints;i++) {
        upmKp.AddBkgPoint(bkg_points[i]);
      }
    }

    if (upmKp.GetNumber() < critical_point / 2) {
      std::cout << " low content " << std::endl;
    }
    newbins.push_back(upmKp);
  }
  return newbins;
}




std::vector<DataBin> DataBin::DividemJpsip()
{

  std::vector<DataBin>  newbins;

  int npoints = points.size();
  int bkg_npoints = bkg_points.size();
  double sumW = GetNumber();

  double sig_points = sumW;
  bool isCFit = false;
  if (int(sumW) == npoints) {
    isCFit = true;
    if (bkg_npoints != 0) { sig_points = sumW - bkg_npoints * ((bkg_points[0]).Getweight()); }
    else { sig_points = sumW; }


  }

  //  printf("npoints: %i, bkg_npoints: %i, sumW: %f, sig_points: %f\n",npoints, bkg_npoints, sumW, sig_points);


  if (sig_points < MINPOINTS) {
    newbins.push_back(*this);
  }
  else {
    sort(points.begin(), points.end(), ComparemJpsip);
    if (bkg_npoints != 0)sort(bkg_points.begin(), bkg_points.end(), ComparemJpsip);

    double critical_point = sumW / 2;
    double i_sumW = 0;
    int split_point = 0;
    while (i_sumW < critical_point) {
      i_sumW += points[split_point].Getweight();
      split_point += 1;
    }
    //  printf("split point: %i\n", split_point);


    DataPoint midpoint = points[split_point - 1];
    DataPoint nexpoint = points[split_point];
    double midmJpsip = (midpoint.GetmJpsip() + nexpoint.GetmJpsip()) / 2.;

    int bkg_split_point = 0;

    if (isCFit && bkg_npoints != 0) {
      while (bkg_points[bkg_split_point].GetmJpsip() < midmJpsip && bkg_split_point < bkg_npoints) {
        bkg_split_point++;
      }
    }

    //  printf("bkg split point: %i\n", bkg_split_point);

    DataPoint minmJpsip(pmin.GetmKp(), midmJpsip, pmin.GetCosThL(), pmin.GetCosThLb(), pmin.GetCosThJpsi(), pmin.GetphiMu(), pmin.GetphiK(), 0);
    DataPoint maxmJpsip(pmax.GetmKp(), midmJpsip, pmax.GetCosThL(), pmax.GetCosThLb(), pmax.GetCosThJpsi(), pmax.GetphiMu(), pmax.GetphiK(), 0);



    DataBin lowmJpsip(pmin, maxmJpsip, MINPOINTS);
    for (int i = 0;i < split_point;i++) {
      lowmJpsip.AddPoint(points[i]);
    }

    if (isCFit && bkg_npoints != 0) {
      for (int i = 0;i < bkg_split_point;i++) {
        lowmJpsip.AddBkgPoint(bkg_points[i]);
      }
    }

    if (lowmJpsip.GetNumber() < critical_point / 2) {
      printf("Somewhat low points\n");
      //newbins.push_back( *this );
      //return newbins;
    }
    newbins.push_back(lowmJpsip);
    DataBin upmJpsip(minmJpsip, pmax, MINPOINTS);
    for (int i = split_point;i < npoints;i++) {
      upmJpsip.AddPoint(points[i]);
    }

    if (isCFit && bkg_npoints != 0) {
      for (int i = bkg_split_point;i < bkg_npoints;i++) {
        upmJpsip.AddBkgPoint(bkg_points[i]);
      }
    }

    if (upmJpsip.GetNumber() < critical_point / 2) {
      std::cout << " low content " << std::endl;
    }
    newbins.push_back(upmJpsip);
  }
  return newbins;
}


std::vector<DataBin> DataBin::DivideCosThL()
{

  std::vector<DataBin>  newbins;

  int npoints = points.size();
  int bkg_npoints = bkg_points.size();
  double sumW = GetNumber();

  double sig_points = sumW;
  bool isCFit = false;
  if (int(sumW) == npoints) {
    isCFit = true;
    if (bkg_npoints != 0) { sig_points = sumW - bkg_npoints * ((bkg_points[0]).Getweight()); }
    else { sig_points = sumW; }

  }

  //  printf("npoints: %i, bkg_npoints: %i, sumW: %f, sig_points: %f\n",npoints, bkg_npoints, sumW, sig_points);


  if (sig_points < MINPOINTS) {
    newbins.push_back(*this);
  }
  else {

#if 1
    sort(points.begin(), points.end(), CompareCosThL);
    if (bkg_npoints != 0)sort(bkg_points.begin(), bkg_points.end(), CompareCosThL);

    double critical_point = sumW / 2;
    double i_sumW = 0;
    int split_point = 0;
    while (i_sumW < critical_point) {
      i_sumW += points[split_point].Getweight();
      split_point += 1;
    }
    //  printf("split point: %i\n", split_point);


    DataPoint midpoint = points[split_point - 1];
    DataPoint nexpoint = points[split_point];
    double midCosThL = (midpoint.GetCosThL() + nexpoint.GetCosThL()) / 2.;

    int bkg_split_point = 0;

    if (isCFit && bkg_npoints != 0) {
      while (bkg_points[bkg_split_point].GetCosThL() < midCosThL && bkg_split_point < bkg_npoints) {
        bkg_split_point++;
      }
    }

    //  printf("bkg split point: %i\n", bkg_split_point);

    DataPoint minCosThL(pmin.GetmKp(), pmin.GetmJpsip(), midCosThL, pmin.GetCosThLb(), pmin.GetCosThJpsi(), pmin.GetphiMu(), pmin.GetphiK(), 0);
    DataPoint maxCosThL(pmax.GetmKp(), pmax.GetmJpsip(), midCosThL, pmax.GetCosThLb(), pmax.GetCosThJpsi(), pmax.GetphiMu(), pmax.GetphiK(), 0);



    DataBin lowCosThL(pmin, maxCosThL, MINPOINTS);
    for (int i = 0;i < split_point;i++) {
      lowCosThL.AddPoint(points[i]);
    }

    if (isCFit && bkg_npoints != 0) {
      for (int i = 0;i < bkg_split_point;i++) {
        lowCosThL.AddBkgPoint(bkg_points[i]);
      }
    }

    if (lowCosThL.GetNumber() < critical_point / 2) {
      printf("Somewhat low points\n");
      //newbins.push_back( *this );
      //return newbins;
    }
    newbins.push_back(lowCosThL);
    DataBin upCosThL(minCosThL, pmax, MINPOINTS);
    for (int i = split_point;i < npoints;i++) {
      upCosThL.AddPoint(points[i]);
    }

    if (isCFit && bkg_npoints != 0) {
      for (int i = bkg_split_point;i < bkg_npoints;i++) {
        upCosThL.AddBkgPoint(bkg_points[i]);
      }
    }

    if (upCosThL.GetNumber() < critical_point / 2) {
      std::cout << " low content " << std::endl;
    }
    newbins.push_back(upCosThL);


#else
    // we divide only once
    double midCosThL = 0.7;
    DataPoint minCosThL(pmin.GetmKp(), pmin.GetmJpsip(), midCosThL, pmin.GetCosThLb(), pmin.GetCosThJpsi(), pmin.GetphiMu(), pmin.GetphiK(), 0);
    DataPoint maxCosThL(pmax.GetmKp(), pmax.GetmJpsip(), midCosThL, pmax.GetCosThLb(), pmax.GetCosThJpsi(), pmax.GetphiMu(), pmax.GetphiK(), 0);

    DataBin lowCosThL(pmin, maxCosThL, MINPOINTS);
    DataBin upCosThL(minCosThL, pmax, MINPOINTS);
    for (int i = 0;i < npoints;i++) {
      DataPoint indexp = points[i];
      if (indexp.GetCosThL() < midCosThL) {
        lowCosThL.AddPoint(indexp);
      }
      else {
        upCosThL.AddPoint(indexp);
      }
    }

    for (int i = 0;i < bkg_npoints;i++) {
      DataPoint bkg_indexp = bkg_points[i];
      if (bkg_indexp.GetCosThL() < midCosThL) {
        lowCosThL.AddBkgPoint(bkg_indexp);
      }
      else {
        upCosThL.AddBkgPoint(bkg_indexp);
      }
    }

    newbins.push_back(lowCosThL);
    newbins.push_back(upCosThL);


#endif
  }

  return newbins;
}



std::vector<DataBin> DataBin::DivideCosThLb()
{

  std::vector<DataBin>  newbins;

  int npoints = points.size();
  int bkg_npoints = bkg_points.size();
  double sumW = GetNumber();

  double sig_points = sumW;
  bool isCFit = false;
  if (int(sumW) == npoints) {
    isCFit = true;
    if (bkg_npoints != 0) { sig_points = sumW - bkg_npoints * ((bkg_points[0]).Getweight()); }
    else { sig_points = sumW; }

  }

  //printf("npoints: %i, bkg_npoints: %i, sumW: %f, sig_points: %f\n",npoints, bkg_npoints, sumW, sig_points);


  if (sig_points < MINPOINTS) {
    newbins.push_back(*this);
  }
  else {

#if 1
    sort(points.begin(), points.end(), CompareCosThLb);
    if (bkg_npoints != 0)sort(bkg_points.begin(), bkg_points.end(), CompareCosThLb);

    double critical_point = sumW / 2;
    double i_sumW = 0;
    int split_point = 0;
    while (i_sumW < critical_point) {
      i_sumW += points[split_point].Getweight();
      split_point += 1;
    }
    //printf("split point: %i\n", split_point);


    DataPoint midpoint = points[split_point - 1];
    DataPoint nexpoint = points[split_point];
    double midCosThLb = (midpoint.GetCosThLb() + nexpoint.GetCosThLb()) / 2.;

    int bkg_split_point = 0;

    if (isCFit && bkg_npoints != 0) {
      while (bkg_points[bkg_split_point].GetCosThLb() < midCosThLb && bkg_split_point < bkg_npoints) {
        bkg_split_point++;
      }
    }

    //printf("bkg split point: %i\n", bkg_split_point);

    DataPoint minCosThLb(pmin.GetmKp(), pmin.GetmJpsip(), pmin.GetCosThL(), midCosThLb, pmin.GetCosThJpsi(), pmin.GetphiMu(), pmin.GetphiK(), 0);
    DataPoint maxCosThLb(pmax.GetmKp(), pmax.GetmJpsip(), pmax.GetCosThL(), midCosThLb, pmax.GetCosThJpsi(), pmax.GetphiMu(), pmax.GetphiK(), 0);


    DataBin lowCosThLb(pmin, maxCosThLb, MINPOINTS);
    for (int i = 0;i < split_point;i++) {
      lowCosThLb.AddPoint(points[i]);
    }

    if (isCFit && bkg_npoints != 0) {
      for (int i = 0;i < bkg_split_point;i++) {
        lowCosThLb.AddBkgPoint(bkg_points[i]);
      }
    }

    if (lowCosThLb.GetNumber() < critical_point / 2) {
      printf("Somewhat low points\n");
      //newbins.push_back( *this );
      //return newbins;
    }
    newbins.push_back(lowCosThLb);
    DataBin upCosThLb(minCosThLb, pmax, MINPOINTS);
    for (int i = split_point;i < npoints;i++) {
      upCosThLb.AddPoint(points[i]);
    }

    if (isCFit && bkg_npoints != 0) {
      for (int i = bkg_split_point;i < bkg_npoints;i++) {
        upCosThLb.AddBkgPoint(bkg_points[i]);
      }
    }

    if (upCosThLb.GetNumber() < critical_point / 2) {
      std::cout << " low content " << std::endl;
    }
    newbins.push_back(upCosThLb);


#else
    // we divide only once
    double midCosThLb = 0.7;
    DataPoint minCosThLb(pmin.GetmKp(), pmin.GetmJpsip(), pmin.GetCosThL(), midCosThLb, pmin.GetCosThJpsi(), pmin.GetphiMu(), pmin.GetphiK(), 0);
    DataPoint maxCosThLb(pmax.GetmKp(), pmax.GetmJpsip(), pmax.GetCosThL(), midCosThLb, pmax.GetCosThJpsi(), pmax.GetphiMu(), pmax.GetphiK(), 0);


    DataBin lowCosThLb(pmin, maxCosThLb, MINPOINTS);
    DataBin upCosThLb(minCosThLb, pmax, MINPOINTS);
    for (int i = 0;i < npoints;i++) {
      DataPoint indexp = points[i];
      if (indexp.GetCosThLb() < midCosThLb) {
        lowCosThLb.AddPoint(indexp);
      }
      else {
        upCosThLb.AddPoint(indexp);
      }
    }

    for (int i = 0;i < bkg_npoints;i++) {
      DataPoint bkg_indexp = bkg_points[i];
      if (bkg_indexp.GetCosThLb() < midCosThLb) {
        lowCosThLb.AddBkgPoint(bkg_indexp);
      }
      else {
        upCosThLb.AddBkgPoint(bkg_indexp);
      }
    }

    newbins.push_back(lowCosThLb);
    newbins.push_back(upCosThLb);


#endif
  }

  return newbins;
}


std::vector<DataBin> DataBin::DivideCosThJpsi()
{

  std::vector<DataBin>  newbins;

  int npoints = points.size();
  int bkg_npoints = bkg_points.size();
  double sumW = GetNumber();

  double sig_points = sumW;
  bool isCFit = false;
  if (int(sumW) == npoints) {
    isCFit = true;
    if (bkg_npoints != 0) { sig_points = sumW - bkg_npoints * ((bkg_points[0]).Getweight()); }
    else { sig_points = sumW; }

  }

  //  printf("npoints: %i, bkg_npoints: %i, sumW: %f, sig_points: %f\n",npoints, bkg_npoints, sumW, sig_points);


  if (sig_points < MINPOINTS) {
    newbins.push_back(*this);
  }
  else {

#if 1
    sort(points.begin(), points.end(), CompareCosThJpsi);
    if (bkg_npoints != 0)sort(bkg_points.begin(), bkg_points.end(), CompareCosThJpsi);

    double critical_point = sumW / 2;
    double i_sumW = 0;
    int split_point = 0;
    while (i_sumW < critical_point) {
      i_sumW += points[split_point].Getweight();
      split_point += 1;
    }
    //  printf("split point: %i\n", split_point);


    DataPoint midpoint = points[split_point - 1];
    DataPoint nexpoint = points[split_point];
    double midCosThJpsi = (midpoint.GetCosThJpsi() + nexpoint.GetCosThJpsi()) / 2.;

    int bkg_split_point = 0;

    if (isCFit && bkg_npoints != 0) {
      while (bkg_points[bkg_split_point].GetCosThJpsi() < midCosThJpsi && bkg_split_point < bkg_npoints) {
        bkg_split_point++;
      }
    }

    //  printf("bkg split point: %i\n", bkg_split_point);
    DataPoint minCosThJpsi(pmin.GetmKp(), pmin.GetmJpsip(), pmin.GetCosThL(), pmin.GetCosThLb(), midCosThJpsi, pmin.GetphiMu(), pmin.GetphiK(), 0);
    DataPoint maxCosThJpsi(pmax.GetmKp(), pmax.GetmJpsip(), pmax.GetCosThL(), pmax.GetCosThLb(), midCosThJpsi, pmax.GetphiMu(), pmax.GetphiK(), 0);


    DataBin lowCosThJpsi(pmin, maxCosThJpsi, MINPOINTS);
    for (int i = 0;i < split_point;i++) {
      lowCosThJpsi.AddPoint(points[i]);
    }

    if (isCFit && bkg_npoints != 0) {
      for (int i = 0;i < bkg_split_point;i++) {
        lowCosThJpsi.AddBkgPoint(bkg_points[i]);
      }
    }

    if (lowCosThJpsi.GetNumber() < critical_point / 2) {
      printf("Somewhat low points\n");
      //newbins.push_back( *this );
      //return newbins;
    }
    newbins.push_back(lowCosThJpsi);
    DataBin upCosThJpsi(minCosThJpsi, pmax, MINPOINTS);
    for (int i = split_point;i < npoints;i++) {
      upCosThJpsi.AddPoint(points[i]);
    }

    if (isCFit && bkg_npoints != 0) {
      for (int i = bkg_split_point;i < bkg_npoints;i++) {
        upCosThJpsi.AddBkgPoint(bkg_points[i]);
      }
    }

    if (upCosThJpsi.GetNumber() < critical_point / 2) {
      std::cout << " low content " << std::endl;
    }
    newbins.push_back(upCosThJpsi);


#else
    // we divide only once
    double midCosThJpsi = 0.7;
    DataPoint minCosThJpsi(pmin.GetmKp(), pmin.GetmJpsip(), pmin.GetCosThL(), pmin.GetCosThLb(), midCosThJpsi, pmin.GetphiMu(), pmin.GetphiK(), 0);
    DataPoint maxCosThJpsi(pmax.GetmKp(), pmax.GetmJpsip(), pmax.GetCosThL(), pmax.GetCosThLb(), midCosThJpsi, pmax.GetphiMu(), pmax.GetphiK(), 0);

    DataBin lowCosThJpsi(pmin, maxCosThJpsi, MINPOINTS);
    DataBin upCosThJpsi(minCosThJpsi, pmax, MINPOINTS);
    for (int i = 0;i < npoints;i++) {
      DataPoint indexp = points[i];
      if (indexp.GetCosThJpsi() < midCosThJpsi) {
        lowCosThJpsi.AddPoint(indexp);
      }
      else {
        upCosThJpsi.AddPoint(indexp);
      }
    }
    for (int i = 0;i < bkg_npoints;i++) {
      DataPoint bkg_indexp = bkg_points[i];
      if (bkg_indexp.GetCosThJpsi() < midCosThJpsi) {
        lowCosThJpsi.AddBkgPoint(bkg_indexp);
      }
      else {
        upCosThJpsi.AddBkgPoint(bkg_indexp);
      }
    }

    newbins.push_back(lowCosThJpsi);
    newbins.push_back(upCosThJpsi);


#endif
  }

  return newbins;
}


std::vector<DataBin> DataBin::DividephiMu()
{

  std::vector<DataBin>  newbins;

  int npoints = points.size();
  int bkg_npoints = bkg_points.size();
  double sumW = GetNumber();

  double sig_points = sumW;
  bool isCFit = false;
  if (int(sumW) == npoints) {
    isCFit = true;
    if (bkg_npoints != 0) { sig_points = sumW - bkg_npoints * ((bkg_points[0]).Getweight()); }
    else { sig_points = sumW; }

  }

  //  printf("npoints: %i, bkg_npoints: %i, sumW: %f, sig_points: %f\n",npoints, bkg_npoints, sumW, sig_points);


  if (sig_points < MINPOINTS) {
    newbins.push_back(*this);
  }
  else {
    sort(points.begin(), points.end(), ComparephiMu);
    if (bkg_npoints != 0)sort(bkg_points.begin(), bkg_points.end(), ComparephiMu);

    double critical_point = sumW / 2;
    double i_sumW = 0;
    int split_point = 0;
    while (i_sumW < critical_point) {
      i_sumW += points[split_point].Getweight();
      split_point += 1;
    }
    //  printf("split point: %i\n", split_point);


    DataPoint midpoint = points[split_point - 1];
    DataPoint nexpoint = points[split_point];
    double midphiMu = (midpoint.GetphiMu() + nexpoint.GetphiMu()) / 2.;

    int bkg_split_point = 0;

    if (isCFit && bkg_npoints != 0) {
      while (bkg_points[bkg_split_point].GetphiMu() < midphiMu && bkg_split_point < bkg_npoints) {
        bkg_split_point++;
      }
    }

    //printf("bkg split point: %i\n", bkg_split_point);


    DataPoint minphiMu(pmin.GetmKp(), pmin.GetmJpsip(), pmin.GetCosThL(), pmin.GetCosThLb(), pmin.GetCosThJpsi(), midphiMu, pmin.GetphiK(), 0);
    DataPoint maxphiMu(pmax.GetmKp(), pmax.GetmJpsip(), pmax.GetCosThL(), pmax.GetCosThLb(), pmax.GetCosThJpsi(), midphiMu, pmax.GetphiK(), 0);


    DataBin lowphiMu(pmin, maxphiMu, MINPOINTS);
    for (int i = 0;i < split_point;i++) {
      lowphiMu.AddPoint(points[i]);
    }

    if (isCFit && bkg_npoints != 0) {
      for (int i = 0;i < bkg_split_point;i++) {
        lowphiMu.AddBkgPoint(bkg_points[i]);
      }
    }

    if (lowphiMu.GetNumber() < critical_point / 2) {
      printf("Somewhat low points\n");
      //newbins.push_back( *this );
      //return newbins;
    }
    newbins.push_back(lowphiMu);
    DataBin upphiMu(minphiMu, pmax, MINPOINTS);
    for (int i = split_point;i < npoints;i++) {
      upphiMu.AddPoint(points[i]);
    }

    if (isCFit && bkg_npoints != 0) {
      for (int i = bkg_split_point;i < bkg_npoints;i++) {
        upphiMu.AddBkgPoint(bkg_points[i]);
      }
    }

    if (upphiMu.GetNumber() < critical_point / 2) {
      std::cout << " low content " << std::endl;
    }
    newbins.push_back(upphiMu);
  }
  return newbins;
}


std::vector<DataBin> DataBin::DividephiK()
{

  std::vector<DataBin>  newbins;

  int npoints = points.size();
  int bkg_npoints = bkg_points.size();
  double sumW = GetNumber();

  double sig_points = sumW;
  bool isCFit = false;
  if (int(sumW) == npoints) {
    isCFit = true;
    if (bkg_npoints != 0) { sig_points = sumW - bkg_npoints * ((bkg_points[0]).Getweight()); }
    else { sig_points = sumW; }

  }

  //printf("npoints: %i, bkg_npoints: %i, sumW: %f, sig_points: %f\n",npoints, bkg_npoints, sumW, sig_points);


  if (sig_points < MINPOINTS) {
    newbins.push_back(*this);
  }
  else {
    sort(points.begin(), points.end(), ComparephiK);
    if (bkg_npoints != 0)sort(bkg_points.begin(), bkg_points.end(), ComparephiK);

    double critical_point = sumW / 2;
    double i_sumW = 0;
    int split_point = 0;
    while (i_sumW < critical_point) {
      i_sumW += points[split_point].Getweight();
      split_point += 1;
    }
    //printf("split point: %i\n", split_point);


    DataPoint midpoint = points[split_point - 1];
    DataPoint nexpoint = points[split_point];
    double midphiK = (midpoint.GetphiK() + nexpoint.GetphiK()) / 2.;

    int bkg_split_point = 0;

    if (isCFit && bkg_npoints != 0) {
      while (bkg_points[bkg_split_point].GetphiK() < midphiK && bkg_split_point < bkg_npoints) {
        bkg_split_point++;
      }
    }

    //printf("bkg split point: %i\n", bkg_split_point);

    DataPoint minphiK(pmin.GetmKp(), pmin.GetmJpsip(), pmin.GetCosThL(), pmin.GetCosThLb(), pmin.GetCosThJpsi(), pmin.GetphiMu(), midphiK, 0);
    DataPoint maxphiK(pmax.GetmKp(), pmax.GetmJpsip(), pmax.GetCosThL(), pmax.GetCosThLb(), pmax.GetCosThJpsi(), pmax.GetphiMu(), midphiK, 0);


    DataBin lowphiK(pmin, maxphiK, MINPOINTS);
    for (int i = 0;i < split_point;i++) {
      lowphiK.AddPoint(points[i]);
    }

    if (isCFit && bkg_npoints != 0) {
      for (int i = 0;i < bkg_split_point;i++) {
        lowphiK.AddBkgPoint(bkg_points[i]);
      }
    }

    if (lowphiK.GetNumber() < critical_point / 2) {
      printf("Somewhat low points\n");
      //newbins.push_back( *this );
      //return newbins;
    }
    newbins.push_back(lowphiK);
    DataBin upphiK(minphiK, pmax, MINPOINTS);
    for (int i = split_point;i < npoints;i++) {
      upphiK.AddPoint(points[i]);
    }

    if (isCFit && bkg_npoints != 0) {
      for (int i = bkg_split_point;i < bkg_npoints;i++) {
        upphiK.AddBkgPoint(bkg_points[i]);
      }
    }

    if (upphiK.GetNumber() < critical_point / 2) {
      std::cout << " low content " << std::endl;
    }
    newbins.push_back(upphiK);
  }
  return newbins;
}





typedef std::vector<DataBin> DataBins;


DataBins DividemKp(DataBins&);
DataBins DividemJpsip(DataBins&);
DataBins DivideCosThL(DataBins&);
DataBins DivideCosThLb(DataBins&);
DataBins DivideCosThJpsi(DataBins&);
DataBins DividephiMu(DataBins&);
DataBins DividephiK(DataBins&);
double foldPhiAngle(double phiAngle);

void MassChi2(RooDataSet* data, TTree* sideband_tree,
  TString MCloc, double f_bkg, TString label,
  int nmKp, int nmJpsip, int nCosThL, int nCosThLb,
  int nCosThJpsi, int nphiMu, int nphiK,
  double kine_min(double*, double*),
  double kine_max(double*, double*), double minimalPoints,
  bool foldPhi)
{

  //  (J/psi p K) => (K J/psi phi)
  double   m_lb = 5.27932;
  double m_k = 0.493677;
  double m_p = 1.019461 - 0.015;//phi 15 MeV window
  double m_jpsi = 3.096916;
  m_jpsi = 0.493677;
  m_p = 3.096916;
  m_k = 1.019461 - 0.015;

  double mKp_min = pow(m_k + m_p, 2);
  double mKp_max = pow(m_lb - m_jpsi, 2);
  double mJpsip_min = pow(m_jpsi + m_p, 2);
  double mJpsip_max = pow(m_lb - m_k, 2);

  int _nCosThL = pow(2, nCosThL);
  int _nCosThLb = pow(2, nCosThLb);
  int _nCosThJpsi = pow(2, nCosThJpsi);
  int _nphiMu = pow(2, nphiMu);
  int _nphiK = pow(2, nphiK);


  gROOT->SetStyle("Plain");
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
  gStyle->SetOptFit(0111);

  double phiMin, phiMax;
  if (foldPhi) {
    phiMin = 0;
    phiMax = PI / 2;
  }
  else {
    phiMin = -PI;
    phiMax = PI;
  }

  DataPoint pmin((mKp_min), (mJpsip_min), 0, 0, 0, phiMin, phiMin, 0);
  DataPoint pmax((mKp_max), (mJpsip_max), 1, 1, 1, phiMax, phiMax, 0);

  DataBin StartBin(pmin, pmax, minimalPoints);




  TH2D* hist = new TH2D("da_mKpvsmJpsip", "da_mKpvsmJpsip", 10, mKp_min, mKp_max, 10, mJpsip_min, mJpsip_max);
  TH2D* histmc = new TH2D("mc_mKpvsmJpsip", "mc_mKpvsmJpsip", 10, mKp_min, mKp_max, 10, mJpsip_min, mJpsip_max);
  TH2D* hist_b(0);
  if (true)hist_b = new TH2D("b_mKpvsmJpsip", "b_mKpvsmJpsip", 10, mKp_min, mKp_max, 10, mJpsip_min, mJpsip_max);


  //  TChain* tree=data;


  double D_mKp, D_mJpsip, D_cosTheta_Lb, D_cosTheta_L, D_cosTheta_Jpsi, D_phiMu, D_phiK, Dsig_sw;
  /*
  tree->SetBranchAddress("mkp",&D_mKp);
  tree->SetBranchAddress("mjpsip",&D_mJpsip);
  tree->SetBranchAddress("cosTheta_Lb",&D_cosTheta_Lb);
  tree->SetBranchAddress("cosTheta_L",&D_cosTheta_L);
  tree->SetBranchAddress("cosTheta_Jpsi",&D_cosTheta_Jpsi);
  tree->SetBranchAddress("phiMu",&D_phiMu);
  tree->SetBranchAddress("phiK",&D_phiK);
  tree->SetBranchAddress("nsig_sw",&Dsig_sw);
*/

  double data_sumWeight = 0;
  int data_npoints = data->numEntries();
  RooArgSet* row;
  double Df_phiK, Df_phiMu;
  for (int i = 0;i < data_npoints;i++) {

    row = (RooArgSet*)data->get(i);
    //row->Print("V");
    D_mKp = ((RooAbsReal*)row->find("mjpsiphi"))->getVal();
    // if(!kine_limits(mkp)) { std::cout << "why " << std::endl;}
    // D_cosTheta_Lb = ((RooAbsReal*)row->find("X_cosTheta_phi"))->getVal();
    // D_cosTheta_L = ((RooAbsReal*)row->find("X_cosTheta_X"))->getVal();
    // D_cosTheta_Jpsi = ((RooAbsReal*)row->find("X_cosTheta_psi"))->getVal();
    // D_phiK = ((RooAbsReal*)row->find("X_phi_K"))->getVal();
    // D_phiMu = ((RooAbsReal*)row->find("X_phi_mu"))->getVal();

    D_cosTheta_Lb = ((RooAbsReal*)row->find("b_cos_theta_phi_X"))->getVal();
    D_cosTheta_L = ((RooAbsReal*)row->find("b_cos_theta_X"))->getVal();
    D_cosTheta_Jpsi = ((RooAbsReal*)row->find("b_cos_theta_psi_X"))->getVal();
    D_phiK = ((RooAbsReal*)row->find("b_phi_k_X"))->getVal();
    D_phiMu = ((RooAbsReal*)row->find("b_phi_mu_X"))->getVal();
    Dsig_sw = 1.0;// ((RooAbsReal*)row->find("nsig_sw"))->getVal();
    D_mJpsip = ((RooAbsReal*)row->find("mjpsik"))->getVal();
    data_sumWeight += Dsig_sw;

    if (D_mKp * D_mKp < mKp_min) continue;
    if (D_mKp * D_mKp > mKp_max) continue;
    if (D_mJpsip * D_mJpsip < mJpsip_min) continue;
    if (D_mJpsip * D_mJpsip > mJpsip_max) continue;


    if (foldPhi) {
      Df_phiK = foldPhiAngle(D_phiK);
      Df_phiMu = foldPhiAngle(D_phiMu);
    }
    else {
      Df_phiK = D_phiK;
      Df_phiMu = D_phiMu;
    }


    hist->Fill((D_mKp) * (D_mKp), (D_mJpsip) * (D_mJpsip), Dsig_sw);
    DataPoint dp(D_mKp * D_mKp, D_mJpsip * D_mJpsip, fabs(D_cosTheta_L), fabs(D_cosTheta_Lb), fabs(D_cosTheta_Jpsi), Df_phiMu,
      Df_phiK, Dsig_sw);
    StartBin.AddPoint(dp);

  }
  cout << "Data sum SW " << data_sumWeight << endl;

  double scale_factor = 0;
  if (sideband_tree) {

    double B_mKp, B_mJpsip, B_cosTheta_Lb, B_cosTheta_L, B_cosTheta_Jpsi, B_phiMu, B_phiK;

    sideband_tree->SetBranchAddress("mjpsiphi", &B_mKp);
    sideband_tree->SetBranchAddress("mjpsik", &B_mJpsip);
    // D_cosTheta_Lb = ((RooAbsReal*)row->find("b_cos_theta_phi_X"))->getVal();
    // D_cosTheta_L = ((RooAbsReal*)row->find("b_cos_theta_X"))->getVal();
    // D_cosTheta_Jpsi = ((RooAbsReal*)row->find("b_cos_theta_psi_X"))->getVal();
    // D_phiK = ((RooAbsReal*)row->find("b_phi_k_X"))->getVal();
    // D_phiMu = ((RooAbsReal*)row->find("b_phi_mu_X"))->getVal();
    sideband_tree->SetBranchAddress("b_cos_theta_phi_X", &B_cosTheta_Lb);
    sideband_tree->SetBranchAddress("b_cos_theta_X", &B_cosTheta_L);
    sideband_tree->SetBranchAddress("b_cos_theta_psi_X", &B_cosTheta_Jpsi);
    sideband_tree->SetBranchAddress("b_phi_mu_X", &B_phiMu);
    sideband_tree->SetBranchAddress("b_phi_k_X", &B_phiK);

    double sideband_entries = sideband_tree->GetEntries();

    scale_factor = (data_npoints / sideband_entries) * f_bkg;

    std::cout << " bkg entries " << sideband_entries << std::endl;
    std::cout << "and number of data points is: " << data_npoints << ", with frac bkg: " << f_bkg << std::endl;
    std::cout << "So scaling factor is: " << scale_factor << std::endl;

    double Bf_phiK, Bf_phiMu;

    for (int i = 0;i < sideband_entries;i++) {
      sideband_tree->GetEntry(i);

      if (B_mKp * B_mKp < mKp_min) continue;
      if (B_mKp * B_mKp > mKp_max) continue;
      if (B_mJpsip * B_mJpsip < mJpsip_min) continue;
      if (B_mJpsip * B_mJpsip > mJpsip_max) continue;

      if (foldPhi) {
        Bf_phiK = foldPhiAngle(B_phiK);
        Bf_phiMu = foldPhiAngle(B_phiMu);
      }
      else {
        Bf_phiK = B_phiK;
        Bf_phiMu = B_phiMu;
      }

      hist_b->Fill((B_mKp) * (B_mKp), (B_mJpsip) * (B_mJpsip), scale_factor);
      DataPoint dp(B_mKp * B_mKp, B_mJpsip * B_mJpsip, fabs(B_cosTheta_L), fabs(B_cosTheta_Lb), fabs(B_cosTheta_Jpsi), Bf_phiMu, Bf_phiK, scale_factor);
      StartBin.AddBkgPoint(dp);

    }
  }



  bool plot((nCosThL == 0) && (nCosThLb == 0) && (nCosThJpsi == 0) && (nphiMu == 0) && (nphiK == 0));
  cout << " nmKp " << nmKp << " nmJpsip " << nmJpsip << " nCosThL " << nCosThL << " nCosThLb " << nCosThLb << " nCosThJpsi " << nCosThJpsi << " nphiMu " << nphiMu << " nphiK " << nphiK << endl;

  DataBins oldbins;
  DataBins newbins;
  oldbins.reserve(pow(2, nmKp + nmJpsip + nCosThL + nCosThLb + nCosThJpsi + nphiMu + nphiK));
  newbins.reserve(pow(2, nmKp + nmJpsip + nCosThL + nCosThLb + nCosThJpsi + nphiMu + nphiK));
  oldbins.push_back(StartBin);

  for (int i = 0;i < 5;i++) {

    if (plot) {
      if (nmKp > 0) {
        //std::cout << " divde mL oldbins size= " << oldbins.size() << std::endl; 
        newbins = DividemKp(oldbins);
        //std::cout << " divde mL newbins size= " << newbins.size() << std::endl; 
        oldbins = newbins;
        nmKp--;
      }
      if (nmJpsip > 0) {
        //std::cout << " divde nmJpsip oldbins size= " << oldbins.size() << std::endl; 
        newbins = DividemJpsip(oldbins);
        //std::cout << " divde nmJpsip newbins size= " << newbins.size() << std::endl; 
        oldbins = newbins;
        nmJpsip--;
      }
    }
    else {

      if (nmKp > 0) {
        //std::cout << " divde mL oldbins size= " << oldbins.size() << std::endl; 
        newbins = DividemKp(oldbins);
        oldbins = newbins;
        //std::cout << " divde mL newbins size= " << newbins.size() << std::endl; 
        nmKp--;
      }
      if (nmJpsip > 0) {
        //std::cout << " divde mJpsip oldbins size= " << oldbins.size() << std::endl; 
        newbins = DividemJpsip(oldbins);
        oldbins = newbins;
        //std::cout << " divde mJpsip newbins size= " << newbins.size() << std::endl; 
        nmJpsip--;
      }

      if (nCosThL > 0) {
        //std::cout << " divde cosRes oldbins size= " << oldbins.size() << std::endl; 
        newbins = DivideCosThL(oldbins);
        //std::cout << " divde cosRes newbins size= " << newbins.size() << std::endl; 
        oldbins = newbins;
        nCosThL--;
      }
      if (nCosThLb > 0) {
        //std::cout << " divde cosX oldbins size= " << oldbins.size() << std::endl; 
        newbins = DivideCosThLb(oldbins);
        //std::cout << " divde cosX newbins size= " << newbins.size() << std::endl; 
        oldbins = newbins;
        nCosThLb--;
      }
      if (nCosThJpsi > 0) {
        //std::cout << " divde cosJpsi oldbins size= " << oldbins.size() << std::endl; 
        newbins = DivideCosThJpsi(oldbins);
        oldbins = newbins;
        //std::cout << " divde cosJpsi newbins size= " << newbins.size() << std::endl; 
        nCosThJpsi--;
      }

      if (nphiMu > 0) {
        //std::cout << " divde phiRes oldbins size= " << oldbins.size() << std::endl; 
        newbins = DividephiMu(oldbins);
        //std::cout << " divde phiRes newbins size= " << newbins.size() << std::endl; 
        oldbins = newbins;
        nphiMu--;
      }
      if (nphiK > 0) {
        //std::cout << " divde phiJpsi oldbins size= " << oldbins.size() << std::endl; 
        newbins = DividephiK(oldbins);
        //std::cout << " divde phiJpsi newbins size= " << newbins.size() << std::endl; 
        oldbins = newbins;
        nphiK--;
      }
    }
  }

  std::cout << "hey!" << std::endl;
  TH2Poly* hpoly = new TH2Poly("dev_mKpvsmJpsip", "dev_mKpvsmJpsip", (mKp_min - 0.005), (mKp_max + 0.005), (mJpsip_min - 0.005), (mJpsip_max + 0.005));
  hpoly->Sumw2();
  TH2Poly* hpoly_dat = new TH2Poly("dat_mKpvsmJpsip", "dat_mKpvsmJpsip", (mKp_min - 0.005), (mKp_max + 0.005), (mJpsip_min - 0.005), (mJpsip_max + 0.005));
  hpoly_dat->Sumw2();
  TH2Poly* hpoly_fit = new TH2Poly("fit_mKpvsmJpsip", "fit_mKpvsmJpsip", (mKp_min - 0.005), (mKp_max + 0.005), (mJpsip_min - 0.005), (mJpsip_max + 0.005));
  hpoly_fit->Sumw2();

  //TH2D * hproj = new TH2D("devproj_mKpvsmJpsip","devproj_mKpvsmJpsip",100,mKp_min-0.005,mKp_max+0.005,100,mJpsip_min-0.005,mJpsip_max+0.005);
  double bwmKp = ((mKp_max + 0.005) - ((mKp_min + 0.005))) / 100.0;
  double bwmJpsip = ((mJpsip_max + 0.005) - ((mJpsip_min + 0.005))) / 100.0;


  //if( plot )
  for (DataBins::iterator ibin = oldbins.begin();ibin != oldbins.end();ibin++) {
    hpoly->AddBin((*ibin).GetMin().GetmKp(), (*ibin).GetMin().GetmJpsip(), (*ibin).GetMax().GetmKp(), (*ibin).GetMax().GetmJpsip());
    hpoly_dat->AddBin((*ibin).GetMin().GetmKp(), (*ibin).GetMin().GetmJpsip(), (*ibin).GetMax().GetmKp(), (*ibin).GetMax().GetmJpsip());
    hpoly_fit->AddBin((*ibin).GetMin().GetmKp(), (*ibin).GetMin().GetmJpsip(), (*ibin).GetMax().GetmKp(), (*ibin).GetMax().GetmJpsip());
  }



  DataPoints mcpoints;
  std::cout << "Import MC from: " << MCloc << std::endl;
  double MC_mKp, MC_mJpsip, MC_cosTheta_Lb, MC_cosTheta_L, MC_cosTheta_Jpsi, MC_phiMu, MC_phiK, MCsig_sw, Amp;

  TChain* smcdata = new TChain("tree");
  smcdata->Add(MCloc);

  smcdata->SetBranchAddress("mjpsiphi", &MC_mKp);
  smcdata->SetBranchAddress("mjpsik", &MC_mJpsip);
  // D_cosTheta_Lb = ((RooAbsReal*)row->find("b_cos_theta_phi_X"))->getVal();
  // D_cosTheta_L = ((RooAbsReal*)row->find("b_cos_theta_X"))->getVal();
  // D_cosTheta_Jpsi = ((RooAbsReal*)row->find("b_cos_theta_psi_X"))->getVal();
  // D_phiK = ((RooAbsReal*)row->find("b_phi_k_X"))->getVal();
  // D_phiMu = ((RooAbsReal*)row->find("b_phi_mu_X"))->getVal();
  smcdata->SetBranchAddress("b_cos_theta_phi_X", &MC_cosTheta_Lb);
  smcdata->SetBranchAddress("b_cos_theta_X", &MC_cosTheta_L);
  smcdata->SetBranchAddress("b_cos_theta_psi_X", &MC_cosTheta_Jpsi);
  smcdata->SetBranchAddress("b_phi_k_X", &MC_phiMu);
  smcdata->SetBranchAddress("b_phi_mu_X", &MC_phiK);
  //if( sideband_tree ){smcdata->SetBranchAddress("Amp_NoBkg",&Amp);}
  //else{smcdata->SetBranchAddress("Amp",&Amp);}
  //  smcdata->SetBranchAddress("Amp",&Amp);
  Amp = 1.0;
  smcdata->SetBranchAddress("w", &MCsig_sw);

  cout << "MC Entries: " << smcdata->GetEntries() << endl;
  //double MC_sumWeight=0;

  double MCf_phiK, MCf_phiMu;

  double suminte = 0.0;
  for (int i = 0;i < smcdata->GetEntries();i++) {
    smcdata->GetEntry(i);

    if (MC_mKp * MC_mKp < mKp_min) continue;
    if (MC_mKp * MC_mKp > mKp_max) continue;
    if (MC_mJpsip * MC_mJpsip < mJpsip_min) continue;
    if (MC_mJpsip * MC_mJpsip > mJpsip_max) continue;

    suminte += MCsig_sw * Amp;

  }

  for (int i = 0;i < smcdata->GetEntries();i++) {
    smcdata->GetEntry(i);

    if (MC_mKp * MC_mKp < mKp_min) continue;
    if (MC_mKp * MC_mKp > mKp_max) continue;
    if (MC_mJpsip * MC_mJpsip < mJpsip_min) continue;
    if (MC_mJpsip * MC_mJpsip > mJpsip_max) continue;


    if (foldPhi) {
      MCf_phiK = foldPhiAngle(MC_phiK);
      MCf_phiMu = foldPhiAngle(MC_phiMu);
    }
    else {
      MCf_phiK = MC_phiK;
      MCf_phiMu = MC_phiMu;
    }


    histmc->Fill((MC_mKp) * (MC_mKp), (MC_mJpsip) * (MC_mJpsip), MCsig_sw * Amp * data_sumWeight / suminte);
    DataPoint dp(MC_mKp * MC_mKp, MC_mJpsip * MC_mJpsip, fabs(MC_cosTheta_L), fabs(MC_cosTheta_Lb), fabs(MC_cosTheta_Jpsi),
      MCf_phiMu, MCf_phiK, MCsig_sw * Amp * data_sumWeight / suminte);

    mcpoints.push_back(dp);

  }


  ///check split everything OK
  int final_nBin = oldbins.size();
  double total_sig = 0; double total_bkg = 0;
  for (int ib = 0; ib < final_nBin; ib++) {
    total_sig += oldbins[ib].GetNumber();
    total_bkg += oldbins[ib].GetBkgNumber();
    //printf("Bin %i: sig %f, bkg %f \n",ib, oldbins[ib].GetNumber(), oldbins[ib].GetBkgNumber());
  }
  printf("total sig %f, total bkg %f \n", total_sig, total_bkg);

  cout << "plots" << endl;
  if (plot) {

    TCanvas* cd = new TCanvas("cd");
    gPad->SetLogz(1);
    gStyle->SetPalette(1);
    hist->GetXaxis()->SetTitle("m^{2}(J/#psi#phi) (GeV^{2})");
    hist->GetYaxis()->SetTitle("m^{2}(J/#psiK) (GeV^{2})");
    hist->DrawCopy("colz");
    cd->SaveAs(TString("data_").Append(label).Append(TString(".C")));

    delete cd;
    delete hist;

    TCanvas* cdmc = new TCanvas("cdmc");
    gPad->SetLogz(1);
    gStyle->SetPalette(1);
    histmc->GetXaxis()->SetTitle("m^{2}(J/#psi#phi) (GeV^{2})");
    histmc->GetYaxis()->SetTitle("m^{2}(J/#psiK) (GeV^{2})");
    histmc->DrawCopy("colz");
    cdmc->SaveAs(TString("mc_").Append(label).Append(TString(".C")));

    delete histmc;
    delete cdmc;

    if (hist_b) {
      TCanvas* cdbkg = new TCanvas("cdbkg");
      gPad->SetLogz(1);
      gStyle->SetPalette(1);
      hist_b->GetXaxis()->SetTitle("m^{2}(J/#psi#phi) (GeV^{2})");
      hist_b->GetYaxis()->SetTitle("m^{2}(J/#psiK) (GeV^{2})");
      hist_b->DrawCopy("colz");
      cdbkg->SaveAs(TString("bkg_").Append(label).Append(TString(".C")));

      delete cdbkg;
      delete hist_b;
    }


  }


  double realnum[10000], mcnum[10000], bnum[10000];
  double realerr[10000], mcerr[10000], berr[10000];
  for (int i = 0;i < 10000;i++) {
    realnum[i] = mcnum[i] = bnum[i] = 0;
    realerr[i] = mcerr[i] = berr[i] = 0;
  }

  int index = 0;
  for (DataBins::iterator ibin = oldbins.begin();ibin != oldbins.end();ibin++) {
    realnum[index] = (*ibin).GetNumber();
    realerr[index] = (*ibin).GetNumErr();

    for (DataPoints::iterator ip = mcpoints.begin();ip != mcpoints.end();ip++) {
      if ((*ibin).Contains((*ip))) {
        mcnum[index] += (*ip).Getweight();
        mcerr[index] += (*ip).Getweight() * (*ip).Getweight();
      }
    }

    mcerr[index] = sqrt(mcerr[index]);

    if (sideband_tree) {
      bnum[index] = (*ibin).GetBkgNumber() * scale_factor;
      berr[index] = sqrt(bnum[index]);
    }
    index++;
  }

  TH1D* reslist = new TH1D("reslist", "reslist", oldbins.size(), 0.0, oldbins.size());
  TH1D* datlist = new TH1D("datlist", "datlist", oldbins.size(), 0.0, oldbins.size());
  TH1D* fitlist = new TH1D("fitlist", "fitlist", oldbins.size(), 0.0, oldbins.size());
  reslist->Sumw2();
  datlist->Sumw2();
  fitlist->Sumw2();

  TH1D* hgauss = new TH1D("hgauss", "", 1000, -10, 10);
  double chi2 = 0;
  double chi21 = 0;
  double ndof = oldbins.size();
  for (int i = 0;i < oldbins.size();i++) {
    double dev = (realnum[i] - mcnum[i]) / sqrt(realerr[i] * realerr[i] + mcerr[i] * mcerr[i] + berr[i] * berr[i]);
    DataBin& bi = oldbins[i];
    //cout<<&(bi)<<" "<<bi.GetMin().GetCosThL()<<" "<<bi.GetMax().GetCosThL()<<endl;

#if 1    
    if (fabs(dev) > 3.0) {
      cout << realnum[i] << " +/- " << realerr[i] << " ; mc " << mcnum[i] << " +/- " << mcerr[i]
        << " berr " << berr[i] << " sig " << dev;
      cout << " mKp " << bi.GetMin().GetmKp() << "  " << bi.GetMax().GetmKp()
        << " mJpsip " << bi.GetMin().GetmJpsip() << " " << bi.GetMax().GetmJpsip() << endl;
      if (!plot) {
        cout << " CosThL " << bi.GetMin().GetCosThL() << "  " << bi.GetMax().GetCosThL()
          << " CosThX " << bi.GetMin().GetCosThLb() << "  " << bi.GetMax().GetCosThLb()
          << " CosThJpsi " << bi.GetMin().GetCosThJpsi() << "  " << bi.GetMax().GetCosThJpsi() << std::endl;
        cout << " phiMu " << bi.GetMin().GetphiMu() << " " << bi.GetMax().GetphiMu()
          << " phiK " << bi.GetMin().GetphiK() << " " << bi.GetMax().GetphiK() << std::endl;
      }
      cout << endl;
    }
#endif
    chi2 += dev * dev;
    hgauss->Fill(dev);
    //    if( plot )
    hpoly->SetBinContent(i + 1, dev);


    double mKpbw = (-bi.GetMin().GetmKp() + bi.GetMax().GetmKp());
    double mJpsipbw = (-bi.GetMin().GetmJpsip() + bi.GetMax().GetmJpsip());
    double area = mKpbw * mJpsipbw;

    hpoly_dat->SetBinContent(i + 1, realnum[i] / area);
    hpoly_dat->SetBinError(i + 1, sqrt(pow(realerr[i], 2) + pow(berr[i], 2)) / area);
    hpoly_fit->SetBinContent(i + 1, mcnum[i] / area);
    hpoly_fit->SetBinError(i + 1, mcerr[i] / area);
    if ((realnum[i] > 0.0) && (mcnum[i] > 0.0))
      chi21 += -2. * realnum[i] * log(mcnum[i] / realnum[i]);

    reslist->SetBinContent(i + 1, realnum[i] - mcnum[i]);
    reslist->SetBinError(i + 1, sqrt(realerr[i] * realerr[i] + mcerr[i] * mcerr[i] + berr[i] * berr[i]));
    datlist->SetBinContent(i + 1, realnum[i]);
    datlist->SetBinError(i + 1, sqrt(realerr[i] * realerr[i] + berr[i] * berr[i]));
    fitlist->SetBinContent(i + 1, mcnum[i]);
    fitlist->SetBinError(i + 1, mcerr[i]);


  }

  cout << chi2 << " / " << ndof << " other " << chi21 << endl;

  TCanvas* c1n = new TCanvas("c1n");
  hgauss->Draw();
  hgauss->Fit("gaus", "wwL");
  c1n->SaveAs(TString("residue_").Append(label).Append(TString(".C")));

  delete c1n;
  //  if( plot ){
  {

    {
      TCanvas* c2reslist = new TCanvas("c2reslist");
      c2reslist->cd(0);
      reslist->Draw();
      c2reslist->SaveAs(TString("reslist_").Append(label).Append(TString(".C")));
      delete c2reslist;
    }
    {
      TCanvas* c2datlist = new TCanvas("c2datlist");
      c2datlist->cd(0);
      datlist->Draw();
      c2datlist->SaveAs(TString("datlist_").Append(label).Append(TString(".C")));
      delete c2datlist;
    }
    {
      TCanvas* c2fitlist = new TCanvas("c2fitlist");
      c2fitlist->cd(0);
      fitlist->Draw();
      c2fitlist->SaveAs(TString("fitlist_").Append(label).Append(TString(".C")));
      delete c2fitlist;
    }


    TCanvas* c2 = new TCanvas("c2");
    c2->cd(0);


    hpoly->GetXaxis()->SetTitle("m^{2}(J/#psi#phi) (GeV^{2})");
    hpoly->GetYaxis()->SetTitle("m^{2}(J/#psiK) (GeV^{2})");

    //color, 51-100,range, -3,3
    hpoly->SetMinimum(-4);
    hpoly->SetMaximum(4);
    double hmin = hpoly->GetMinimum();
    double hmax = hpoly->GetMaximum();
    gStyle->SetPalette(1);
    //  cout<<"orginal max, min"<<hmin<<" "<<hmax<<endl;
    // cout<<"a, b"<<49./(hmax-hmin)<<" "<<(51*hmax-100*hmin)/(hmax-hmin)<<endl;
    int cmin = int(7.91 * hmin + 79.7);
    int cmax = int(7.91 * hmax + 79.7);

    int NewPalette[50];
    for (int i = cmin; i < cmax;i++) {
      NewPalette[i - cmin] = i;
    }
    //  gStyle->SetPalette(cmax-cmin,NewPalette);

    hpoly->Draw("colz");
    if (kine_max != 0) {
      TF1* fmax = new TF1("fmax", kine_max, (mKp_min - 0.005), (mKp_max + 0.005), 0);
      TF1* fmin = new TF1("fmin", kine_min, (mKp_min - 0.005), (mKp_max + 0.005), 0);
      fmin->SetLineColor(0);
      fmax->SetLineColor(0);
      fmin->SetFillColor(0);
      fmax->SetFillColor(0);
      fmin->Draw("same");
      fmax->Draw("same");
    }

    c2->SaveAs(TString("resmap_").Append(label).Append(TString(".C")));
    c2->SaveAs(TString("resmap_").Append(label).Append(TString(".pdf")));
    delete c2;

    {

      TCanvas* c2 = new TCanvas("c2dat");
      c2->cd(0);

      hpoly_dat->GetXaxis()->SetTitle("m^{2}(J/#psi#phi) (GeV^{2})");
      hpoly_dat->GetYaxis()->SetTitle("m^{2}(J/#psiK) (GeV^{2})");
      hpoly_dat->Draw("colz");
      if (kine_max != 0) {
        TF1* fmax = new TF1("fmax", kine_max, (mKp_min - 0.005), (mKp_max + 0.005), 0);
        TF1* fmin = new TF1("fmin", kine_min, (mKp_min - 0.005), (mKp_max + 0.005), 0);
        fmin->SetLineColor(0);
        fmax->SetLineColor(0);
        fmin->SetFillColor(0);
        fmax->SetFillColor(0);
        fmin->Draw("same");
        fmax->Draw("same");
      }

      c2->SaveAs(TString("mapdat_").Append(label).Append(TString(".C")));
      delete c2;
    }

    {

      TCanvas* c2 = new TCanvas("c2fit");
      c2->cd(0);

      hpoly_fit->GetXaxis()->SetTitle("m^{2}(J/#psi#phi) (GeV^{2})");
      hpoly_fit->GetYaxis()->SetTitle("m^{2}(J/#psiK) (GeV^{2})");
      hpoly_fit->Draw("colz");
      if (kine_max != 0) {
        TF1* fmax = new TF1("fmax", kine_max, (mKp_min - 0.005), (mKp_max + 0.005), 0);
        TF1* fmin = new TF1("fmin", kine_min, (mKp_min - 0.005), (mKp_max + 0.005), 0);
        fmin->SetLineColor(0);
        fmax->SetLineColor(0);
        fmin->SetFillColor(0);
        fmax->SetFillColor(0);
        fmin->Draw("same");
        fmax->Draw("same");
      }

      c2->SaveAs(TString("mapfit_").Append(label).Append(TString(".C")));
      delete c2;
    }

  }


  //  tt_file->Close(); 44444444444444444444444444444
}

DataBins DividemKp(DataBins& oldbins)
{
  DataBins newbins;
  for (DataBins::iterator ibin = oldbins.begin(); ibin != oldbins.end(); ibin++) {
    DataBins temp = (*ibin).DividemKp();
    if (temp.size() == 2) {
      if (temp[0].GetBkgNumber() + temp[1].GetBkgNumber() != (*ibin).GetBkgNumber()) {
        std::cout << "Bad bkg split in dividmkp" << std::endl;
        std::cout << temp[0].GetBkgNumber() << ", " << temp[1].GetBkgNumber() << ", " << (*ibin).GetBkgNumber() << std::endl;
      }
      if (TMath::Nint(temp[0].GetNumber() + temp[1].GetNumber()) != TMath::Nint((*ibin).GetNumber())) {
        std::cout << "Bad signal split in dividmkp" << std::endl;
      }
    }

    for (DataBins::iterator itmp = temp.begin(); itmp != temp.end(); itmp++) {
      newbins.push_back((*itmp));
    }
  }
  return newbins;
}

DataBins DividemJpsip(DataBins& oldbins)
{
  DataBins newbins;
  for (DataBins::iterator ibin = oldbins.begin(); ibin != oldbins.end(); ibin++) {
    DataBins temp = (*ibin).DividemJpsip();
    for (DataBins::iterator itmp = temp.begin(); itmp != temp.end(); itmp++) {
      newbins.push_back((*itmp));
    }
    if (temp.size() == 2) {
      if (temp[0].GetBkgNumber() + temp[1].GetBkgNumber() != (*ibin).GetBkgNumber()) {
        std::cout << "Bad bkg split in dividmjpsip" << std::endl;
      }
      if (TMath::Nint(temp[0].GetNumber() + temp[1].GetNumber()) != TMath::Nint((*ibin).GetNumber())) {
        std::cout << "Bad signal split in dividemjpsip" << std::endl;
      }
    }

  }
  return newbins;
}

DataBins DivideCosThL(DataBins& oldbins)
{
  DataBins newbins;
  for (DataBins::iterator ibin = oldbins.begin(); ibin != oldbins.end(); ibin++) {
    DataBins temp = (*ibin).DivideCosThL();
    for (DataBins::iterator itmp = temp.begin(); itmp != temp.end(); itmp++) {
      newbins.push_back((*itmp));
    }
    if (temp.size() == 2) {
      if (temp[0].GetBkgNumber() + temp[1].GetBkgNumber() != (*ibin).GetBkgNumber()) {
        std::cout << "Bad bkg divide in cosThL" << std::endl;
      }
      if (TMath::Nint(temp[0].GetNumber() + temp[1].GetNumber()) != TMath::Nint((*ibin).GetNumber())) {
        std::cout << "Bad signal divide in cosThL" << std::endl;
      }
    }

  }
  return newbins;
}
DataBins DivideCosThLb(DataBins& oldbins)
{
  DataBins newbins;
  for (DataBins::iterator ibin = oldbins.begin(); ibin != oldbins.end(); ibin++) {
    DataBins temp = (*ibin).DivideCosThLb();
    for (DataBins::iterator itmp = temp.begin(); itmp != temp.end(); itmp++) {
      newbins.push_back((*itmp));
    }
    if (temp.size() == 2) {
      if (temp[0].GetBkgNumber() + temp[1].GetBkgNumber() != (*ibin).GetBkgNumber()) {
        std::cout << "Bad bkg divide in Dividecosthlb" << std::endl;
      }
      if (TMath::Nint(temp[0].GetNumber() + temp[1].GetNumber()) != TMath::Nint((*ibin).GetNumber())) {
        std::cout << "Bad sig divide in dividcosthelb" << std::endl;
      }
    }

  }
  return newbins;
}
DataBins DivideCosThJpsi(DataBins& oldbins)
{
  DataBins newbins;
  for (DataBins::iterator ibin = oldbins.begin(); ibin != oldbins.end(); ibin++) {
    DataBins temp = (*ibin).DivideCosThJpsi();
    for (DataBins::iterator itmp = temp.begin(); itmp != temp.end(); itmp++) {
      newbins.push_back((*itmp));
    }
    if (temp.size() == 2) {
      if (temp[0].GetBkgNumber() + temp[1].GetBkgNumber() != (*ibin).GetBkgNumber()) {
        std::cout << "Bad bkg divide in Dividcosthjpsi" << std::endl;
      }
      if (TMath::Nint(temp[0].GetNumber() + temp[1].GetNumber()) != TMath::Nint((*ibin).GetNumber())) {
        std::cout << "Bad signal divide in dividcosthjpsi" << std::endl;
      }
    }

  }
  return newbins;
}
DataBins DividephiMu(DataBins& oldbins)
{
  DataBins newbins;
  for (DataBins::iterator ibin = oldbins.begin(); ibin != oldbins.end(); ibin++) {
    DataBins temp = (*ibin).DividephiMu();
    for (DataBins::iterator itmp = temp.begin(); itmp != temp.end(); itmp++) {
      newbins.push_back((*itmp));
    }
    if (temp.size() == 2) {
      if (temp[0].GetBkgNumber() + temp[1].GetBkgNumber() != (*ibin).GetBkgNumber()) {
        std::cout << "Bad background divide in phimu" << std::endl;
      }
      if (TMath::Nint(temp[0].GetNumber() + temp[1].GetNumber()) != TMath::Nint((*ibin).GetNumber())) {
        std::cout << "Bad signal divide in dividephimu" << std::endl;
      }
    }

  }
  return newbins;
}
DataBins DividephiK(DataBins& oldbins)
{
  DataBins newbins;
  for (DataBins::iterator ibin = oldbins.begin(); ibin != oldbins.end(); ibin++) {
    DataBins temp = (*ibin).DividephiK();
    for (DataBins::iterator itmp = temp.begin(); itmp != temp.end(); itmp++) {
      newbins.push_back((*itmp));
    }
    if (temp.size() == 2) {
      if (temp[0].GetBkgNumber() + temp[1].GetBkgNumber() != (*ibin).GetBkgNumber()) {
        std::cout << "Bad bkg divide in dividephik" << std::endl;
      }
      if (TMath::Nint(temp[0].GetNumber() + temp[1].GetNumber()) != TMath::Nint((*ibin).GetNumber())) {
        std::cout << "Bad signal divide in dividphiK" << std::endl;
      }
    }

  }
  return newbins;
}


double foldPhiAngle(double phiAngle) {
  double foldedAngle;

  if (phiAngle > 0) {
    if (phiAngle < PI / 2) {
      foldedAngle = phiAngle;
    }
    else {
      foldedAngle = PI - phiAngle; //D_phiX_Res - 2*(D_phiX_Res - PI/2);
    }
  }
  else {
    if (phiAngle > -PI / 2) {
      foldedAngle = -phiAngle;
    }
    else {
      foldedAngle = PI + phiAngle;
    }
  }

  return foldedAngle;
}
