  int seed = 1+atoi( argv[1] );
  g_rnd = new TRandom3();
  g_rnd->SetSeed(seed);  
  for(int i=0; i<list->GetSize(); ++i) {
     RooArgList *ires = (RooArgList*)(list->At(i));
     SetKPar(ires,5,true);
  }
  for(int i=0; i<listX->GetSize(); ++i) {
    RooArgList *ires = (RooArgList*)(listX->At(i));  
    SetXPar(ires,5,true);
    ResetXPar(ires,true);
  }
  for(int i=0; i<listZ->GetSize(); ++i) {
    RooArgList *ires = (RooArgList*)(listZ->At(i));  
    SetKPar(ires,5,true);
  }
//  width_4140.setRange(0.01,0.25);
//  width_4140.setRange(0.01,0.25);  
  //setup reference
 //ResetKPar(K_1D1,false, 0, 1);        
 