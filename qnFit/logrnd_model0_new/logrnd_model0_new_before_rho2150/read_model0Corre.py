from operator import index
import re
import pandas as pd
from dataclasses import make_dataclass
import sys, os
import matplotlib.pyplot as plt



def read_model0Corre(mydir):
    one_log = make_dataclass("one_log", [("file",str), ("which_global",int), ("which_para",int), ("global_corre",float), ("FCN",float), ("status",str), ("method",str)])
    df1 = pd.DataFrame()
    #f1 = open("./logrnd_model0_new/logrnd_model0_new/fit-3925.log","r+")

    for path, dirs, files in os.walk(mydir):
        for filename in files:
            print(filename)
            fullpath = os.path.join(path, filename)
            f1 = open(fullpath,"r+")
            which_global = 0 #several global correlation in the fit.
            fcn_temp = 0
            fcn_final = 0
            status = ""
            method = ""
            for l1 in f1:
                pattern_global = re.compile("GLOBAL")
                pattern_status = re.compile("STATUS")
                if re.search(pattern_status, l1):
                    status = l1.split()[3].split("=")[1]
                    method = l1.split()[2]
                    fcn = l1.split()[0].split("=")[1]
                    if (float(fcn) <= float(fcn_temp)):
                        fcn_temp = float(fcn)
                        fcn_final = float(fcn_temp)
                if re.search(pattern_global,l1):
                    which_para = 1
                    for i in range(200):
                        l1 = f1.readline()
                        l1_first = l1.split()[0]
                        try:
                            float(l1_first)
                        except ValueError:
                            break
                        if (abs(float(l1_first) - which_para)) < 0.0001:
                            global_corre = float(l1.split()[1])
                            df1 = df1.append([one_log(filename, which_global, which_para, global_corre, fcn_final, status, method)], ignore_index=True)
                            which_para += 1
                    which_global += 1
    return df1

if __name__ == "__main__":
    df1 = read_model0Corre("/disk401/lhcb/qinning/b2dpp_farm10/qnFit/logrnd_model0_new/logrnd_model0_new_ChangeAllToDouble")
    df1.to_csv("/disk401/lhcb/qinning/b2dpp_farm10/qnFit/logrnd_model0_new/correlation.csv")
    #df2 = pd.read_csv("/disk401/lhcb/qinning/b2dpp_farm10/qnFit/logrnd_model0_new/correlation.csv")
    #df_para = df2[(abs(df2["FCN"]-df2["FCN"].min())<1) & (df2["global_corre"]>0.999)].which_para

    