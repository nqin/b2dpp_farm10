from math import sqrt
import matplotlib.pyplot as plt
import numpy as np
import sys, os
import ROOT
import pandas as pd
from dataclasses import make_dataclass


def read_logscan(logfile,JPlabel):
    one_log = make_dataclass("one_log", [("status", str), ("id", str), ("nll",float)])
    df1 = pd.DataFrame()
    f1 = open(logfile,"r+")
    for l1 in f1:
        nll_temp = l1.split()[1]
        status_temp = l1.split()[3]
        id_temp = l1.split()[0].split("//")[1].split("-")[1].split(".")[0]
        df1 = df1.append([one_log(str(status_temp), str(id_temp), float(nll_temp))], ignore_index=True)
    return df1



dflog = read_logscan("model0NLL.log","1/2+")
ax = dflog.nll.plot.hist(bins=100,range=(-11127,-11080))
plt.show()


