# The log files when checking HESSE not OK

1. logrnd_model0_new: at the very beginning.
2. logrnd_model0_new_ChangeAllToDouble: change the functions in cu_DPFPropogator.cu to double form.
3. logrnd_model0_new_NRmpp: change the NR form to $e^{-\alpha m}$
4. logrnd_model0_new_NRFix: change the NR form to $e^{-\alpha m^{2}}$ and fix $\alpha$.
5. logrnd_model0_new_NRalphabeta_mpp2: change the NR form to $e^{(-\alpha+i*\beta) m^{2}}$
6. logrnd_model0_new_NRalphabeta_mpp: change the NR form to $e^{(-\alpha+i*\beta) m}$

Comparing 1 and 2, no obvious difference. The minNLL are the same.