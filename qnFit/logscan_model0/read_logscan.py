from math import sqrt
import matplotlib.pyplot as plt
import numpy as np
import sys, os
import ROOT
import pandas as pd
from dataclasses import make_dataclass


def read_logscan(logfile,JPlabel):
    one_log = make_dataclass("one_log", [("mass", float), ("id", str), ("nll",float)])
    df1 = pd.DataFrame()
    f1 = open(logfile,"r+")
    for l1 in f1:
        nll_temp = l1.split()[1]
        mass_temp = l1.split()[0].split("/")[1].split("-")[1]
        id_temp = l1.split()[0].split("/")[1].split("-")[2].split(".")[0]
        df1 = pd.concat([df1, pd.DataFrame([one_log(float(mass_temp), str(id_temp), float(nll_temp))])], ignore_index=True)

    all_mass = np.linspace(float(2.800),float(3.300),100,endpoint=False)
    all_mass_2 = np.linspace(float(3.300),float(3.400),10,endpoint=False)
    all_mass = np.concatenate((all_mass,all_mass_2))
    all_nll = []

    for one_mass in all_mass:
        min_nll = df1[abs(df1.mass-one_mass)<0.00001].nll.min()
        all_nll.append(min_nll)

    arr_nll = np.array(all_nll)
    arr_nll = -arr_nll - 11130.4
    arr_significance = []
    arr_significance2 = []
    for i in range(len(arr_nll)):
        #temp_significance = ROOT.RooStats.PValueToSignificance(ROOT.TMath.Prob(arr_nll[i], 5))
        temp_significance2 = sqrt(2) * ROOT.TMath.ErfcInverse(ROOT.TMath.Prob(2*arr_nll[i], 10))
        #sqrt(2)*TMath::ErfcInverse(TMath::Prob(80,5))
        #temp_significance2 = sqrt(2*arr_nll[i])
        #arr_significance.append(temp_significance)
        arr_significance2.append(temp_significance2)
    #plt.plot(all_mass, arr_significance,'^',label=JPlabel)
    plt.plot(all_mass, arr_significance2,'.',label=JPlabel)


read_logscan("check_1p.txt","1/2+")
read_logscan("check_1m.txt","1/2-")
read_logscan("check_3p.txt","3/2+")
read_logscan("check_3m.txt","3/2-")
#read_logscan("check_5p.txt","5/2+")
#read_logscan("check_5m.txt","5/2-")
plt.ylim(-1, 7)
plt.ylabel("Significance / $\sigma$")
plt.xlabel("Mass / GeV")
plt.legend()
plt.savefig("scan.pdf")
#plt.show()


