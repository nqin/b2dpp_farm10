#!/bin/bash
# This script is used to scan the D0p mass spectrum.
# $1: log id and random seed
# $2: which GPU to use
# $3: start point
# $4: end point
id=${1}
ngpu=${2}
njobs=10
#mass_begin=${3}#[2.800,2.925],[2.925,3.050],[3.050,3.175],[3.175,3.300]
mass_temp=${3}
mass_end=${4}
echo "round of random fit for each scan: ${njobs}"
all_id=`echo "(${mass_end} - ${mass_temp}) / 0.005 * ${njobs}" | bc`
end_id=`echo "$id + $all_id" | bc`
echo "id range [$id, $end_id]"

while [ $(echo "${mass_temp} < ${mass_end}" | bc -l) -ge 1 ]; do
    ifit=0
    while [ ${ifit} -lt ${njobs} ]; do
       nid=`printf "%04d" ${id}`
       echo "id=${nid}"
       #../bin/FitScan${ngpu}_farm10 ${id} ${mass_temp} >& logscan_model2/logscan_Xchain/fit-$mass_temp-$nid.log
       ../bin/FitScan${ngpu}_farm10 ${id} ${mass_temp} >& logscan_model2/fit-$mass_temp-$nid.log
       #cp logscan/fit-$nid.log logscan${ngpu}/fit-$nid.log
       id=$((id + 1))  
       ifit=$((ifit + 1))  
    done
    mass_temp=`echo "${mass_temp} + 0.005" | bc`
    echo "mass_temp = ${mass_temp}"
done

wait
echo "all jobs are done!"
