from math import sqrt
import os
import numpy as np
import pandas as pd
import multiprocessing as mp

def com_df_group(df_1,df_2):
    if df_1["Group"] == df_2["Group"]:
        return False
    else:
        return True
def com_df_league(df_1,df_2):
    if df_1["League"] == df_2["League"]:
        return False
    else:
        return True
def how_many_match(df_1,df_2):
    if len(df_1)!=len(df_2):
        print("The length of two dataframes is not equal!")
        return -999
    list_match = np.zeros((2,len(df_1)))
    for i in range(len(df_1)):
        for j in range(len(df_2)):
            if com_df_group(df_1.iloc[i],df_2.iloc[j]) and com_df_league(df_1.iloc[i],df_2.iloc[j]):
                list_match[0][i] += 1
    for i in range(len(df_2)):
        for j in range(len(df_1)):
            if com_df_group(df_2.iloc[i],df_1.iloc[j]) and com_df_league(df_2.iloc[i],df_1.iloc[j]):
                list_match[1][i] += 1
    return list_match
def pick_specific(df_rank1, df_rank2, which_to_pick:int, pick_number:int,Champion_16_temp):
    if which_to_pick == 1:
        df_temp1 = df_rank1.copy()
        df_rank1 = df_rank2
        df_rank2 = df_temp1
    df_rank2_picked = df_rank2.iloc[pick_number]
    df_rank1_todrop = df_rank1[(df_rank1["Group"]==df_rank2_picked["Group"]) | (df_rank1["League"]==df_rank2_picked["League"])]  # type: ignore
    df_rank1_inpool = df_rank1.drop(df_rank1_todrop.index).reset_index(drop=True)
    df_rank1_picked = df_rank1_inpool.iloc[np.random.randint(0,len(df_rank1_inpool))]
    df_rank1 = df_rank1.drop(df_rank1.loc[df_rank1["Name"]==df_rank1_picked["Name"]].index)
    df_rank2 = df_rank2.drop(df_rank2.loc[df_rank2["Name"]==df_rank2_picked["Name"]].index)
    if which_to_pick == 1:
        Champion_16_temp[df_rank1_picked["TH2F"]-1][df_rank2_picked["TH2F"]-1] += 1  
        return df_rank2,df_rank1,Champion_16_temp
    else:
        Champion_16_temp[df_rank2_picked["TH2F"]-1][df_rank1_picked["TH2F"]-1] += 1  
        return df_rank1,df_rank2,Champion_16_temp
def one_round(df_rank1,df_rank2,which_to_pick,Champion_16_temp):
    pick_number = np.random.randint(0,len(df_rank2))
    df_rank1,df_rank2,Champion_16_temp = pick_specific(df_rank1,df_rank2,which_to_pick,pick_number,Champion_16_temp)
    return df_rank1,df_rank2,Champion_16_temp

"""
def run_times_for_multiprocessing(Champion_16):
    np.random.seed(os.getpid())
    df_all_team = pd.read_csv("EuroChampion.csv")
    df_rank1_original = df_all_team[df_all_team["Rank"]==1]
    df_rank2_original = df_all_team[df_all_team["Rank"]==2]
    df_rank1 = df_all_team[df_all_team["Rank"]==1]
    df_rank2 = df_all_team[df_all_team["Rank"]==2]
    Champion_16 = np.zeros([8,8])

    MC_times = 0
    fail_times = 0
    total_times = 100
    while MC_times < total_times:
        #if MC_times%100 == 0:
        #    print(MC_times)
        df_rank1 = df_all_team[df_all_team["Rank"]==1]
        df_rank2 = df_all_team[df_all_team["Rank"]==2]
        for i in range(8):
            Champion_16_temp = np.zeros([8,8])
            if len(df_rank2)>5:
                df_rank1,df_rank2,Champion_16_temp = one_round(df_rank1,df_rank2,2,Champion_16_temp)
                Champion_16 += Champion_16_temp
            else:
                list_match = how_many_match(df_rank1,df_rank2)
                rank1_match = np.prod([item-1 for item in list_match[0]])
                rank2_match = np.prod([item-1 for item in list_match[1]])
                #if MC_times>30:
                #    print(MC_times)
                #    print(list_match)
                if len(list_match[0]) == 3:
                    if np.sum(list_match[0]) == 4:
                        if(np.array(list_match[0]) == 2).any():
                            fail_times += 1
                            break
                    elif np.sum(list_match[0]) == 5:
                        if(np.array(list_match[0]) == 3).any():
                            fail_times += 1
                            break
                if rank2_match and rank1_match:
                    df_rank1,df_rank2,Champion_16_temp = one_round(df_rank1,df_rank2,2,Champion_16_temp)
                    Champion_16 += Champion_16_temp
                #elif not rank2_match and rank1_match:
                elif not rank2_match:
                    for j in range(len(list_match[1])):
                        if list_match[1][j]>1:
                            continue
                        elif list_match[1][j]==1:
                            df_rank1,df_rank2,Champion_16_temp = pick_specific(df_rank1,df_rank2,2,j,Champion_16_temp)
                            Champion_16 += Champion_16_temp
                            break
                        else:
                            print("error1")
                #elif not rank1_match and rank2_match:
                elif not rank1_match:
                    for j in range(len(list_match[0])):
                        if list_match[0][j]>1:
                            continue
                        elif list_match[0][j]==1:
                            df_rank1,df_rank2,Champion_16_temp = pick_specific(df_rank1,df_rank2,1,j,Champion_16_temp)
                            Champion_16 += Champion_16_temp
                            break
                        else:
                            print("error2")
                else:
                    print("error3")
        MC_times += 1
    print(Champion_16)
"""


fn = "./Champion16.csv"
def run_times_for_multiprocessing(q):
    np.random.seed(os.getpid())
    df_all_team = pd.read_csv("EuroChampion.csv")
    df_rank1_original = df_all_team[df_all_team["Rank"]==1]
    df_rank2_original = df_all_team[df_all_team["Rank"]==2]
    df_rank1 = df_all_team[df_all_team["Rank"]==1]
    df_rank2 = df_all_team[df_all_team["Rank"]==2]
    Champion_16 = np.zeros([8,8])

    MC_times = 0
    fail_times = 0
    total_times = 100
    while MC_times < total_times:
        #if MC_times%100 == 0:
        #    print(MC_times)
        df_rank1 = df_all_team[df_all_team["Rank"]==1]
        df_rank2 = df_all_team[df_all_team["Rank"]==2]
        for i in range(8):
            Champion_16_temp = np.zeros([8,8])
            if len(df_rank2)>5:
                df_rank1,df_rank2,Champion_16_temp = one_round(df_rank1,df_rank2,2,Champion_16_temp)
                Champion_16 += Champion_16_temp
            else:
                list_match = how_many_match(df_rank1,df_rank2)
                rank1_match = np.prod([item-1 for item in list_match[0]])
                rank2_match = np.prod([item-1 for item in list_match[1]])
                #if MC_times>30:
                #    print(MC_times)
                #    print(list_match)
                if len(list_match[0]) == 3:
                    if np.sum(list_match[0]) == 4:
                        if(np.array(list_match[0]) == 2).any():
                            fail_times += 1
                            break
                    elif np.sum(list_match[0]) == 5:
                        if(np.array(list_match[0]) == 3).any():
                            fail_times += 1
                            break
                if rank2_match and rank1_match:
                    df_rank1,df_rank2,Champion_16_temp = one_round(df_rank1,df_rank2,2,Champion_16_temp)
                    Champion_16 += Champion_16_temp
                #elif not rank2_match and rank1_match:
                elif not rank2_match:
                    for j in range(len(list_match[1])):
                        if list_match[1][j]>1:
                            continue
                        elif list_match[1][j]==1:
                            df_rank1,df_rank2,Champion_16_temp = pick_specific(df_rank1,df_rank2,2,j,Champion_16_temp)
                            Champion_16 += Champion_16_temp
                            break
                        else:
                            print("error1")
                #elif not rank1_match and rank2_match:
                elif not rank1_match:
                    for j in range(len(list_match[0])):
                        if list_match[0][j]>1:
                            continue
                        elif list_match[0][j]==1:
                            df_rank1,df_rank2,Champion_16_temp = pick_specific(df_rank1,df_rank2,1,j,Champion_16_temp)
                            Champion_16 += Champion_16_temp
                            break
                        else:
                            print("error2")
                else:
                    print("error3")
        MC_times += 1
    #print(Champion_16)
    text=""
    for i in range(len(Champion_16[0])):
        for j in range(len(Champion_16[0])):
            text += str(Champion_16[i][j]) + ","
    with open(fn, "rb") as f:
        size = len(f.read())
    res = "Process-{}-{}".format(os.getpid(), size)
    q.put(text)
    return res

def listener(q):
    '''listens for messages on the q, writes to file. '''

    with open(fn, 'w') as f:
        while 1:
            m = q.get()
            if m == 'kill':
                f.write('killed')
                break
            f.write(str(m) + '\n')
            f.flush()

if __name__ == "__main__":
    Champion_16 = np.zeros([8,8])
#    pool = Pool(processes=10)
#    pool.map(run_times_for_multiprocessing, [Champion_16 for i in range(10)])
#    pool.close()
#    pool.join()

    manager = mp.Manager()
    q = manager.Queue()
    pool = mp.Pool(processes=150)
    watcher = pool.apply_async(listener, (q,))

    jobs = []
    for i in range(100):
        job = pool.apply_async(run_times_for_multiprocessing, (q,))
        jobs.append(job)
    print("done")
    #https://stackoverflow.com/questions/13446445/python-multiprocessing-safely-writing-to-a-file


#def main():
#    #must use Manager queue here, or will not work
#    manager = mp.Manager()
#    q = manager.Queue()    
#    pool = mp.Pool(mp.cpu_count() + 2)
#
#    #put listener to work first
#    watcher = pool.apply_async(listener, (q,))
#
#    #fire off workers
#    jobs = []
#    for i in range(80):
#        job = pool.apply_async(worker, (i, q))
#        jobs.append(job)
