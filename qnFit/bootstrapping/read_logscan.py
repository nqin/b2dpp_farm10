from math import sqrt
import matplotlib.pyplot as plt
import numpy as np
import sys, os
import ROOT
import pandas as pd
from dataclasses import make_dataclass


def read_logscan(logfile):
    one_log = make_dataclass("one_log", [("mass", float), ("id", str), ("nll",float), ("status", str)])
    df1 = pd.DataFrame()
    f1 = open(logfile,"r+")
    for l1 in f1:
        nll_temp = l1.split()[1]
        mass_temp = l1.split()[0].split("//")[1].split("-")[1]
        id_temp = l1.split()[0].split("//")[1].split("-")[2].split(".")[0]
        status_temp = l1.split()[3]
        df1 = df1.append([one_log(float(mass_temp), str(id_temp), float(nll_temp), str(status_temp))], ignore_index=True)
    return df1

# Use pd.groupby and pd.merge to get the id of minimum nll for each bootstrapping sample.
df1 = read_logscan("/disk401/lhcb/qinning/b2dpp_farm10/qnFit/bootstrapping/check_log.txt")
df_converged = df1[df1["status"] == "CONVERGED"]
df_groupby = df_converged[["mass","nll"]].groupby(by="mass")["nll"].min()
df_merge = pd.merge(df_groupby,df_converged,how="left",on=["mass","nll"])
df_merge_firstid = df_merge.groupby(by="mass")["id"].min()
df_merge = pd.merge(df_merge_firstid,df_merge,how="left",on=["mass","id"])


# With technological advancements emerging, automotive digitalization was accelerated by social distance
# and disrupted manufacturing during the covid-19 pandemic. An increasing number of automotive players 
# are exploring new business models to increase profit margins.
# 
# As the crucial customers engagement bridge and important sales channel, digital touchpoints such as App,
# WeChat Official Accounts and Wechat Mini-Programs are playing an increasingly significant role in OEM
# business process. Hence, digital touchpoints strategy and positioning identification and feature enhancement
# is more important than ever.
#
# As an Accenture consultant, your task is to conduct a pain point analysis of Mercedes Benz app and derive implementable
# suggestions for performance improvement.
# Requirements
# 1. App pain points analysis.
# 2. Corresponding enhance proposal with market best practices.
# 3. Deliver your PPT in English and required templates
# 4. Attach your research script to your email(Excel Preferred)

