#!/usr/bin/env python
# coding: utf-8

from math import sqrt
import matplotlib.pyplot as plt
import numpy as np
import sys, os
from sys import argv
import ROOT
import pandas as pd
from dataclasses import make_dataclass

def cal_asym_std(df_input, mean_input):
    if df_input.empty:
        return -999
    else:
        return sqrt(pow(df_input - mean_input,2).sum()/df_input.size)

def read_logfile(logfile):
    one_log = make_dataclass("one_log", [("id", str), ("nll",float), ("status", str)])
    df1 = pd.DataFrame()
    f1 = open(logfile,"r+")
    for l1 in f1:
        nll_temp = l1.split()[1]
        id_temp = l1.split()[0].split("//")[1].split("-")[1].split(".")[0]
        status_temp = l1.split()[3]
        df1 = df1.append([one_log(str(id_temp), float(nll_temp), str(status_temp))], ignore_index=True)
    return df1

def read_reverse_order(file_name):
    with open(file_name, 'rb') as read_obj:
        read_obj.seek(0, os.SEEK_END)
        pointer_location = read_obj.tell()
        buffer = bytearray()
        while pointer_location >= 0:
            read_obj.seek(pointer_location)
            pointer_location = pointer_location -1
            new_byte = read_obj.read(1)
            if new_byte == b'\n':
                yield buffer.decode()[::-1]
                buffer = bytearray()
            else:
                buffer.extend(new_byte)
        if len(buffer) > 0:
            yield buffer.decode()[::-1]

def read_FF(logfile_name):
    i = 0
    temp_dict = {}
    for line in read_reverse_order(logfile_name):
        if(i>18):
            break
        if(i>5):
            #print({line.split()[0]:line.split()[1]})
            temp_dict[line.split()[0]] = line.split()[1]
            #print(line.split()[1])
            #print(line.split()[0])
        i = i+1
    return temp_dict

df_FF = pd.read_csv("FF_result.csv")
df_FF.describe()

dict_FF_model0 = {"X_f2_2150":5.12, "X_rho_2150":9.36, "X_f2_1950":3.63, "X_NR0m":2.41, "X_Fwave":49.77, "Y_L_2765":8.51, "Y_L_2940":0.14, "Y_L_2860":4.07, "Y_L_2880":0.28, "Y_L_2900":4.22, "Y_L_3130":12.26, "Y_L_3189":13.40}


temp_name = "X_f2_2150"
df_FF_temp = df_FF[df_FF[temp_name] > dict_FF_model0[temp_name]]


list_up = []
list_lo = []
for key,value in dict_FF_model0.items():
    df_FF_up = df_FF[ df_FF[key] > value ][key]
    df_FF_lo = df_FF[ df_FF[key] < value ][key]
    list_up.append(cal_asym_std(df_FF_up, value))
    list_lo.append(cal_asym_std(df_FF_lo, value))

i=0
for key,value in dict_FF_model0.items():
    temp_FF = "$" + str(value) + "^{+" + "{:.2f}".format(list_up[i]) +"}" + "_{-" + "{:.2f}".format(list_lo[i]) +"}$ "
    line = "{:<15} & {:<30} \\\\".format(key, temp_FF)
    print(line)
    i = i+1

#df_mean = pd.DataFrame(read_FF("test.log"),index=[0])
#df_log_model0 = read_logfile("/disk401/lhcb/qinning/b2dpp_farm10/qnFit/logrnd_model0_new/logrnd_model0_new/sortThelog.log")
#df_log_model0_new = df_log_model0[ df_log_model0["nll"] == df_log_model0["nll"].min() ]
#arr_log_model0 = df_log_model0_new.values.tolist()
#for i in range(len(arr_log_model0)):
#    temp_filename = "/disk401/lhcb/qinning/b2dpp_farm10/qnFit/logrnd_model0_new/logrnd_model0_new/fit-"+str(arr_log_model0[i][0])+".log"
#    df_mean = df_mean.append(read_FF(temp_filename),ignore_index=True)
#df_mean = df_mean.drop([0])
#print(df_mean)






