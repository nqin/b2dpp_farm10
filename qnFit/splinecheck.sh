#!/bin/bash 
#source ../gpu10.sh
id=${1}
ngpu=${2}
phase=${3}

#str="testKM4Pc-fullPHSP-fineBinning_ExtLst_openCoupleChannel-4440${Pn1}${J1}-4457${Pn2}${J2}-4312${Pn3}${J3}-4600${Pn4}${J4}_ImMass_wlt500"
njobs=10
all_id=`echo "${njobs}*8" | bc`
end_id=`echo "$id + $all_id" | bc`
echo "id range [$id, $end_id]"
itemp=0
while [ ${itemp} -lt 8 ]; do
ifit=0
    while [ "$ifit" -lt "$njobs" ]; do
        #echo "Number = $id"
        nid=`printf  "%04d" $id`
        echo $nid
        ../bin/FitSplineCheck${ngpu}_farm10 ${id} ${phase} >& ./splinecheck_changeX00/fit_${phase}_${nid}.log
        #../bin/FitRnd${ngpu} $id >& logrnd/fit-$nid.log 
        id=$((id + 1))  
        ifit=$((ifit + 1))  
    done
    echo "fixed phase = ${phase}"
    itemp=`echo "${itemp} + 1" | bc`
    phase=`echo "${phase} + 0.2" | bc`
done

wait
python ./send_email.py $id $ngpu $njobs

echo all jobs are done!
