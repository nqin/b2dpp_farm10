from math import sqrt
import matplotlib.pyplot as plt
import numpy as np
import sys, os
import ROOT
import pandas as pd
from dataclasses import make_dataclass


def read_logscan(logfile,JPlabel):
    one_log = make_dataclass("one_log", [("mass", float), ("id", str), ("nll",float)])
    df1 = pd.DataFrame()
    f1 = open(logfile,"r+")
    for l1 in f1:
        nll_temp = l1.split()[1]
        mass_temp = l1.split()[0].split("/")[3].split("-")[1]
        id_temp = l1.split()[0].split("/")[3].split("-")[2].split(".")[0]
        df1 = df1.append([one_log(float(mass_temp), str(id_temp), float(nll_temp))], ignore_index=True)

    #all_mass = np.linspace(float(2.800),float(3.300),100,endpoint=False)
    all_mass = np.linspace(float(1.870),float(2.370),100,endpoint=False)
    all_nll = []

    for one_mass in all_mass:
        min_nll = df1[abs(df1.mass-one_mass)<0.00001].nll.min()
        all_nll.append(min_nll)

    arr_nll = np.array(all_nll)
    arr_nll = -arr_nll - 11111.6
    arr_significance = []
    arr_significance2 = []
    for i in range(len(arr_nll)):
        #temp_significance = ROOT.RooStats.PValueToSignificance(ROOT.TMath.Prob(arr_nll[i], 5))
        if(arr_nll[i]<0):
            temp_significance2 = -0.5
        else:
            temp_significance2 = sqrt(2) * ROOT.TMath.ErfcInverse(ROOT.TMath.Prob(arr_nll[i], 5))
        #sqrt(2)*TMath::ErfcInverse(TMath::Prob(80,5))
        #temp_significance2 = sqrt(2*arr_nll[i])
        #arr_significance.append(temp_significance)
        arr_significance2.append(temp_significance2)
    #plt.plot(all_mass, arr_significance,'^',label=JPlabel)
    plt.plot(all_mass, arr_significance2,'.',label=JPlabel)


read_logscan("/disk401/lhcb/qinning/b2dpp_farm10/qnFit/logscan_model2/logscan_Xchain/check_0p.txt","0+")
read_logscan("/disk401/lhcb/qinning/b2dpp_farm10/qnFit/logscan_model2/logscan_Xchain/check_0m.txt","0-")
read_logscan("/disk401/lhcb/qinning/b2dpp_farm10/qnFit/logscan_model2/logscan_Xchain/check_1p.txt","1+")
read_logscan("/disk401/lhcb/qinning/b2dpp_farm10/qnFit/logscan_model2/logscan_Xchain/check_1m.txt","1-")
read_logscan("/disk401/lhcb/qinning/b2dpp_farm10/qnFit/logscan_model2/logscan_Xchain/check_2p.txt","2+")
read_logscan("/disk401/lhcb/qinning/b2dpp_farm10/qnFit/logscan_model2/logscan_Xchain/check_2m.txt","2-")
plt.ylim(-1, 5)
plt.ylabel("Significance / $\sigma$")
plt.xlabel("Mass / GeV")
plt.legend()
plt.savefig("scan.pdf")
plt.savefig("scan.png")
#plt.show()


# With technological advancements emerging, automotive digitalization was accelerated by social distance
# and disrupted manufacturing during the covid-19 pandemic. An increasing number of automotive players 
# are exploring new business models to increase profit margins.
# 
# As the crucial customers engagement bridge and important sales channel, digital touchpoints such as App,
# WeChat Official Accounts and Wechat Mini-Programs are playing an increasingly significant role in OEM
# business process. Hence, digital touchpoints strategy and positioning identification and feature enhancement
# is more important than ever.
#
# As an Accenture consultant, your task is to conduct a pain point analysis of Mercedes Benz app and derive implementable
# suggestions for performance improvement.
# Requirements
# 1. App pain points analysis.
# 2. Corresponding enhance proposal with market best practices.
# 3. Deliver your PPT in English and required templates
# 4. Attach your research script to your email(Excel Preferred)

