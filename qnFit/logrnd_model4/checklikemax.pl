#!/usr/bin/perl
$dir = $ARGV[0];
opendir DIR, $dir || die "Can\'t open dir";
while ($file = readdir DIR) 
{
  if($file =~ /$ARGV[1]/) {
    @like = check("$file");  
    if($like[0]!=0) {printf "% #50s % #10.2f % #5i % #12s  \n",  $file, $like[0], $like[1], $like[2] }
  }
}

sub check{
    my @tau=();
    $file = "$dir/$_[0]";
    open(INP, $file) || die "file doesn't exist:$!";
    $like = 0;
    $like0 = 0;
    $edm = 0;
    $stus = "UNDEF";
    $call = 0;
    $chpar = -1;
    $npar = 0;
    while(<INP>) {
      if(/STATUS/) {
  	my @par =  split(' ', $_);
        my @li = split('=', $par[0]);
        if($li[1] <= $like0) { 
          $like = $li[1];	    
          $like0=$li[1];          
          my @li = split('=', $par[3]);
          $stus = $li[1];
        }
      }
      if(/DEFINITIONS/) {
          $chpar=1;
#          print "here !\n";
      }
      if($chpar > 0) {
        my @li = split(' ', $_);
        if(/\*\*/) { $chpar = -1; }
        if( ($chpar > 0) && ($li[0] > 0) ) {$npar = $li[0];}
      }  
        
    }
    close INP;
    push(@tau, $like, $npar, $stus);
    return @tau;
}
#if($isok eq 1) {
#  print "ok\n";
#} else {
#  print "notok\n";
#}
