import re, sys

#usage:
# python GetParas.py /home/zhanglm/workdir/Lb/code/code-test1/Fit/log-test/fit2Z-m1.log fit.func new.func

if len(sys.argv) <2:
   print "====>>> Please specify the log file <<<===="
   print "====>>> Please specify a \"XXX.func\" file as a template<<<===="
   exit(0)
if len(sys.argv) <3:
   print "====>>> Please specify a \"XXX.func\" file as a template<<<===="
   exit(0)

#open the log file
logfile_name = sys.argv[1]
f_log=open(logfile_name)
data = f_log.read()
para_blocks = data.split("EXT PARAMETER")

#get the printed parameter block
last = para_blocks[-1]
last_but_one = para_blocks[-2]

#index of parameters
par_index = re.compile("[0-9]{1,3}") 

def get_para_value_error(par_block):
   entries = par_block.split("\n")
   para_value={}
   
   for entry in entries:
      if "ERR DEF" in entry: break 
      if "FCN=" in entry: break
      entry = entry.split()
      if not entry: continue
      #print entry
      index = par_index.findall(entry[0])
      if index:
         para_value[entry[1]] = (entry[2],entry[3]) # parameter name = (value,error)
   return para_value

last_full_print = get_para_value_error(last)
second_last_print = get_para_value_error(last_but_one)

if len(second_last_print) > len(last_full_print):
   print len(second_last_print),len(last_full_print)
   last_full_print = second_last_print
   print "The last parameter block is not complete. "



# Now we read the range and set-constant flag from XXX.func file
# The method is:
# 1) read each variable and its properties from XXX.func file
# 2) replace its initial (fitted) value with new value in YYY.log file
# 3) Is its uncertainty useful?
parfile_name = sys.argv[2]
f_conf = open(parfile_name)
# write to new file, specify its name
g = open(sys.argv[3],"w")


print "\n"*5
name_pattern = re.compile("[a-zA-Z][a-zA-Z_0-9]*")
value_pattern = re.compile("[-+]?[0-9\.]+[eE]?[+-]?[0-9]*")
variable_value_pattern = re.compile("[a-zA-Z][a-zA-Z_0-9]*[ \t\n\r\f\v]*=[ \t\n\r\f\v]*[-+]?[0-9]*\.?[0-9]*[eE]?[+-]?[0-9]*[ \t\n\r\f\v]*\+/-[ \t\n\r\f\v][-+]?[0-9]*\.?[0-9]*[eE]?[+-]?[0-9]*") 
variable_value_pattern_C = re.compile("[a-zA-Z][a-zA-Z_0-9]*[ \t\n\r\f\v]*=[ \t\n\r\f\v]*[-+]?[0-9]*\.?[0-9]*[eE]?[+-]?[0-9]*[ \t\n\r\f\v]*C") 
for line in f_conf.readlines():
   v_v = variable_value_pattern.findall(line)
   if not v_v: v_v = variable_value_pattern_C.findall(line)
   v_v = v_v[0]

   v_v = v_v.split("=")
   name = name_pattern.findall(v_v[0])[0]
   value_error = value_pattern.findall(v_v[1])
   value = value_error[0]
   error = ""
   if len(value_error) ==2:
      error = value_error[1]
   if name in last_full_print:
      line = line.replace(value,"%.8f"%(float(last_full_print[name][0])))
      #line = line.replace(value,str(float(last_full_print[name][0])))
      if error:
         line = line.replace(error,"%.8f"%(float(last_full_print[name][1])))
         #line = line.replace(error,str(float(last_full_print[name][1])))
#         print "%10s +/- %-10s    updated to    %10s +/- %-10s"%(value,error,str(float(last_full_print[name][0])),str(float(last_full_print[name][1])))

   g.write(line)

f_conf.close()
g.close()
#print len(second_last_print),  len(last_full_print)
