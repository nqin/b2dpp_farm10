#!/bin/bash 
#source ../gpu10.sh
id=${1}
ngpu=${2}
bootstrap_start=${3}
state=${4}

if [ "$HOSTNAME" = hepgpu10 ]
then
    machine="farm10"
elif [ "$HOSTNAME" = hepfarm40 ]
then
    machine="farm40"
elif [ "$HOSTNAME" = hepfarm41 ]
then
    machine="farm41"
else
    echo "not right gpu farm" $HOSTNAME
fi

#str="testKM4Pc-fullPHSP-fineBinning_ExtLst_openCoupleChannel-4440${Pn1}${J1}-4457${Pn2}${J2}-4312${Pn3}${J3}-4600${Pn4}${J4}_ImMass_wlt500"
njobs=10
all_id=`echo "${njobs}*100" | bc`
end_id=`echo "$id + $all_id" | bc`
echo "id range [$id, $end_id]"
itemp=0
while [ ${itemp} -lt 100 ]; do
ifit=0
    while [ "$ifit" -lt "$njobs" ]; do
        #echo "Number = $id"
        nid=`printf  "%04d" $id`
        echo $nid
        ../bin/FitBootstrapping${ngpu}_${state}_${machine} ${id} ${bootstrap_start} fit-${bootstrap_start}-${nid} >& ./bootstrapping/log_bootstrapping/fit-${bootstrap_start}-${nid}.log
        #../bin/FitRnd${ngpu} $id >& logrnd/fit-$nid.log 
        id=$((id + 1))  
        ifit=$((ifit + 1))  
    done
    echo "bootstrap_id = ${bootstrap_start}"
    itemp=`echo "${itemp} + 1" | bc`
    bootstrap_start=`echo "${bootstrap_start} + 1" | bc`
done

wait
python ./send_email.py $id $ngpu $njobs

echo all jobs are done!
