  int typeR(360);
  int typeKM(360);
  int typeFlatte = FLATTE;
  typeR = BREITWIGNER;
  typeKM = BREITWIGNER;
  TList *listX = new TList();


  //ppbar-NonRes
  //spin-0, only has 1 helicity coupling, 
  RooRealVar m0_NR0m("m0_NR0m","m0",-1);
  RooRealVar width_NR0m("width_NR0m","width",0.,-10,10);
  //helicity coupling for 1/2, 1/2
  RooRealVar ar_H0_NR0m("ar_H0_NR0m","",0,-10,10);
  RooRealVar ai_H0_NR0m("ai_H0_NR0m","",0.,-10,10);
  //helicity coupling for 1/2, -1/2
  RooRealVar ar_H1_NR0m("ar_H1_NR0m","",0.);
  RooRealVar ai_H1_NR0m("ai_H1_NR0m","",0.);  

  RooArgList* X_NR0m = new RooArgList(ar_H0_NR0m,ai_H0_NR0m,
				      ar_H1_NR0m,ai_H1_NR0m,"X_NR0m");
  X_NR0m->add(m0_NR0m);
  X_NR0m->add(width_NR0m);
  X_NR0m->add(RooRealConstant::value(typeKM));
  X_NR0m->add(RooRealConstant::value(0));//spin
  X_NR0m->add(RooRealConstant::value(-1)); //parity
 
  //ppbar-NonRes
  RooRealVar m0_NR1m("m0_NR1m","m0",-1);
  RooRealVar width_NR1m("width_NR1m","width",0.,-10,10);
  //helicity coupling for 1/2, 1/2
  RooRealVar ar_H0_NR1m("ar_H0_NR1m","",0,-10,10);
  RooRealVar ai_H0_NR1m("ai_H0_NR1m","",0.,-10,10);
  //helicity coupling for 1/2, -1/2
  RooRealVar ar_H1_NR1m("ar_H1_NR1m","",0.,-10,10);
  RooRealVar ai_H1_NR1m("ai_H1_NR1m","",0.,-10,10);  

  RooArgList* X_NR1m = new RooArgList(ar_H0_NR1m,ai_H0_NR1m,
				      ar_H1_NR1m,ai_H1_NR1m,"X_NR1m");
  X_NR1m->add(m0_NR1m);
  X_NR1m->add(width_NR1m);
  X_NR1m->add(RooRealConstant::value(typeKM));
  X_NR1m->add(RooRealConstant::value(1));//spin
  X_NR1m->add(RooRealConstant::value(-1)); //parity
 
  RooRealVar m0_NR1p("m0_NR1p","m0",-1);
  RooRealVar width_NR1p("width_NR1p","width",0,-10,10);
  //helicity coupling for 1/2, 1/2
  RooRealVar ar_H0_NR1p("ar_H0_NR1p","",0,-10,10);
  RooRealVar ai_H0_NR1p("ai_H0_NR1p","",0.,-10,10);
  //helicity coupling for 1/2, -1/2
  RooRealVar ar_H1_NR1p("ar_H1_NR1p","",0.,-10,10);
  RooRealVar ai_H1_NR1p("ai_H1_NR1p","",0.,-10,10);  

  RooArgList* X_NR1p = new RooArgList(ar_H0_NR1p,ai_H0_NR1p,
				      ar_H1_NR1p,ai_H1_NR1p,"X_NR1p");
  X_NR1p->add(m0_NR1p);
  X_NR1p->add(width_NR1p);
  X_NR1p->add(RooRealConstant::value(typeKM));
  X_NR1p->add(RooRealConstant::value(1));//spin
  X_NR1p->add(RooRealConstant::value(1)); //parity
 

  //ppbar-NonRes
  RooRealVar m0_NR2("m0_NR2","m0",-1);
  RooRealVar width_NR2("width_NR2","width",0.);
  //helicity coupling for 1/2, 1/2
  RooRealVar ar_H0_NR2("ar_H0_NR2","",0,-10,10);
  RooRealVar ai_H0_NR2("ai_H0_NR2","",0.,-10,10);
  //helicity coupling for 1/2, -1/2
  RooRealVar ar_H1_NR2("ar_H1_NR2","",0.,-10,10);
  RooRealVar ai_H1_NR2("ai_H1_NR2","",0.,-10,10);  

  RooArgList* X_NR2 = new RooArgList(ar_H0_NR2,ai_H0_NR2,
				      ar_H1_NR2,ai_H1_NR2,"X_NR2");
  X_NR2->add(m0_NR2);
  X_NR2->add(width_NR2);
  X_NR2->add(RooRealConstant::value(typeKM));
  X_NR2->add(RooRealConstant::value(2));//spin
  X_NR2->add(RooRealConstant::value(1)); //parity
 

  //Dp-NonRes
  //spin-0, only has 1 helicity coupling, 
  RooRealVar m0_Dp_NR("m0_Dp_NR","m0",-1);
  RooRealVar width_Dp_NR("width_Dp_NR","width",0.0,-10,10);
  //helicity coupling for 1/2, 1/2
  RooRealVar ar_H0_Dp_NR("ar_H0_Dp_NR","",0.,-10,10);
  RooRealVar ai_H0_Dp_NR("ai_H0_Dp_NR","",0.,-10,10);
  //helicity coupling for 1/2, -1/2
  RooRealVar ar_H1_Dp_NR("ar_H1_Dp_NR","",0.,-10,10);
  RooRealVar ai_H1_Dp_NR("ai_H1_Dp_NR","",0.,-10,10);  

  RooArgList* Dp_NR = new RooArgList(ar_H0_Dp_NR,ai_H0_Dp_NR,
				      ar_H1_Dp_NR,ai_H1_Dp_NR,"Dp_NR");
  Dp_NR->add(m0_Dp_NR);
  Dp_NR->add(width_Dp_NR);
  Dp_NR->add(RooRealConstant::value(typeKM));
  Dp_NR->add(RooRealConstant::value(1));//spin
  Dp_NR->add(RooRealConstant::value(-1)); //parity



  //ppbar
  //spin-0, only has 1 helicity coupling, 
  RooRealVar m0_1835("m0_1835","m0",1.8265);
  RooRealVar width_1835("width_1835","width",0.242);
  //helicity coupling for 1/2, 1/2
  RooRealVar ar_H0_1835("ar_H0_1835","",1.);
  RooRealVar ai_H0_1835("ai_H0_1835","",0.);
  //helicity coupling for 1/2, -1/2
  RooRealVar ar_H1_1835("ar_H1_1835","",0.);
  RooRealVar ai_H1_1835("ai_H1_1835","",0.);  

  RooArgList* X_X_1835 = new RooArgList(ar_H0_1835,ai_H0_1835,
				      ar_H1_1835,ai_H1_1835,"X_X_1835");
  X_X_1835->add(m0_1835);
  X_X_1835->add(width_1835);
  X_X_1835->add(RooRealConstant::value(typeKM));
  RooRealVar J1835("J1835","",0);
  RooRealVar P1835("P1835","",-1);  
  X_X_1835->add(J1835);//spin
  X_X_1835->add(P1835); //parity

//====Spin 2=====//
  //spin2
  RooRealVar m0_f_1950("m0_f_1950","m0",1.936);
  RooRealVar width_f_1950("width_f_1950","width",0.464);
  //helicity coupling for 1/2, 1/2
  RooRealVar ar_H0_f_1950("ar_H0_f_1950","",0.,-10,10);
  RooRealVar ai_H0_f_1950("ai_H0_f_1950","",0.,-10,10);
  //helicity coupling for 1/2, -1/2
  RooRealVar ar_H1_f_1950("ar_H1_f_1950","",0.,-10,10);
  RooRealVar ai_H1_f_1950("ai_H1_f_1950","",0.,-10,10);  
  RooArgList* X_f2_1950 = new RooArgList(ar_H0_f_1950,ai_H0_f_1950,
				      ar_H1_f_1950,ai_H1_f_1950,"X_f2_1950");
  X_f2_1950->add(m0_f_1950);
  X_f2_1950->add(width_f_1950);
  X_f2_1950->add(RooRealConstant::value(typeKM));
  X_f2_1950->add(RooRealConstant::value(2));//spin
  X_f2_1950->add(RooRealConstant::value(1)); //parity

  //spin2
  RooRealVar m0_f_2010("m0_f_2010","m0",2.011);
  RooRealVar width_f_2010("width_f_2010","width",0.202);
  //helicity coupling for 1/2, 1/2
  RooRealVar ar_H0_f_2010("ar_H0_f_2010","",0.,-10,10);
  RooRealVar ai_H0_f_2010("ai_H0_f_2010","",0.,-10,10);
  //helicity coupling for 1/2, -1/2
  RooRealVar ar_H1_f_2010("ar_H1_f_2010","",0.,-10,10);
  RooRealVar ai_H1_f_2010("ai_H1_f_2010","",0.,-10,10);  
  RooArgList* X_f2_2010 = new RooArgList(ar_H0_f_2010,ai_H0_f_2010,
				      ar_H1_f_2010,ai_H1_f_2010,"X_f2_2010");
  X_f2_2010->add(m0_f_2010);
  X_f2_2010->add(width_f_2010);
  X_f2_2010->add(RooRealConstant::value(typeKM));
  X_f2_2010->add(RooRealConstant::value(2));//spin
  X_f2_2010->add(RooRealConstant::value(1)); //parity


  //spin2
  RooRealVar m0_f_2150("m0_f_2150","m0",2.157);
  RooRealVar width_f_2150("width_f_2150","width",0.152);
  //helicity coupling for 1/2, 1/2
  RooRealVar ar_H0_f_2150("ar_H0_f_2150","",0.,-100,100);
  RooRealVar ai_H0_f_2150("ai_H0_f_2150","",0.,-100,100);
  //helicity coupling for 1/2, -1/2
  RooRealVar ar_H1_f_2150("ar_H1_f_2150","",0.,-100,100);
  RooRealVar ai_H1_f_2150("ai_H1_f_2150","",0.,-100,100);  
  RooArgList* X_f2_2150 = new RooArgList(ar_H0_f_2150,ai_H0_f_2150,
				      ar_H1_f_2150,ai_H1_f_2150,"X_f2_2150");
  X_f2_2150->add(m0_f_2150);
  X_f2_2150->add(width_f_2150);
  X_f2_2150->add(RooRealConstant::value(typeKM));
  X_f2_2150->add(RooRealConstant::value(2));//spin
  X_f2_2150->add(RooRealConstant::value(1)); //parity

  RooRealVar m0_pi2_2100("m0_pi2_2100","m0",2.090);
  RooRealVar width_pi2_2100("width_pi2_2100","width",0.625);
  //helicity coupling for 1/2, 1/2
  RooRealVar ar_H0_pi2_2100("ar_H0_pi2_2100","",0.,-100,100);
  RooRealVar ai_H0_pi2_2100("ai_H0_pi2_2100","",0.,-100,100);
  //helicity coupling for 1/2, -1/2
  RooRealVar ar_H1_pi2_2100("ar_H1_pi2_2100","",0.,-100,100);
  RooRealVar ai_H1_pi2_2100("ai_H1_pi2_2100","",0.,-100,100);  
  RooArgList* X_pi2_2100 = new RooArgList(ar_H0_pi2_2100,ai_H0_pi2_2100,
				      ar_H1_pi2_2100,ai_H1_pi2_2100,"X_pi2_2100");
  X_pi2_2100->add(m0_pi2_2100);
  X_pi2_2100->add(width_pi2_2100);
  X_pi2_2100->add(RooRealConstant::value(typeKM));
  X_pi2_2100->add(RooRealConstant::value(2));//spin
  X_pi2_2100->add(RooRealConstant::value(-1)); //parity


//====f0====/
  //spin-0, only has 1 helicity coupling, 
  RooRealVar m0_f_2020("m0_f_2020","m0",1.992);
  RooRealVar width_f_2020("width_f_2020","width",0.442);
  //helicity coupling for 1/2, 1/2
  RooRealVar ar_H0_f_2020("ar_H0_f_2020","",0.,-10,10);
  RooRealVar ai_H0_f_2020("ai_H0_f_2020","",0.,-10,10);
  //helicity coupling for 1/2, -1/2
  RooRealVar ar_H1_f_2020("ar_H1_f_2020","",0.);
  RooRealVar ai_H1_f_2020("ai_H1_f_2020","",0.);  
  RooArgList* X_f_2020 = new RooArgList(ar_H0_f_2020,ai_H0_f_2020,
				      ar_H1_f_2020,ai_H1_f_2020,"X_f_2020");
  X_f_2020->add(m0_f_2020);
  X_f_2020->add(width_f_2020);
  X_f_2020->add(RooRealConstant::value(typeKM));
  X_f_2020->add(RooRealConstant::value(0));//spin
  X_f_2020->add(RooRealConstant::value(1)); //parity
 
  //spin-0, only has 1 helicity coupling, 
  RooRealVar m0_f_2100("m0_f_2100","m0",2.086);
  RooRealVar width_f_2100("width_f_2100","width",0.286);
  //helicity coupling for 1/2, 1/2
  RooRealVar ar_H0_f_2100("ar_H0_f_2100","",0.,-10,10);
  RooRealVar ai_H0_f_2100("ai_H0_f_2100","",0.,-10,10);
  //helicity coupling for 1/2, -1/2
  RooRealVar ar_H1_f_2100("ar_H1_f_2100","",0.);
  RooRealVar ai_H1_f_2100("ai_H1_f_2100","",0.);  
  RooArgList* X_f_2100 = new RooArgList(ar_H0_f_2100,ai_H0_f_2100,
				      ar_H1_f_2100,ai_H1_f_2100,"X_f_2100");
  X_f_2100->add(m0_f_2100);
  X_f_2100->add(width_f_2100);
  X_f_2100->add(RooRealConstant::value(typeKM));
  X_f_2100->add(RooRealConstant::value(0));//spin
  X_f_2100->add(RooRealConstant::value(1)); //parity
 
  //spin-0, only has 1 helicity coupling, 
  RooRealVar m0_f_2200("m0_f_2200","m0",2.187);
  RooRealVar width_f_2200("width_f_2200","width",0.207);
  //helicity coupling for 1/2, 1/2
  RooRealVar ar_H0_f_2200("ar_H0_f_2200","",0.,-10,10);
  RooRealVar ai_H0_f_2200("ai_H0_f_2200","",0.,-10,10);
  //helicity coupling for 1/2, -1/2
  RooRealVar ar_H1_f_2200("ar_H1_f_2200","",0.);
  RooRealVar ai_H1_f_2200("ai_H1_f_2200","",0.);  
  RooArgList* X_f_2200 = new RooArgList(ar_H0_f_2200,ai_H0_f_2200,
				      ar_H1_f_2200,ai_H1_f_2200,"X_f_2200");
  X_f_2200->add(m0_f_2200);
  X_f_2200->add(width_f_2200);
  X_f_2200->add(RooRealConstant::value(typeKM));
  X_f_2200->add(RooRealConstant::value(0));//spin
  X_f_2200->add(RooRealConstant::value(1)); //parity
 
   //spin-0, only has 1 helicity coupling, 
  RooRealVar m0_a_1950("m0_a_1950","m0",1.931);
  RooRealVar width_a_1950("width_a_1950","width",0.271);
  //helicity coupling for 1/2, 1/2
  RooRealVar ar_H0_a_1950("ar_H0_a_1950","",0.,-10,10);
  RooRealVar ai_H0_a_1950("ai_H0_a_1950","",0.,-10,10);
  //helicity coupling for 1/2, -1/2
  RooRealVar ar_H1_a_1950("ar_H1_a_1950","",0.);
  RooRealVar ai_H1_a_1950("ai_H1_a_1950","",0.);  
  RooArgList* X_a_1950 = new RooArgList(ar_H0_a_1950,ai_H0_a_1950,
				      ar_H1_a_1950,ai_H1_a_1950,"X_a_1950");
  X_a_1950->add(m0_a_1950);
  X_a_1950->add(width_a_1950);
  X_a_1950->add(RooRealConstant::value(typeKM));
  X_a_1950->add(RooRealConstant::value(0));//spin
  X_a_1950->add(RooRealConstant::value(1)); //parity

  //spin-0, only has 1 helicity coupling, 
  RooRealVar m0_f_1710("m0_f_1710","m0",1.704);
  RooRealVar width_f_1710("width_f_1710","width",0.123);
  //helicity coupling for 1/2, 1/2
  RooRealVar ar_H0_f_1710("ar_H0_f_1710","",0.,-10,10);
  RooRealVar ai_H0_f_1710("ai_H0_f_1710","",0.,-10,10);
  //helicity coupling for 1/2, -1/2
  RooRealVar ar_H1_f_1710("ar_H1_f_1710","",0.);
  RooRealVar ai_H1_f_1710("ai_H1_f_1710","",0.);  
  RooArgList* X_f_1710 = new RooArgList(ar_H0_f_1710,ai_H0_f_1710,
				      ar_H1_f_1710,ai_H1_f_1710,"X_f_1710");
  X_f_1710->add(m0_f_1710);
  X_f_1710->add(width_f_1710);
  X_f_1710->add(RooRealConstant::value(typeKM));
  X_f_1710->add(RooRealConstant::value(0));//spin
  X_f_1710->add(RooRealConstant::value(1)); //parity

//====rho====//
  RooRealVar m0_2150("m0_2150","m0",2.1);
  RooRealVar width_2150("width_2150","width",0.234);
//  RooRealVar m0_2150("m0_2150","m0",2.1,2.05,2.20);
//  RooRealVar width_2150("width_2150","width",0.234,0.08,0.25);
  //helicity coupling for 1/2, 1/2
  RooRealVar ar_H0_2150("ar_H0_2150","",0.,-10,10);
  RooRealVar ai_H0_2150("ai_H0_2150","",0.,-10,10);
  //helicity coupling for 1/2, -1/2
  RooRealVar ar_H1_2150("ar_H1_2150","",0.,-10,10);
  RooRealVar ai_H1_2150("ai_H1_2150","",0.,-10,10);  
  RooArgList* X_rho_2150 = new RooArgList(ar_H0_2150,ai_H0_2150,
				      ar_H1_2150,ai_H1_2150,"X_rho_2150");
  X_rho_2150->add(m0_2150);
  X_rho_2150->add(width_2150);
  X_rho_2150->add(RooRealConstant::value(typeKM));
  X_rho_2150->add(RooRealConstant::value(1));//spin
  X_rho_2150->add(RooRealConstant::value(-1)); //parity

  RooRealVar m0_phi_2170("m0_phi_2170","m0",2.159);
  RooRealVar width_phi_2170("width_phi_2170","width",0.137);
  //helicity coupling for 1/2, 1/2
  RooRealVar ar_H0_phi_2170("ar_H0_phi_2170","",0.,-10,10);
  RooRealVar ai_H0_phi_2170("ai_H0_phi_2170","",0.,-10,10);
  //helicity coupling for 1/2, -1/2
  RooRealVar ar_H1_phi_2170("ar_H1_phi_2170","",0.,-10,10);
  RooRealVar ai_H1_phi_2170("ai_H1_phi_2170","",0.,-10,10);  
  RooArgList* X_phi_2170 = new RooArgList(ar_H0_phi_2170,ai_H0_phi_2170,
				      ar_H1_phi_2170,ai_H1_phi_2170,"X_phi_2170");
  X_phi_2170->add(m0_phi_2170);
  X_phi_2170->add(width_phi_2170);
  X_phi_2170->add(RooRealConstant::value(typeKM));
  X_phi_2170->add(RooRealConstant::value(1));//spin
  X_phi_2170->add(RooRealConstant::value(-1)); //parity

  //spin-1--
  RooRealVar m0_1900("m0_1900","m0",1.90);
  RooRealVar width_1900("width_1900","width",0.05);
  //helicity coupling for 1/2, 1/2
  RooRealVar ar_H0_1900("ar_H0_1900","",0.,-10,10);
  RooRealVar ai_H0_1900("ai_H0_1900","",0.,-10,10);
  //helicity coupling for 1/2, -1/2
  RooRealVar ar_H1_1900("ar_H1_1900","",0.,-10,10);
  RooRealVar ai_H1_1900("ai_H1_1900","",0.,-10,10);  
  RooArgList* X_rho_1900 = new RooArgList(ar_H0_1900,ai_H0_1900,
				      ar_H1_1900,ai_H1_1900,"X_rho_1900");
  X_rho_1900->add(m0_1900);
  X_rho_1900->add(width_1900);
  X_rho_1900->add(RooRealConstant::value(typeKM));
  X_rho_1900->add(RooRealConstant::value(1));//spin
  X_rho_1900->add(RooRealConstant::value(-1)); //parity

  //spin-0, only has 1 helicity coupling, 
  RooRealVar m0_1700("m0_1700","m0",1.72);
  RooRealVar width_1700("width_1700","width",0.25);
  //helicity coupling for 1/2, 1/2
  RooRealVar ar_H0_1700("ar_H0_1700","",0.,-10,10);
  RooRealVar ai_H0_1700("ai_H0_1700","",0.,-10,10);
  //helicity coupling for 1/2, -1/2
  RooRealVar ar_H1_1700("ar_H1_1700","",0.,-10,10);
  RooRealVar ai_H1_1700("ai_H1_1700","",0.,-10,10);  
  RooArgList* X_rho_1700 = new RooArgList(ar_H0_1700,ai_H0_1700,
				      ar_H1_1700,ai_H1_1700,"X_rho_1700");
  X_rho_1700->add(m0_1700);
  X_rho_1700->add(width_1700);
  X_rho_1700->add(RooRealConstant::value(typeKM));
  X_rho_1700->add(RooRealConstant::value(1));//spin
  X_rho_1700->add(RooRealConstant::value(-1)); //parity



//***D0p
  TList *list = new TList();
  // 1/2-
  RooRealVar m0_3900("m0_3900","m0",3.856,3.5,4.0);
  RooRealVar width_3900("width_3900","width",0.068,0.01,0.5);
  //2 Helicity couplings, (1/2,1/2) and (1/2,-1/2); 
  RooRealVar ar_H0_3900("ar_H0_3900","",0.,-10,10);
  RooRealVar ai_H0_3900("ai_H0_3900","",0.,-10,10);
  RooRealVar ar_H1_3900("ar_H1_3900","",0.,-10,10);
  RooRealVar ai_H1_3900("ai_H1_3900","",0.,-10,10);  

  RooArgList* L_3900 = new RooArgList(ar_H0_3900,ai_H0_3900,
				      ar_H1_3900,ai_H1_3900,"Y_L_3900");
  L_3900->add(m0_3900);
  L_3900->add(width_3900);
  RooRealVar T3900("T3900","type of 3900",typeR);
  RooRealVar J3900("J3900","",1);
  RooRealVar P3900("P3900","",-1);
  L_3900->add(T3900);
  L_3900->add(J3900);
  L_3900->add(P3900);
  
  // 1+
  RooRealVar m0_2860("m0_2860","m0",2.8561);
  RooRealVar width_2860("width_2860","width",0.068);
  //2 Helicity couplings, (1/2,1/2) and (1/2,-1/2); 
  RooRealVar ar_H0_2860("ar_H0_2860","",0.,-10,10);
  RooRealVar ai_H0_2860("ai_H0_2860","",0.,-10,10);
  RooRealVar ar_H1_2860("ar_H1_2860","",0.,-10,10);
  RooRealVar ai_H1_2860("ai_H1_2860","",0.,-10,10);  

  RooArgList* Y_L_2860 = new RooArgList(ar_H0_2860,ai_H0_2860,
				      ar_H1_2860,ai_H1_2860,"Y_L_2860");
  Y_L_2860->add(m0_2860);
  Y_L_2860->add(width_2860);
  RooRealVar T2860("T2860","type of 2860",typeR);
  RooRealVar J2860("J2860","",3);
  RooRealVar P2860("P2860","",1);
  Y_L_2860->add(T2860);
  Y_L_2860->add(J2860);
  Y_L_2860->add(P2860);

  // 1+
  RooRealVar m0_2880("m0_2880","m0",2.881);
  RooRealVar width_2880("width_2880","width",0.0056);
  //2 Helicity couplings, (1/2,1/2) and (1/2,-1/2); 
  RooRealVar ar_H0_2880("ar_H0_2880","",0.,-10,10);
  RooRealVar ai_H0_2880("ai_H0_2880","",0.,-10,10);
  RooRealVar ar_H1_2880("ar_H1_2880","",0.,-10,10);
  RooRealVar ai_H1_2880("ai_H1_2880","",0.,-10,10);  

  RooArgList* Y_L_2880 = new RooArgList(ar_H0_2880,ai_H0_2880,
				      ar_H1_2880,ai_H1_2880,"Y_L_2880");
  Y_L_2880->add(m0_2880);
  Y_L_2880->add(width_2880);
  RooRealVar T2880("T2880","type of 2880",typeR);
  RooRealVar J2880("J2880","",5);
  RooRealVar P2880("P2880","",1);
  Y_L_2880->add(T2880);
  Y_L_2880->add(J2880);
  Y_L_2880->add(P2880);

  // 3/2-
  RooRealVar m0_2940("m0_2940","m0",2.9396);
  RooRealVar width_2940("width_2940","width",0.020);
  //2 Helicity couplings, (1/2,1/2) and (1/2,-1/2); 
  RooRealVar ar_H0_2940("ar_H0_2940","",0.,-10,10);
  RooRealVar ai_H0_2940("ai_H0_2940","",0.,-10,10);
  RooRealVar ar_H1_2940("ar_H1_2940","",0.,-10,10);
  RooRealVar ai_H1_2940("ai_H1_2940","",0.,-10,10);  

  RooArgList* Y_L_2940 = new RooArgList(ar_H0_2940,ai_H0_2940,
				      ar_H1_2940,ai_H1_2940,"Y_L_2940");
  Y_L_2940->add(m0_2940);
  Y_L_2940->add(width_2940);
  RooRealVar T2940("T2940","type of 2940",typeR);
  RooRealVar J2940("J2940","",3);
  RooRealVar P2940("P2940","",-1);
  Y_L_2940->add(T2940);
  Y_L_2940->add(J2940);
  Y_L_2940->add(P2940);

  // 1+

  RooRealVar m0_3262("m0_3262","m0",3.262,3.2,3.5);
//  RooRealVar m0_3262("m0_3262","m0",3.262);
//  RooRealVar width_3262("width_3262","width",0.100,0.001,1.000);
  RooRealVar width_3262("width_3262","width",0.10);
  //2 Helicity couplings, (1/2,1/2) and (1/2,-1/2); 
  RooRealVar ar_H0_3262("ar_H0_3262","",0.,-10,10);
  RooRealVar ai_H0_3262("ai_H0_3262","",0.,-10,10);
  RooRealVar ar_H1_3262("ar_H1_3262","",0.,-10,10);
  RooRealVar ai_H1_3262("ai_H1_3262","",0.,-10,10);  

  RooArgList* Y_S_3262 = new RooArgList(ar_H0_3262,ai_H0_3262,
				      ar_H1_3262,ai_H1_3262,"Y_S_3262");
  Y_S_3262->add(m0_3262);
  Y_S_3262->add(width_3262);
  RooRealVar T3262("T3262","type of 3262",typeR);
  RooRealVar J3262("J3262","",3);
  RooRealVar P3262("P3262","",1);
  Y_S_3262->add(T3262);
  Y_S_3262->add(J3262);
  Y_S_3262->add(P3262);

  RooRealVar m0_3268("m0_3268","m0",3.268,3.2,3.5);
//  RooRealVar width_3268("width_3268","width",0.100,0.001,1.000);
  RooRealVar width_3268("width_3268","width",0.02);
  //2 Helicity couplings, (1/2,1/2) and (1/2,-1/2); 
  RooRealVar ar_H0_3268("ar_H0_3268","",0.,-10,10);
  RooRealVar ai_H0_3268("ai_H0_3268","",0.,-10,10);
  RooRealVar ar_H1_3268("ar_H1_3268","",0.,-10,10);
  RooRealVar ai_H1_3268("ai_H1_3268","",0.,-10,10);  

  RooArgList* Y_S_3268 = new RooArgList(ar_H0_3268,ai_H0_3268,
				      ar_H1_3268,ai_H1_3268,"Y_S_3268");
  Y_S_3268->add(m0_3268);
  Y_S_3268->add(width_3268);
  RooRealVar T3268("T3268","type of 3268",typeR);
  RooRealVar J3268("J3268","",5);
  RooRealVar P3268("P3268","",1);
  Y_S_3268->add(T3268);
  Y_S_3268->add(J3268);
  Y_S_3268->add(P3268);

  // 1+
  RooRealVar m0_3700("m0_3700","m0",3.6);
//  RooRealVar width_3700("width_3700","width",0.100,0.001,1.000);
  RooRealVar width_3700("width_3700","width",0.20);
  //2 Helicity couplings, (1/2,1/2) and (1/2,-1/2); 
  RooRealVar ar_H0_3700("ar_H0_3700","",0.,-10,10);
  RooRealVar ai_H0_3700("ai_H0_3700","",0.,-10,10);
  RooRealVar ar_H1_3700("ar_H1_3700","",0.,-10,10);
  RooRealVar ai_H1_3700("ai_H1_3700","",0.,-10,10);  

  RooArgList* Y_S_3700 = new RooArgList(ar_H0_3700,ai_H0_3700,
				      ar_H1_3700,ai_H1_3700,"Y_S_3700");
  Y_S_3700->add(m0_3700);
  Y_S_3700->add(width_3700);
  RooRealVar T3700("T3700","type of 3700",typeR);
  RooRealVar J3700("J3700","",3);
  RooRealVar P3700("P3700","",1);
  Y_S_3700->add(T3700);
  Y_S_3700->add(J3700);
  Y_S_3700->add(P3700);

  // 1+
  RooRealVar m0_4000("m0_4000","m0",4);
//  RooRealVar width_4000("width_4000","width",0.100,0.001,1.000);
  RooRealVar width_4000("width_4000","width",0.20);
  //2 Helicity couplings, (1/2,1/2) and (1/2,-1/2); 
  RooRealVar ar_H0_4000("ar_H0_4000","",0.,-10,10);
  RooRealVar ai_H0_4000("ai_H0_4000","",0.,-10,10);
  RooRealVar ar_H1_4000("ar_H1_4000","",0.,-10,10);
  RooRealVar ai_H1_4000("ai_H1_4000","",0.,-10,10);  

  RooArgList* Y_S_4000 = new RooArgList(ar_H0_4000,ai_H0_4000,
				      ar_H1_4000,ai_H1_4000,"Y_S_4000");
  Y_S_4000->add(m0_4000);
  Y_S_4000->add(width_4000);
  RooRealVar T4000("T4000","type of 4000",typeR);
  RooRealVar J4000("J4000","",3);
  RooRealVar P4000("P4000","",1);
  Y_S_4000->add(T4000);
  Y_S_4000->add(J4000);
  Y_S_4000->add(P4000);

  // 1+
  RooRealVar m0_2765("m0_2765","m0",2.7666);
  RooRealVar width_2765("width_2765","width",0.05);
  //2 Helicity couplings, (1/2,1/2) and (1/2,-1/2); 
  RooRealVar ar_H0_2765("ar_H0_2765","",0.,-10,10);
  RooRealVar ai_H0_2765("ai_H0_2765","",0.,-10,10);
  RooRealVar ar_H1_2765("ar_H1_2765","",0.,-10,10);
  RooRealVar ai_H1_2765("ai_H1_2765","",0.,-10,10);  

  RooArgList* Y_L_2765 = new RooArgList(ar_H0_2765,ai_H0_2765,
				      ar_H1_2765,ai_H1_2765,"Y_L_2765");
  Y_L_2765->add(m0_2765);
  Y_L_2765->add(width_2765);
  RooRealVar T2765("T2765","type of 2765",typeR);
  RooRealVar J2765("J2765","",1);
  RooRealVar P2765("P2765","",-1);
  Y_L_2765->add(T2765);
  Y_L_2765->add(J2765);
  Y_L_2765->add(P2765);

