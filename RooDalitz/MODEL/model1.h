// Compare with model 0, don't use spline, use 5 states with JPC = 0++ instead
// In model 0, spline has 12*2 = 24 parameters
// In model 1, five states has 5*2 = 10 parameters
    listX->Add(X_NR0m); 
//    listX->Add(X_Fwave);
    listX->Add(X_f2_2150);
    listX->Add(X_rho_2150);
    listX->Add(X_f2_1950);
    // States with JPC = 0++
    listX->Add(X_a_1950);
    listX->Add(X_f_2020);
    listX->Add(X_f_2100);
    listX->Add(X_f_2200);
    listX->Add(X_f_2330);




    list->Add(Y_L_2940);    //2P
    list->Add(Y_L_2860);    //1D
    list->Add(Y_L_2880);    //1D
    list->Add(Y_L_2900);
    list->Add(Y_L_3130);    //3S
    list->Add(Y_L_3189);    //3S
    list->Add(Y_L_2765);