import os
from multiprocessing import Pool
from sys import argv

#pattern = argv[1]
pattern = "pattern"


#vars = ["mppbar", "mD0p", "mD0pbar", "cosTheta_X", "cosTheta_L","mD0pZoomIn"]
vars = ["log_B0_PT"]

ncpus = len(vars)

pool = Pool(processes=ncpus)
err_logs = []

key = ""
#key = "cos_kst_g_m05_l_05"
imgdir = "imgs_" + pattern 
cut = ""
#cut = "abs(cosTheta_Kst_Ds)<0.5"


if not os.path.exists(imgdir):
    os.system("mkdir "+imgdir)

def plot_all():
    for var in vars:
        if var == "mD0pZoomIn":
            cmd = "root -l -q -b './plot_PTweight.C(\"mD0p\", \"ZoomIn\", \"pattern\", \"\", 1)'"
        else:
            cmd = "root -l -q -b './plot_PTweight.C(\"{}\")'".format(var)
        args = cmd,
        pool.apply_async(os.system, args, error_callback=err_logs.append)
    print("Pool join... ")
    pool.close()
    pool.join()
    print("Pool finsh... ")
    if err_logs:
        print("Errors:", err_logs)

plot_all()

