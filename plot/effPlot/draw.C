{
    gROOT->ProcessLine(".x ~/lhcbStyle.C");
    
    lhcbStyle->SetPalette(57);
    double mylablesize = 0.1;
    double mylablesize1 = 0.08;
    lhcbStyle->SetPadRightMargin(0.01); // increase for colz plot
    TFile *f = new TFile("ploteff_new.root","read");
    TH1F *h100 = (TH1F*)f->Get("h100");
    TCanvas *c1 = new TCanvas("c1", "c1", 800, 600);
    h100->GetYaxis()->SetNdivisions(510);
    h100->SetXTitle("#it{m}^{2}_{#it{p#bar{p}}} [GeV^{2}]");
    h100->SetYTitle("#it{m}^{2}_{#it{D^{0}p}} [GeV^{2}]");
    h100->Draw("COLZ");
    c1->SetRightMargin(0.12);

    lhcbLatex->SetTextFont(132);
    lhcbLatex->SetTextColor(1);
    lhcbLatex->SetTextAlign(12);
    lhcbLatex->SetTextSize(0.07);
    lhcbLatex->SetNDC(1);
    lhcbLatex->DrawLatex(0.6, 0.85, "#splitline{LHCb}{Simulation}");
    c1->SaveAs("./figs/effm1m2.png");
    c1->SaveAs("./figs/effm1m2.pdf");
    //delete c1;

    TH1F *h102 = (TH1F*)f->Get("h102");
    h102->GetYaxis()->SetNdivisions(510);
    h102->SetXTitle("#it{m}_{#it{p#bar{p}}} [GeV^{2}]");
    h102->SetYTitle("cos#it{#theta_{X}} ");
    h102->Draw("COLZ");
    lhcbLatex->DrawLatex(0.6, 0.85, "#splitline{LHCb}{Simulation}");
    c1->SaveAs("./figs/effmcos.png");
    c1->SaveAs("./figs/effmcos.pdf");
    //delete c1;

    TH1F *h200 = (TH1F*)f->Get("h200");
    h200->GetYaxis()->SetNdivisions(510);
    h200->SetXTitle("#it{m}^{2}_{#it{p#bar{p}}} [GeV^{2}]");
    h200->SetYTitle("#it{m}^{2}_{#it{D^{0}p}} [GeV^{2}]");
    h200->Draw("COLZ");
    c1->SaveAs("./figs/effm1m2_bkg.png");
    c1->SaveAs("./figs/effm1m2_bkg.pdf");
    //delete c1;

    TH1F *h202 = (TH1F*)f->Get("h202");
    h202->GetYaxis()->SetNdivisions(510);
    h202->SetXTitle("#it{m}_{#it{p#bar{p}}} [GeV^{2}]");
    h202->SetYTitle("cos#it{#theta_{X}} ");
    h202->Draw("COLZ");
    c1->SaveAs("./figs/effmcos_bkg.png");
    c1->SaveAs("./figs/effmcos_bkg.pdf");
    //delete c1;
}