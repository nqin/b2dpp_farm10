//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Sep 14 15:32:08 2022 by ROOT version 6.22/08
// from TTree mytree/DecayTree
// found on file: dataAFB.root
//////////////////////////////////////////////////////////

#ifndef mytree_h
#define mytree_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class mytree {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.
   static constexpr Int_t kMaxB0_ENDVERTEX_COV = 1;
   static constexpr Int_t kMaxB0_OWNPV_COV = 1;
   static constexpr Int_t kMaxD0_ENDVERTEX_COV = 1;
   static constexpr Int_t kMaxD0_OWNPV_COV = 1;
   static constexpr Int_t kMaxD0_ORIVX_COV = 1;
   static constexpr Int_t kMaxpi_OWNPV_COV = 1;
   static constexpr Int_t kMaxpi_ORIVX_COV = 1;
   static constexpr Int_t kMaxK_OWNPV_COV = 1;
   static constexpr Int_t kMaxK_ORIVX_COV = 1;
   static constexpr Int_t kMaxR_ENDVERTEX_COV = 1;
   static constexpr Int_t kMaxR_OWNPV_COV = 1;
   static constexpr Int_t kMaxR_ORIVX_COV = 1;
   static constexpr Int_t kMaxpp_OWNPV_COV = 1;
   static constexpr Int_t kMaxpp_ORIVX_COV = 1;
   static constexpr Int_t kMaxpm_OWNPV_COV = 1;
   static constexpr Int_t kMaxpm_ORIVX_COV = 1;

   // Declaration of leaf types
   Double_t        B0_DiraAngleError;
   Double_t        B0_DiraCosError;
   Double_t        B0_DiraAngle;
   Double_t        B0_DiraCos;
   Double_t        B0_ENDVERTEX_X;
   Double_t        B0_ENDVERTEX_Y;
   Double_t        B0_ENDVERTEX_Z;
   Double_t        B0_ENDVERTEX_XERR;
   Double_t        B0_ENDVERTEX_YERR;
   Double_t        B0_ENDVERTEX_ZERR;
   Double_t        B0_ENDVERTEX_CHI2;
   Int_t           B0_ENDVERTEX_NDOF;
   Float_t         B0_ENDVERTEX_COV_[3][3];
   Double_t        B0_OWNPV_X;
   Double_t        B0_OWNPV_Y;
   Double_t        B0_OWNPV_Z;
   Double_t        B0_OWNPV_XERR;
   Double_t        B0_OWNPV_YERR;
   Double_t        B0_OWNPV_ZERR;
   Double_t        B0_OWNPV_CHI2;
   Int_t           B0_OWNPV_NDOF;
   Float_t         B0_OWNPV_COV_[3][3];
   Double_t        B0_IP_OWNPV;
   Double_t        B0_IPCHI2_OWNPV;
   Double_t        B0_FD_OWNPV;
   Double_t        B0_FDCHI2_OWNPV;
   Double_t        B0_DIRA_OWNPV;
   Double_t        B0_P;
   Double_t        B0_PT;
   Double_t        B0_PE;
   Double_t        B0_PX;
   Double_t        B0_PY;
   Double_t        B0_PZ;
   Double_t        B0_MM;
   Double_t        B0_MMERR;
   Double_t        B0_M;
   Int_t           B0_ID;
   Double_t        B0_TAU;
   Double_t        B0_TAUERR;
   Double_t        B0_TAUCHI2;
   Double_t        B0_NumVtxWithinChi2WindowOneTrack;
   Double_t        B0_SmallestDeltaChi2OneTrack;
   Double_t        B0_SmallestDeltaChi2MassOneTrack;
   Double_t        B0_SmallestDeltaChi2TwoTracks;
   Double_t        B0_SmallestDeltaChi2MassTwoTracks;
   Double_t        B0_BDDTF_CHI2NDOF;
   Double_t        B0_BDDTF_CTAU;
   Double_t        B0_BDDTF_CTAUS;
   Double_t        B0_BDDTF_DIRA;
   Double_t        B0_BDDTF_E;
   Double_t        B0_BDDTF_FDCHI2;
   Double_t        B0_BDDTF_FDS;
   Double_t        B0_BDDTF_IP;
   Double_t        B0_BDDTF_IPCHI2;
   Double_t        B0_BDDTF_M;
   Double_t        B0_BDDTF_P;
   Double_t        B0_BDDTF_PT;
   Double_t        B0_BDDTF_PX;
   Double_t        B0_BDDTF_PY;
   Double_t        B0_BDDTF_PZ;
   Double_t        B0_BDDTF_TAU;
   Double_t        B0_D0_BDDTF_CTAU;
   Double_t        B0_D0_BDDTF_CTAUS;
   Double_t        B0_D0_BDDTF_DIRA;
   Double_t        B0_D0_BDDTF_E;
   Double_t        B0_D0_BDDTF_FDCHI2;
   Double_t        B0_D0_BDDTF_FDS;
   Double_t        B0_D0_BDDTF_IP;
   Double_t        B0_D0_BDDTF_IPCHI2;
   Double_t        B0_D0_BDDTF_M;
   Double_t        B0_D0_BDDTF_P;
   Double_t        B0_D0_BDDTF_PT;
   Double_t        B0_D0_BDDTF_PX;
   Double_t        B0_D0_BDDTF_PY;
   Double_t        B0_D0_BDDTF_PZ;
   Double_t        B0_D0_BDDTF_TAU;
   Double_t        B0_pm_BDDTF_DIRA;
   Double_t        B0_pm_BDDTF_E;
   Double_t        B0_pm_BDDTF_FDCHI2;
   Double_t        B0_pm_BDDTF_FDS;
   Double_t        B0_pm_BDDTF_IP;
   Double_t        B0_pm_BDDTF_IPCHI2;
   Double_t        B0_pm_BDDTF_M;
   Double_t        B0_pm_BDDTF_P;
   Double_t        B0_pm_BDDTF_PT;
   Double_t        B0_pm_BDDTF_PX;
   Double_t        B0_pm_BDDTF_PY;
   Double_t        B0_pm_BDDTF_PZ;
   Double_t        B0_pm_BDDTF_TAU;
   Double_t        B0_pp_BDDTF_DIRA;
   Double_t        B0_pp_BDDTF_E;
   Double_t        B0_pp_BDDTF_FDCHI2;
   Double_t        B0_pp_BDDTF_FDS;
   Double_t        B0_pp_BDDTF_IP;
   Double_t        B0_pp_BDDTF_IPCHI2;
   Double_t        B0_pp_BDDTF_M;
   Double_t        B0_pp_BDDTF_P;
   Double_t        B0_pp_BDDTF_PT;
   Double_t        B0_pp_BDDTF_PX;
   Double_t        B0_pp_BDDTF_PY;
   Double_t        B0_pp_BDDTF_PZ;
   Double_t        B0_pp_BDDTF_TAU;
   Double_t        B0_BDTF_CHI2NDOF;
   Double_t        B0_BDTF_CTAU;
   Double_t        B0_BDTF_CTAUS;
   Double_t        B0_BDTF_DIRA;
   Double_t        B0_BDTF_E;
   Double_t        B0_BDTF_FDCHI2;
   Double_t        B0_BDTF_FDS;
   Double_t        B0_BDTF_IP;
   Double_t        B0_BDTF_IPCHI2;
   Double_t        B0_BDTF_M;
   Double_t        B0_BDTF_P;
   Double_t        B0_BDTF_PT;
   Double_t        B0_BDTF_PX;
   Double_t        B0_BDTF_PY;
   Double_t        B0_BDTF_PZ;
   Double_t        B0_BDTF_TAU;
   Double_t        B0_D0_BDTF_CTAU;
   Double_t        B0_D0_BDTF_CTAUS;
   Double_t        B0_D0_BDTF_DIRA;
   Double_t        B0_D0_BDTF_E;
   Double_t        B0_D0_BDTF_FDCHI2;
   Double_t        B0_D0_BDTF_FDS;
   Double_t        B0_D0_BDTF_IP;
   Double_t        B0_D0_BDTF_IPCHI2;
   Double_t        B0_D0_BDTF_M;
   Double_t        B0_D0_BDTF_P;
   Double_t        B0_D0_BDTF_PT;
   Double_t        B0_D0_BDTF_PX;
   Double_t        B0_D0_BDTF_PY;
   Double_t        B0_D0_BDTF_PZ;
   Double_t        B0_D0_BDTF_TAU;
   Double_t        B0_D0_DDTF_CTAU;
   Double_t        B0_D0_DDTF_CTAUS;
   Double_t        B0_D0_DDTF_DIRA;
   Double_t        B0_D0_DDTF_E;
   Double_t        B0_D0_DDTF_FDCHI2;
   Double_t        B0_D0_DDTF_FDS;
   Double_t        B0_D0_DDTF_IP;
   Double_t        B0_D0_DDTF_IPCHI2;
   Double_t        B0_D0_DDTF_M;
   Double_t        B0_D0_DDTF_P;
   Double_t        B0_D0_DDTF_PT;
   Double_t        B0_D0_DDTF_PX;
   Double_t        B0_D0_DDTF_PY;
   Double_t        B0_D0_DDTF_PZ;
   Double_t        B0_D0_DDTF_TAU;
   Double_t        B0_DDTF_CHI2NDOF;
   Double_t        B0_DDTF_CTAU;
   Double_t        B0_DDTF_CTAUS;
   Double_t        B0_DDTF_DIRA;
   Double_t        B0_DDTF_E;
   Double_t        B0_DDTF_FDCHI2;
   Double_t        B0_DDTF_FDS;
   Double_t        B0_DDTF_IP;
   Double_t        B0_DDTF_IPCHI2;
   Double_t        B0_DDTF_M;
   Double_t        B0_DDTF_P;
   Double_t        B0_DDTF_PT;
   Double_t        B0_DDTF_PX;
   Double_t        B0_DDTF_PY;
   Double_t        B0_DDTF_PZ;
   Double_t        B0_DDTF_TAU;
   Double_t        B0_ETA;
   Double_t        B0_LOKI_DIRA;
   Double_t        B0_LOKI_FDCHI2;
   Double_t        B0_LOKI_FDS;
   Double_t        B0_LOKI_LV01;
   Double_t        B0_LOKI_LV02;
   Double_t        B0_LOKI_MAXDOCA;
   Double_t        B0_LOKI_TAU;
   Double_t        B0_PHI;
   Double_t        B0_Y;
   Int_t           B0_Cons_nPV;
   Float_t         B0_Cons_D0_Kplus_ID[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_D0_Kplus_PE[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_D0_Kplus_PX[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_D0_Kplus_PY[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_D0_Kplus_PZ[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_D0_piplus_ID[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_D0_piplus_PE[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_D0_piplus_PX[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_D0_piplus_PY[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_D0_piplus_PZ[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_M[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_MERR[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_P[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_PERR[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_PV_key[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_chi2[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_ctau[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_ctauErr[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_decayLength[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_decayLengthErr[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_nDOF[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_nIter[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_rho_770_0_pplus_0_ID[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_rho_770_0_pplus_0_PE[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_rho_770_0_pplus_0_PX[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_rho_770_0_pplus_0_PY[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_rho_770_0_pplus_0_PZ[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_rho_770_0_pplus_ID[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_rho_770_0_pplus_PE[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_rho_770_0_pplus_PX[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_rho_770_0_pplus_PY[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_rho_770_0_pplus_PZ[100];   //[B0_Cons_nPV]
   Float_t         B0_Cons_status[100];   //[B0_Cons_nPV]
   Bool_t          B0_L0Global_Dec;
   Bool_t          B0_L0Global_TIS;
   Bool_t          B0_L0Global_TOS;
   Bool_t          B0_Hlt1Global_Dec;
   Bool_t          B0_Hlt1Global_TIS;
   Bool_t          B0_Hlt1Global_TOS;
   Bool_t          B0_Hlt1Phys_Dec;
   Bool_t          B0_Hlt1Phys_TIS;
   Bool_t          B0_Hlt1Phys_TOS;
   Bool_t          B0_Hlt2Global_Dec;
   Bool_t          B0_Hlt2Global_TIS;
   Bool_t          B0_Hlt2Global_TOS;
   Bool_t          B0_Hlt2Phys_Dec;
   Bool_t          B0_Hlt2Phys_TIS;
   Bool_t          B0_Hlt2Phys_TOS;
   Bool_t          B0_L0MuonDecision_Dec;
   Bool_t          B0_L0MuonDecision_TIS;
   Bool_t          B0_L0MuonDecision_TOS;
   Bool_t          B0_L0MuonNoSPDDecision_Dec;
   Bool_t          B0_L0MuonNoSPDDecision_TIS;
   Bool_t          B0_L0MuonNoSPDDecision_TOS;
   Bool_t          B0_L0MuonEWDecision_Dec;
   Bool_t          B0_L0MuonEWDecision_TIS;
   Bool_t          B0_L0MuonEWDecision_TOS;
   Bool_t          B0_L0DiMuonDecision_Dec;
   Bool_t          B0_L0DiMuonDecision_TIS;
   Bool_t          B0_L0DiMuonDecision_TOS;
   Bool_t          B0_L0HadronDecision_Dec;
   Bool_t          B0_L0HadronDecision_TIS;
   Bool_t          B0_L0HadronDecision_TOS;
   Bool_t          B0_L0PhotonDecision_Dec;
   Bool_t          B0_L0PhotonDecision_TIS;
   Bool_t          B0_L0PhotonDecision_TOS;
   Bool_t          B0_L0ElectronDecision_Dec;
   Bool_t          B0_L0ElectronDecision_TIS;
   Bool_t          B0_L0ElectronDecision_TOS;
   Bool_t          B0_Hlt1TrackMVADecision_Dec;
   Bool_t          B0_Hlt1TrackMVADecision_TIS;
   Bool_t          B0_Hlt1TrackMVADecision_TOS;
   Bool_t          B0_Hlt1TwoTrackMVADecision_Dec;
   Bool_t          B0_Hlt1TwoTrackMVADecision_TIS;
   Bool_t          B0_Hlt1TwoTrackMVADecision_TOS;
   Bool_t          B0_Hlt1TrackMuonDecision_Dec;
   Bool_t          B0_Hlt1TrackMuonDecision_TIS;
   Bool_t          B0_Hlt1TrackMuonDecision_TOS;
   Bool_t          B0_Hlt1TrackPhotonDecision_Dec;
   Bool_t          B0_Hlt1TrackPhotonDecision_TIS;
   Bool_t          B0_Hlt1TrackPhotonDecision_TOS;
   Bool_t          B0_Hlt1TrackForwardPassThroughDecision_Dec;
   Bool_t          B0_Hlt1TrackForwardPassThroughDecision_TIS;
   Bool_t          B0_Hlt1TrackForwardPassThroughDecision_TOS;
   Bool_t          B0_Hlt1TrackForwardPassThroughLooseDecision_Dec;
   Bool_t          B0_Hlt1TrackForwardPassThroughLooseDecision_TIS;
   Bool_t          B0_Hlt1TrackForwardPassThroughLooseDecision_TOS;
   Bool_t          B0_Hlt1TrackAllL0Decision_Dec;
   Bool_t          B0_Hlt1TrackAllL0Decision_TIS;
   Bool_t          B0_Hlt1TrackAllL0Decision_TOS;
   Bool_t          B0_Hlt1GlobalDecision_Dec;
   Bool_t          B0_Hlt1GlobalDecision_TIS;
   Bool_t          B0_Hlt1GlobalDecision_TOS;
   Bool_t          B0_Hlt2Topo2BodyDecision_Dec;
   Bool_t          B0_Hlt2Topo2BodyDecision_TIS;
   Bool_t          B0_Hlt2Topo2BodyDecision_TOS;
   Bool_t          B0_Hlt2Topo3BodyDecision_Dec;
   Bool_t          B0_Hlt2Topo3BodyDecision_TIS;
   Bool_t          B0_Hlt2Topo3BodyDecision_TOS;
   Bool_t          B0_Hlt2Topo4BodyDecision_Dec;
   Bool_t          B0_Hlt2Topo4BodyDecision_TIS;
   Bool_t          B0_Hlt2Topo4BodyDecision_TOS;
   Bool_t          B0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_Dec;
   Bool_t          B0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_TIS;
   Bool_t          B0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_TOS;
   Bool_t          B0_Hlt2TopoMu3BodyDecision_Dec;
   Bool_t          B0_Hlt2TopoMu3BodyDecision_TIS;
   Bool_t          B0_Hlt2TopoMu3BodyDecision_TOS;
   Bool_t          B0_Hlt2TopoMu4BodyDecision_Dec;
   Bool_t          B0_Hlt2TopoMu4BodyDecision_TIS;
   Bool_t          B0_Hlt2TopoMu4BodyDecision_TOS;
   Bool_t          B0_Hlt2TopoE2BodyDecision_Dec;
   Bool_t          B0_Hlt2TopoE2BodyDecision_TIS;
   Bool_t          B0_Hlt2TopoE2BodyDecision_TOS;
   Bool_t          B0_Hlt2TopoE3BodyDecision_Dec;
   Bool_t          B0_Hlt2TopoE3BodyDecision_TIS;
   Bool_t          B0_Hlt2TopoE3BodyDecision_TOS;
   Bool_t          B0_Hlt2TopoE4BodyDecision_Dec;
   Bool_t          B0_Hlt2TopoE4BodyDecision_TIS;
   Bool_t          B0_Hlt2TopoE4BodyDecision_TOS;
   Bool_t          B0_Hlt2TopoRad2BodyDecision_Dec;
   Bool_t          B0_Hlt2TopoRad2BodyDecision_TIS;
   Bool_t          B0_Hlt2TopoRad2BodyDecision_TOS;
   Bool_t          B0_Hlt2SingleMuonDecision_Dec;
   Bool_t          B0_Hlt2SingleMuonDecision_TIS;
   Bool_t          B0_Hlt2SingleMuonDecision_TOS;
   Bool_t          B0_Hlt2RadiativeTopoTrackDecision_Dec;
   Bool_t          B0_Hlt2RadiativeTopoTrackDecision_TIS;
   Bool_t          B0_Hlt2RadiativeTopoTrackDecision_TOS;
   Bool_t          B0_Hlt2RadiativeTopoPhotonDecision_Dec;
   Bool_t          B0_Hlt2RadiativeTopoPhotonDecision_TIS;
   Bool_t          B0_Hlt2RadiativeTopoPhotonDecision_TOS;
   Bool_t          B0_Hlt2Topo2BodySimpleDecision_Dec;
   Bool_t          B0_Hlt2Topo2BodySimpleDecision_TIS;
   Bool_t          B0_Hlt2Topo2BodySimpleDecision_TOS;
   Bool_t          B0_Hlt2Topo3BodySimpleDecision_Dec;
   Bool_t          B0_Hlt2Topo3BodySimpleDecision_TIS;
   Bool_t          B0_Hlt2Topo3BodySimpleDecision_TOS;
   Bool_t          B0_Hlt2Topo4BodySimpleDecision_Dec;
   Bool_t          B0_Hlt2Topo4BodySimpleDecision_TIS;
   Bool_t          B0_Hlt2Topo4BodySimpleDecision_TOS;
   Bool_t          B0_Hlt2Topo2BodyBBDTDecision_Dec;
   Bool_t          B0_Hlt2Topo2BodyBBDTDecision_TIS;
   Bool_t          B0_Hlt2Topo2BodyBBDTDecision_TOS;
   Bool_t          B0_Hlt2Topo3BodyBBDTDecision_Dec;
   Bool_t          B0_Hlt2Topo3BodyBBDTDecision_TIS;
   Bool_t          B0_Hlt2Topo3BodyBBDTDecision_TOS;
   Bool_t          B0_Hlt2Topo4BodyBBDTDecision_Dec;
   Bool_t          B0_Hlt2Topo4BodyBBDTDecision_TIS;
   Bool_t          B0_Hlt2Topo4BodyBBDTDecision_TOS;
   Bool_t          B0_Hlt2TopoE2BodyBBDTDecision_Dec;
   Bool_t          B0_Hlt2TopoE2BodyBBDTDecision_TIS;
   Bool_t          B0_Hlt2TopoE2BodyBBDTDecision_TOS;
   Bool_t          B0_Hlt2TopoE3BodyBBDTDecision_Dec;
   Bool_t          B0_Hlt2TopoE3BodyBBDTDecision_TIS;
   Bool_t          B0_Hlt2TopoE3BodyBBDTDecision_TOS;
   Bool_t          B0_Hlt2TopoE4BodyBBDTDecision_Dec;
   Bool_t          B0_Hlt2TopoE4BodyBBDTDecision_TIS;
   Bool_t          B0_Hlt2TopoE4BodyBBDTDecision_TOS;
   Bool_t          B0_Hlt2TopoMu2BodyBBDTDecision_Dec;
   Bool_t          B0_Hlt2TopoMu2BodyBBDTDecision_TIS;
   Bool_t          B0_Hlt2TopoMu2BodyBBDTDecision_TOS;
   Bool_t          B0_Hlt2TopoMu3BodyBBDTDecision_Dec;
   Bool_t          B0_Hlt2TopoMu3BodyBBDTDecision_TIS;
   Bool_t          B0_Hlt2TopoMu3BodyBBDTDecision_TOS;
   Bool_t          B0_Hlt2TopoMu4BodyBBDTDecision_Dec;
   Bool_t          B0_Hlt2TopoMu4BodyBBDTDecision_TIS;
   Bool_t          B0_Hlt2TopoMu4BodyBBDTDecision_TOS;
   Bool_t          B0_Hlt2GlobalDecision_Dec;
   Bool_t          B0_Hlt2GlobalDecision_TIS;
   Bool_t          B0_Hlt2GlobalDecision_TOS;
   Bool_t          B0_Hlt2TopoRad2BodyBBDTDecision_Dec;
   Bool_t          B0_Hlt2TopoRad2BodyBBDTDecision_TIS;
   Bool_t          B0_Hlt2TopoRad2BodyBBDTDecision_TOS;
   Double_t        D0_CosTheta;
   Double_t        D0_ENDVERTEX_X;
   Double_t        D0_ENDVERTEX_Y;
   Double_t        D0_ENDVERTEX_Z;
   Double_t        D0_ENDVERTEX_XERR;
   Double_t        D0_ENDVERTEX_YERR;
   Double_t        D0_ENDVERTEX_ZERR;
   Double_t        D0_ENDVERTEX_CHI2;
   Int_t           D0_ENDVERTEX_NDOF;
   Float_t         D0_ENDVERTEX_COV_[3][3];
   Double_t        D0_OWNPV_X;
   Double_t        D0_OWNPV_Y;
   Double_t        D0_OWNPV_Z;
   Double_t        D0_OWNPV_XERR;
   Double_t        D0_OWNPV_YERR;
   Double_t        D0_OWNPV_ZERR;
   Double_t        D0_OWNPV_CHI2;
   Int_t           D0_OWNPV_NDOF;
   Float_t         D0_OWNPV_COV_[3][3];
   Double_t        D0_IP_OWNPV;
   Double_t        D0_IPCHI2_OWNPV;
   Double_t        D0_FD_OWNPV;
   Double_t        D0_FDCHI2_OWNPV;
   Double_t        D0_DIRA_OWNPV;
   Double_t        D0_ORIVX_X;
   Double_t        D0_ORIVX_Y;
   Double_t        D0_ORIVX_Z;
   Double_t        D0_ORIVX_XERR;
   Double_t        D0_ORIVX_YERR;
   Double_t        D0_ORIVX_ZERR;
   Double_t        D0_ORIVX_CHI2;
   Int_t           D0_ORIVX_NDOF;
   Float_t         D0_ORIVX_COV_[3][3];
   Double_t        D0_FD_ORIVX;
   Double_t        D0_FDCHI2_ORIVX;
   Double_t        D0_DIRA_ORIVX;
   Double_t        D0_P;
   Double_t        D0_PT;
   Double_t        D0_PE;
   Double_t        D0_PX;
   Double_t        D0_PY;
   Double_t        D0_PZ;
   Double_t        D0_MM;
   Double_t        D0_MMERR;
   Double_t        D0_M;
   Int_t           D0_ID;
   Double_t        D0_TAU;
   Double_t        D0_TAUERR;
   Double_t        D0_TAUCHI2;
   Double_t        D0_NumVtxWithinChi2WindowOneTrack;
   Double_t        D0_SmallestDeltaChi2OneTrack;
   Double_t        D0_SmallestDeltaChi2MassOneTrack;
   Double_t        D0_SmallestDeltaChi2TwoTracks;
   Double_t        D0_SmallestDeltaChi2MassTwoTracks;
   Double_t        D0_ETA;
   Double_t        D0_LOKI_DIRA;
   Double_t        D0_LOKI_FDCHI2;
   Double_t        D0_LOKI_FDS;
   Double_t        D0_LOKI_LV01;
   Double_t        D0_LOKI_LV02;
   Double_t        D0_LOKI_MAXDOCA;
   Double_t        D0_LOKI_TAU;
   Double_t        D0_PHI;
   Double_t        D0_Y;
   Bool_t          D0_L0Global_Dec;
   Bool_t          D0_L0Global_TIS;
   Bool_t          D0_L0Global_TOS;
   Bool_t          D0_Hlt1Global_Dec;
   Bool_t          D0_Hlt1Global_TIS;
   Bool_t          D0_Hlt1Global_TOS;
   Bool_t          D0_Hlt1Phys_Dec;
   Bool_t          D0_Hlt1Phys_TIS;
   Bool_t          D0_Hlt1Phys_TOS;
   Bool_t          D0_Hlt2Global_Dec;
   Bool_t          D0_Hlt2Global_TIS;
   Bool_t          D0_Hlt2Global_TOS;
   Bool_t          D0_Hlt2Phys_Dec;
   Bool_t          D0_Hlt2Phys_TIS;
   Bool_t          D0_Hlt2Phys_TOS;
   Bool_t          D0_L0MuonDecision_Dec;
   Bool_t          D0_L0MuonDecision_TIS;
   Bool_t          D0_L0MuonDecision_TOS;
   Bool_t          D0_L0MuonNoSPDDecision_Dec;
   Bool_t          D0_L0MuonNoSPDDecision_TIS;
   Bool_t          D0_L0MuonNoSPDDecision_TOS;
   Bool_t          D0_L0MuonEWDecision_Dec;
   Bool_t          D0_L0MuonEWDecision_TIS;
   Bool_t          D0_L0MuonEWDecision_TOS;
   Bool_t          D0_L0DiMuonDecision_Dec;
   Bool_t          D0_L0DiMuonDecision_TIS;
   Bool_t          D0_L0DiMuonDecision_TOS;
   Bool_t          D0_L0HadronDecision_Dec;
   Bool_t          D0_L0HadronDecision_TIS;
   Bool_t          D0_L0HadronDecision_TOS;
   Bool_t          D0_L0PhotonDecision_Dec;
   Bool_t          D0_L0PhotonDecision_TIS;
   Bool_t          D0_L0PhotonDecision_TOS;
   Bool_t          D0_L0ElectronDecision_Dec;
   Bool_t          D0_L0ElectronDecision_TIS;
   Bool_t          D0_L0ElectronDecision_TOS;
   Bool_t          D0_Hlt1TrackMVADecision_Dec;
   Bool_t          D0_Hlt1TrackMVADecision_TIS;
   Bool_t          D0_Hlt1TrackMVADecision_TOS;
   Bool_t          D0_Hlt1TwoTrackMVADecision_Dec;
   Bool_t          D0_Hlt1TwoTrackMVADecision_TIS;
   Bool_t          D0_Hlt1TwoTrackMVADecision_TOS;
   Bool_t          D0_Hlt1TrackMuonDecision_Dec;
   Bool_t          D0_Hlt1TrackMuonDecision_TIS;
   Bool_t          D0_Hlt1TrackMuonDecision_TOS;
   Bool_t          D0_Hlt1TrackPhotonDecision_Dec;
   Bool_t          D0_Hlt1TrackPhotonDecision_TIS;
   Bool_t          D0_Hlt1TrackPhotonDecision_TOS;
   Bool_t          D0_Hlt1TrackForwardPassThroughDecision_Dec;
   Bool_t          D0_Hlt1TrackForwardPassThroughDecision_TIS;
   Bool_t          D0_Hlt1TrackForwardPassThroughDecision_TOS;
   Bool_t          D0_Hlt1TrackForwardPassThroughLooseDecision_Dec;
   Bool_t          D0_Hlt1TrackForwardPassThroughLooseDecision_TIS;
   Bool_t          D0_Hlt1TrackForwardPassThroughLooseDecision_TOS;
   Bool_t          D0_Hlt1TrackAllL0Decision_Dec;
   Bool_t          D0_Hlt1TrackAllL0Decision_TIS;
   Bool_t          D0_Hlt1TrackAllL0Decision_TOS;
   Bool_t          D0_Hlt1GlobalDecision_Dec;
   Bool_t          D0_Hlt1GlobalDecision_TIS;
   Bool_t          D0_Hlt1GlobalDecision_TOS;
   Bool_t          D0_Hlt2Topo2BodyDecision_Dec;
   Bool_t          D0_Hlt2Topo2BodyDecision_TIS;
   Bool_t          D0_Hlt2Topo2BodyDecision_TOS;
   Bool_t          D0_Hlt2Topo3BodyDecision_Dec;
   Bool_t          D0_Hlt2Topo3BodyDecision_TIS;
   Bool_t          D0_Hlt2Topo3BodyDecision_TOS;
   Bool_t          D0_Hlt2Topo4BodyDecision_Dec;
   Bool_t          D0_Hlt2Topo4BodyDecision_TIS;
   Bool_t          D0_Hlt2Topo4BodyDecision_TOS;
   Bool_t          D0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_Dec;
   Bool_t          D0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_TIS;
   Bool_t          D0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_TOS;
   Bool_t          D0_Hlt2TopoMu3BodyDecision_Dec;
   Bool_t          D0_Hlt2TopoMu3BodyDecision_TIS;
   Bool_t          D0_Hlt2TopoMu3BodyDecision_TOS;
   Bool_t          D0_Hlt2TopoMu4BodyDecision_Dec;
   Bool_t          D0_Hlt2TopoMu4BodyDecision_TIS;
   Bool_t          D0_Hlt2TopoMu4BodyDecision_TOS;
   Bool_t          D0_Hlt2TopoE2BodyDecision_Dec;
   Bool_t          D0_Hlt2TopoE2BodyDecision_TIS;
   Bool_t          D0_Hlt2TopoE2BodyDecision_TOS;
   Bool_t          D0_Hlt2TopoE3BodyDecision_Dec;
   Bool_t          D0_Hlt2TopoE3BodyDecision_TIS;
   Bool_t          D0_Hlt2TopoE3BodyDecision_TOS;
   Bool_t          D0_Hlt2TopoE4BodyDecision_Dec;
   Bool_t          D0_Hlt2TopoE4BodyDecision_TIS;
   Bool_t          D0_Hlt2TopoE4BodyDecision_TOS;
   Bool_t          D0_Hlt2TopoRad2BodyDecision_Dec;
   Bool_t          D0_Hlt2TopoRad2BodyDecision_TIS;
   Bool_t          D0_Hlt2TopoRad2BodyDecision_TOS;
   Bool_t          D0_Hlt2SingleMuonDecision_Dec;
   Bool_t          D0_Hlt2SingleMuonDecision_TIS;
   Bool_t          D0_Hlt2SingleMuonDecision_TOS;
   Bool_t          D0_Hlt2RadiativeTopoTrackDecision_Dec;
   Bool_t          D0_Hlt2RadiativeTopoTrackDecision_TIS;
   Bool_t          D0_Hlt2RadiativeTopoTrackDecision_TOS;
   Bool_t          D0_Hlt2RadiativeTopoPhotonDecision_Dec;
   Bool_t          D0_Hlt2RadiativeTopoPhotonDecision_TIS;
   Bool_t          D0_Hlt2RadiativeTopoPhotonDecision_TOS;
   Bool_t          D0_Hlt2Topo2BodySimpleDecision_Dec;
   Bool_t          D0_Hlt2Topo2BodySimpleDecision_TIS;
   Bool_t          D0_Hlt2Topo2BodySimpleDecision_TOS;
   Bool_t          D0_Hlt2Topo3BodySimpleDecision_Dec;
   Bool_t          D0_Hlt2Topo3BodySimpleDecision_TIS;
   Bool_t          D0_Hlt2Topo3BodySimpleDecision_TOS;
   Bool_t          D0_Hlt2Topo4BodySimpleDecision_Dec;
   Bool_t          D0_Hlt2Topo4BodySimpleDecision_TIS;
   Bool_t          D0_Hlt2Topo4BodySimpleDecision_TOS;
   Bool_t          D0_Hlt2Topo2BodyBBDTDecision_Dec;
   Bool_t          D0_Hlt2Topo2BodyBBDTDecision_TIS;
   Bool_t          D0_Hlt2Topo2BodyBBDTDecision_TOS;
   Bool_t          D0_Hlt2Topo3BodyBBDTDecision_Dec;
   Bool_t          D0_Hlt2Topo3BodyBBDTDecision_TIS;
   Bool_t          D0_Hlt2Topo3BodyBBDTDecision_TOS;
   Bool_t          D0_Hlt2Topo4BodyBBDTDecision_Dec;
   Bool_t          D0_Hlt2Topo4BodyBBDTDecision_TIS;
   Bool_t          D0_Hlt2Topo4BodyBBDTDecision_TOS;
   Bool_t          D0_Hlt2TopoE2BodyBBDTDecision_Dec;
   Bool_t          D0_Hlt2TopoE2BodyBBDTDecision_TIS;
   Bool_t          D0_Hlt2TopoE2BodyBBDTDecision_TOS;
   Bool_t          D0_Hlt2TopoE3BodyBBDTDecision_Dec;
   Bool_t          D0_Hlt2TopoE3BodyBBDTDecision_TIS;
   Bool_t          D0_Hlt2TopoE3BodyBBDTDecision_TOS;
   Bool_t          D0_Hlt2TopoE4BodyBBDTDecision_Dec;
   Bool_t          D0_Hlt2TopoE4BodyBBDTDecision_TIS;
   Bool_t          D0_Hlt2TopoE4BodyBBDTDecision_TOS;
   Bool_t          D0_Hlt2TopoMu2BodyBBDTDecision_Dec;
   Bool_t          D0_Hlt2TopoMu2BodyBBDTDecision_TIS;
   Bool_t          D0_Hlt2TopoMu2BodyBBDTDecision_TOS;
   Bool_t          D0_Hlt2TopoMu3BodyBBDTDecision_Dec;
   Bool_t          D0_Hlt2TopoMu3BodyBBDTDecision_TIS;
   Bool_t          D0_Hlt2TopoMu3BodyBBDTDecision_TOS;
   Bool_t          D0_Hlt2TopoMu4BodyBBDTDecision_Dec;
   Bool_t          D0_Hlt2TopoMu4BodyBBDTDecision_TIS;
   Bool_t          D0_Hlt2TopoMu4BodyBBDTDecision_TOS;
   Bool_t          D0_Hlt2GlobalDecision_Dec;
   Bool_t          D0_Hlt2GlobalDecision_TIS;
   Bool_t          D0_Hlt2GlobalDecision_TOS;
   Bool_t          D0_Hlt2TopoRad2BodyBBDTDecision_Dec;
   Bool_t          D0_Hlt2TopoRad2BodyBBDTDecision_TIS;
   Bool_t          D0_Hlt2TopoRad2BodyBBDTDecision_TOS;
   Double_t        pi_MC12TuneV2_ProbNNe;
   Double_t        pi_MC12TuneV2_ProbNNmu;
   Double_t        pi_MC12TuneV2_ProbNNpi;
   Double_t        pi_MC12TuneV2_ProbNNk;
   Double_t        pi_MC12TuneV2_ProbNNp;
   Double_t        pi_MC12TuneV2_ProbNNghost;
   Double_t        pi_MC12TuneV3_ProbNNe;
   Double_t        pi_MC12TuneV3_ProbNNmu;
   Double_t        pi_MC12TuneV3_ProbNNpi;
   Double_t        pi_MC12TuneV3_ProbNNk;
   Double_t        pi_MC12TuneV3_ProbNNp;
   Double_t        pi_MC12TuneV3_ProbNNghost;
   Double_t        pi_MC12TuneV4_ProbNNe;
   Double_t        pi_MC12TuneV4_ProbNNmu;
   Double_t        pi_MC12TuneV4_ProbNNpi;
   Double_t        pi_MC12TuneV4_ProbNNk;
   Double_t        pi_MC12TuneV4_ProbNNp;
   Double_t        pi_MC12TuneV4_ProbNNghost;
   Double_t        pi_MC15TuneV1_ProbNNe;
   Double_t        pi_MC15TuneV1_ProbNNmu;
   Double_t        pi_MC15TuneV1_ProbNNpi;
   Double_t        pi_MC15TuneV1_ProbNNk;
   Double_t        pi_MC15TuneV1_ProbNNp;
   Double_t        pi_MC15TuneV1_ProbNNghost;
   Double_t        pi_CosTheta;
   Double_t        pi_OWNPV_X;
   Double_t        pi_OWNPV_Y;
   Double_t        pi_OWNPV_Z;
   Double_t        pi_OWNPV_XERR;
   Double_t        pi_OWNPV_YERR;
   Double_t        pi_OWNPV_ZERR;
   Double_t        pi_OWNPV_CHI2;
   Int_t           pi_OWNPV_NDOF;
   Float_t         pi_OWNPV_COV_[3][3];
   Double_t        pi_IP_OWNPV;
   Double_t        pi_IPCHI2_OWNPV;
   Double_t        pi_ORIVX_X;
   Double_t        pi_ORIVX_Y;
   Double_t        pi_ORIVX_Z;
   Double_t        pi_ORIVX_XERR;
   Double_t        pi_ORIVX_YERR;
   Double_t        pi_ORIVX_ZERR;
   Double_t        pi_ORIVX_CHI2;
   Int_t           pi_ORIVX_NDOF;
   Float_t         pi_ORIVX_COV_[3][3];
   Double_t        pi_P;
   Double_t        pi_PT;
   Double_t        pi_PE;
   Double_t        pi_PX;
   Double_t        pi_PY;
   Double_t        pi_PZ;
   Double_t        pi_M;
   Int_t           pi_ID;
   Double_t        pi_PIDe;
   Double_t        pi_PIDmu;
   Double_t        pi_PIDK;
   Double_t        pi_PIDp;
   Double_t        pi_PIDd;
   Double_t        pi_ProbNNe;
   Double_t        pi_ProbNNk;
   Double_t        pi_ProbNNp;
   Double_t        pi_ProbNNpi;
   Double_t        pi_ProbNNmu;
   Double_t        pi_ProbNNd;
   Double_t        pi_ProbNNghost;
   Bool_t          pi_hasMuon;
   Bool_t          pi_isMuon;
   Bool_t          pi_hasRich;
   Bool_t          pi_UsedRichAerogel;
   Bool_t          pi_UsedRich1Gas;
   Bool_t          pi_UsedRich2Gas;
   Bool_t          pi_RichAboveElThres;
   Bool_t          pi_RichAboveMuThres;
   Bool_t          pi_RichAbovePiThres;
   Bool_t          pi_RichAboveKaThres;
   Bool_t          pi_RichAbovePrThres;
   Bool_t          pi_hasCalo;
   Bool_t          pi_L0Global_Dec;
   Bool_t          pi_L0Global_TIS;
   Bool_t          pi_L0Global_TOS;
   Bool_t          pi_Hlt1Global_Dec;
   Bool_t          pi_Hlt1Global_TIS;
   Bool_t          pi_Hlt1Global_TOS;
   Bool_t          pi_Hlt1Phys_Dec;
   Bool_t          pi_Hlt1Phys_TIS;
   Bool_t          pi_Hlt1Phys_TOS;
   Bool_t          pi_Hlt2Global_Dec;
   Bool_t          pi_Hlt2Global_TIS;
   Bool_t          pi_Hlt2Global_TOS;
   Bool_t          pi_Hlt2Phys_Dec;
   Bool_t          pi_Hlt2Phys_TIS;
   Bool_t          pi_Hlt2Phys_TOS;
   Int_t           pi_TRACK_Type;
   Int_t           pi_TRACK_Key;
   Double_t        pi_TRACK_CHI2NDOF;
   Double_t        pi_TRACK_PCHI2;
   Double_t        pi_TRACK_MatchCHI2;
   Double_t        pi_TRACK_GhostProb;
   Double_t        pi_TRACK_CloneDist;
   Double_t        pi_TRACK_Likelihood;
   Double_t        pi_ETA;
   Double_t        pi_KL;
   Double_t        pi_PHI;
   Double_t        pi_Y;
   Double_t        K_MC12TuneV2_ProbNNe;
   Double_t        K_MC12TuneV2_ProbNNmu;
   Double_t        K_MC12TuneV2_ProbNNpi;
   Double_t        K_MC12TuneV2_ProbNNk;
   Double_t        K_MC12TuneV2_ProbNNp;
   Double_t        K_MC12TuneV2_ProbNNghost;
   Double_t        K_MC12TuneV3_ProbNNe;
   Double_t        K_MC12TuneV3_ProbNNmu;
   Double_t        K_MC12TuneV3_ProbNNpi;
   Double_t        K_MC12TuneV3_ProbNNk;
   Double_t        K_MC12TuneV3_ProbNNp;
   Double_t        K_MC12TuneV3_ProbNNghost;
   Double_t        K_MC12TuneV4_ProbNNe;
   Double_t        K_MC12TuneV4_ProbNNmu;
   Double_t        K_MC12TuneV4_ProbNNpi;
   Double_t        K_MC12TuneV4_ProbNNk;
   Double_t        K_MC12TuneV4_ProbNNp;
   Double_t        K_MC12TuneV4_ProbNNghost;
   Double_t        K_MC15TuneV1_ProbNNe;
   Double_t        K_MC15TuneV1_ProbNNmu;
   Double_t        K_MC15TuneV1_ProbNNpi;
   Double_t        K_MC15TuneV1_ProbNNk;
   Double_t        K_MC15TuneV1_ProbNNp;
   Double_t        K_MC15TuneV1_ProbNNghost;
   Double_t        K_CosTheta;
   Double_t        K_OWNPV_X;
   Double_t        K_OWNPV_Y;
   Double_t        K_OWNPV_Z;
   Double_t        K_OWNPV_XERR;
   Double_t        K_OWNPV_YERR;
   Double_t        K_OWNPV_ZERR;
   Double_t        K_OWNPV_CHI2;
   Int_t           K_OWNPV_NDOF;
   Float_t         K_OWNPV_COV_[3][3];
   Double_t        K_IP_OWNPV;
   Double_t        K_IPCHI2_OWNPV;
   Double_t        K_ORIVX_X;
   Double_t        K_ORIVX_Y;
   Double_t        K_ORIVX_Z;
   Double_t        K_ORIVX_XERR;
   Double_t        K_ORIVX_YERR;
   Double_t        K_ORIVX_ZERR;
   Double_t        K_ORIVX_CHI2;
   Int_t           K_ORIVX_NDOF;
   Float_t         K_ORIVX_COV_[3][3];
   Double_t        K_P;
   Double_t        K_PT;
   Double_t        K_PE;
   Double_t        K_PX;
   Double_t        K_PY;
   Double_t        K_PZ;
   Double_t        K_M;
   Int_t           K_ID;
   Double_t        K_PIDe;
   Double_t        K_PIDmu;
   Double_t        K_PIDK;
   Double_t        K_PIDp;
   Double_t        K_PIDd;
   Double_t        K_ProbNNe;
   Double_t        K_ProbNNk;
   Double_t        K_ProbNNp;
   Double_t        K_ProbNNpi;
   Double_t        K_ProbNNmu;
   Double_t        K_ProbNNd;
   Double_t        K_ProbNNghost;
   Bool_t          K_hasMuon;
   Bool_t          K_isMuon;
   Bool_t          K_hasRich;
   Bool_t          K_UsedRichAerogel;
   Bool_t          K_UsedRich1Gas;
   Bool_t          K_UsedRich2Gas;
   Bool_t          K_RichAboveElThres;
   Bool_t          K_RichAboveMuThres;
   Bool_t          K_RichAbovePiThres;
   Bool_t          K_RichAboveKaThres;
   Bool_t          K_RichAbovePrThres;
   Bool_t          K_hasCalo;
   Bool_t          K_L0Global_Dec;
   Bool_t          K_L0Global_TIS;
   Bool_t          K_L0Global_TOS;
   Bool_t          K_Hlt1Global_Dec;
   Bool_t          K_Hlt1Global_TIS;
   Bool_t          K_Hlt1Global_TOS;
   Bool_t          K_Hlt1Phys_Dec;
   Bool_t          K_Hlt1Phys_TIS;
   Bool_t          K_Hlt1Phys_TOS;
   Bool_t          K_Hlt2Global_Dec;
   Bool_t          K_Hlt2Global_TIS;
   Bool_t          K_Hlt2Global_TOS;
   Bool_t          K_Hlt2Phys_Dec;
   Bool_t          K_Hlt2Phys_TIS;
   Bool_t          K_Hlt2Phys_TOS;
   Int_t           K_TRACK_Type;
   Int_t           K_TRACK_Key;
   Double_t        K_TRACK_CHI2NDOF;
   Double_t        K_TRACK_PCHI2;
   Double_t        K_TRACK_MatchCHI2;
   Double_t        K_TRACK_GhostProb;
   Double_t        K_TRACK_CloneDist;
   Double_t        K_TRACK_Likelihood;
   Double_t        K_ETA;
   Double_t        K_KL;
   Double_t        K_PHI;
   Double_t        K_Y;
   Double_t        R_CosTheta;
   Double_t        R_ENDVERTEX_X;
   Double_t        R_ENDVERTEX_Y;
   Double_t        R_ENDVERTEX_Z;
   Double_t        R_ENDVERTEX_XERR;
   Double_t        R_ENDVERTEX_YERR;
   Double_t        R_ENDVERTEX_ZERR;
   Double_t        R_ENDVERTEX_CHI2;
   Int_t           R_ENDVERTEX_NDOF;
   Float_t         R_ENDVERTEX_COV_[3][3];
   Double_t        R_OWNPV_X;
   Double_t        R_OWNPV_Y;
   Double_t        R_OWNPV_Z;
   Double_t        R_OWNPV_XERR;
   Double_t        R_OWNPV_YERR;
   Double_t        R_OWNPV_ZERR;
   Double_t        R_OWNPV_CHI2;
   Int_t           R_OWNPV_NDOF;
   Float_t         R_OWNPV_COV_[3][3];
   Double_t        R_IP_OWNPV;
   Double_t        R_IPCHI2_OWNPV;
   Double_t        R_FD_OWNPV;
   Double_t        R_FDCHI2_OWNPV;
   Double_t        R_DIRA_OWNPV;
   Double_t        R_ORIVX_X;
   Double_t        R_ORIVX_Y;
   Double_t        R_ORIVX_Z;
   Double_t        R_ORIVX_XERR;
   Double_t        R_ORIVX_YERR;
   Double_t        R_ORIVX_ZERR;
   Double_t        R_ORIVX_CHI2;
   Int_t           R_ORIVX_NDOF;
   Float_t         R_ORIVX_COV_[3][3];
   Double_t        R_FD_ORIVX;
   Double_t        R_FDCHI2_ORIVX;
   Double_t        R_DIRA_ORIVX;
   Double_t        R_P;
   Double_t        R_PT;
   Double_t        R_PE;
   Double_t        R_PX;
   Double_t        R_PY;
   Double_t        R_PZ;
   Double_t        R_MM;
   Double_t        R_MMERR;
   Double_t        R_M;
   Int_t           R_ID;
   Double_t        R_TAU;
   Double_t        R_TAUERR;
   Double_t        R_TAUCHI2;
   Bool_t          R_L0Global_Dec;
   Bool_t          R_L0Global_TIS;
   Bool_t          R_L0Global_TOS;
   Bool_t          R_Hlt1Global_Dec;
   Bool_t          R_Hlt1Global_TIS;
   Bool_t          R_Hlt1Global_TOS;
   Bool_t          R_Hlt1Phys_Dec;
   Bool_t          R_Hlt1Phys_TIS;
   Bool_t          R_Hlt1Phys_TOS;
   Bool_t          R_Hlt2Global_Dec;
   Bool_t          R_Hlt2Global_TIS;
   Bool_t          R_Hlt2Global_TOS;
   Bool_t          R_Hlt2Phys_Dec;
   Bool_t          R_Hlt2Phys_TIS;
   Bool_t          R_Hlt2Phys_TOS;
   Double_t        R_NumVtxWithinChi2WindowOneTrack;
   Double_t        R_SmallestDeltaChi2OneTrack;
   Double_t        R_SmallestDeltaChi2MassOneTrack;
   Double_t        R_SmallestDeltaChi2TwoTracks;
   Double_t        R_SmallestDeltaChi2MassTwoTracks;
   Double_t        R_ETA;
   Double_t        R_LOKI_DIRA;
   Double_t        R_LOKI_FDCHI2;
   Double_t        R_LOKI_FDS;
   Double_t        R_LOKI_LV01;
   Double_t        R_LOKI_LV02;
   Double_t        R_LOKI_MAXDOCA;
   Double_t        R_LOKI_TAU;
   Double_t        R_PHI;
   Double_t        R_Y;
   Double_t        pp_MC12TuneV2_ProbNNe;
   Double_t        pp_MC12TuneV2_ProbNNmu;
   Double_t        pp_MC12TuneV2_ProbNNpi;
   Double_t        pp_MC12TuneV2_ProbNNk;
   Double_t        pp_MC12TuneV2_ProbNNp;
   Double_t        pp_MC12TuneV2_ProbNNghost;
   Double_t        pp_MC12TuneV3_ProbNNe;
   Double_t        pp_MC12TuneV3_ProbNNmu;
   Double_t        pp_MC12TuneV3_ProbNNpi;
   Double_t        pp_MC12TuneV3_ProbNNk;
   Double_t        pp_MC12TuneV3_ProbNNp;
   Double_t        pp_MC12TuneV3_ProbNNghost;
   Double_t        pp_MC12TuneV4_ProbNNe;
   Double_t        pp_MC12TuneV4_ProbNNmu;
   Double_t        pp_MC12TuneV4_ProbNNpi;
   Double_t        pp_MC12TuneV4_ProbNNk;
   Double_t        pp_MC12TuneV4_ProbNNp;
   Double_t        pp_MC12TuneV4_ProbNNghost;
   Double_t        pp_MC15TuneV1_ProbNNe;
   Double_t        pp_MC15TuneV1_ProbNNmu;
   Double_t        pp_MC15TuneV1_ProbNNpi;
   Double_t        pp_MC15TuneV1_ProbNNk;
   Double_t        pp_MC15TuneV1_ProbNNp;
   Double_t        pp_MC15TuneV1_ProbNNghost;
   Double_t        pp_CosTheta;
   Double_t        pp_OWNPV_X;
   Double_t        pp_OWNPV_Y;
   Double_t        pp_OWNPV_Z;
   Double_t        pp_OWNPV_XERR;
   Double_t        pp_OWNPV_YERR;
   Double_t        pp_OWNPV_ZERR;
   Double_t        pp_OWNPV_CHI2;
   Int_t           pp_OWNPV_NDOF;
   Float_t         pp_OWNPV_COV_[3][3];
   Double_t        pp_IP_OWNPV;
   Double_t        pp_IPCHI2_OWNPV;
   Double_t        pp_ORIVX_X;
   Double_t        pp_ORIVX_Y;
   Double_t        pp_ORIVX_Z;
   Double_t        pp_ORIVX_XERR;
   Double_t        pp_ORIVX_YERR;
   Double_t        pp_ORIVX_ZERR;
   Double_t        pp_ORIVX_CHI2;
   Int_t           pp_ORIVX_NDOF;
   Float_t         pp_ORIVX_COV_[3][3];
   Double_t        pp_P;
   Double_t        pp_PT;
   Double_t        pp_PE;
   Double_t        pp_PX;
   Double_t        pp_PY;
   Double_t        pp_PZ;
   Double_t        pp_M;
   Int_t           pp_ID;
   Double_t        pp_PIDe;
   Double_t        pp_PIDmu;
   Double_t        pp_PIDK;
   Double_t        pp_PIDp;
   Double_t        pp_PIDd;
   Double_t        pp_ProbNNe;
   Double_t        pp_ProbNNk;
   Double_t        pp_ProbNNp;
   Double_t        pp_ProbNNpi;
   Double_t        pp_ProbNNmu;
   Double_t        pp_ProbNNd;
   Double_t        pp_ProbNNghost;
   Bool_t          pp_hasMuon;
   Bool_t          pp_isMuon;
   Bool_t          pp_hasRich;
   Bool_t          pp_UsedRichAerogel;
   Bool_t          pp_UsedRich1Gas;
   Bool_t          pp_UsedRich2Gas;
   Bool_t          pp_RichAboveElThres;
   Bool_t          pp_RichAboveMuThres;
   Bool_t          pp_RichAbovePiThres;
   Bool_t          pp_RichAboveKaThres;
   Bool_t          pp_RichAbovePrThres;
   Bool_t          pp_hasCalo;
   Bool_t          pp_L0Global_Dec;
   Bool_t          pp_L0Global_TIS;
   Bool_t          pp_L0Global_TOS;
   Bool_t          pp_Hlt1Global_Dec;
   Bool_t          pp_Hlt1Global_TIS;
   Bool_t          pp_Hlt1Global_TOS;
   Bool_t          pp_Hlt1Phys_Dec;
   Bool_t          pp_Hlt1Phys_TIS;
   Bool_t          pp_Hlt1Phys_TOS;
   Bool_t          pp_Hlt2Global_Dec;
   Bool_t          pp_Hlt2Global_TIS;
   Bool_t          pp_Hlt2Global_TOS;
   Bool_t          pp_Hlt2Phys_Dec;
   Bool_t          pp_Hlt2Phys_TIS;
   Bool_t          pp_Hlt2Phys_TOS;
   Int_t           pp_TRACK_Type;
   Int_t           pp_TRACK_Key;
   Double_t        pp_TRACK_CHI2NDOF;
   Double_t        pp_TRACK_PCHI2;
   Double_t        pp_TRACK_MatchCHI2;
   Double_t        pp_TRACK_GhostProb;
   Double_t        pp_TRACK_CloneDist;
   Double_t        pp_TRACK_Likelihood;
   Double_t        pp_ETA;
   Double_t        pp_KL;
   Double_t        pp_PHI;
   Double_t        pp_Y;
   Double_t        pm_MC12TuneV2_ProbNNe;
   Double_t        pm_MC12TuneV2_ProbNNmu;
   Double_t        pm_MC12TuneV2_ProbNNpi;
   Double_t        pm_MC12TuneV2_ProbNNk;
   Double_t        pm_MC12TuneV2_ProbNNp;
   Double_t        pm_MC12TuneV2_ProbNNghost;
   Double_t        pm_MC12TuneV3_ProbNNe;
   Double_t        pm_MC12TuneV3_ProbNNmu;
   Double_t        pm_MC12TuneV3_ProbNNpi;
   Double_t        pm_MC12TuneV3_ProbNNk;
   Double_t        pm_MC12TuneV3_ProbNNp;
   Double_t        pm_MC12TuneV3_ProbNNghost;
   Double_t        pm_MC12TuneV4_ProbNNe;
   Double_t        pm_MC12TuneV4_ProbNNmu;
   Double_t        pm_MC12TuneV4_ProbNNpi;
   Double_t        pm_MC12TuneV4_ProbNNk;
   Double_t        pm_MC12TuneV4_ProbNNp;
   Double_t        pm_MC12TuneV4_ProbNNghost;
   Double_t        pm_MC15TuneV1_ProbNNe;
   Double_t        pm_MC15TuneV1_ProbNNmu;
   Double_t        pm_MC15TuneV1_ProbNNpi;
   Double_t        pm_MC15TuneV1_ProbNNk;
   Double_t        pm_MC15TuneV1_ProbNNp;
   Double_t        pm_MC15TuneV1_ProbNNghost;
   Double_t        pm_CosTheta;
   Double_t        pm_OWNPV_X;
   Double_t        pm_OWNPV_Y;
   Double_t        pm_OWNPV_Z;
   Double_t        pm_OWNPV_XERR;
   Double_t        pm_OWNPV_YERR;
   Double_t        pm_OWNPV_ZERR;
   Double_t        pm_OWNPV_CHI2;
   Int_t           pm_OWNPV_NDOF;
   Float_t         pm_OWNPV_COV_[3][3];
   Double_t        pm_IP_OWNPV;
   Double_t        pm_IPCHI2_OWNPV;
   Double_t        pm_ORIVX_X;
   Double_t        pm_ORIVX_Y;
   Double_t        pm_ORIVX_Z;
   Double_t        pm_ORIVX_XERR;
   Double_t        pm_ORIVX_YERR;
   Double_t        pm_ORIVX_ZERR;
   Double_t        pm_ORIVX_CHI2;
   Int_t           pm_ORIVX_NDOF;
   Float_t         pm_ORIVX_COV_[3][3];
   Double_t        pm_P;
   Double_t        pm_PT;
   Double_t        pm_PE;
   Double_t        pm_PX;
   Double_t        pm_PY;
   Double_t        pm_PZ;
   Double_t        pm_M;
   Int_t           pm_ID;
   Double_t        pm_PIDe;
   Double_t        pm_PIDmu;
   Double_t        pm_PIDK;
   Double_t        pm_PIDp;
   Double_t        pm_PIDd;
   Double_t        pm_ProbNNe;
   Double_t        pm_ProbNNk;
   Double_t        pm_ProbNNp;
   Double_t        pm_ProbNNpi;
   Double_t        pm_ProbNNmu;
   Double_t        pm_ProbNNd;
   Double_t        pm_ProbNNghost;
   Bool_t          pm_hasMuon;
   Bool_t          pm_isMuon;
   Bool_t          pm_hasRich;
   Bool_t          pm_UsedRichAerogel;
   Bool_t          pm_UsedRich1Gas;
   Bool_t          pm_UsedRich2Gas;
   Bool_t          pm_RichAboveElThres;
   Bool_t          pm_RichAboveMuThres;
   Bool_t          pm_RichAbovePiThres;
   Bool_t          pm_RichAboveKaThres;
   Bool_t          pm_RichAbovePrThres;
   Bool_t          pm_hasCalo;
   Bool_t          pm_L0Global_Dec;
   Bool_t          pm_L0Global_TIS;
   Bool_t          pm_L0Global_TOS;
   Bool_t          pm_Hlt1Global_Dec;
   Bool_t          pm_Hlt1Global_TIS;
   Bool_t          pm_Hlt1Global_TOS;
   Bool_t          pm_Hlt1Phys_Dec;
   Bool_t          pm_Hlt1Phys_TIS;
   Bool_t          pm_Hlt1Phys_TOS;
   Bool_t          pm_Hlt2Global_Dec;
   Bool_t          pm_Hlt2Global_TIS;
   Bool_t          pm_Hlt2Global_TOS;
   Bool_t          pm_Hlt2Phys_Dec;
   Bool_t          pm_Hlt2Phys_TIS;
   Bool_t          pm_Hlt2Phys_TOS;
   Int_t           pm_TRACK_Type;
   Int_t           pm_TRACK_Key;
   Double_t        pm_TRACK_CHI2NDOF;
   Double_t        pm_TRACK_PCHI2;
   Double_t        pm_TRACK_MatchCHI2;
   Double_t        pm_TRACK_GhostProb;
   Double_t        pm_TRACK_CloneDist;
   Double_t        pm_TRACK_Likelihood;
   Double_t        pm_ETA;
   Double_t        pm_KL;
   Double_t        pm_PHI;
   Double_t        pm_Y;
   UInt_t          nCandidate;
   ULong64_t       totCandidates;
   ULong64_t       EventInSequence;
   UInt_t          runNumber;
   ULong64_t       eventNumber;
   UInt_t          BCID;
   Int_t           BCType;
   UInt_t          OdinTCK;
   UInt_t          L0DUTCK;
   UInt_t          HLT1TCK;
   UInt_t          HLT2TCK;
   ULong64_t       GpsTime;
   Short_t         Polarity;
   Int_t           nPV;
   Float_t         PVX[100];   //[nPV]
   Float_t         PVY[100];   //[nPV]
   Float_t         PVZ[100];   //[nPV]
   Float_t         PVXERR[100];   //[nPV]
   Float_t         PVYERR[100];   //[nPV]
   Float_t         PVZERR[100];   //[nPV]
   Float_t         PVCHI2[100];   //[nPV]
   Float_t         PVNDOF[100];   //[nPV]
   Float_t         PVNTRACKS[100];   //[nPV]
   Int_t           nPVs;
   Int_t           nTracks;
   Int_t           nLongTracks;
   Int_t           nDownstreamTracks;
   Int_t           nUpstreamTracks;
   Int_t           nVeloTracks;
   Int_t           nTTracks;
   Int_t           nBackTracks;
   Int_t           nRich1Hits;
   Int_t           nRich2Hits;
   Int_t           nVeloClusters;
   Int_t           nITClusters;
   Int_t           nTTClusters;
   Int_t           nOTClusters;
   Int_t           nSPDHits;
   Int_t           nMuonCoordsS0;
   Int_t           nMuonCoordsS1;
   Int_t           nMuonCoordsS2;
   Int_t           nMuonCoordsS3;
   Int_t           nMuonCoordsS4;
   Int_t           nMuonTracks;
   Double_t        mass_pp;
   Double_t        mass_Dp;
   Double_t        mass_Dpbar;
   Double_t        B_mass_con;
   Double_t        log_B0_PT;
   Double_t        log_B0_P;
   Double_t        log_acos_B0_LOKI_DIRA;
   Double_t        log_abs_DTau;
   Double_t        log_D0_FD_OWNPV;
   Double_t        log_acos_D0_LOKI_DIRA;
   Double_t        min_D_daughter_pt;
   Double_t        min_p_pt;
   Double_t        max_ip_chi2;
   Double_t        BDZDis;
   Float_t         mvaBDT;
   Float_t         Sweight_sig;
   Float_t         Sweight_bkg;
   Double_t        mppbar;
   Double_t        mD0p;
   Double_t        mD0pbar;
   Double_t        cosTheta_X;
   Double_t        phi_X;
   Double_t        cosTheta_L;
   Double_t        phi_L;
   Double_t        cosTheta_p;
   Double_t        cosTheta_pbar;
   Double_t        AFB_cosTheta_p;
   Double_t        eff;

   // List of branches
   TBranch        *b_B0_DiraAngleError;   //!
   TBranch        *b_B0_DiraCosError;   //!
   TBranch        *b_B0_DiraAngle;   //!
   TBranch        *b_B0_DiraCos;   //!
   TBranch        *b_B0_ENDVERTEX_X;   //!
   TBranch        *b_B0_ENDVERTEX_Y;   //!
   TBranch        *b_B0_ENDVERTEX_Z;   //!
   TBranch        *b_B0_ENDVERTEX_XERR;   //!
   TBranch        *b_B0_ENDVERTEX_YERR;   //!
   TBranch        *b_B0_ENDVERTEX_ZERR;   //!
   TBranch        *b_B0_ENDVERTEX_CHI2;   //!
   TBranch        *b_B0_ENDVERTEX_NDOF;   //!
   TBranch        *b_B0_ENDVERTEX_COV_;   //!
   TBranch        *b_B0_OWNPV_X;   //!
   TBranch        *b_B0_OWNPV_Y;   //!
   TBranch        *b_B0_OWNPV_Z;   //!
   TBranch        *b_B0_OWNPV_XERR;   //!
   TBranch        *b_B0_OWNPV_YERR;   //!
   TBranch        *b_B0_OWNPV_ZERR;   //!
   TBranch        *b_B0_OWNPV_CHI2;   //!
   TBranch        *b_B0_OWNPV_NDOF;   //!
   TBranch        *b_B0_OWNPV_COV_;   //!
   TBranch        *b_B0_IP_OWNPV;   //!
   TBranch        *b_B0_IPCHI2_OWNPV;   //!
   TBranch        *b_B0_FD_OWNPV;   //!
   TBranch        *b_B0_FDCHI2_OWNPV;   //!
   TBranch        *b_B0_DIRA_OWNPV;   //!
   TBranch        *b_B0_P;   //!
   TBranch        *b_B0_PT;   //!
   TBranch        *b_B0_PE;   //!
   TBranch        *b_B0_PX;   //!
   TBranch        *b_B0_PY;   //!
   TBranch        *b_B0_PZ;   //!
   TBranch        *b_B0_MM;   //!
   TBranch        *b_B0_MMERR;   //!
   TBranch        *b_B0_M;   //!
   TBranch        *b_B0_ID;   //!
   TBranch        *b_B0_TAU;   //!
   TBranch        *b_B0_TAUERR;   //!
   TBranch        *b_B0_TAUCHI2;   //!
   TBranch        *b_B0_NumVtxWithinChi2WindowOneTrack;   //!
   TBranch        *b_B0_SmallestDeltaChi2OneTrack;   //!
   TBranch        *b_B0_SmallestDeltaChi2MassOneTrack;   //!
   TBranch        *b_B0_SmallestDeltaChi2TwoTracks;   //!
   TBranch        *b_B0_SmallestDeltaChi2MassTwoTracks;   //!
   TBranch        *b_B0_BDDTF_CHI2NDOF;   //!
   TBranch        *b_B0_BDDTF_CTAU;   //!
   TBranch        *b_B0_BDDTF_CTAUS;   //!
   TBranch        *b_B0_BDDTF_DIRA;   //!
   TBranch        *b_B0_BDDTF_E;   //!
   TBranch        *b_B0_BDDTF_FDCHI2;   //!
   TBranch        *b_B0_BDDTF_FDS;   //!
   TBranch        *b_B0_BDDTF_IP;   //!
   TBranch        *b_B0_BDDTF_IPCHI2;   //!
   TBranch        *b_B0_BDDTF_M;   //!
   TBranch        *b_B0_BDDTF_P;   //!
   TBranch        *b_B0_BDDTF_PT;   //!
   TBranch        *b_B0_BDDTF_PX;   //!
   TBranch        *b_B0_BDDTF_PY;   //!
   TBranch        *b_B0_BDDTF_PZ;   //!
   TBranch        *b_B0_BDDTF_TAU;   //!
   TBranch        *b_B0_D0_BDDTF_CTAU;   //!
   TBranch        *b_B0_D0_BDDTF_CTAUS;   //!
   TBranch        *b_B0_D0_BDDTF_DIRA;   //!
   TBranch        *b_B0_D0_BDDTF_E;   //!
   TBranch        *b_B0_D0_BDDTF_FDCHI2;   //!
   TBranch        *b_B0_D0_BDDTF_FDS;   //!
   TBranch        *b_B0_D0_BDDTF_IP;   //!
   TBranch        *b_B0_D0_BDDTF_IPCHI2;   //!
   TBranch        *b_B0_D0_BDDTF_M;   //!
   TBranch        *b_B0_D0_BDDTF_P;   //!
   TBranch        *b_B0_D0_BDDTF_PT;   //!
   TBranch        *b_B0_D0_BDDTF_PX;   //!
   TBranch        *b_B0_D0_BDDTF_PY;   //!
   TBranch        *b_B0_D0_BDDTF_PZ;   //!
   TBranch        *b_B0_D0_BDDTF_TAU;   //!
   TBranch        *b_B0_pm_BDDTF_DIRA;   //!
   TBranch        *b_B0_pm_BDDTF_E;   //!
   TBranch        *b_B0_pm_BDDTF_FDCHI2;   //!
   TBranch        *b_B0_pm_BDDTF_FDS;   //!
   TBranch        *b_B0_pm_BDDTF_IP;   //!
   TBranch        *b_B0_pm_BDDTF_IPCHI2;   //!
   TBranch        *b_B0_pm_BDDTF_M;   //!
   TBranch        *b_B0_pm_BDDTF_P;   //!
   TBranch        *b_B0_pm_BDDTF_PT;   //!
   TBranch        *b_B0_pm_BDDTF_PX;   //!
   TBranch        *b_B0_pm_BDDTF_PY;   //!
   TBranch        *b_B0_pm_BDDTF_PZ;   //!
   TBranch        *b_B0_pm_BDDTF_TAU;   //!
   TBranch        *b_B0_pp_BDDTF_DIRA;   //!
   TBranch        *b_B0_pp_BDDTF_E;   //!
   TBranch        *b_B0_pp_BDDTF_FDCHI2;   //!
   TBranch        *b_B0_pp_BDDTF_FDS;   //!
   TBranch        *b_B0_pp_BDDTF_IP;   //!
   TBranch        *b_B0_pp_BDDTF_IPCHI2;   //!
   TBranch        *b_B0_pp_BDDTF_M;   //!
   TBranch        *b_B0_pp_BDDTF_P;   //!
   TBranch        *b_B0_pp_BDDTF_PT;   //!
   TBranch        *b_B0_pp_BDDTF_PX;   //!
   TBranch        *b_B0_pp_BDDTF_PY;   //!
   TBranch        *b_B0_pp_BDDTF_PZ;   //!
   TBranch        *b_B0_pp_BDDTF_TAU;   //!
   TBranch        *b_B0_BDTF_CHI2NDOF;   //!
   TBranch        *b_B0_BDTF_CTAU;   //!
   TBranch        *b_B0_BDTF_CTAUS;   //!
   TBranch        *b_B0_BDTF_DIRA;   //!
   TBranch        *b_B0_BDTF_E;   //!
   TBranch        *b_B0_BDTF_FDCHI2;   //!
   TBranch        *b_B0_BDTF_FDS;   //!
   TBranch        *b_B0_BDTF_IP;   //!
   TBranch        *b_B0_BDTF_IPCHI2;   //!
   TBranch        *b_B0_BDTF_M;   //!
   TBranch        *b_B0_BDTF_P;   //!
   TBranch        *b_B0_BDTF_PT;   //!
   TBranch        *b_B0_BDTF_PX;   //!
   TBranch        *b_B0_BDTF_PY;   //!
   TBranch        *b_B0_BDTF_PZ;   //!
   TBranch        *b_B0_BDTF_TAU;   //!
   TBranch        *b_B0_D0_BDTF_CTAU;   //!
   TBranch        *b_B0_D0_BDTF_CTAUS;   //!
   TBranch        *b_B0_D0_BDTF_DIRA;   //!
   TBranch        *b_B0_D0_BDTF_E;   //!
   TBranch        *b_B0_D0_BDTF_FDCHI2;   //!
   TBranch        *b_B0_D0_BDTF_FDS;   //!
   TBranch        *b_B0_D0_BDTF_IP;   //!
   TBranch        *b_B0_D0_BDTF_IPCHI2;   //!
   TBranch        *b_B0_D0_BDTF_M;   //!
   TBranch        *b_B0_D0_BDTF_P;   //!
   TBranch        *b_B0_D0_BDTF_PT;   //!
   TBranch        *b_B0_D0_BDTF_PX;   //!
   TBranch        *b_B0_D0_BDTF_PY;   //!
   TBranch        *b_B0_D0_BDTF_PZ;   //!
   TBranch        *b_B0_D0_BDTF_TAU;   //!
   TBranch        *b_B0_D0_DDTF_CTAU;   //!
   TBranch        *b_B0_D0_DDTF_CTAUS;   //!
   TBranch        *b_B0_D0_DDTF_DIRA;   //!
   TBranch        *b_B0_D0_DDTF_E;   //!
   TBranch        *b_B0_D0_DDTF_FDCHI2;   //!
   TBranch        *b_B0_D0_DDTF_FDS;   //!
   TBranch        *b_B0_D0_DDTF_IP;   //!
   TBranch        *b_B0_D0_DDTF_IPCHI2;   //!
   TBranch        *b_B0_D0_DDTF_M;   //!
   TBranch        *b_B0_D0_DDTF_P;   //!
   TBranch        *b_B0_D0_DDTF_PT;   //!
   TBranch        *b_B0_D0_DDTF_PX;   //!
   TBranch        *b_B0_D0_DDTF_PY;   //!
   TBranch        *b_B0_D0_DDTF_PZ;   //!
   TBranch        *b_B0_D0_DDTF_TAU;   //!
   TBranch        *b_B0_DDTF_CHI2NDOF;   //!
   TBranch        *b_B0_DDTF_CTAU;   //!
   TBranch        *b_B0_DDTF_CTAUS;   //!
   TBranch        *b_B0_DDTF_DIRA;   //!
   TBranch        *b_B0_DDTF_E;   //!
   TBranch        *b_B0_DDTF_FDCHI2;   //!
   TBranch        *b_B0_DDTF_FDS;   //!
   TBranch        *b_B0_DDTF_IP;   //!
   TBranch        *b_B0_DDTF_IPCHI2;   //!
   TBranch        *b_B0_DDTF_M;   //!
   TBranch        *b_B0_DDTF_P;   //!
   TBranch        *b_B0_DDTF_PT;   //!
   TBranch        *b_B0_DDTF_PX;   //!
   TBranch        *b_B0_DDTF_PY;   //!
   TBranch        *b_B0_DDTF_PZ;   //!
   TBranch        *b_B0_DDTF_TAU;   //!
   TBranch        *b_B0_ETA;   //!
   TBranch        *b_B0_LOKI_DIRA;   //!
   TBranch        *b_B0_LOKI_FDCHI2;   //!
   TBranch        *b_B0_LOKI_FDS;   //!
   TBranch        *b_B0_LOKI_LV01;   //!
   TBranch        *b_B0_LOKI_LV02;   //!
   TBranch        *b_B0_LOKI_MAXDOCA;   //!
   TBranch        *b_B0_LOKI_TAU;   //!
   TBranch        *b_B0_PHI;   //!
   TBranch        *b_B0_Y;   //!
   TBranch        *b_B0_Cons_nPV;   //!
   TBranch        *b_B0_Cons_D0_Kplus_ID;   //!
   TBranch        *b_B0_Cons_D0_Kplus_PE;   //!
   TBranch        *b_B0_Cons_D0_Kplus_PX;   //!
   TBranch        *b_B0_Cons_D0_Kplus_PY;   //!
   TBranch        *b_B0_Cons_D0_Kplus_PZ;   //!
   TBranch        *b_B0_Cons_D0_piplus_ID;   //!
   TBranch        *b_B0_Cons_D0_piplus_PE;   //!
   TBranch        *b_B0_Cons_D0_piplus_PX;   //!
   TBranch        *b_B0_Cons_D0_piplus_PY;   //!
   TBranch        *b_B0_Cons_D0_piplus_PZ;   //!
   TBranch        *b_B0_Cons_M;   //!
   TBranch        *b_B0_Cons_MERR;   //!
   TBranch        *b_B0_Cons_P;   //!
   TBranch        *b_B0_Cons_PERR;   //!
   TBranch        *b_B0_Cons_PV_key;   //!
   TBranch        *b_B0_Cons_chi2;   //!
   TBranch        *b_B0_Cons_ctau;   //!
   TBranch        *b_B0_Cons_ctauErr;   //!
   TBranch        *b_B0_Cons_decayLength;   //!
   TBranch        *b_B0_Cons_decayLengthErr;   //!
   TBranch        *b_B0_Cons_nDOF;   //!
   TBranch        *b_B0_Cons_nIter;   //!
   TBranch        *b_B0_Cons_rho_770_0_pplus_0_ID;   //!
   TBranch        *b_B0_Cons_rho_770_0_pplus_0_PE;   //!
   TBranch        *b_B0_Cons_rho_770_0_pplus_0_PX;   //!
   TBranch        *b_B0_Cons_rho_770_0_pplus_0_PY;   //!
   TBranch        *b_B0_Cons_rho_770_0_pplus_0_PZ;   //!
   TBranch        *b_B0_Cons_rho_770_0_pplus_ID;   //!
   TBranch        *b_B0_Cons_rho_770_0_pplus_PE;   //!
   TBranch        *b_B0_Cons_rho_770_0_pplus_PX;   //!
   TBranch        *b_B0_Cons_rho_770_0_pplus_PY;   //!
   TBranch        *b_B0_Cons_rho_770_0_pplus_PZ;   //!
   TBranch        *b_B0_Cons_status;   //!
   TBranch        *b_B0_L0Global_Dec;   //!
   TBranch        *b_B0_L0Global_TIS;   //!
   TBranch        *b_B0_L0Global_TOS;   //!
   TBranch        *b_B0_Hlt1Global_Dec;   //!
   TBranch        *b_B0_Hlt1Global_TIS;   //!
   TBranch        *b_B0_Hlt1Global_TOS;   //!
   TBranch        *b_B0_Hlt1Phys_Dec;   //!
   TBranch        *b_B0_Hlt1Phys_TIS;   //!
   TBranch        *b_B0_Hlt1Phys_TOS;   //!
   TBranch        *b_B0_Hlt2Global_Dec;   //!
   TBranch        *b_B0_Hlt2Global_TIS;   //!
   TBranch        *b_B0_Hlt2Global_TOS;   //!
   TBranch        *b_B0_Hlt2Phys_Dec;   //!
   TBranch        *b_B0_Hlt2Phys_TIS;   //!
   TBranch        *b_B0_Hlt2Phys_TOS;   //!
   TBranch        *b_B0_L0MuonDecision_Dec;   //!
   TBranch        *b_B0_L0MuonDecision_TIS;   //!
   TBranch        *b_B0_L0MuonDecision_TOS;   //!
   TBranch        *b_B0_L0MuonNoSPDDecision_Dec;   //!
   TBranch        *b_B0_L0MuonNoSPDDecision_TIS;   //!
   TBranch        *b_B0_L0MuonNoSPDDecision_TOS;   //!
   TBranch        *b_B0_L0MuonEWDecision_Dec;   //!
   TBranch        *b_B0_L0MuonEWDecision_TIS;   //!
   TBranch        *b_B0_L0MuonEWDecision_TOS;   //!
   TBranch        *b_B0_L0DiMuonDecision_Dec;   //!
   TBranch        *b_B0_L0DiMuonDecision_TIS;   //!
   TBranch        *b_B0_L0DiMuonDecision_TOS;   //!
   TBranch        *b_B0_L0HadronDecision_Dec;   //!
   TBranch        *b_B0_L0HadronDecision_TIS;   //!
   TBranch        *b_B0_L0HadronDecision_TOS;   //!
   TBranch        *b_B0_L0PhotonDecision_Dec;   //!
   TBranch        *b_B0_L0PhotonDecision_TIS;   //!
   TBranch        *b_B0_L0PhotonDecision_TOS;   //!
   TBranch        *b_B0_L0ElectronDecision_Dec;   //!
   TBranch        *b_B0_L0ElectronDecision_TIS;   //!
   TBranch        *b_B0_L0ElectronDecision_TOS;   //!
   TBranch        *b_B0_Hlt1TrackMVADecision_Dec;   //!
   TBranch        *b_B0_Hlt1TrackMVADecision_TIS;   //!
   TBranch        *b_B0_Hlt1TrackMVADecision_TOS;   //!
   TBranch        *b_B0_Hlt1TwoTrackMVADecision_Dec;   //!
   TBranch        *b_B0_Hlt1TwoTrackMVADecision_TIS;   //!
   TBranch        *b_B0_Hlt1TwoTrackMVADecision_TOS;   //!
   TBranch        *b_B0_Hlt1TrackMuonDecision_Dec;   //!
   TBranch        *b_B0_Hlt1TrackMuonDecision_TIS;   //!
   TBranch        *b_B0_Hlt1TrackMuonDecision_TOS;   //!
   TBranch        *b_B0_Hlt1TrackPhotonDecision_Dec;   //!
   TBranch        *b_B0_Hlt1TrackPhotonDecision_TIS;   //!
   TBranch        *b_B0_Hlt1TrackPhotonDecision_TOS;   //!
   TBranch        *b_B0_Hlt1TrackForwardPassThroughDecision_Dec;   //!
   TBranch        *b_B0_Hlt1TrackForwardPassThroughDecision_TIS;   //!
   TBranch        *b_B0_Hlt1TrackForwardPassThroughDecision_TOS;   //!
   TBranch        *b_B0_Hlt1TrackForwardPassThroughLooseDecision_Dec;   //!
   TBranch        *b_B0_Hlt1TrackForwardPassThroughLooseDecision_TIS;   //!
   TBranch        *b_B0_Hlt1TrackForwardPassThroughLooseDecision_TOS;   //!
   TBranch        *b_B0_Hlt1TrackAllL0Decision_Dec;   //!
   TBranch        *b_B0_Hlt1TrackAllL0Decision_TIS;   //!
   TBranch        *b_B0_Hlt1TrackAllL0Decision_TOS;   //!
   TBranch        *b_B0_Hlt1GlobalDecision_Dec;   //!
   TBranch        *b_B0_Hlt1GlobalDecision_TIS;   //!
   TBranch        *b_B0_Hlt1GlobalDecision_TOS;   //!
   TBranch        *b_B0_Hlt2Topo2BodyDecision_Dec;   //!
   TBranch        *b_B0_Hlt2Topo2BodyDecision_TIS;   //!
   TBranch        *b_B0_Hlt2Topo2BodyDecision_TOS;   //!
   TBranch        *b_B0_Hlt2Topo3BodyDecision_Dec;   //!
   TBranch        *b_B0_Hlt2Topo3BodyDecision_TIS;   //!
   TBranch        *b_B0_Hlt2Topo3BodyDecision_TOS;   //!
   TBranch        *b_B0_Hlt2Topo4BodyDecision_Dec;   //!
   TBranch        *b_B0_Hlt2Topo4BodyDecision_TIS;   //!
   TBranch        *b_B0_Hlt2Topo4BodyDecision_TOS;   //!
   TBranch        *b_B0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_Dec;   //!
   TBranch        *b_B0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_TIS;   //!
   TBranch        *b_B0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_TOS;   //!
   TBranch        *b_B0_Hlt2TopoMu3BodyDecision_Dec;   //!
   TBranch        *b_B0_Hlt2TopoMu3BodyDecision_TIS;   //!
   TBranch        *b_B0_Hlt2TopoMu3BodyDecision_TOS;   //!
   TBranch        *b_B0_Hlt2TopoMu4BodyDecision_Dec;   //!
   TBranch        *b_B0_Hlt2TopoMu4BodyDecision_TIS;   //!
   TBranch        *b_B0_Hlt2TopoMu4BodyDecision_TOS;   //!
   TBranch        *b_B0_Hlt2TopoE2BodyDecision_Dec;   //!
   TBranch        *b_B0_Hlt2TopoE2BodyDecision_TIS;   //!
   TBranch        *b_B0_Hlt2TopoE2BodyDecision_TOS;   //!
   TBranch        *b_B0_Hlt2TopoE3BodyDecision_Dec;   //!
   TBranch        *b_B0_Hlt2TopoE3BodyDecision_TIS;   //!
   TBranch        *b_B0_Hlt2TopoE3BodyDecision_TOS;   //!
   TBranch        *b_B0_Hlt2TopoE4BodyDecision_Dec;   //!
   TBranch        *b_B0_Hlt2TopoE4BodyDecision_TIS;   //!
   TBranch        *b_B0_Hlt2TopoE4BodyDecision_TOS;   //!
   TBranch        *b_B0_Hlt2TopoRad2BodyDecision_Dec;   //!
   TBranch        *b_B0_Hlt2TopoRad2BodyDecision_TIS;   //!
   TBranch        *b_B0_Hlt2TopoRad2BodyDecision_TOS;   //!
   TBranch        *b_B0_Hlt2SingleMuonDecision_Dec;   //!
   TBranch        *b_B0_Hlt2SingleMuonDecision_TIS;   //!
   TBranch        *b_B0_Hlt2SingleMuonDecision_TOS;   //!
   TBranch        *b_B0_Hlt2RadiativeTopoTrackDecision_Dec;   //!
   TBranch        *b_B0_Hlt2RadiativeTopoTrackDecision_TIS;   //!
   TBranch        *b_B0_Hlt2RadiativeTopoTrackDecision_TOS;   //!
   TBranch        *b_B0_Hlt2RadiativeTopoPhotonDecision_Dec;   //!
   TBranch        *b_B0_Hlt2RadiativeTopoPhotonDecision_TIS;   //!
   TBranch        *b_B0_Hlt2RadiativeTopoPhotonDecision_TOS;   //!
   TBranch        *b_B0_Hlt2Topo2BodySimpleDecision_Dec;   //!
   TBranch        *b_B0_Hlt2Topo2BodySimpleDecision_TIS;   //!
   TBranch        *b_B0_Hlt2Topo2BodySimpleDecision_TOS;   //!
   TBranch        *b_B0_Hlt2Topo3BodySimpleDecision_Dec;   //!
   TBranch        *b_B0_Hlt2Topo3BodySimpleDecision_TIS;   //!
   TBranch        *b_B0_Hlt2Topo3BodySimpleDecision_TOS;   //!
   TBranch        *b_B0_Hlt2Topo4BodySimpleDecision_Dec;   //!
   TBranch        *b_B0_Hlt2Topo4BodySimpleDecision_TIS;   //!
   TBranch        *b_B0_Hlt2Topo4BodySimpleDecision_TOS;   //!
   TBranch        *b_B0_Hlt2Topo2BodyBBDTDecision_Dec;   //!
   TBranch        *b_B0_Hlt2Topo2BodyBBDTDecision_TIS;   //!
   TBranch        *b_B0_Hlt2Topo2BodyBBDTDecision_TOS;   //!
   TBranch        *b_B0_Hlt2Topo3BodyBBDTDecision_Dec;   //!
   TBranch        *b_B0_Hlt2Topo3BodyBBDTDecision_TIS;   //!
   TBranch        *b_B0_Hlt2Topo3BodyBBDTDecision_TOS;   //!
   TBranch        *b_B0_Hlt2Topo4BodyBBDTDecision_Dec;   //!
   TBranch        *b_B0_Hlt2Topo4BodyBBDTDecision_TIS;   //!
   TBranch        *b_B0_Hlt2Topo4BodyBBDTDecision_TOS;   //!
   TBranch        *b_B0_Hlt2TopoE2BodyBBDTDecision_Dec;   //!
   TBranch        *b_B0_Hlt2TopoE2BodyBBDTDecision_TIS;   //!
   TBranch        *b_B0_Hlt2TopoE2BodyBBDTDecision_TOS;   //!
   TBranch        *b_B0_Hlt2TopoE3BodyBBDTDecision_Dec;   //!
   TBranch        *b_B0_Hlt2TopoE3BodyBBDTDecision_TIS;   //!
   TBranch        *b_B0_Hlt2TopoE3BodyBBDTDecision_TOS;   //!
   TBranch        *b_B0_Hlt2TopoE4BodyBBDTDecision_Dec;   //!
   TBranch        *b_B0_Hlt2TopoE4BodyBBDTDecision_TIS;   //!
   TBranch        *b_B0_Hlt2TopoE4BodyBBDTDecision_TOS;   //!
   TBranch        *b_B0_Hlt2TopoMu2BodyBBDTDecision_Dec;   //!
   TBranch        *b_B0_Hlt2TopoMu2BodyBBDTDecision_TIS;   //!
   TBranch        *b_B0_Hlt2TopoMu2BodyBBDTDecision_TOS;   //!
   TBranch        *b_B0_Hlt2TopoMu3BodyBBDTDecision_Dec;   //!
   TBranch        *b_B0_Hlt2TopoMu3BodyBBDTDecision_TIS;   //!
   TBranch        *b_B0_Hlt2TopoMu3BodyBBDTDecision_TOS;   //!
   TBranch        *b_B0_Hlt2TopoMu4BodyBBDTDecision_Dec;   //!
   TBranch        *b_B0_Hlt2TopoMu4BodyBBDTDecision_TIS;   //!
   TBranch        *b_B0_Hlt2TopoMu4BodyBBDTDecision_TOS;   //!
   TBranch        *b_B0_Hlt2GlobalDecision_Dec;   //!
   TBranch        *b_B0_Hlt2GlobalDecision_TIS;   //!
   TBranch        *b_B0_Hlt2GlobalDecision_TOS;   //!
   TBranch        *b_B0_Hlt2TopoRad2BodyBBDTDecision_Dec;   //!
   TBranch        *b_B0_Hlt2TopoRad2BodyBBDTDecision_TIS;   //!
   TBranch        *b_B0_Hlt2TopoRad2BodyBBDTDecision_TOS;   //!
   TBranch        *b_D0_CosTheta;   //!
   TBranch        *b_D0_ENDVERTEX_X;   //!
   TBranch        *b_D0_ENDVERTEX_Y;   //!
   TBranch        *b_D0_ENDVERTEX_Z;   //!
   TBranch        *b_D0_ENDVERTEX_XERR;   //!
   TBranch        *b_D0_ENDVERTEX_YERR;   //!
   TBranch        *b_D0_ENDVERTEX_ZERR;   //!
   TBranch        *b_D0_ENDVERTEX_CHI2;   //!
   TBranch        *b_D0_ENDVERTEX_NDOF;   //!
   TBranch        *b_D0_ENDVERTEX_COV_;   //!
   TBranch        *b_D0_OWNPV_X;   //!
   TBranch        *b_D0_OWNPV_Y;   //!
   TBranch        *b_D0_OWNPV_Z;   //!
   TBranch        *b_D0_OWNPV_XERR;   //!
   TBranch        *b_D0_OWNPV_YERR;   //!
   TBranch        *b_D0_OWNPV_ZERR;   //!
   TBranch        *b_D0_OWNPV_CHI2;   //!
   TBranch        *b_D0_OWNPV_NDOF;   //!
   TBranch        *b_D0_OWNPV_COV_;   //!
   TBranch        *b_D0_IP_OWNPV;   //!
   TBranch        *b_D0_IPCHI2_OWNPV;   //!
   TBranch        *b_D0_FD_OWNPV;   //!
   TBranch        *b_D0_FDCHI2_OWNPV;   //!
   TBranch        *b_D0_DIRA_OWNPV;   //!
   TBranch        *b_D0_ORIVX_X;   //!
   TBranch        *b_D0_ORIVX_Y;   //!
   TBranch        *b_D0_ORIVX_Z;   //!
   TBranch        *b_D0_ORIVX_XERR;   //!
   TBranch        *b_D0_ORIVX_YERR;   //!
   TBranch        *b_D0_ORIVX_ZERR;   //!
   TBranch        *b_D0_ORIVX_CHI2;   //!
   TBranch        *b_D0_ORIVX_NDOF;   //!
   TBranch        *b_D0_ORIVX_COV_;   //!
   TBranch        *b_D0_FD_ORIVX;   //!
   TBranch        *b_D0_FDCHI2_ORIVX;   //!
   TBranch        *b_D0_DIRA_ORIVX;   //!
   TBranch        *b_D0_P;   //!
   TBranch        *b_D0_PT;   //!
   TBranch        *b_D0_PE;   //!
   TBranch        *b_D0_PX;   //!
   TBranch        *b_D0_PY;   //!
   TBranch        *b_D0_PZ;   //!
   TBranch        *b_D0_MM;   //!
   TBranch        *b_D0_MMERR;   //!
   TBranch        *b_D0_M;   //!
   TBranch        *b_D0_ID;   //!
   TBranch        *b_D0_TAU;   //!
   TBranch        *b_D0_TAUERR;   //!
   TBranch        *b_D0_TAUCHI2;   //!
   TBranch        *b_D0_NumVtxWithinChi2WindowOneTrack;   //!
   TBranch        *b_D0_SmallestDeltaChi2OneTrack;   //!
   TBranch        *b_D0_SmallestDeltaChi2MassOneTrack;   //!
   TBranch        *b_D0_SmallestDeltaChi2TwoTracks;   //!
   TBranch        *b_D0_SmallestDeltaChi2MassTwoTracks;   //!
   TBranch        *b_D0_ETA;   //!
   TBranch        *b_D0_LOKI_DIRA;   //!
   TBranch        *b_D0_LOKI_FDCHI2;   //!
   TBranch        *b_D0_LOKI_FDS;   //!
   TBranch        *b_D0_LOKI_LV01;   //!
   TBranch        *b_D0_LOKI_LV02;   //!
   TBranch        *b_D0_LOKI_MAXDOCA;   //!
   TBranch        *b_D0_LOKI_TAU;   //!
   TBranch        *b_D0_PHI;   //!
   TBranch        *b_D0_Y;   //!
   TBranch        *b_D0_L0Global_Dec;   //!
   TBranch        *b_D0_L0Global_TIS;   //!
   TBranch        *b_D0_L0Global_TOS;   //!
   TBranch        *b_D0_Hlt1Global_Dec;   //!
   TBranch        *b_D0_Hlt1Global_TIS;   //!
   TBranch        *b_D0_Hlt1Global_TOS;   //!
   TBranch        *b_D0_Hlt1Phys_Dec;   //!
   TBranch        *b_D0_Hlt1Phys_TIS;   //!
   TBranch        *b_D0_Hlt1Phys_TOS;   //!
   TBranch        *b_D0_Hlt2Global_Dec;   //!
   TBranch        *b_D0_Hlt2Global_TIS;   //!
   TBranch        *b_D0_Hlt2Global_TOS;   //!
   TBranch        *b_D0_Hlt2Phys_Dec;   //!
   TBranch        *b_D0_Hlt2Phys_TIS;   //!
   TBranch        *b_D0_Hlt2Phys_TOS;   //!
   TBranch        *b_D0_L0MuonDecision_Dec;   //!
   TBranch        *b_D0_L0MuonDecision_TIS;   //!
   TBranch        *b_D0_L0MuonDecision_TOS;   //!
   TBranch        *b_D0_L0MuonNoSPDDecision_Dec;   //!
   TBranch        *b_D0_L0MuonNoSPDDecision_TIS;   //!
   TBranch        *b_D0_L0MuonNoSPDDecision_TOS;   //!
   TBranch        *b_D0_L0MuonEWDecision_Dec;   //!
   TBranch        *b_D0_L0MuonEWDecision_TIS;   //!
   TBranch        *b_D0_L0MuonEWDecision_TOS;   //!
   TBranch        *b_D0_L0DiMuonDecision_Dec;   //!
   TBranch        *b_D0_L0DiMuonDecision_TIS;   //!
   TBranch        *b_D0_L0DiMuonDecision_TOS;   //!
   TBranch        *b_D0_L0HadronDecision_Dec;   //!
   TBranch        *b_D0_L0HadronDecision_TIS;   //!
   TBranch        *b_D0_L0HadronDecision_TOS;   //!
   TBranch        *b_D0_L0PhotonDecision_Dec;   //!
   TBranch        *b_D0_L0PhotonDecision_TIS;   //!
   TBranch        *b_D0_L0PhotonDecision_TOS;   //!
   TBranch        *b_D0_L0ElectronDecision_Dec;   //!
   TBranch        *b_D0_L0ElectronDecision_TIS;   //!
   TBranch        *b_D0_L0ElectronDecision_TOS;   //!
   TBranch        *b_D0_Hlt1TrackMVADecision_Dec;   //!
   TBranch        *b_D0_Hlt1TrackMVADecision_TIS;   //!
   TBranch        *b_D0_Hlt1TrackMVADecision_TOS;   //!
   TBranch        *b_D0_Hlt1TwoTrackMVADecision_Dec;   //!
   TBranch        *b_D0_Hlt1TwoTrackMVADecision_TIS;   //!
   TBranch        *b_D0_Hlt1TwoTrackMVADecision_TOS;   //!
   TBranch        *b_D0_Hlt1TrackMuonDecision_Dec;   //!
   TBranch        *b_D0_Hlt1TrackMuonDecision_TIS;   //!
   TBranch        *b_D0_Hlt1TrackMuonDecision_TOS;   //!
   TBranch        *b_D0_Hlt1TrackPhotonDecision_Dec;   //!
   TBranch        *b_D0_Hlt1TrackPhotonDecision_TIS;   //!
   TBranch        *b_D0_Hlt1TrackPhotonDecision_TOS;   //!
   TBranch        *b_D0_Hlt1TrackForwardPassThroughDecision_Dec;   //!
   TBranch        *b_D0_Hlt1TrackForwardPassThroughDecision_TIS;   //!
   TBranch        *b_D0_Hlt1TrackForwardPassThroughDecision_TOS;   //!
   TBranch        *b_D0_Hlt1TrackForwardPassThroughLooseDecision_Dec;   //!
   TBranch        *b_D0_Hlt1TrackForwardPassThroughLooseDecision_TIS;   //!
   TBranch        *b_D0_Hlt1TrackForwardPassThroughLooseDecision_TOS;   //!
   TBranch        *b_D0_Hlt1TrackAllL0Decision_Dec;   //!
   TBranch        *b_D0_Hlt1TrackAllL0Decision_TIS;   //!
   TBranch        *b_D0_Hlt1TrackAllL0Decision_TOS;   //!
   TBranch        *b_D0_Hlt1GlobalDecision_Dec;   //!
   TBranch        *b_D0_Hlt1GlobalDecision_TIS;   //!
   TBranch        *b_D0_Hlt1GlobalDecision_TOS;   //!
   TBranch        *b_D0_Hlt2Topo2BodyDecision_Dec;   //!
   TBranch        *b_D0_Hlt2Topo2BodyDecision_TIS;   //!
   TBranch        *b_D0_Hlt2Topo2BodyDecision_TOS;   //!
   TBranch        *b_D0_Hlt2Topo3BodyDecision_Dec;   //!
   TBranch        *b_D0_Hlt2Topo3BodyDecision_TIS;   //!
   TBranch        *b_D0_Hlt2Topo3BodyDecision_TOS;   //!
   TBranch        *b_D0_Hlt2Topo4BodyDecision_Dec;   //!
   TBranch        *b_D0_Hlt2Topo4BodyDecision_TIS;   //!
   TBranch        *b_D0_Hlt2Topo4BodyDecision_TOS;   //!
   TBranch        *b_D0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_Dec;   //!
   TBranch        *b_D0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_TIS;   //!
   TBranch        *b_D0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_TOS;   //!
   TBranch        *b_D0_Hlt2TopoMu3BodyDecision_Dec;   //!
   TBranch        *b_D0_Hlt2TopoMu3BodyDecision_TIS;   //!
   TBranch        *b_D0_Hlt2TopoMu3BodyDecision_TOS;   //!
   TBranch        *b_D0_Hlt2TopoMu4BodyDecision_Dec;   //!
   TBranch        *b_D0_Hlt2TopoMu4BodyDecision_TIS;   //!
   TBranch        *b_D0_Hlt2TopoMu4BodyDecision_TOS;   //!
   TBranch        *b_D0_Hlt2TopoE2BodyDecision_Dec;   //!
   TBranch        *b_D0_Hlt2TopoE2BodyDecision_TIS;   //!
   TBranch        *b_D0_Hlt2TopoE2BodyDecision_TOS;   //!
   TBranch        *b_D0_Hlt2TopoE3BodyDecision_Dec;   //!
   TBranch        *b_D0_Hlt2TopoE3BodyDecision_TIS;   //!
   TBranch        *b_D0_Hlt2TopoE3BodyDecision_TOS;   //!
   TBranch        *b_D0_Hlt2TopoE4BodyDecision_Dec;   //!
   TBranch        *b_D0_Hlt2TopoE4BodyDecision_TIS;   //!
   TBranch        *b_D0_Hlt2TopoE4BodyDecision_TOS;   //!
   TBranch        *b_D0_Hlt2TopoRad2BodyDecision_Dec;   //!
   TBranch        *b_D0_Hlt2TopoRad2BodyDecision_TIS;   //!
   TBranch        *b_D0_Hlt2TopoRad2BodyDecision_TOS;   //!
   TBranch        *b_D0_Hlt2SingleMuonDecision_Dec;   //!
   TBranch        *b_D0_Hlt2SingleMuonDecision_TIS;   //!
   TBranch        *b_D0_Hlt2SingleMuonDecision_TOS;   //!
   TBranch        *b_D0_Hlt2RadiativeTopoTrackDecision_Dec;   //!
   TBranch        *b_D0_Hlt2RadiativeTopoTrackDecision_TIS;   //!
   TBranch        *b_D0_Hlt2RadiativeTopoTrackDecision_TOS;   //!
   TBranch        *b_D0_Hlt2RadiativeTopoPhotonDecision_Dec;   //!
   TBranch        *b_D0_Hlt2RadiativeTopoPhotonDecision_TIS;   //!
   TBranch        *b_D0_Hlt2RadiativeTopoPhotonDecision_TOS;   //!
   TBranch        *b_D0_Hlt2Topo2BodySimpleDecision_Dec;   //!
   TBranch        *b_D0_Hlt2Topo2BodySimpleDecision_TIS;   //!
   TBranch        *b_D0_Hlt2Topo2BodySimpleDecision_TOS;   //!
   TBranch        *b_D0_Hlt2Topo3BodySimpleDecision_Dec;   //!
   TBranch        *b_D0_Hlt2Topo3BodySimpleDecision_TIS;   //!
   TBranch        *b_D0_Hlt2Topo3BodySimpleDecision_TOS;   //!
   TBranch        *b_D0_Hlt2Topo4BodySimpleDecision_Dec;   //!
   TBranch        *b_D0_Hlt2Topo4BodySimpleDecision_TIS;   //!
   TBranch        *b_D0_Hlt2Topo4BodySimpleDecision_TOS;   //!
   TBranch        *b_D0_Hlt2Topo2BodyBBDTDecision_Dec;   //!
   TBranch        *b_D0_Hlt2Topo2BodyBBDTDecision_TIS;   //!
   TBranch        *b_D0_Hlt2Topo2BodyBBDTDecision_TOS;   //!
   TBranch        *b_D0_Hlt2Topo3BodyBBDTDecision_Dec;   //!
   TBranch        *b_D0_Hlt2Topo3BodyBBDTDecision_TIS;   //!
   TBranch        *b_D0_Hlt2Topo3BodyBBDTDecision_TOS;   //!
   TBranch        *b_D0_Hlt2Topo4BodyBBDTDecision_Dec;   //!
   TBranch        *b_D0_Hlt2Topo4BodyBBDTDecision_TIS;   //!
   TBranch        *b_D0_Hlt2Topo4BodyBBDTDecision_TOS;   //!
   TBranch        *b_D0_Hlt2TopoE2BodyBBDTDecision_Dec;   //!
   TBranch        *b_D0_Hlt2TopoE2BodyBBDTDecision_TIS;   //!
   TBranch        *b_D0_Hlt2TopoE2BodyBBDTDecision_TOS;   //!
   TBranch        *b_D0_Hlt2TopoE3BodyBBDTDecision_Dec;   //!
   TBranch        *b_D0_Hlt2TopoE3BodyBBDTDecision_TIS;   //!
   TBranch        *b_D0_Hlt2TopoE3BodyBBDTDecision_TOS;   //!
   TBranch        *b_D0_Hlt2TopoE4BodyBBDTDecision_Dec;   //!
   TBranch        *b_D0_Hlt2TopoE4BodyBBDTDecision_TIS;   //!
   TBranch        *b_D0_Hlt2TopoE4BodyBBDTDecision_TOS;   //!
   TBranch        *b_D0_Hlt2TopoMu2BodyBBDTDecision_Dec;   //!
   TBranch        *b_D0_Hlt2TopoMu2BodyBBDTDecision_TIS;   //!
   TBranch        *b_D0_Hlt2TopoMu2BodyBBDTDecision_TOS;   //!
   TBranch        *b_D0_Hlt2TopoMu3BodyBBDTDecision_Dec;   //!
   TBranch        *b_D0_Hlt2TopoMu3BodyBBDTDecision_TIS;   //!
   TBranch        *b_D0_Hlt2TopoMu3BodyBBDTDecision_TOS;   //!
   TBranch        *b_D0_Hlt2TopoMu4BodyBBDTDecision_Dec;   //!
   TBranch        *b_D0_Hlt2TopoMu4BodyBBDTDecision_TIS;   //!
   TBranch        *b_D0_Hlt2TopoMu4BodyBBDTDecision_TOS;   //!
   TBranch        *b_D0_Hlt2GlobalDecision_Dec;   //!
   TBranch        *b_D0_Hlt2GlobalDecision_TIS;   //!
   TBranch        *b_D0_Hlt2GlobalDecision_TOS;   //!
   TBranch        *b_D0_Hlt2TopoRad2BodyBBDTDecision_Dec;   //!
   TBranch        *b_D0_Hlt2TopoRad2BodyBBDTDecision_TIS;   //!
   TBranch        *b_D0_Hlt2TopoRad2BodyBBDTDecision_TOS;   //!
   TBranch        *b_pi_MC12TuneV2_ProbNNe;   //!
   TBranch        *b_pi_MC12TuneV2_ProbNNmu;   //!
   TBranch        *b_pi_MC12TuneV2_ProbNNpi;   //!
   TBranch        *b_pi_MC12TuneV2_ProbNNk;   //!
   TBranch        *b_pi_MC12TuneV2_ProbNNp;   //!
   TBranch        *b_pi_MC12TuneV2_ProbNNghost;   //!
   TBranch        *b_pi_MC12TuneV3_ProbNNe;   //!
   TBranch        *b_pi_MC12TuneV3_ProbNNmu;   //!
   TBranch        *b_pi_MC12TuneV3_ProbNNpi;   //!
   TBranch        *b_pi_MC12TuneV3_ProbNNk;   //!
   TBranch        *b_pi_MC12TuneV3_ProbNNp;   //!
   TBranch        *b_pi_MC12TuneV3_ProbNNghost;   //!
   TBranch        *b_pi_MC12TuneV4_ProbNNe;   //!
   TBranch        *b_pi_MC12TuneV4_ProbNNmu;   //!
   TBranch        *b_pi_MC12TuneV4_ProbNNpi;   //!
   TBranch        *b_pi_MC12TuneV4_ProbNNk;   //!
   TBranch        *b_pi_MC12TuneV4_ProbNNp;   //!
   TBranch        *b_pi_MC12TuneV4_ProbNNghost;   //!
   TBranch        *b_pi_MC15TuneV1_ProbNNe;   //!
   TBranch        *b_pi_MC15TuneV1_ProbNNmu;   //!
   TBranch        *b_pi_MC15TuneV1_ProbNNpi;   //!
   TBranch        *b_pi_MC15TuneV1_ProbNNk;   //!
   TBranch        *b_pi_MC15TuneV1_ProbNNp;   //!
   TBranch        *b_pi_MC15TuneV1_ProbNNghost;   //!
   TBranch        *b_pi_CosTheta;   //!
   TBranch        *b_pi_OWNPV_X;   //!
   TBranch        *b_pi_OWNPV_Y;   //!
   TBranch        *b_pi_OWNPV_Z;   //!
   TBranch        *b_pi_OWNPV_XERR;   //!
   TBranch        *b_pi_OWNPV_YERR;   //!
   TBranch        *b_pi_OWNPV_ZERR;   //!
   TBranch        *b_pi_OWNPV_CHI2;   //!
   TBranch        *b_pi_OWNPV_NDOF;   //!
   TBranch        *b_pi_OWNPV_COV_;   //!
   TBranch        *b_pi_IP_OWNPV;   //!
   TBranch        *b_pi_IPCHI2_OWNPV;   //!
   TBranch        *b_pi_ORIVX_X;   //!
   TBranch        *b_pi_ORIVX_Y;   //!
   TBranch        *b_pi_ORIVX_Z;   //!
   TBranch        *b_pi_ORIVX_XERR;   //!
   TBranch        *b_pi_ORIVX_YERR;   //!
   TBranch        *b_pi_ORIVX_ZERR;   //!
   TBranch        *b_pi_ORIVX_CHI2;   //!
   TBranch        *b_pi_ORIVX_NDOF;   //!
   TBranch        *b_pi_ORIVX_COV_;   //!
   TBranch        *b_pi_P;   //!
   TBranch        *b_pi_PT;   //!
   TBranch        *b_pi_PE;   //!
   TBranch        *b_pi_PX;   //!
   TBranch        *b_pi_PY;   //!
   TBranch        *b_pi_PZ;   //!
   TBranch        *b_pi_M;   //!
   TBranch        *b_pi_ID;   //!
   TBranch        *b_pi_PIDe;   //!
   TBranch        *b_pi_PIDmu;   //!
   TBranch        *b_pi_PIDK;   //!
   TBranch        *b_pi_PIDp;   //!
   TBranch        *b_pi_PIDd;   //!
   TBranch        *b_pi_ProbNNe;   //!
   TBranch        *b_pi_ProbNNk;   //!
   TBranch        *b_pi_ProbNNp;   //!
   TBranch        *b_pi_ProbNNpi;   //!
   TBranch        *b_pi_ProbNNmu;   //!
   TBranch        *b_pi_ProbNNd;   //!
   TBranch        *b_pi_ProbNNghost;   //!
   TBranch        *b_pi_hasMuon;   //!
   TBranch        *b_pi_isMuon;   //!
   TBranch        *b_pi_hasRich;   //!
   TBranch        *b_pi_UsedRichAerogel;   //!
   TBranch        *b_pi_UsedRich1Gas;   //!
   TBranch        *b_pi_UsedRich2Gas;   //!
   TBranch        *b_pi_RichAboveElThres;   //!
   TBranch        *b_pi_RichAboveMuThres;   //!
   TBranch        *b_pi_RichAbovePiThres;   //!
   TBranch        *b_pi_RichAboveKaThres;   //!
   TBranch        *b_pi_RichAbovePrThres;   //!
   TBranch        *b_pi_hasCalo;   //!
   TBranch        *b_pi_L0Global_Dec;   //!
   TBranch        *b_pi_L0Global_TIS;   //!
   TBranch        *b_pi_L0Global_TOS;   //!
   TBranch        *b_pi_Hlt1Global_Dec;   //!
   TBranch        *b_pi_Hlt1Global_TIS;   //!
   TBranch        *b_pi_Hlt1Global_TOS;   //!
   TBranch        *b_pi_Hlt1Phys_Dec;   //!
   TBranch        *b_pi_Hlt1Phys_TIS;   //!
   TBranch        *b_pi_Hlt1Phys_TOS;   //!
   TBranch        *b_pi_Hlt2Global_Dec;   //!
   TBranch        *b_pi_Hlt2Global_TIS;   //!
   TBranch        *b_pi_Hlt2Global_TOS;   //!
   TBranch        *b_pi_Hlt2Phys_Dec;   //!
   TBranch        *b_pi_Hlt2Phys_TIS;   //!
   TBranch        *b_pi_Hlt2Phys_TOS;   //!
   TBranch        *b_pi_TRACK_Type;   //!
   TBranch        *b_pi_TRACK_Key;   //!
   TBranch        *b_pi_TRACK_CHI2NDOF;   //!
   TBranch        *b_pi_TRACK_PCHI2;   //!
   TBranch        *b_pi_TRACK_MatchCHI2;   //!
   TBranch        *b_pi_TRACK_GhostProb;   //!
   TBranch        *b_pi_TRACK_CloneDist;   //!
   TBranch        *b_pi_TRACK_Likelihood;   //!
   TBranch        *b_pi_ETA;   //!
   TBranch        *b_pi_KL;   //!
   TBranch        *b_pi_PHI;   //!
   TBranch        *b_pi_Y;   //!
   TBranch        *b_K_MC12TuneV2_ProbNNe;   //!
   TBranch        *b_K_MC12TuneV2_ProbNNmu;   //!
   TBranch        *b_K_MC12TuneV2_ProbNNpi;   //!
   TBranch        *b_K_MC12TuneV2_ProbNNk;   //!
   TBranch        *b_K_MC12TuneV2_ProbNNp;   //!
   TBranch        *b_K_MC12TuneV2_ProbNNghost;   //!
   TBranch        *b_K_MC12TuneV3_ProbNNe;   //!
   TBranch        *b_K_MC12TuneV3_ProbNNmu;   //!
   TBranch        *b_K_MC12TuneV3_ProbNNpi;   //!
   TBranch        *b_K_MC12TuneV3_ProbNNk;   //!
   TBranch        *b_K_MC12TuneV3_ProbNNp;   //!
   TBranch        *b_K_MC12TuneV3_ProbNNghost;   //!
   TBranch        *b_K_MC12TuneV4_ProbNNe;   //!
   TBranch        *b_K_MC12TuneV4_ProbNNmu;   //!
   TBranch        *b_K_MC12TuneV4_ProbNNpi;   //!
   TBranch        *b_K_MC12TuneV4_ProbNNk;   //!
   TBranch        *b_K_MC12TuneV4_ProbNNp;   //!
   TBranch        *b_K_MC12TuneV4_ProbNNghost;   //!
   TBranch        *b_K_MC15TuneV1_ProbNNe;   //!
   TBranch        *b_K_MC15TuneV1_ProbNNmu;   //!
   TBranch        *b_K_MC15TuneV1_ProbNNpi;   //!
   TBranch        *b_K_MC15TuneV1_ProbNNk;   //!
   TBranch        *b_K_MC15TuneV1_ProbNNp;   //!
   TBranch        *b_K_MC15TuneV1_ProbNNghost;   //!
   TBranch        *b_K_CosTheta;   //!
   TBranch        *b_K_OWNPV_X;   //!
   TBranch        *b_K_OWNPV_Y;   //!
   TBranch        *b_K_OWNPV_Z;   //!
   TBranch        *b_K_OWNPV_XERR;   //!
   TBranch        *b_K_OWNPV_YERR;   //!
   TBranch        *b_K_OWNPV_ZERR;   //!
   TBranch        *b_K_OWNPV_CHI2;   //!
   TBranch        *b_K_OWNPV_NDOF;   //!
   TBranch        *b_K_OWNPV_COV_;   //!
   TBranch        *b_K_IP_OWNPV;   //!
   TBranch        *b_K_IPCHI2_OWNPV;   //!
   TBranch        *b_K_ORIVX_X;   //!
   TBranch        *b_K_ORIVX_Y;   //!
   TBranch        *b_K_ORIVX_Z;   //!
   TBranch        *b_K_ORIVX_XERR;   //!
   TBranch        *b_K_ORIVX_YERR;   //!
   TBranch        *b_K_ORIVX_ZERR;   //!
   TBranch        *b_K_ORIVX_CHI2;   //!
   TBranch        *b_K_ORIVX_NDOF;   //!
   TBranch        *b_K_ORIVX_COV_;   //!
   TBranch        *b_K_P;   //!
   TBranch        *b_K_PT;   //!
   TBranch        *b_K_PE;   //!
   TBranch        *b_K_PX;   //!
   TBranch        *b_K_PY;   //!
   TBranch        *b_K_PZ;   //!
   TBranch        *b_K_M;   //!
   TBranch        *b_K_ID;   //!
   TBranch        *b_K_PIDe;   //!
   TBranch        *b_K_PIDmu;   //!
   TBranch        *b_K_PIDK;   //!
   TBranch        *b_K_PIDp;   //!
   TBranch        *b_K_PIDd;   //!
   TBranch        *b_K_ProbNNe;   //!
   TBranch        *b_K_ProbNNk;   //!
   TBranch        *b_K_ProbNNp;   //!
   TBranch        *b_K_ProbNNpi;   //!
   TBranch        *b_K_ProbNNmu;   //!
   TBranch        *b_K_ProbNNd;   //!
   TBranch        *b_K_ProbNNghost;   //!
   TBranch        *b_K_hasMuon;   //!
   TBranch        *b_K_isMuon;   //!
   TBranch        *b_K_hasRich;   //!
   TBranch        *b_K_UsedRichAerogel;   //!
   TBranch        *b_K_UsedRich1Gas;   //!
   TBranch        *b_K_UsedRich2Gas;   //!
   TBranch        *b_K_RichAboveElThres;   //!
   TBranch        *b_K_RichAboveMuThres;   //!
   TBranch        *b_K_RichAbovePiThres;   //!
   TBranch        *b_K_RichAboveKaThres;   //!
   TBranch        *b_K_RichAbovePrThres;   //!
   TBranch        *b_K_hasCalo;   //!
   TBranch        *b_K_L0Global_Dec;   //!
   TBranch        *b_K_L0Global_TIS;   //!
   TBranch        *b_K_L0Global_TOS;   //!
   TBranch        *b_K_Hlt1Global_Dec;   //!
   TBranch        *b_K_Hlt1Global_TIS;   //!
   TBranch        *b_K_Hlt1Global_TOS;   //!
   TBranch        *b_K_Hlt1Phys_Dec;   //!
   TBranch        *b_K_Hlt1Phys_TIS;   //!
   TBranch        *b_K_Hlt1Phys_TOS;   //!
   TBranch        *b_K_Hlt2Global_Dec;   //!
   TBranch        *b_K_Hlt2Global_TIS;   //!
   TBranch        *b_K_Hlt2Global_TOS;   //!
   TBranch        *b_K_Hlt2Phys_Dec;   //!
   TBranch        *b_K_Hlt2Phys_TIS;   //!
   TBranch        *b_K_Hlt2Phys_TOS;   //!
   TBranch        *b_K_TRACK_Type;   //!
   TBranch        *b_K_TRACK_Key;   //!
   TBranch        *b_K_TRACK_CHI2NDOF;   //!
   TBranch        *b_K_TRACK_PCHI2;   //!
   TBranch        *b_K_TRACK_MatchCHI2;   //!
   TBranch        *b_K_TRACK_GhostProb;   //!
   TBranch        *b_K_TRACK_CloneDist;   //!
   TBranch        *b_K_TRACK_Likelihood;   //!
   TBranch        *b_K_ETA;   //!
   TBranch        *b_K_KL;   //!
   TBranch        *b_K_PHI;   //!
   TBranch        *b_K_Y;   //!
   TBranch        *b_R_CosTheta;   //!
   TBranch        *b_R_ENDVERTEX_X;   //!
   TBranch        *b_R_ENDVERTEX_Y;   //!
   TBranch        *b_R_ENDVERTEX_Z;   //!
   TBranch        *b_R_ENDVERTEX_XERR;   //!
   TBranch        *b_R_ENDVERTEX_YERR;   //!
   TBranch        *b_R_ENDVERTEX_ZERR;   //!
   TBranch        *b_R_ENDVERTEX_CHI2;   //!
   TBranch        *b_R_ENDVERTEX_NDOF;   //!
   TBranch        *b_R_ENDVERTEX_COV_;   //!
   TBranch        *b_R_OWNPV_X;   //!
   TBranch        *b_R_OWNPV_Y;   //!
   TBranch        *b_R_OWNPV_Z;   //!
   TBranch        *b_R_OWNPV_XERR;   //!
   TBranch        *b_R_OWNPV_YERR;   //!
   TBranch        *b_R_OWNPV_ZERR;   //!
   TBranch        *b_R_OWNPV_CHI2;   //!
   TBranch        *b_R_OWNPV_NDOF;   //!
   TBranch        *b_R_OWNPV_COV_;   //!
   TBranch        *b_R_IP_OWNPV;   //!
   TBranch        *b_R_IPCHI2_OWNPV;   //!
   TBranch        *b_R_FD_OWNPV;   //!
   TBranch        *b_R_FDCHI2_OWNPV;   //!
   TBranch        *b_R_DIRA_OWNPV;   //!
   TBranch        *b_R_ORIVX_X;   //!
   TBranch        *b_R_ORIVX_Y;   //!
   TBranch        *b_R_ORIVX_Z;   //!
   TBranch        *b_R_ORIVX_XERR;   //!
   TBranch        *b_R_ORIVX_YERR;   //!
   TBranch        *b_R_ORIVX_ZERR;   //!
   TBranch        *b_R_ORIVX_CHI2;   //!
   TBranch        *b_R_ORIVX_NDOF;   //!
   TBranch        *b_R_ORIVX_COV_;   //!
   TBranch        *b_R_FD_ORIVX;   //!
   TBranch        *b_R_FDCHI2_ORIVX;   //!
   TBranch        *b_R_DIRA_ORIVX;   //!
   TBranch        *b_R_P;   //!
   TBranch        *b_R_PT;   //!
   TBranch        *b_R_PE;   //!
   TBranch        *b_R_PX;   //!
   TBranch        *b_R_PY;   //!
   TBranch        *b_R_PZ;   //!
   TBranch        *b_R_MM;   //!
   TBranch        *b_R_MMERR;   //!
   TBranch        *b_R_M;   //!
   TBranch        *b_R_ID;   //!
   TBranch        *b_R_TAU;   //!
   TBranch        *b_R_TAUERR;   //!
   TBranch        *b_R_TAUCHI2;   //!
   TBranch        *b_R_L0Global_Dec;   //!
   TBranch        *b_R_L0Global_TIS;   //!
   TBranch        *b_R_L0Global_TOS;   //!
   TBranch        *b_R_Hlt1Global_Dec;   //!
   TBranch        *b_R_Hlt1Global_TIS;   //!
   TBranch        *b_R_Hlt1Global_TOS;   //!
   TBranch        *b_R_Hlt1Phys_Dec;   //!
   TBranch        *b_R_Hlt1Phys_TIS;   //!
   TBranch        *b_R_Hlt1Phys_TOS;   //!
   TBranch        *b_R_Hlt2Global_Dec;   //!
   TBranch        *b_R_Hlt2Global_TIS;   //!
   TBranch        *b_R_Hlt2Global_TOS;   //!
   TBranch        *b_R_Hlt2Phys_Dec;   //!
   TBranch        *b_R_Hlt2Phys_TIS;   //!
   TBranch        *b_R_Hlt2Phys_TOS;   //!
   TBranch        *b_R_NumVtxWithinChi2WindowOneTrack;   //!
   TBranch        *b_R_SmallestDeltaChi2OneTrack;   //!
   TBranch        *b_R_SmallestDeltaChi2MassOneTrack;   //!
   TBranch        *b_R_SmallestDeltaChi2TwoTracks;   //!
   TBranch        *b_R_SmallestDeltaChi2MassTwoTracks;   //!
   TBranch        *b_R_ETA;   //!
   TBranch        *b_R_LOKI_DIRA;   //!
   TBranch        *b_R_LOKI_FDCHI2;   //!
   TBranch        *b_R_LOKI_FDS;   //!
   TBranch        *b_R_LOKI_LV01;   //!
   TBranch        *b_R_LOKI_LV02;   //!
   TBranch        *b_R_LOKI_MAXDOCA;   //!
   TBranch        *b_R_LOKI_TAU;   //!
   TBranch        *b_R_PHI;   //!
   TBranch        *b_R_Y;   //!
   TBranch        *b_pp_MC12TuneV2_ProbNNe;   //!
   TBranch        *b_pp_MC12TuneV2_ProbNNmu;   //!
   TBranch        *b_pp_MC12TuneV2_ProbNNpi;   //!
   TBranch        *b_pp_MC12TuneV2_ProbNNk;   //!
   TBranch        *b_pp_MC12TuneV2_ProbNNp;   //!
   TBranch        *b_pp_MC12TuneV2_ProbNNghost;   //!
   TBranch        *b_pp_MC12TuneV3_ProbNNe;   //!
   TBranch        *b_pp_MC12TuneV3_ProbNNmu;   //!
   TBranch        *b_pp_MC12TuneV3_ProbNNpi;   //!
   TBranch        *b_pp_MC12TuneV3_ProbNNk;   //!
   TBranch        *b_pp_MC12TuneV3_ProbNNp;   //!
   TBranch        *b_pp_MC12TuneV3_ProbNNghost;   //!
   TBranch        *b_pp_MC12TuneV4_ProbNNe;   //!
   TBranch        *b_pp_MC12TuneV4_ProbNNmu;   //!
   TBranch        *b_pp_MC12TuneV4_ProbNNpi;   //!
   TBranch        *b_pp_MC12TuneV4_ProbNNk;   //!
   TBranch        *b_pp_MC12TuneV4_ProbNNp;   //!
   TBranch        *b_pp_MC12TuneV4_ProbNNghost;   //!
   TBranch        *b_pp_MC15TuneV1_ProbNNe;   //!
   TBranch        *b_pp_MC15TuneV1_ProbNNmu;   //!
   TBranch        *b_pp_MC15TuneV1_ProbNNpi;   //!
   TBranch        *b_pp_MC15TuneV1_ProbNNk;   //!
   TBranch        *b_pp_MC15TuneV1_ProbNNp;   //!
   TBranch        *b_pp_MC15TuneV1_ProbNNghost;   //!
   TBranch        *b_pp_CosTheta;   //!
   TBranch        *b_pp_OWNPV_X;   //!
   TBranch        *b_pp_OWNPV_Y;   //!
   TBranch        *b_pp_OWNPV_Z;   //!
   TBranch        *b_pp_OWNPV_XERR;   //!
   TBranch        *b_pp_OWNPV_YERR;   //!
   TBranch        *b_pp_OWNPV_ZERR;   //!
   TBranch        *b_pp_OWNPV_CHI2;   //!
   TBranch        *b_pp_OWNPV_NDOF;   //!
   TBranch        *b_pp_OWNPV_COV_;   //!
   TBranch        *b_pp_IP_OWNPV;   //!
   TBranch        *b_pp_IPCHI2_OWNPV;   //!
   TBranch        *b_pp_ORIVX_X;   //!
   TBranch        *b_pp_ORIVX_Y;   //!
   TBranch        *b_pp_ORIVX_Z;   //!
   TBranch        *b_pp_ORIVX_XERR;   //!
   TBranch        *b_pp_ORIVX_YERR;   //!
   TBranch        *b_pp_ORIVX_ZERR;   //!
   TBranch        *b_pp_ORIVX_CHI2;   //!
   TBranch        *b_pp_ORIVX_NDOF;   //!
   TBranch        *b_pp_ORIVX_COV_;   //!
   TBranch        *b_pp_P;   //!
   TBranch        *b_pp_PT;   //!
   TBranch        *b_pp_PE;   //!
   TBranch        *b_pp_PX;   //!
   TBranch        *b_pp_PY;   //!
   TBranch        *b_pp_PZ;   //!
   TBranch        *b_pp_M;   //!
   TBranch        *b_pp_ID;   //!
   TBranch        *b_pp_PIDe;   //!
   TBranch        *b_pp_PIDmu;   //!
   TBranch        *b_pp_PIDK;   //!
   TBranch        *b_pp_PIDp;   //!
   TBranch        *b_pp_PIDd;   //!
   TBranch        *b_pp_ProbNNe;   //!
   TBranch        *b_pp_ProbNNk;   //!
   TBranch        *b_pp_ProbNNp;   //!
   TBranch        *b_pp_ProbNNpi;   //!
   TBranch        *b_pp_ProbNNmu;   //!
   TBranch        *b_pp_ProbNNd;   //!
   TBranch        *b_pp_ProbNNghost;   //!
   TBranch        *b_pp_hasMuon;   //!
   TBranch        *b_pp_isMuon;   //!
   TBranch        *b_pp_hasRich;   //!
   TBranch        *b_pp_UsedRichAerogel;   //!
   TBranch        *b_pp_UsedRich1Gas;   //!
   TBranch        *b_pp_UsedRich2Gas;   //!
   TBranch        *b_pp_RichAboveElThres;   //!
   TBranch        *b_pp_RichAboveMuThres;   //!
   TBranch        *b_pp_RichAbovePiThres;   //!
   TBranch        *b_pp_RichAboveKaThres;   //!
   TBranch        *b_pp_RichAbovePrThres;   //!
   TBranch        *b_pp_hasCalo;   //!
   TBranch        *b_pp_L0Global_Dec;   //!
   TBranch        *b_pp_L0Global_TIS;   //!
   TBranch        *b_pp_L0Global_TOS;   //!
   TBranch        *b_pp_Hlt1Global_Dec;   //!
   TBranch        *b_pp_Hlt1Global_TIS;   //!
   TBranch        *b_pp_Hlt1Global_TOS;   //!
   TBranch        *b_pp_Hlt1Phys_Dec;   //!
   TBranch        *b_pp_Hlt1Phys_TIS;   //!
   TBranch        *b_pp_Hlt1Phys_TOS;   //!
   TBranch        *b_pp_Hlt2Global_Dec;   //!
   TBranch        *b_pp_Hlt2Global_TIS;   //!
   TBranch        *b_pp_Hlt2Global_TOS;   //!
   TBranch        *b_pp_Hlt2Phys_Dec;   //!
   TBranch        *b_pp_Hlt2Phys_TIS;   //!
   TBranch        *b_pp_Hlt2Phys_TOS;   //!
   TBranch        *b_pp_TRACK_Type;   //!
   TBranch        *b_pp_TRACK_Key;   //!
   TBranch        *b_pp_TRACK_CHI2NDOF;   //!
   TBranch        *b_pp_TRACK_PCHI2;   //!
   TBranch        *b_pp_TRACK_MatchCHI2;   //!
   TBranch        *b_pp_TRACK_GhostProb;   //!
   TBranch        *b_pp_TRACK_CloneDist;   //!
   TBranch        *b_pp_TRACK_Likelihood;   //!
   TBranch        *b_pp_ETA;   //!
   TBranch        *b_pp_KL;   //!
   TBranch        *b_pp_PHI;   //!
   TBranch        *b_pp_Y;   //!
   TBranch        *b_pm_MC12TuneV2_ProbNNe;   //!
   TBranch        *b_pm_MC12TuneV2_ProbNNmu;   //!
   TBranch        *b_pm_MC12TuneV2_ProbNNpi;   //!
   TBranch        *b_pm_MC12TuneV2_ProbNNk;   //!
   TBranch        *b_pm_MC12TuneV2_ProbNNp;   //!
   TBranch        *b_pm_MC12TuneV2_ProbNNghost;   //!
   TBranch        *b_pm_MC12TuneV3_ProbNNe;   //!
   TBranch        *b_pm_MC12TuneV3_ProbNNmu;   //!
   TBranch        *b_pm_MC12TuneV3_ProbNNpi;   //!
   TBranch        *b_pm_MC12TuneV3_ProbNNk;   //!
   TBranch        *b_pm_MC12TuneV3_ProbNNp;   //!
   TBranch        *b_pm_MC12TuneV3_ProbNNghost;   //!
   TBranch        *b_pm_MC12TuneV4_ProbNNe;   //!
   TBranch        *b_pm_MC12TuneV4_ProbNNmu;   //!
   TBranch        *b_pm_MC12TuneV4_ProbNNpi;   //!
   TBranch        *b_pm_MC12TuneV4_ProbNNk;   //!
   TBranch        *b_pm_MC12TuneV4_ProbNNp;   //!
   TBranch        *b_pm_MC12TuneV4_ProbNNghost;   //!
   TBranch        *b_pm_MC15TuneV1_ProbNNe;   //!
   TBranch        *b_pm_MC15TuneV1_ProbNNmu;   //!
   TBranch        *b_pm_MC15TuneV1_ProbNNpi;   //!
   TBranch        *b_pm_MC15TuneV1_ProbNNk;   //!
   TBranch        *b_pm_MC15TuneV1_ProbNNp;   //!
   TBranch        *b_pm_MC15TuneV1_ProbNNghost;   //!
   TBranch        *b_pm_CosTheta;   //!
   TBranch        *b_pm_OWNPV_X;   //!
   TBranch        *b_pm_OWNPV_Y;   //!
   TBranch        *b_pm_OWNPV_Z;   //!
   TBranch        *b_pm_OWNPV_XERR;   //!
   TBranch        *b_pm_OWNPV_YERR;   //!
   TBranch        *b_pm_OWNPV_ZERR;   //!
   TBranch        *b_pm_OWNPV_CHI2;   //!
   TBranch        *b_pm_OWNPV_NDOF;   //!
   TBranch        *b_pm_OWNPV_COV_;   //!
   TBranch        *b_pm_IP_OWNPV;   //!
   TBranch        *b_pm_IPCHI2_OWNPV;   //!
   TBranch        *b_pm_ORIVX_X;   //!
   TBranch        *b_pm_ORIVX_Y;   //!
   TBranch        *b_pm_ORIVX_Z;   //!
   TBranch        *b_pm_ORIVX_XERR;   //!
   TBranch        *b_pm_ORIVX_YERR;   //!
   TBranch        *b_pm_ORIVX_ZERR;   //!
   TBranch        *b_pm_ORIVX_CHI2;   //!
   TBranch        *b_pm_ORIVX_NDOF;   //!
   TBranch        *b_pm_ORIVX_COV_;   //!
   TBranch        *b_pm_P;   //!
   TBranch        *b_pm_PT;   //!
   TBranch        *b_pm_PE;   //!
   TBranch        *b_pm_PX;   //!
   TBranch        *b_pm_PY;   //!
   TBranch        *b_pm_PZ;   //!
   TBranch        *b_pm_M;   //!
   TBranch        *b_pm_ID;   //!
   TBranch        *b_pm_PIDe;   //!
   TBranch        *b_pm_PIDmu;   //!
   TBranch        *b_pm_PIDK;   //!
   TBranch        *b_pm_PIDp;   //!
   TBranch        *b_pm_PIDd;   //!
   TBranch        *b_pm_ProbNNe;   //!
   TBranch        *b_pm_ProbNNk;   //!
   TBranch        *b_pm_ProbNNp;   //!
   TBranch        *b_pm_ProbNNpi;   //!
   TBranch        *b_pm_ProbNNmu;   //!
   TBranch        *b_pm_ProbNNd;   //!
   TBranch        *b_pm_ProbNNghost;   //!
   TBranch        *b_pm_hasMuon;   //!
   TBranch        *b_pm_isMuon;   //!
   TBranch        *b_pm_hasRich;   //!
   TBranch        *b_pm_UsedRichAerogel;   //!
   TBranch        *b_pm_UsedRich1Gas;   //!
   TBranch        *b_pm_UsedRich2Gas;   //!
   TBranch        *b_pm_RichAboveElThres;   //!
   TBranch        *b_pm_RichAboveMuThres;   //!
   TBranch        *b_pm_RichAbovePiThres;   //!
   TBranch        *b_pm_RichAboveKaThres;   //!
   TBranch        *b_pm_RichAbovePrThres;   //!
   TBranch        *b_pm_hasCalo;   //!
   TBranch        *b_pm_L0Global_Dec;   //!
   TBranch        *b_pm_L0Global_TIS;   //!
   TBranch        *b_pm_L0Global_TOS;   //!
   TBranch        *b_pm_Hlt1Global_Dec;   //!
   TBranch        *b_pm_Hlt1Global_TIS;   //!
   TBranch        *b_pm_Hlt1Global_TOS;   //!
   TBranch        *b_pm_Hlt1Phys_Dec;   //!
   TBranch        *b_pm_Hlt1Phys_TIS;   //!
   TBranch        *b_pm_Hlt1Phys_TOS;   //!
   TBranch        *b_pm_Hlt2Global_Dec;   //!
   TBranch        *b_pm_Hlt2Global_TIS;   //!
   TBranch        *b_pm_Hlt2Global_TOS;   //!
   TBranch        *b_pm_Hlt2Phys_Dec;   //!
   TBranch        *b_pm_Hlt2Phys_TIS;   //!
   TBranch        *b_pm_Hlt2Phys_TOS;   //!
   TBranch        *b_pm_TRACK_Type;   //!
   TBranch        *b_pm_TRACK_Key;   //!
   TBranch        *b_pm_TRACK_CHI2NDOF;   //!
   TBranch        *b_pm_TRACK_PCHI2;   //!
   TBranch        *b_pm_TRACK_MatchCHI2;   //!
   TBranch        *b_pm_TRACK_GhostProb;   //!
   TBranch        *b_pm_TRACK_CloneDist;   //!
   TBranch        *b_pm_TRACK_Likelihood;   //!
   TBranch        *b_pm_ETA;   //!
   TBranch        *b_pm_KL;   //!
   TBranch        *b_pm_PHI;   //!
   TBranch        *b_pm_Y;   //!
   TBranch        *b_nCandidate;   //!
   TBranch        *b_totCandidates;   //!
   TBranch        *b_EventInSequence;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_BCID;   //!
   TBranch        *b_BCType;   //!
   TBranch        *b_OdinTCK;   //!
   TBranch        *b_L0DUTCK;   //!
   TBranch        *b_HLT1TCK;   //!
   TBranch        *b_HLT2TCK;   //!
   TBranch        *b_GpsTime;   //!
   TBranch        *b_Polarity;   //!
   TBranch        *b_nPV;   //!
   TBranch        *b_PVX;   //!
   TBranch        *b_PVY;   //!
   TBranch        *b_PVZ;   //!
   TBranch        *b_PVXERR;   //!
   TBranch        *b_PVYERR;   //!
   TBranch        *b_PVZERR;   //!
   TBranch        *b_PVCHI2;   //!
   TBranch        *b_PVNDOF;   //!
   TBranch        *b_PVNTRACKS;   //!
   TBranch        *b_nPVs;   //!
   TBranch        *b_nTracks;   //!
   TBranch        *b_nLongTracks;   //!
   TBranch        *b_nDownstreamTracks;   //!
   TBranch        *b_nUpstreamTracks;   //!
   TBranch        *b_nVeloTracks;   //!
   TBranch        *b_nTTracks;   //!
   TBranch        *b_nBackTracks;   //!
   TBranch        *b_nRich1Hits;   //!
   TBranch        *b_nRich2Hits;   //!
   TBranch        *b_nVeloClusters;   //!
   TBranch        *b_nITClusters;   //!
   TBranch        *b_nTTClusters;   //!
   TBranch        *b_nOTClusters;   //!
   TBranch        *b_nSPDHits;   //!
   TBranch        *b_nMuonCoordsS0;   //!
   TBranch        *b_nMuonCoordsS1;   //!
   TBranch        *b_nMuonCoordsS2;   //!
   TBranch        *b_nMuonCoordsS3;   //!
   TBranch        *b_nMuonCoordsS4;   //!
   TBranch        *b_nMuonTracks;   //!
   TBranch        *b_mass_pp;   //!
   TBranch        *b_mass_Dp;   //!
   TBranch        *b_mass_Dpbar;   //!
   TBranch        *b_B_mass_con;   //!
   TBranch        *b_log_B0_PT;   //!
   TBranch        *b_log_B0_P;   //!
   TBranch        *b_log_acos_B0_LOKI_DIRA;   //!
   TBranch        *b_log_abs_DTau;   //!
   TBranch        *b_log_D0_FD_OWNPV;   //!
   TBranch        *b_log_acos_D0_LOKI_DIRA;   //!
   TBranch        *b_min_D_daughter_pt;   //!
   TBranch        *b_min_p_pt;   //!
   TBranch        *b_max_ip_chi2;   //!
   TBranch        *b_BDZDis;   //!
   TBranch        *b_mvaBDT;   //!
   TBranch        *b_Sweight_sig;   //!
   TBranch        *b_Sweight_bkg;   //!
   TBranch        *b_mppbar;   //!
   TBranch        *b_mD0p;   //!
   TBranch        *b_mD0pbar;   //!
   TBranch        *b_cosTheta_X;   //!
   TBranch        *b_phi_X;   //!
   TBranch        *b_cosTheta_L;   //!
   TBranch        *b_phi_L;   //!
   TBranch        *b_cosTheta_p;   //!
   TBranch        *b_cosTheta_pbar;   //!
   TBranch        *b_AFB_cosTheta_p;   //!
   TBranch        *b_eff;   //!

   mytree(TTree *tree=0);
   virtual ~mytree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef mytree_cxx
mytree::mytree(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("dataAFB.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("dataAFB.root");
      }
      f->GetObject("mytree",tree);

   }
   Init(tree);
}

mytree::~mytree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t mytree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t mytree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void mytree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("B0_DiraAngleError", &B0_DiraAngleError, &b_B0_DiraAngleError);
   fChain->SetBranchAddress("B0_DiraCosError", &B0_DiraCosError, &b_B0_DiraCosError);
   fChain->SetBranchAddress("B0_DiraAngle", &B0_DiraAngle, &b_B0_DiraAngle);
   fChain->SetBranchAddress("B0_DiraCos", &B0_DiraCos, &b_B0_DiraCos);
   fChain->SetBranchAddress("B0_ENDVERTEX_X", &B0_ENDVERTEX_X, &b_B0_ENDVERTEX_X);
   fChain->SetBranchAddress("B0_ENDVERTEX_Y", &B0_ENDVERTEX_Y, &b_B0_ENDVERTEX_Y);
   fChain->SetBranchAddress("B0_ENDVERTEX_Z", &B0_ENDVERTEX_Z, &b_B0_ENDVERTEX_Z);
   fChain->SetBranchAddress("B0_ENDVERTEX_XERR", &B0_ENDVERTEX_XERR, &b_B0_ENDVERTEX_XERR);
   fChain->SetBranchAddress("B0_ENDVERTEX_YERR", &B0_ENDVERTEX_YERR, &b_B0_ENDVERTEX_YERR);
   fChain->SetBranchAddress("B0_ENDVERTEX_ZERR", &B0_ENDVERTEX_ZERR, &b_B0_ENDVERTEX_ZERR);
   fChain->SetBranchAddress("B0_ENDVERTEX_CHI2", &B0_ENDVERTEX_CHI2, &b_B0_ENDVERTEX_CHI2);
   fChain->SetBranchAddress("B0_ENDVERTEX_NDOF", &B0_ENDVERTEX_NDOF, &b_B0_ENDVERTEX_NDOF);
   fChain->SetBranchAddress("B0_ENDVERTEX_COV_", B0_ENDVERTEX_COV_, &b_B0_ENDVERTEX_COV_);
   fChain->SetBranchAddress("B0_OWNPV_X", &B0_OWNPV_X, &b_B0_OWNPV_X);
   fChain->SetBranchAddress("B0_OWNPV_Y", &B0_OWNPV_Y, &b_B0_OWNPV_Y);
   fChain->SetBranchAddress("B0_OWNPV_Z", &B0_OWNPV_Z, &b_B0_OWNPV_Z);
   fChain->SetBranchAddress("B0_OWNPV_XERR", &B0_OWNPV_XERR, &b_B0_OWNPV_XERR);
   fChain->SetBranchAddress("B0_OWNPV_YERR", &B0_OWNPV_YERR, &b_B0_OWNPV_YERR);
   fChain->SetBranchAddress("B0_OWNPV_ZERR", &B0_OWNPV_ZERR, &b_B0_OWNPV_ZERR);
   fChain->SetBranchAddress("B0_OWNPV_CHI2", &B0_OWNPV_CHI2, &b_B0_OWNPV_CHI2);
   fChain->SetBranchAddress("B0_OWNPV_NDOF", &B0_OWNPV_NDOF, &b_B0_OWNPV_NDOF);
   fChain->SetBranchAddress("B0_OWNPV_COV_", B0_OWNPV_COV_, &b_B0_OWNPV_COV_);
   fChain->SetBranchAddress("B0_IP_OWNPV", &B0_IP_OWNPV, &b_B0_IP_OWNPV);
   fChain->SetBranchAddress("B0_IPCHI2_OWNPV", &B0_IPCHI2_OWNPV, &b_B0_IPCHI2_OWNPV);
   fChain->SetBranchAddress("B0_FD_OWNPV", &B0_FD_OWNPV, &b_B0_FD_OWNPV);
   fChain->SetBranchAddress("B0_FDCHI2_OWNPV", &B0_FDCHI2_OWNPV, &b_B0_FDCHI2_OWNPV);
   fChain->SetBranchAddress("B0_DIRA_OWNPV", &B0_DIRA_OWNPV, &b_B0_DIRA_OWNPV);
   fChain->SetBranchAddress("B0_P", &B0_P, &b_B0_P);
   fChain->SetBranchAddress("B0_PT", &B0_PT, &b_B0_PT);
   fChain->SetBranchAddress("B0_PE", &B0_PE, &b_B0_PE);
   fChain->SetBranchAddress("B0_PX", &B0_PX, &b_B0_PX);
   fChain->SetBranchAddress("B0_PY", &B0_PY, &b_B0_PY);
   fChain->SetBranchAddress("B0_PZ", &B0_PZ, &b_B0_PZ);
   fChain->SetBranchAddress("B0_MM", &B0_MM, &b_B0_MM);
   fChain->SetBranchAddress("B0_MMERR", &B0_MMERR, &b_B0_MMERR);
   fChain->SetBranchAddress("B0_M", &B0_M, &b_B0_M);
   fChain->SetBranchAddress("B0_ID", &B0_ID, &b_B0_ID);
   fChain->SetBranchAddress("B0_TAU", &B0_TAU, &b_B0_TAU);
   fChain->SetBranchAddress("B0_TAUERR", &B0_TAUERR, &b_B0_TAUERR);
   fChain->SetBranchAddress("B0_TAUCHI2", &B0_TAUCHI2, &b_B0_TAUCHI2);
   fChain->SetBranchAddress("B0_NumVtxWithinChi2WindowOneTrack", &B0_NumVtxWithinChi2WindowOneTrack, &b_B0_NumVtxWithinChi2WindowOneTrack);
   fChain->SetBranchAddress("B0_SmallestDeltaChi2OneTrack", &B0_SmallestDeltaChi2OneTrack, &b_B0_SmallestDeltaChi2OneTrack);
   fChain->SetBranchAddress("B0_SmallestDeltaChi2MassOneTrack", &B0_SmallestDeltaChi2MassOneTrack, &b_B0_SmallestDeltaChi2MassOneTrack);
   fChain->SetBranchAddress("B0_SmallestDeltaChi2TwoTracks", &B0_SmallestDeltaChi2TwoTracks, &b_B0_SmallestDeltaChi2TwoTracks);
   fChain->SetBranchAddress("B0_SmallestDeltaChi2MassTwoTracks", &B0_SmallestDeltaChi2MassTwoTracks, &b_B0_SmallestDeltaChi2MassTwoTracks);
   fChain->SetBranchAddress("B0_BDDTF_CHI2NDOF", &B0_BDDTF_CHI2NDOF, &b_B0_BDDTF_CHI2NDOF);
   fChain->SetBranchAddress("B0_BDDTF_CTAU", &B0_BDDTF_CTAU, &b_B0_BDDTF_CTAU);
   fChain->SetBranchAddress("B0_BDDTF_CTAUS", &B0_BDDTF_CTAUS, &b_B0_BDDTF_CTAUS);
   fChain->SetBranchAddress("B0_BDDTF_DIRA", &B0_BDDTF_DIRA, &b_B0_BDDTF_DIRA);
   fChain->SetBranchAddress("B0_BDDTF_E", &B0_BDDTF_E, &b_B0_BDDTF_E);
   fChain->SetBranchAddress("B0_BDDTF_FDCHI2", &B0_BDDTF_FDCHI2, &b_B0_BDDTF_FDCHI2);
   fChain->SetBranchAddress("B0_BDDTF_FDS", &B0_BDDTF_FDS, &b_B0_BDDTF_FDS);
   fChain->SetBranchAddress("B0_BDDTF_IP", &B0_BDDTF_IP, &b_B0_BDDTF_IP);
   fChain->SetBranchAddress("B0_BDDTF_IPCHI2", &B0_BDDTF_IPCHI2, &b_B0_BDDTF_IPCHI2);
   fChain->SetBranchAddress("B0_BDDTF_M", &B0_BDDTF_M, &b_B0_BDDTF_M);
   fChain->SetBranchAddress("B0_BDDTF_P", &B0_BDDTF_P, &b_B0_BDDTF_P);
   fChain->SetBranchAddress("B0_BDDTF_PT", &B0_BDDTF_PT, &b_B0_BDDTF_PT);
   fChain->SetBranchAddress("B0_BDDTF_PX", &B0_BDDTF_PX, &b_B0_BDDTF_PX);
   fChain->SetBranchAddress("B0_BDDTF_PY", &B0_BDDTF_PY, &b_B0_BDDTF_PY);
   fChain->SetBranchAddress("B0_BDDTF_PZ", &B0_BDDTF_PZ, &b_B0_BDDTF_PZ);
   fChain->SetBranchAddress("B0_BDDTF_TAU", &B0_BDDTF_TAU, &b_B0_BDDTF_TAU);
   fChain->SetBranchAddress("B0_D0_BDDTF_CTAU", &B0_D0_BDDTF_CTAU, &b_B0_D0_BDDTF_CTAU);
   fChain->SetBranchAddress("B0_D0_BDDTF_CTAUS", &B0_D0_BDDTF_CTAUS, &b_B0_D0_BDDTF_CTAUS);
   fChain->SetBranchAddress("B0_D0_BDDTF_DIRA", &B0_D0_BDDTF_DIRA, &b_B0_D0_BDDTF_DIRA);
   fChain->SetBranchAddress("B0_D0_BDDTF_E", &B0_D0_BDDTF_E, &b_B0_D0_BDDTF_E);
   fChain->SetBranchAddress("B0_D0_BDDTF_FDCHI2", &B0_D0_BDDTF_FDCHI2, &b_B0_D0_BDDTF_FDCHI2);
   fChain->SetBranchAddress("B0_D0_BDDTF_FDS", &B0_D0_BDDTF_FDS, &b_B0_D0_BDDTF_FDS);
   fChain->SetBranchAddress("B0_D0_BDDTF_IP", &B0_D0_BDDTF_IP, &b_B0_D0_BDDTF_IP);
   fChain->SetBranchAddress("B0_D0_BDDTF_IPCHI2", &B0_D0_BDDTF_IPCHI2, &b_B0_D0_BDDTF_IPCHI2);
   fChain->SetBranchAddress("B0_D0_BDDTF_M", &B0_D0_BDDTF_M, &b_B0_D0_BDDTF_M);
   fChain->SetBranchAddress("B0_D0_BDDTF_P", &B0_D0_BDDTF_P, &b_B0_D0_BDDTF_P);
   fChain->SetBranchAddress("B0_D0_BDDTF_PT", &B0_D0_BDDTF_PT, &b_B0_D0_BDDTF_PT);
   fChain->SetBranchAddress("B0_D0_BDDTF_PX", &B0_D0_BDDTF_PX, &b_B0_D0_BDDTF_PX);
   fChain->SetBranchAddress("B0_D0_BDDTF_PY", &B0_D0_BDDTF_PY, &b_B0_D0_BDDTF_PY);
   fChain->SetBranchAddress("B0_D0_BDDTF_PZ", &B0_D0_BDDTF_PZ, &b_B0_D0_BDDTF_PZ);
   fChain->SetBranchAddress("B0_D0_BDDTF_TAU", &B0_D0_BDDTF_TAU, &b_B0_D0_BDDTF_TAU);
   fChain->SetBranchAddress("B0_pm_BDDTF_DIRA", &B0_pm_BDDTF_DIRA, &b_B0_pm_BDDTF_DIRA);
   fChain->SetBranchAddress("B0_pm_BDDTF_E", &B0_pm_BDDTF_E, &b_B0_pm_BDDTF_E);
   fChain->SetBranchAddress("B0_pm_BDDTF_FDCHI2", &B0_pm_BDDTF_FDCHI2, &b_B0_pm_BDDTF_FDCHI2);
   fChain->SetBranchAddress("B0_pm_BDDTF_FDS", &B0_pm_BDDTF_FDS, &b_B0_pm_BDDTF_FDS);
   fChain->SetBranchAddress("B0_pm_BDDTF_IP", &B0_pm_BDDTF_IP, &b_B0_pm_BDDTF_IP);
   fChain->SetBranchAddress("B0_pm_BDDTF_IPCHI2", &B0_pm_BDDTF_IPCHI2, &b_B0_pm_BDDTF_IPCHI2);
   fChain->SetBranchAddress("B0_pm_BDDTF_M", &B0_pm_BDDTF_M, &b_B0_pm_BDDTF_M);
   fChain->SetBranchAddress("B0_pm_BDDTF_P", &B0_pm_BDDTF_P, &b_B0_pm_BDDTF_P);
   fChain->SetBranchAddress("B0_pm_BDDTF_PT", &B0_pm_BDDTF_PT, &b_B0_pm_BDDTF_PT);
   fChain->SetBranchAddress("B0_pm_BDDTF_PX", &B0_pm_BDDTF_PX, &b_B0_pm_BDDTF_PX);
   fChain->SetBranchAddress("B0_pm_BDDTF_PY", &B0_pm_BDDTF_PY, &b_B0_pm_BDDTF_PY);
   fChain->SetBranchAddress("B0_pm_BDDTF_PZ", &B0_pm_BDDTF_PZ, &b_B0_pm_BDDTF_PZ);
   fChain->SetBranchAddress("B0_pm_BDDTF_TAU", &B0_pm_BDDTF_TAU, &b_B0_pm_BDDTF_TAU);
   fChain->SetBranchAddress("B0_pp_BDDTF_DIRA", &B0_pp_BDDTF_DIRA, &b_B0_pp_BDDTF_DIRA);
   fChain->SetBranchAddress("B0_pp_BDDTF_E", &B0_pp_BDDTF_E, &b_B0_pp_BDDTF_E);
   fChain->SetBranchAddress("B0_pp_BDDTF_FDCHI2", &B0_pp_BDDTF_FDCHI2, &b_B0_pp_BDDTF_FDCHI2);
   fChain->SetBranchAddress("B0_pp_BDDTF_FDS", &B0_pp_BDDTF_FDS, &b_B0_pp_BDDTF_FDS);
   fChain->SetBranchAddress("B0_pp_BDDTF_IP", &B0_pp_BDDTF_IP, &b_B0_pp_BDDTF_IP);
   fChain->SetBranchAddress("B0_pp_BDDTF_IPCHI2", &B0_pp_BDDTF_IPCHI2, &b_B0_pp_BDDTF_IPCHI2);
   fChain->SetBranchAddress("B0_pp_BDDTF_M", &B0_pp_BDDTF_M, &b_B0_pp_BDDTF_M);
   fChain->SetBranchAddress("B0_pp_BDDTF_P", &B0_pp_BDDTF_P, &b_B0_pp_BDDTF_P);
   fChain->SetBranchAddress("B0_pp_BDDTF_PT", &B0_pp_BDDTF_PT, &b_B0_pp_BDDTF_PT);
   fChain->SetBranchAddress("B0_pp_BDDTF_PX", &B0_pp_BDDTF_PX, &b_B0_pp_BDDTF_PX);
   fChain->SetBranchAddress("B0_pp_BDDTF_PY", &B0_pp_BDDTF_PY, &b_B0_pp_BDDTF_PY);
   fChain->SetBranchAddress("B0_pp_BDDTF_PZ", &B0_pp_BDDTF_PZ, &b_B0_pp_BDDTF_PZ);
   fChain->SetBranchAddress("B0_pp_BDDTF_TAU", &B0_pp_BDDTF_TAU, &b_B0_pp_BDDTF_TAU);
   fChain->SetBranchAddress("B0_BDTF_CHI2NDOF", &B0_BDTF_CHI2NDOF, &b_B0_BDTF_CHI2NDOF);
   fChain->SetBranchAddress("B0_BDTF_CTAU", &B0_BDTF_CTAU, &b_B0_BDTF_CTAU);
   fChain->SetBranchAddress("B0_BDTF_CTAUS", &B0_BDTF_CTAUS, &b_B0_BDTF_CTAUS);
   fChain->SetBranchAddress("B0_BDTF_DIRA", &B0_BDTF_DIRA, &b_B0_BDTF_DIRA);
   fChain->SetBranchAddress("B0_BDTF_E", &B0_BDTF_E, &b_B0_BDTF_E);
   fChain->SetBranchAddress("B0_BDTF_FDCHI2", &B0_BDTF_FDCHI2, &b_B0_BDTF_FDCHI2);
   fChain->SetBranchAddress("B0_BDTF_FDS", &B0_BDTF_FDS, &b_B0_BDTF_FDS);
   fChain->SetBranchAddress("B0_BDTF_IP", &B0_BDTF_IP, &b_B0_BDTF_IP);
   fChain->SetBranchAddress("B0_BDTF_IPCHI2", &B0_BDTF_IPCHI2, &b_B0_BDTF_IPCHI2);
   fChain->SetBranchAddress("B0_BDTF_M", &B0_BDTF_M, &b_B0_BDTF_M);
   fChain->SetBranchAddress("B0_BDTF_P", &B0_BDTF_P, &b_B0_BDTF_P);
   fChain->SetBranchAddress("B0_BDTF_PT", &B0_BDTF_PT, &b_B0_BDTF_PT);
   fChain->SetBranchAddress("B0_BDTF_PX", &B0_BDTF_PX, &b_B0_BDTF_PX);
   fChain->SetBranchAddress("B0_BDTF_PY", &B0_BDTF_PY, &b_B0_BDTF_PY);
   fChain->SetBranchAddress("B0_BDTF_PZ", &B0_BDTF_PZ, &b_B0_BDTF_PZ);
   fChain->SetBranchAddress("B0_BDTF_TAU", &B0_BDTF_TAU, &b_B0_BDTF_TAU);
   fChain->SetBranchAddress("B0_D0_BDTF_CTAU", &B0_D0_BDTF_CTAU, &b_B0_D0_BDTF_CTAU);
   fChain->SetBranchAddress("B0_D0_BDTF_CTAUS", &B0_D0_BDTF_CTAUS, &b_B0_D0_BDTF_CTAUS);
   fChain->SetBranchAddress("B0_D0_BDTF_DIRA", &B0_D0_BDTF_DIRA, &b_B0_D0_BDTF_DIRA);
   fChain->SetBranchAddress("B0_D0_BDTF_E", &B0_D0_BDTF_E, &b_B0_D0_BDTF_E);
   fChain->SetBranchAddress("B0_D0_BDTF_FDCHI2", &B0_D0_BDTF_FDCHI2, &b_B0_D0_BDTF_FDCHI2);
   fChain->SetBranchAddress("B0_D0_BDTF_FDS", &B0_D0_BDTF_FDS, &b_B0_D0_BDTF_FDS);
   fChain->SetBranchAddress("B0_D0_BDTF_IP", &B0_D0_BDTF_IP, &b_B0_D0_BDTF_IP);
   fChain->SetBranchAddress("B0_D0_BDTF_IPCHI2", &B0_D0_BDTF_IPCHI2, &b_B0_D0_BDTF_IPCHI2);
   fChain->SetBranchAddress("B0_D0_BDTF_M", &B0_D0_BDTF_M, &b_B0_D0_BDTF_M);
   fChain->SetBranchAddress("B0_D0_BDTF_P", &B0_D0_BDTF_P, &b_B0_D0_BDTF_P);
   fChain->SetBranchAddress("B0_D0_BDTF_PT", &B0_D0_BDTF_PT, &b_B0_D0_BDTF_PT);
   fChain->SetBranchAddress("B0_D0_BDTF_PX", &B0_D0_BDTF_PX, &b_B0_D0_BDTF_PX);
   fChain->SetBranchAddress("B0_D0_BDTF_PY", &B0_D0_BDTF_PY, &b_B0_D0_BDTF_PY);
   fChain->SetBranchAddress("B0_D0_BDTF_PZ", &B0_D0_BDTF_PZ, &b_B0_D0_BDTF_PZ);
   fChain->SetBranchAddress("B0_D0_BDTF_TAU", &B0_D0_BDTF_TAU, &b_B0_D0_BDTF_TAU);
   fChain->SetBranchAddress("B0_D0_DDTF_CTAU", &B0_D0_DDTF_CTAU, &b_B0_D0_DDTF_CTAU);
   fChain->SetBranchAddress("B0_D0_DDTF_CTAUS", &B0_D0_DDTF_CTAUS, &b_B0_D0_DDTF_CTAUS);
   fChain->SetBranchAddress("B0_D0_DDTF_DIRA", &B0_D0_DDTF_DIRA, &b_B0_D0_DDTF_DIRA);
   fChain->SetBranchAddress("B0_D0_DDTF_E", &B0_D0_DDTF_E, &b_B0_D0_DDTF_E);
   fChain->SetBranchAddress("B0_D0_DDTF_FDCHI2", &B0_D0_DDTF_FDCHI2, &b_B0_D0_DDTF_FDCHI2);
   fChain->SetBranchAddress("B0_D0_DDTF_FDS", &B0_D0_DDTF_FDS, &b_B0_D0_DDTF_FDS);
   fChain->SetBranchAddress("B0_D0_DDTF_IP", &B0_D0_DDTF_IP, &b_B0_D0_DDTF_IP);
   fChain->SetBranchAddress("B0_D0_DDTF_IPCHI2", &B0_D0_DDTF_IPCHI2, &b_B0_D0_DDTF_IPCHI2);
   fChain->SetBranchAddress("B0_D0_DDTF_M", &B0_D0_DDTF_M, &b_B0_D0_DDTF_M);
   fChain->SetBranchAddress("B0_D0_DDTF_P", &B0_D0_DDTF_P, &b_B0_D0_DDTF_P);
   fChain->SetBranchAddress("B0_D0_DDTF_PT", &B0_D0_DDTF_PT, &b_B0_D0_DDTF_PT);
   fChain->SetBranchAddress("B0_D0_DDTF_PX", &B0_D0_DDTF_PX, &b_B0_D0_DDTF_PX);
   fChain->SetBranchAddress("B0_D0_DDTF_PY", &B0_D0_DDTF_PY, &b_B0_D0_DDTF_PY);
   fChain->SetBranchAddress("B0_D0_DDTF_PZ", &B0_D0_DDTF_PZ, &b_B0_D0_DDTF_PZ);
   fChain->SetBranchAddress("B0_D0_DDTF_TAU", &B0_D0_DDTF_TAU, &b_B0_D0_DDTF_TAU);
   fChain->SetBranchAddress("B0_DDTF_CHI2NDOF", &B0_DDTF_CHI2NDOF, &b_B0_DDTF_CHI2NDOF);
   fChain->SetBranchAddress("B0_DDTF_CTAU", &B0_DDTF_CTAU, &b_B0_DDTF_CTAU);
   fChain->SetBranchAddress("B0_DDTF_CTAUS", &B0_DDTF_CTAUS, &b_B0_DDTF_CTAUS);
   fChain->SetBranchAddress("B0_DDTF_DIRA", &B0_DDTF_DIRA, &b_B0_DDTF_DIRA);
   fChain->SetBranchAddress("B0_DDTF_E", &B0_DDTF_E, &b_B0_DDTF_E);
   fChain->SetBranchAddress("B0_DDTF_FDCHI2", &B0_DDTF_FDCHI2, &b_B0_DDTF_FDCHI2);
   fChain->SetBranchAddress("B0_DDTF_FDS", &B0_DDTF_FDS, &b_B0_DDTF_FDS);
   fChain->SetBranchAddress("B0_DDTF_IP", &B0_DDTF_IP, &b_B0_DDTF_IP);
   fChain->SetBranchAddress("B0_DDTF_IPCHI2", &B0_DDTF_IPCHI2, &b_B0_DDTF_IPCHI2);
   fChain->SetBranchAddress("B0_DDTF_M", &B0_DDTF_M, &b_B0_DDTF_M);
   fChain->SetBranchAddress("B0_DDTF_P", &B0_DDTF_P, &b_B0_DDTF_P);
   fChain->SetBranchAddress("B0_DDTF_PT", &B0_DDTF_PT, &b_B0_DDTF_PT);
   fChain->SetBranchAddress("B0_DDTF_PX", &B0_DDTF_PX, &b_B0_DDTF_PX);
   fChain->SetBranchAddress("B0_DDTF_PY", &B0_DDTF_PY, &b_B0_DDTF_PY);
   fChain->SetBranchAddress("B0_DDTF_PZ", &B0_DDTF_PZ, &b_B0_DDTF_PZ);
   fChain->SetBranchAddress("B0_DDTF_TAU", &B0_DDTF_TAU, &b_B0_DDTF_TAU);
   fChain->SetBranchAddress("B0_ETA", &B0_ETA, &b_B0_ETA);
   fChain->SetBranchAddress("B0_LOKI_DIRA", &B0_LOKI_DIRA, &b_B0_LOKI_DIRA);
   fChain->SetBranchAddress("B0_LOKI_FDCHI2", &B0_LOKI_FDCHI2, &b_B0_LOKI_FDCHI2);
   fChain->SetBranchAddress("B0_LOKI_FDS", &B0_LOKI_FDS, &b_B0_LOKI_FDS);
   fChain->SetBranchAddress("B0_LOKI_LV01", &B0_LOKI_LV01, &b_B0_LOKI_LV01);
   fChain->SetBranchAddress("B0_LOKI_LV02", &B0_LOKI_LV02, &b_B0_LOKI_LV02);
   fChain->SetBranchAddress("B0_LOKI_MAXDOCA", &B0_LOKI_MAXDOCA, &b_B0_LOKI_MAXDOCA);
   fChain->SetBranchAddress("B0_LOKI_TAU", &B0_LOKI_TAU, &b_B0_LOKI_TAU);
   fChain->SetBranchAddress("B0_PHI", &B0_PHI, &b_B0_PHI);
   fChain->SetBranchAddress("B0_Y", &B0_Y, &b_B0_Y);
   fChain->SetBranchAddress("B0_Cons_nPV", &B0_Cons_nPV, &b_B0_Cons_nPV);
   fChain->SetBranchAddress("B0_Cons_D0_Kplus_ID", B0_Cons_D0_Kplus_ID, &b_B0_Cons_D0_Kplus_ID);
   fChain->SetBranchAddress("B0_Cons_D0_Kplus_PE", B0_Cons_D0_Kplus_PE, &b_B0_Cons_D0_Kplus_PE);
   fChain->SetBranchAddress("B0_Cons_D0_Kplus_PX", B0_Cons_D0_Kplus_PX, &b_B0_Cons_D0_Kplus_PX);
   fChain->SetBranchAddress("B0_Cons_D0_Kplus_PY", B0_Cons_D0_Kplus_PY, &b_B0_Cons_D0_Kplus_PY);
   fChain->SetBranchAddress("B0_Cons_D0_Kplus_PZ", B0_Cons_D0_Kplus_PZ, &b_B0_Cons_D0_Kplus_PZ);
   fChain->SetBranchAddress("B0_Cons_D0_piplus_ID", B0_Cons_D0_piplus_ID, &b_B0_Cons_D0_piplus_ID);
   fChain->SetBranchAddress("B0_Cons_D0_piplus_PE", B0_Cons_D0_piplus_PE, &b_B0_Cons_D0_piplus_PE);
   fChain->SetBranchAddress("B0_Cons_D0_piplus_PX", B0_Cons_D0_piplus_PX, &b_B0_Cons_D0_piplus_PX);
   fChain->SetBranchAddress("B0_Cons_D0_piplus_PY", B0_Cons_D0_piplus_PY, &b_B0_Cons_D0_piplus_PY);
   fChain->SetBranchAddress("B0_Cons_D0_piplus_PZ", B0_Cons_D0_piplus_PZ, &b_B0_Cons_D0_piplus_PZ);
   fChain->SetBranchAddress("B0_Cons_M", B0_Cons_M, &b_B0_Cons_M);
   fChain->SetBranchAddress("B0_Cons_MERR", B0_Cons_MERR, &b_B0_Cons_MERR);
   fChain->SetBranchAddress("B0_Cons_P", B0_Cons_P, &b_B0_Cons_P);
   fChain->SetBranchAddress("B0_Cons_PERR", B0_Cons_PERR, &b_B0_Cons_PERR);
   fChain->SetBranchAddress("B0_Cons_PV_key", B0_Cons_PV_key, &b_B0_Cons_PV_key);
   fChain->SetBranchAddress("B0_Cons_chi2", B0_Cons_chi2, &b_B0_Cons_chi2);
   fChain->SetBranchAddress("B0_Cons_ctau", B0_Cons_ctau, &b_B0_Cons_ctau);
   fChain->SetBranchAddress("B0_Cons_ctauErr", B0_Cons_ctauErr, &b_B0_Cons_ctauErr);
   fChain->SetBranchAddress("B0_Cons_decayLength", B0_Cons_decayLength, &b_B0_Cons_decayLength);
   fChain->SetBranchAddress("B0_Cons_decayLengthErr", B0_Cons_decayLengthErr, &b_B0_Cons_decayLengthErr);
   fChain->SetBranchAddress("B0_Cons_nDOF", B0_Cons_nDOF, &b_B0_Cons_nDOF);
   fChain->SetBranchAddress("B0_Cons_nIter", B0_Cons_nIter, &b_B0_Cons_nIter);
   fChain->SetBranchAddress("B0_Cons_rho_770_0_pplus_0_ID", B0_Cons_rho_770_0_pplus_0_ID, &b_B0_Cons_rho_770_0_pplus_0_ID);
   fChain->SetBranchAddress("B0_Cons_rho_770_0_pplus_0_PE", B0_Cons_rho_770_0_pplus_0_PE, &b_B0_Cons_rho_770_0_pplus_0_PE);
   fChain->SetBranchAddress("B0_Cons_rho_770_0_pplus_0_PX", B0_Cons_rho_770_0_pplus_0_PX, &b_B0_Cons_rho_770_0_pplus_0_PX);
   fChain->SetBranchAddress("B0_Cons_rho_770_0_pplus_0_PY", B0_Cons_rho_770_0_pplus_0_PY, &b_B0_Cons_rho_770_0_pplus_0_PY);
   fChain->SetBranchAddress("B0_Cons_rho_770_0_pplus_0_PZ", B0_Cons_rho_770_0_pplus_0_PZ, &b_B0_Cons_rho_770_0_pplus_0_PZ);
   fChain->SetBranchAddress("B0_Cons_rho_770_0_pplus_ID", B0_Cons_rho_770_0_pplus_ID, &b_B0_Cons_rho_770_0_pplus_ID);
   fChain->SetBranchAddress("B0_Cons_rho_770_0_pplus_PE", B0_Cons_rho_770_0_pplus_PE, &b_B0_Cons_rho_770_0_pplus_PE);
   fChain->SetBranchAddress("B0_Cons_rho_770_0_pplus_PX", B0_Cons_rho_770_0_pplus_PX, &b_B0_Cons_rho_770_0_pplus_PX);
   fChain->SetBranchAddress("B0_Cons_rho_770_0_pplus_PY", B0_Cons_rho_770_0_pplus_PY, &b_B0_Cons_rho_770_0_pplus_PY);
   fChain->SetBranchAddress("B0_Cons_rho_770_0_pplus_PZ", B0_Cons_rho_770_0_pplus_PZ, &b_B0_Cons_rho_770_0_pplus_PZ);
   fChain->SetBranchAddress("B0_Cons_status", B0_Cons_status, &b_B0_Cons_status);
   fChain->SetBranchAddress("B0_L0Global_Dec", &B0_L0Global_Dec, &b_B0_L0Global_Dec);
   fChain->SetBranchAddress("B0_L0Global_TIS", &B0_L0Global_TIS, &b_B0_L0Global_TIS);
   fChain->SetBranchAddress("B0_L0Global_TOS", &B0_L0Global_TOS, &b_B0_L0Global_TOS);
   fChain->SetBranchAddress("B0_Hlt1Global_Dec", &B0_Hlt1Global_Dec, &b_B0_Hlt1Global_Dec);
   fChain->SetBranchAddress("B0_Hlt1Global_TIS", &B0_Hlt1Global_TIS, &b_B0_Hlt1Global_TIS);
   fChain->SetBranchAddress("B0_Hlt1Global_TOS", &B0_Hlt1Global_TOS, &b_B0_Hlt1Global_TOS);
   fChain->SetBranchAddress("B0_Hlt1Phys_Dec", &B0_Hlt1Phys_Dec, &b_B0_Hlt1Phys_Dec);
   fChain->SetBranchAddress("B0_Hlt1Phys_TIS", &B0_Hlt1Phys_TIS, &b_B0_Hlt1Phys_TIS);
   fChain->SetBranchAddress("B0_Hlt1Phys_TOS", &B0_Hlt1Phys_TOS, &b_B0_Hlt1Phys_TOS);
   fChain->SetBranchAddress("B0_Hlt2Global_Dec", &B0_Hlt2Global_Dec, &b_B0_Hlt2Global_Dec);
   fChain->SetBranchAddress("B0_Hlt2Global_TIS", &B0_Hlt2Global_TIS, &b_B0_Hlt2Global_TIS);
   fChain->SetBranchAddress("B0_Hlt2Global_TOS", &B0_Hlt2Global_TOS, &b_B0_Hlt2Global_TOS);
   fChain->SetBranchAddress("B0_Hlt2Phys_Dec", &B0_Hlt2Phys_Dec, &b_B0_Hlt2Phys_Dec);
   fChain->SetBranchAddress("B0_Hlt2Phys_TIS", &B0_Hlt2Phys_TIS, &b_B0_Hlt2Phys_TIS);
   fChain->SetBranchAddress("B0_Hlt2Phys_TOS", &B0_Hlt2Phys_TOS, &b_B0_Hlt2Phys_TOS);
   fChain->SetBranchAddress("B0_L0MuonDecision_Dec", &B0_L0MuonDecision_Dec, &b_B0_L0MuonDecision_Dec);
   fChain->SetBranchAddress("B0_L0MuonDecision_TIS", &B0_L0MuonDecision_TIS, &b_B0_L0MuonDecision_TIS);
   fChain->SetBranchAddress("B0_L0MuonDecision_TOS", &B0_L0MuonDecision_TOS, &b_B0_L0MuonDecision_TOS);
   fChain->SetBranchAddress("B0_L0MuonNoSPDDecision_Dec", &B0_L0MuonNoSPDDecision_Dec, &b_B0_L0MuonNoSPDDecision_Dec);
   fChain->SetBranchAddress("B0_L0MuonNoSPDDecision_TIS", &B0_L0MuonNoSPDDecision_TIS, &b_B0_L0MuonNoSPDDecision_TIS);
   fChain->SetBranchAddress("B0_L0MuonNoSPDDecision_TOS", &B0_L0MuonNoSPDDecision_TOS, &b_B0_L0MuonNoSPDDecision_TOS);
   fChain->SetBranchAddress("B0_L0MuonEWDecision_Dec", &B0_L0MuonEWDecision_Dec, &b_B0_L0MuonEWDecision_Dec);
   fChain->SetBranchAddress("B0_L0MuonEWDecision_TIS", &B0_L0MuonEWDecision_TIS, &b_B0_L0MuonEWDecision_TIS);
   fChain->SetBranchAddress("B0_L0MuonEWDecision_TOS", &B0_L0MuonEWDecision_TOS, &b_B0_L0MuonEWDecision_TOS);
   fChain->SetBranchAddress("B0_L0DiMuonDecision_Dec", &B0_L0DiMuonDecision_Dec, &b_B0_L0DiMuonDecision_Dec);
   fChain->SetBranchAddress("B0_L0DiMuonDecision_TIS", &B0_L0DiMuonDecision_TIS, &b_B0_L0DiMuonDecision_TIS);
   fChain->SetBranchAddress("B0_L0DiMuonDecision_TOS", &B0_L0DiMuonDecision_TOS, &b_B0_L0DiMuonDecision_TOS);
   fChain->SetBranchAddress("B0_L0HadronDecision_Dec", &B0_L0HadronDecision_Dec, &b_B0_L0HadronDecision_Dec);
   fChain->SetBranchAddress("B0_L0HadronDecision_TIS", &B0_L0HadronDecision_TIS, &b_B0_L0HadronDecision_TIS);
   fChain->SetBranchAddress("B0_L0HadronDecision_TOS", &B0_L0HadronDecision_TOS, &b_B0_L0HadronDecision_TOS);
   fChain->SetBranchAddress("B0_L0PhotonDecision_Dec", &B0_L0PhotonDecision_Dec, &b_B0_L0PhotonDecision_Dec);
   fChain->SetBranchAddress("B0_L0PhotonDecision_TIS", &B0_L0PhotonDecision_TIS, &b_B0_L0PhotonDecision_TIS);
   fChain->SetBranchAddress("B0_L0PhotonDecision_TOS", &B0_L0PhotonDecision_TOS, &b_B0_L0PhotonDecision_TOS);
   fChain->SetBranchAddress("B0_L0ElectronDecision_Dec", &B0_L0ElectronDecision_Dec, &b_B0_L0ElectronDecision_Dec);
   fChain->SetBranchAddress("B0_L0ElectronDecision_TIS", &B0_L0ElectronDecision_TIS, &b_B0_L0ElectronDecision_TIS);
   fChain->SetBranchAddress("B0_L0ElectronDecision_TOS", &B0_L0ElectronDecision_TOS, &b_B0_L0ElectronDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt1TrackMVADecision_Dec", &B0_Hlt1TrackMVADecision_Dec, &b_B0_Hlt1TrackMVADecision_Dec);
   fChain->SetBranchAddress("B0_Hlt1TrackMVADecision_TIS", &B0_Hlt1TrackMVADecision_TIS, &b_B0_Hlt1TrackMVADecision_TIS);
   fChain->SetBranchAddress("B0_Hlt1TrackMVADecision_TOS", &B0_Hlt1TrackMVADecision_TOS, &b_B0_Hlt1TrackMVADecision_TOS);
   fChain->SetBranchAddress("B0_Hlt1TwoTrackMVADecision_Dec", &B0_Hlt1TwoTrackMVADecision_Dec, &b_B0_Hlt1TwoTrackMVADecision_Dec);
   fChain->SetBranchAddress("B0_Hlt1TwoTrackMVADecision_TIS", &B0_Hlt1TwoTrackMVADecision_TIS, &b_B0_Hlt1TwoTrackMVADecision_TIS);
   fChain->SetBranchAddress("B0_Hlt1TwoTrackMVADecision_TOS", &B0_Hlt1TwoTrackMVADecision_TOS, &b_B0_Hlt1TwoTrackMVADecision_TOS);
   fChain->SetBranchAddress("B0_Hlt1TrackMuonDecision_Dec", &B0_Hlt1TrackMuonDecision_Dec, &b_B0_Hlt1TrackMuonDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt1TrackMuonDecision_TIS", &B0_Hlt1TrackMuonDecision_TIS, &b_B0_Hlt1TrackMuonDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt1TrackMuonDecision_TOS", &B0_Hlt1TrackMuonDecision_TOS, &b_B0_Hlt1TrackMuonDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt1TrackPhotonDecision_Dec", &B0_Hlt1TrackPhotonDecision_Dec, &b_B0_Hlt1TrackPhotonDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt1TrackPhotonDecision_TIS", &B0_Hlt1TrackPhotonDecision_TIS, &b_B0_Hlt1TrackPhotonDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt1TrackPhotonDecision_TOS", &B0_Hlt1TrackPhotonDecision_TOS, &b_B0_Hlt1TrackPhotonDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt1TrackForwardPassThroughDecision_Dec", &B0_Hlt1TrackForwardPassThroughDecision_Dec, &b_B0_Hlt1TrackForwardPassThroughDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt1TrackForwardPassThroughDecision_TIS", &B0_Hlt1TrackForwardPassThroughDecision_TIS, &b_B0_Hlt1TrackForwardPassThroughDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt1TrackForwardPassThroughDecision_TOS", &B0_Hlt1TrackForwardPassThroughDecision_TOS, &b_B0_Hlt1TrackForwardPassThroughDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt1TrackForwardPassThroughLooseDecision_Dec", &B0_Hlt1TrackForwardPassThroughLooseDecision_Dec, &b_B0_Hlt1TrackForwardPassThroughLooseDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt1TrackForwardPassThroughLooseDecision_TIS", &B0_Hlt1TrackForwardPassThroughLooseDecision_TIS, &b_B0_Hlt1TrackForwardPassThroughLooseDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt1TrackForwardPassThroughLooseDecision_TOS", &B0_Hlt1TrackForwardPassThroughLooseDecision_TOS, &b_B0_Hlt1TrackForwardPassThroughLooseDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt1TrackAllL0Decision_Dec", &B0_Hlt1TrackAllL0Decision_Dec, &b_B0_Hlt1TrackAllL0Decision_Dec);
   fChain->SetBranchAddress("B0_Hlt1TrackAllL0Decision_TIS", &B0_Hlt1TrackAllL0Decision_TIS, &b_B0_Hlt1TrackAllL0Decision_TIS);
   fChain->SetBranchAddress("B0_Hlt1TrackAllL0Decision_TOS", &B0_Hlt1TrackAllL0Decision_TOS, &b_B0_Hlt1TrackAllL0Decision_TOS);
   fChain->SetBranchAddress("B0_Hlt1GlobalDecision_Dec", &B0_Hlt1GlobalDecision_Dec, &b_B0_Hlt1GlobalDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt1GlobalDecision_TIS", &B0_Hlt1GlobalDecision_TIS, &b_B0_Hlt1GlobalDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt1GlobalDecision_TOS", &B0_Hlt1GlobalDecision_TOS, &b_B0_Hlt1GlobalDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt2Topo2BodyDecision_Dec", &B0_Hlt2Topo2BodyDecision_Dec, &b_B0_Hlt2Topo2BodyDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt2Topo2BodyDecision_TIS", &B0_Hlt2Topo2BodyDecision_TIS, &b_B0_Hlt2Topo2BodyDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt2Topo2BodyDecision_TOS", &B0_Hlt2Topo2BodyDecision_TOS, &b_B0_Hlt2Topo2BodyDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt2Topo3BodyDecision_Dec", &B0_Hlt2Topo3BodyDecision_Dec, &b_B0_Hlt2Topo3BodyDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt2Topo3BodyDecision_TIS", &B0_Hlt2Topo3BodyDecision_TIS, &b_B0_Hlt2Topo3BodyDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt2Topo3BodyDecision_TOS", &B0_Hlt2Topo3BodyDecision_TOS, &b_B0_Hlt2Topo3BodyDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt2Topo4BodyDecision_Dec", &B0_Hlt2Topo4BodyDecision_Dec, &b_B0_Hlt2Topo4BodyDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt2Topo4BodyDecision_TIS", &B0_Hlt2Topo4BodyDecision_TIS, &b_B0_Hlt2Topo4BodyDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt2Topo4BodyDecision_TOS", &B0_Hlt2Topo4BodyDecision_TOS, &b_B0_Hlt2Topo4BodyDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_Dec", &B0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_Dec, &b_B0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_TIS", &B0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_TIS, &b_B0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_TOS", &B0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_TOS, &b_B0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt2TopoMu3BodyDecision_Dec", &B0_Hlt2TopoMu3BodyDecision_Dec, &b_B0_Hlt2TopoMu3BodyDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt2TopoMu3BodyDecision_TIS", &B0_Hlt2TopoMu3BodyDecision_TIS, &b_B0_Hlt2TopoMu3BodyDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt2TopoMu3BodyDecision_TOS", &B0_Hlt2TopoMu3BodyDecision_TOS, &b_B0_Hlt2TopoMu3BodyDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt2TopoMu4BodyDecision_Dec", &B0_Hlt2TopoMu4BodyDecision_Dec, &b_B0_Hlt2TopoMu4BodyDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt2TopoMu4BodyDecision_TIS", &B0_Hlt2TopoMu4BodyDecision_TIS, &b_B0_Hlt2TopoMu4BodyDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt2TopoMu4BodyDecision_TOS", &B0_Hlt2TopoMu4BodyDecision_TOS, &b_B0_Hlt2TopoMu4BodyDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt2TopoE2BodyDecision_Dec", &B0_Hlt2TopoE2BodyDecision_Dec, &b_B0_Hlt2TopoE2BodyDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt2TopoE2BodyDecision_TIS", &B0_Hlt2TopoE2BodyDecision_TIS, &b_B0_Hlt2TopoE2BodyDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt2TopoE2BodyDecision_TOS", &B0_Hlt2TopoE2BodyDecision_TOS, &b_B0_Hlt2TopoE2BodyDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt2TopoE3BodyDecision_Dec", &B0_Hlt2TopoE3BodyDecision_Dec, &b_B0_Hlt2TopoE3BodyDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt2TopoE3BodyDecision_TIS", &B0_Hlt2TopoE3BodyDecision_TIS, &b_B0_Hlt2TopoE3BodyDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt2TopoE3BodyDecision_TOS", &B0_Hlt2TopoE3BodyDecision_TOS, &b_B0_Hlt2TopoE3BodyDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt2TopoE4BodyDecision_Dec", &B0_Hlt2TopoE4BodyDecision_Dec, &b_B0_Hlt2TopoE4BodyDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt2TopoE4BodyDecision_TIS", &B0_Hlt2TopoE4BodyDecision_TIS, &b_B0_Hlt2TopoE4BodyDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt2TopoE4BodyDecision_TOS", &B0_Hlt2TopoE4BodyDecision_TOS, &b_B0_Hlt2TopoE4BodyDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt2TopoRad2BodyDecision_Dec", &B0_Hlt2TopoRad2BodyDecision_Dec, &b_B0_Hlt2TopoRad2BodyDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt2TopoRad2BodyDecision_TIS", &B0_Hlt2TopoRad2BodyDecision_TIS, &b_B0_Hlt2TopoRad2BodyDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt2TopoRad2BodyDecision_TOS", &B0_Hlt2TopoRad2BodyDecision_TOS, &b_B0_Hlt2TopoRad2BodyDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt2SingleMuonDecision_Dec", &B0_Hlt2SingleMuonDecision_Dec, &b_B0_Hlt2SingleMuonDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt2SingleMuonDecision_TIS", &B0_Hlt2SingleMuonDecision_TIS, &b_B0_Hlt2SingleMuonDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt2SingleMuonDecision_TOS", &B0_Hlt2SingleMuonDecision_TOS, &b_B0_Hlt2SingleMuonDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt2RadiativeTopoTrackDecision_Dec", &B0_Hlt2RadiativeTopoTrackDecision_Dec, &b_B0_Hlt2RadiativeTopoTrackDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt2RadiativeTopoTrackDecision_TIS", &B0_Hlt2RadiativeTopoTrackDecision_TIS, &b_B0_Hlt2RadiativeTopoTrackDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt2RadiativeTopoTrackDecision_TOS", &B0_Hlt2RadiativeTopoTrackDecision_TOS, &b_B0_Hlt2RadiativeTopoTrackDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt2RadiativeTopoPhotonDecision_Dec", &B0_Hlt2RadiativeTopoPhotonDecision_Dec, &b_B0_Hlt2RadiativeTopoPhotonDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt2RadiativeTopoPhotonDecision_TIS", &B0_Hlt2RadiativeTopoPhotonDecision_TIS, &b_B0_Hlt2RadiativeTopoPhotonDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt2RadiativeTopoPhotonDecision_TOS", &B0_Hlt2RadiativeTopoPhotonDecision_TOS, &b_B0_Hlt2RadiativeTopoPhotonDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt2Topo2BodySimpleDecision_Dec", &B0_Hlt2Topo2BodySimpleDecision_Dec, &b_B0_Hlt2Topo2BodySimpleDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt2Topo2BodySimpleDecision_TIS", &B0_Hlt2Topo2BodySimpleDecision_TIS, &b_B0_Hlt2Topo2BodySimpleDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt2Topo2BodySimpleDecision_TOS", &B0_Hlt2Topo2BodySimpleDecision_TOS, &b_B0_Hlt2Topo2BodySimpleDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt2Topo3BodySimpleDecision_Dec", &B0_Hlt2Topo3BodySimpleDecision_Dec, &b_B0_Hlt2Topo3BodySimpleDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt2Topo3BodySimpleDecision_TIS", &B0_Hlt2Topo3BodySimpleDecision_TIS, &b_B0_Hlt2Topo3BodySimpleDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt2Topo3BodySimpleDecision_TOS", &B0_Hlt2Topo3BodySimpleDecision_TOS, &b_B0_Hlt2Topo3BodySimpleDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt2Topo4BodySimpleDecision_Dec", &B0_Hlt2Topo4BodySimpleDecision_Dec, &b_B0_Hlt2Topo4BodySimpleDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt2Topo4BodySimpleDecision_TIS", &B0_Hlt2Topo4BodySimpleDecision_TIS, &b_B0_Hlt2Topo4BodySimpleDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt2Topo4BodySimpleDecision_TOS", &B0_Hlt2Topo4BodySimpleDecision_TOS, &b_B0_Hlt2Topo4BodySimpleDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt2Topo2BodyBBDTDecision_Dec", &B0_Hlt2Topo2BodyBBDTDecision_Dec, &b_B0_Hlt2Topo2BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt2Topo2BodyBBDTDecision_TIS", &B0_Hlt2Topo2BodyBBDTDecision_TIS, &b_B0_Hlt2Topo2BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt2Topo2BodyBBDTDecision_TOS", &B0_Hlt2Topo2BodyBBDTDecision_TOS, &b_B0_Hlt2Topo2BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt2Topo3BodyBBDTDecision_Dec", &B0_Hlt2Topo3BodyBBDTDecision_Dec, &b_B0_Hlt2Topo3BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt2Topo3BodyBBDTDecision_TIS", &B0_Hlt2Topo3BodyBBDTDecision_TIS, &b_B0_Hlt2Topo3BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt2Topo3BodyBBDTDecision_TOS", &B0_Hlt2Topo3BodyBBDTDecision_TOS, &b_B0_Hlt2Topo3BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt2Topo4BodyBBDTDecision_Dec", &B0_Hlt2Topo4BodyBBDTDecision_Dec, &b_B0_Hlt2Topo4BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt2Topo4BodyBBDTDecision_TIS", &B0_Hlt2Topo4BodyBBDTDecision_TIS, &b_B0_Hlt2Topo4BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt2Topo4BodyBBDTDecision_TOS", &B0_Hlt2Topo4BodyBBDTDecision_TOS, &b_B0_Hlt2Topo4BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt2TopoE2BodyBBDTDecision_Dec", &B0_Hlt2TopoE2BodyBBDTDecision_Dec, &b_B0_Hlt2TopoE2BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt2TopoE2BodyBBDTDecision_TIS", &B0_Hlt2TopoE2BodyBBDTDecision_TIS, &b_B0_Hlt2TopoE2BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt2TopoE2BodyBBDTDecision_TOS", &B0_Hlt2TopoE2BodyBBDTDecision_TOS, &b_B0_Hlt2TopoE2BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt2TopoE3BodyBBDTDecision_Dec", &B0_Hlt2TopoE3BodyBBDTDecision_Dec, &b_B0_Hlt2TopoE3BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt2TopoE3BodyBBDTDecision_TIS", &B0_Hlt2TopoE3BodyBBDTDecision_TIS, &b_B0_Hlt2TopoE3BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt2TopoE3BodyBBDTDecision_TOS", &B0_Hlt2TopoE3BodyBBDTDecision_TOS, &b_B0_Hlt2TopoE3BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt2TopoE4BodyBBDTDecision_Dec", &B0_Hlt2TopoE4BodyBBDTDecision_Dec, &b_B0_Hlt2TopoE4BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt2TopoE4BodyBBDTDecision_TIS", &B0_Hlt2TopoE4BodyBBDTDecision_TIS, &b_B0_Hlt2TopoE4BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt2TopoE4BodyBBDTDecision_TOS", &B0_Hlt2TopoE4BodyBBDTDecision_TOS, &b_B0_Hlt2TopoE4BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt2TopoMu2BodyBBDTDecision_Dec", &B0_Hlt2TopoMu2BodyBBDTDecision_Dec, &b_B0_Hlt2TopoMu2BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt2TopoMu2BodyBBDTDecision_TIS", &B0_Hlt2TopoMu2BodyBBDTDecision_TIS, &b_B0_Hlt2TopoMu2BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt2TopoMu2BodyBBDTDecision_TOS", &B0_Hlt2TopoMu2BodyBBDTDecision_TOS, &b_B0_Hlt2TopoMu2BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt2TopoMu3BodyBBDTDecision_Dec", &B0_Hlt2TopoMu3BodyBBDTDecision_Dec, &b_B0_Hlt2TopoMu3BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt2TopoMu3BodyBBDTDecision_TIS", &B0_Hlt2TopoMu3BodyBBDTDecision_TIS, &b_B0_Hlt2TopoMu3BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt2TopoMu3BodyBBDTDecision_TOS", &B0_Hlt2TopoMu3BodyBBDTDecision_TOS, &b_B0_Hlt2TopoMu3BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt2TopoMu4BodyBBDTDecision_Dec", &B0_Hlt2TopoMu4BodyBBDTDecision_Dec, &b_B0_Hlt2TopoMu4BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt2TopoMu4BodyBBDTDecision_TIS", &B0_Hlt2TopoMu4BodyBBDTDecision_TIS, &b_B0_Hlt2TopoMu4BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt2TopoMu4BodyBBDTDecision_TOS", &B0_Hlt2TopoMu4BodyBBDTDecision_TOS, &b_B0_Hlt2TopoMu4BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt2GlobalDecision_Dec", &B0_Hlt2GlobalDecision_Dec, &b_B0_Hlt2GlobalDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt2GlobalDecision_TIS", &B0_Hlt2GlobalDecision_TIS, &b_B0_Hlt2GlobalDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt2GlobalDecision_TOS", &B0_Hlt2GlobalDecision_TOS, &b_B0_Hlt2GlobalDecision_TOS);
   fChain->SetBranchAddress("B0_Hlt2TopoRad2BodyBBDTDecision_Dec", &B0_Hlt2TopoRad2BodyBBDTDecision_Dec, &b_B0_Hlt2TopoRad2BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("B0_Hlt2TopoRad2BodyBBDTDecision_TIS", &B0_Hlt2TopoRad2BodyBBDTDecision_TIS, &b_B0_Hlt2TopoRad2BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("B0_Hlt2TopoRad2BodyBBDTDecision_TOS", &B0_Hlt2TopoRad2BodyBBDTDecision_TOS, &b_B0_Hlt2TopoRad2BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("D0_CosTheta", &D0_CosTheta, &b_D0_CosTheta);
   fChain->SetBranchAddress("D0_ENDVERTEX_X", &D0_ENDVERTEX_X, &b_D0_ENDVERTEX_X);
   fChain->SetBranchAddress("D0_ENDVERTEX_Y", &D0_ENDVERTEX_Y, &b_D0_ENDVERTEX_Y);
   fChain->SetBranchAddress("D0_ENDVERTEX_Z", &D0_ENDVERTEX_Z, &b_D0_ENDVERTEX_Z);
   fChain->SetBranchAddress("D0_ENDVERTEX_XERR", &D0_ENDVERTEX_XERR, &b_D0_ENDVERTEX_XERR);
   fChain->SetBranchAddress("D0_ENDVERTEX_YERR", &D0_ENDVERTEX_YERR, &b_D0_ENDVERTEX_YERR);
   fChain->SetBranchAddress("D0_ENDVERTEX_ZERR", &D0_ENDVERTEX_ZERR, &b_D0_ENDVERTEX_ZERR);
   fChain->SetBranchAddress("D0_ENDVERTEX_CHI2", &D0_ENDVERTEX_CHI2, &b_D0_ENDVERTEX_CHI2);
   fChain->SetBranchAddress("D0_ENDVERTEX_NDOF", &D0_ENDVERTEX_NDOF, &b_D0_ENDVERTEX_NDOF);
   fChain->SetBranchAddress("D0_ENDVERTEX_COV_", D0_ENDVERTEX_COV_, &b_D0_ENDVERTEX_COV_);
   fChain->SetBranchAddress("D0_OWNPV_X", &D0_OWNPV_X, &b_D0_OWNPV_X);
   fChain->SetBranchAddress("D0_OWNPV_Y", &D0_OWNPV_Y, &b_D0_OWNPV_Y);
   fChain->SetBranchAddress("D0_OWNPV_Z", &D0_OWNPV_Z, &b_D0_OWNPV_Z);
   fChain->SetBranchAddress("D0_OWNPV_XERR", &D0_OWNPV_XERR, &b_D0_OWNPV_XERR);
   fChain->SetBranchAddress("D0_OWNPV_YERR", &D0_OWNPV_YERR, &b_D0_OWNPV_YERR);
   fChain->SetBranchAddress("D0_OWNPV_ZERR", &D0_OWNPV_ZERR, &b_D0_OWNPV_ZERR);
   fChain->SetBranchAddress("D0_OWNPV_CHI2", &D0_OWNPV_CHI2, &b_D0_OWNPV_CHI2);
   fChain->SetBranchAddress("D0_OWNPV_NDOF", &D0_OWNPV_NDOF, &b_D0_OWNPV_NDOF);
   fChain->SetBranchAddress("D0_OWNPV_COV_", D0_OWNPV_COV_, &b_D0_OWNPV_COV_);
   fChain->SetBranchAddress("D0_IP_OWNPV", &D0_IP_OWNPV, &b_D0_IP_OWNPV);
   fChain->SetBranchAddress("D0_IPCHI2_OWNPV", &D0_IPCHI2_OWNPV, &b_D0_IPCHI2_OWNPV);
   fChain->SetBranchAddress("D0_FD_OWNPV", &D0_FD_OWNPV, &b_D0_FD_OWNPV);
   fChain->SetBranchAddress("D0_FDCHI2_OWNPV", &D0_FDCHI2_OWNPV, &b_D0_FDCHI2_OWNPV);
   fChain->SetBranchAddress("D0_DIRA_OWNPV", &D0_DIRA_OWNPV, &b_D0_DIRA_OWNPV);
   fChain->SetBranchAddress("D0_ORIVX_X", &D0_ORIVX_X, &b_D0_ORIVX_X);
   fChain->SetBranchAddress("D0_ORIVX_Y", &D0_ORIVX_Y, &b_D0_ORIVX_Y);
   fChain->SetBranchAddress("D0_ORIVX_Z", &D0_ORIVX_Z, &b_D0_ORIVX_Z);
   fChain->SetBranchAddress("D0_ORIVX_XERR", &D0_ORIVX_XERR, &b_D0_ORIVX_XERR);
   fChain->SetBranchAddress("D0_ORIVX_YERR", &D0_ORIVX_YERR, &b_D0_ORIVX_YERR);
   fChain->SetBranchAddress("D0_ORIVX_ZERR", &D0_ORIVX_ZERR, &b_D0_ORIVX_ZERR);
   fChain->SetBranchAddress("D0_ORIVX_CHI2", &D0_ORIVX_CHI2, &b_D0_ORIVX_CHI2);
   fChain->SetBranchAddress("D0_ORIVX_NDOF", &D0_ORIVX_NDOF, &b_D0_ORIVX_NDOF);
   fChain->SetBranchAddress("D0_ORIVX_COV_", D0_ORIVX_COV_, &b_D0_ORIVX_COV_);
   fChain->SetBranchAddress("D0_FD_ORIVX", &D0_FD_ORIVX, &b_D0_FD_ORIVX);
   fChain->SetBranchAddress("D0_FDCHI2_ORIVX", &D0_FDCHI2_ORIVX, &b_D0_FDCHI2_ORIVX);
   fChain->SetBranchAddress("D0_DIRA_ORIVX", &D0_DIRA_ORIVX, &b_D0_DIRA_ORIVX);
   fChain->SetBranchAddress("D0_P", &D0_P, &b_D0_P);
   fChain->SetBranchAddress("D0_PT", &D0_PT, &b_D0_PT);
   fChain->SetBranchAddress("D0_PE", &D0_PE, &b_D0_PE);
   fChain->SetBranchAddress("D0_PX", &D0_PX, &b_D0_PX);
   fChain->SetBranchAddress("D0_PY", &D0_PY, &b_D0_PY);
   fChain->SetBranchAddress("D0_PZ", &D0_PZ, &b_D0_PZ);
   fChain->SetBranchAddress("D0_MM", &D0_MM, &b_D0_MM);
   fChain->SetBranchAddress("D0_MMERR", &D0_MMERR, &b_D0_MMERR);
   fChain->SetBranchAddress("D0_M", &D0_M, &b_D0_M);
   fChain->SetBranchAddress("D0_ID", &D0_ID, &b_D0_ID);
   fChain->SetBranchAddress("D0_TAU", &D0_TAU, &b_D0_TAU);
   fChain->SetBranchAddress("D0_TAUERR", &D0_TAUERR, &b_D0_TAUERR);
   fChain->SetBranchAddress("D0_TAUCHI2", &D0_TAUCHI2, &b_D0_TAUCHI2);
   fChain->SetBranchAddress("D0_NumVtxWithinChi2WindowOneTrack", &D0_NumVtxWithinChi2WindowOneTrack, &b_D0_NumVtxWithinChi2WindowOneTrack);
   fChain->SetBranchAddress("D0_SmallestDeltaChi2OneTrack", &D0_SmallestDeltaChi2OneTrack, &b_D0_SmallestDeltaChi2OneTrack);
   fChain->SetBranchAddress("D0_SmallestDeltaChi2MassOneTrack", &D0_SmallestDeltaChi2MassOneTrack, &b_D0_SmallestDeltaChi2MassOneTrack);
   fChain->SetBranchAddress("D0_SmallestDeltaChi2TwoTracks", &D0_SmallestDeltaChi2TwoTracks, &b_D0_SmallestDeltaChi2TwoTracks);
   fChain->SetBranchAddress("D0_SmallestDeltaChi2MassTwoTracks", &D0_SmallestDeltaChi2MassTwoTracks, &b_D0_SmallestDeltaChi2MassTwoTracks);
   fChain->SetBranchAddress("D0_ETA", &D0_ETA, &b_D0_ETA);
   fChain->SetBranchAddress("D0_LOKI_DIRA", &D0_LOKI_DIRA, &b_D0_LOKI_DIRA);
   fChain->SetBranchAddress("D0_LOKI_FDCHI2", &D0_LOKI_FDCHI2, &b_D0_LOKI_FDCHI2);
   fChain->SetBranchAddress("D0_LOKI_FDS", &D0_LOKI_FDS, &b_D0_LOKI_FDS);
   fChain->SetBranchAddress("D0_LOKI_LV01", &D0_LOKI_LV01, &b_D0_LOKI_LV01);
   fChain->SetBranchAddress("D0_LOKI_LV02", &D0_LOKI_LV02, &b_D0_LOKI_LV02);
   fChain->SetBranchAddress("D0_LOKI_MAXDOCA", &D0_LOKI_MAXDOCA, &b_D0_LOKI_MAXDOCA);
   fChain->SetBranchAddress("D0_LOKI_TAU", &D0_LOKI_TAU, &b_D0_LOKI_TAU);
   fChain->SetBranchAddress("D0_PHI", &D0_PHI, &b_D0_PHI);
   fChain->SetBranchAddress("D0_Y", &D0_Y, &b_D0_Y);
   fChain->SetBranchAddress("D0_L0Global_Dec", &D0_L0Global_Dec, &b_D0_L0Global_Dec);
   fChain->SetBranchAddress("D0_L0Global_TIS", &D0_L0Global_TIS, &b_D0_L0Global_TIS);
   fChain->SetBranchAddress("D0_L0Global_TOS", &D0_L0Global_TOS, &b_D0_L0Global_TOS);
   fChain->SetBranchAddress("D0_Hlt1Global_Dec", &D0_Hlt1Global_Dec, &b_D0_Hlt1Global_Dec);
   fChain->SetBranchAddress("D0_Hlt1Global_TIS", &D0_Hlt1Global_TIS, &b_D0_Hlt1Global_TIS);
   fChain->SetBranchAddress("D0_Hlt1Global_TOS", &D0_Hlt1Global_TOS, &b_D0_Hlt1Global_TOS);
   fChain->SetBranchAddress("D0_Hlt1Phys_Dec", &D0_Hlt1Phys_Dec, &b_D0_Hlt1Phys_Dec);
   fChain->SetBranchAddress("D0_Hlt1Phys_TIS", &D0_Hlt1Phys_TIS, &b_D0_Hlt1Phys_TIS);
   fChain->SetBranchAddress("D0_Hlt1Phys_TOS", &D0_Hlt1Phys_TOS, &b_D0_Hlt1Phys_TOS);
   fChain->SetBranchAddress("D0_Hlt2Global_Dec", &D0_Hlt2Global_Dec, &b_D0_Hlt2Global_Dec);
   fChain->SetBranchAddress("D0_Hlt2Global_TIS", &D0_Hlt2Global_TIS, &b_D0_Hlt2Global_TIS);
   fChain->SetBranchAddress("D0_Hlt2Global_TOS", &D0_Hlt2Global_TOS, &b_D0_Hlt2Global_TOS);
   fChain->SetBranchAddress("D0_Hlt2Phys_Dec", &D0_Hlt2Phys_Dec, &b_D0_Hlt2Phys_Dec);
   fChain->SetBranchAddress("D0_Hlt2Phys_TIS", &D0_Hlt2Phys_TIS, &b_D0_Hlt2Phys_TIS);
   fChain->SetBranchAddress("D0_Hlt2Phys_TOS", &D0_Hlt2Phys_TOS, &b_D0_Hlt2Phys_TOS);
   fChain->SetBranchAddress("D0_L0MuonDecision_Dec", &D0_L0MuonDecision_Dec, &b_D0_L0MuonDecision_Dec);
   fChain->SetBranchAddress("D0_L0MuonDecision_TIS", &D0_L0MuonDecision_TIS, &b_D0_L0MuonDecision_TIS);
   fChain->SetBranchAddress("D0_L0MuonDecision_TOS", &D0_L0MuonDecision_TOS, &b_D0_L0MuonDecision_TOS);
   fChain->SetBranchAddress("D0_L0MuonNoSPDDecision_Dec", &D0_L0MuonNoSPDDecision_Dec, &b_D0_L0MuonNoSPDDecision_Dec);
   fChain->SetBranchAddress("D0_L0MuonNoSPDDecision_TIS", &D0_L0MuonNoSPDDecision_TIS, &b_D0_L0MuonNoSPDDecision_TIS);
   fChain->SetBranchAddress("D0_L0MuonNoSPDDecision_TOS", &D0_L0MuonNoSPDDecision_TOS, &b_D0_L0MuonNoSPDDecision_TOS);
   fChain->SetBranchAddress("D0_L0MuonEWDecision_Dec", &D0_L0MuonEWDecision_Dec, &b_D0_L0MuonEWDecision_Dec);
   fChain->SetBranchAddress("D0_L0MuonEWDecision_TIS", &D0_L0MuonEWDecision_TIS, &b_D0_L0MuonEWDecision_TIS);
   fChain->SetBranchAddress("D0_L0MuonEWDecision_TOS", &D0_L0MuonEWDecision_TOS, &b_D0_L0MuonEWDecision_TOS);
   fChain->SetBranchAddress("D0_L0DiMuonDecision_Dec", &D0_L0DiMuonDecision_Dec, &b_D0_L0DiMuonDecision_Dec);
   fChain->SetBranchAddress("D0_L0DiMuonDecision_TIS", &D0_L0DiMuonDecision_TIS, &b_D0_L0DiMuonDecision_TIS);
   fChain->SetBranchAddress("D0_L0DiMuonDecision_TOS", &D0_L0DiMuonDecision_TOS, &b_D0_L0DiMuonDecision_TOS);
   fChain->SetBranchAddress("D0_L0HadronDecision_Dec", &D0_L0HadronDecision_Dec, &b_D0_L0HadronDecision_Dec);
   fChain->SetBranchAddress("D0_L0HadronDecision_TIS", &D0_L0HadronDecision_TIS, &b_D0_L0HadronDecision_TIS);
   fChain->SetBranchAddress("D0_L0HadronDecision_TOS", &D0_L0HadronDecision_TOS, &b_D0_L0HadronDecision_TOS);
   fChain->SetBranchAddress("D0_L0PhotonDecision_Dec", &D0_L0PhotonDecision_Dec, &b_D0_L0PhotonDecision_Dec);
   fChain->SetBranchAddress("D0_L0PhotonDecision_TIS", &D0_L0PhotonDecision_TIS, &b_D0_L0PhotonDecision_TIS);
   fChain->SetBranchAddress("D0_L0PhotonDecision_TOS", &D0_L0PhotonDecision_TOS, &b_D0_L0PhotonDecision_TOS);
   fChain->SetBranchAddress("D0_L0ElectronDecision_Dec", &D0_L0ElectronDecision_Dec, &b_D0_L0ElectronDecision_Dec);
   fChain->SetBranchAddress("D0_L0ElectronDecision_TIS", &D0_L0ElectronDecision_TIS, &b_D0_L0ElectronDecision_TIS);
   fChain->SetBranchAddress("D0_L0ElectronDecision_TOS", &D0_L0ElectronDecision_TOS, &b_D0_L0ElectronDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt1TrackMVADecision_Dec", &D0_Hlt1TrackMVADecision_Dec, &b_D0_Hlt1TrackMVADecision_Dec);
   fChain->SetBranchAddress("D0_Hlt1TrackMVADecision_TIS", &D0_Hlt1TrackMVADecision_TIS, &b_D0_Hlt1TrackMVADecision_TIS);
   fChain->SetBranchAddress("D0_Hlt1TrackMVADecision_TOS", &D0_Hlt1TrackMVADecision_TOS, &b_D0_Hlt1TrackMVADecision_TOS);
   fChain->SetBranchAddress("D0_Hlt1TwoTrackMVADecision_Dec", &D0_Hlt1TwoTrackMVADecision_Dec, &b_D0_Hlt1TwoTrackMVADecision_Dec);
   fChain->SetBranchAddress("D0_Hlt1TwoTrackMVADecision_TIS", &D0_Hlt1TwoTrackMVADecision_TIS, &b_D0_Hlt1TwoTrackMVADecision_TIS);
   fChain->SetBranchAddress("D0_Hlt1TwoTrackMVADecision_TOS", &D0_Hlt1TwoTrackMVADecision_TOS, &b_D0_Hlt1TwoTrackMVADecision_TOS);
   fChain->SetBranchAddress("D0_Hlt1TrackMuonDecision_Dec", &D0_Hlt1TrackMuonDecision_Dec, &b_D0_Hlt1TrackMuonDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt1TrackMuonDecision_TIS", &D0_Hlt1TrackMuonDecision_TIS, &b_D0_Hlt1TrackMuonDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt1TrackMuonDecision_TOS", &D0_Hlt1TrackMuonDecision_TOS, &b_D0_Hlt1TrackMuonDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt1TrackPhotonDecision_Dec", &D0_Hlt1TrackPhotonDecision_Dec, &b_D0_Hlt1TrackPhotonDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt1TrackPhotonDecision_TIS", &D0_Hlt1TrackPhotonDecision_TIS, &b_D0_Hlt1TrackPhotonDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt1TrackPhotonDecision_TOS", &D0_Hlt1TrackPhotonDecision_TOS, &b_D0_Hlt1TrackPhotonDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt1TrackForwardPassThroughDecision_Dec", &D0_Hlt1TrackForwardPassThroughDecision_Dec, &b_D0_Hlt1TrackForwardPassThroughDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt1TrackForwardPassThroughDecision_TIS", &D0_Hlt1TrackForwardPassThroughDecision_TIS, &b_D0_Hlt1TrackForwardPassThroughDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt1TrackForwardPassThroughDecision_TOS", &D0_Hlt1TrackForwardPassThroughDecision_TOS, &b_D0_Hlt1TrackForwardPassThroughDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt1TrackForwardPassThroughLooseDecision_Dec", &D0_Hlt1TrackForwardPassThroughLooseDecision_Dec, &b_D0_Hlt1TrackForwardPassThroughLooseDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt1TrackForwardPassThroughLooseDecision_TIS", &D0_Hlt1TrackForwardPassThroughLooseDecision_TIS, &b_D0_Hlt1TrackForwardPassThroughLooseDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt1TrackForwardPassThroughLooseDecision_TOS", &D0_Hlt1TrackForwardPassThroughLooseDecision_TOS, &b_D0_Hlt1TrackForwardPassThroughLooseDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt1TrackAllL0Decision_Dec", &D0_Hlt1TrackAllL0Decision_Dec, &b_D0_Hlt1TrackAllL0Decision_Dec);
   fChain->SetBranchAddress("D0_Hlt1TrackAllL0Decision_TIS", &D0_Hlt1TrackAllL0Decision_TIS, &b_D0_Hlt1TrackAllL0Decision_TIS);
   fChain->SetBranchAddress("D0_Hlt1TrackAllL0Decision_TOS", &D0_Hlt1TrackAllL0Decision_TOS, &b_D0_Hlt1TrackAllL0Decision_TOS);
   fChain->SetBranchAddress("D0_Hlt1GlobalDecision_Dec", &D0_Hlt1GlobalDecision_Dec, &b_D0_Hlt1GlobalDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt1GlobalDecision_TIS", &D0_Hlt1GlobalDecision_TIS, &b_D0_Hlt1GlobalDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt1GlobalDecision_TOS", &D0_Hlt1GlobalDecision_TOS, &b_D0_Hlt1GlobalDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt2Topo2BodyDecision_Dec", &D0_Hlt2Topo2BodyDecision_Dec, &b_D0_Hlt2Topo2BodyDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt2Topo2BodyDecision_TIS", &D0_Hlt2Topo2BodyDecision_TIS, &b_D0_Hlt2Topo2BodyDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt2Topo2BodyDecision_TOS", &D0_Hlt2Topo2BodyDecision_TOS, &b_D0_Hlt2Topo2BodyDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt2Topo3BodyDecision_Dec", &D0_Hlt2Topo3BodyDecision_Dec, &b_D0_Hlt2Topo3BodyDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt2Topo3BodyDecision_TIS", &D0_Hlt2Topo3BodyDecision_TIS, &b_D0_Hlt2Topo3BodyDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt2Topo3BodyDecision_TOS", &D0_Hlt2Topo3BodyDecision_TOS, &b_D0_Hlt2Topo3BodyDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt2Topo4BodyDecision_Dec", &D0_Hlt2Topo4BodyDecision_Dec, &b_D0_Hlt2Topo4BodyDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt2Topo4BodyDecision_TIS", &D0_Hlt2Topo4BodyDecision_TIS, &b_D0_Hlt2Topo4BodyDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt2Topo4BodyDecision_TOS", &D0_Hlt2Topo4BodyDecision_TOS, &b_D0_Hlt2Topo4BodyDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_Dec", &D0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_Dec, &b_D0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_TIS", &D0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_TIS, &b_D0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_TOS", &D0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_TOS, &b_D0_Hlt2IncPhiDecisionHlt2TopoMu2BodyDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt2TopoMu3BodyDecision_Dec", &D0_Hlt2TopoMu3BodyDecision_Dec, &b_D0_Hlt2TopoMu3BodyDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt2TopoMu3BodyDecision_TIS", &D0_Hlt2TopoMu3BodyDecision_TIS, &b_D0_Hlt2TopoMu3BodyDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt2TopoMu3BodyDecision_TOS", &D0_Hlt2TopoMu3BodyDecision_TOS, &b_D0_Hlt2TopoMu3BodyDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt2TopoMu4BodyDecision_Dec", &D0_Hlt2TopoMu4BodyDecision_Dec, &b_D0_Hlt2TopoMu4BodyDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt2TopoMu4BodyDecision_TIS", &D0_Hlt2TopoMu4BodyDecision_TIS, &b_D0_Hlt2TopoMu4BodyDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt2TopoMu4BodyDecision_TOS", &D0_Hlt2TopoMu4BodyDecision_TOS, &b_D0_Hlt2TopoMu4BodyDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt2TopoE2BodyDecision_Dec", &D0_Hlt2TopoE2BodyDecision_Dec, &b_D0_Hlt2TopoE2BodyDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt2TopoE2BodyDecision_TIS", &D0_Hlt2TopoE2BodyDecision_TIS, &b_D0_Hlt2TopoE2BodyDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt2TopoE2BodyDecision_TOS", &D0_Hlt2TopoE2BodyDecision_TOS, &b_D0_Hlt2TopoE2BodyDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt2TopoE3BodyDecision_Dec", &D0_Hlt2TopoE3BodyDecision_Dec, &b_D0_Hlt2TopoE3BodyDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt2TopoE3BodyDecision_TIS", &D0_Hlt2TopoE3BodyDecision_TIS, &b_D0_Hlt2TopoE3BodyDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt2TopoE3BodyDecision_TOS", &D0_Hlt2TopoE3BodyDecision_TOS, &b_D0_Hlt2TopoE3BodyDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt2TopoE4BodyDecision_Dec", &D0_Hlt2TopoE4BodyDecision_Dec, &b_D0_Hlt2TopoE4BodyDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt2TopoE4BodyDecision_TIS", &D0_Hlt2TopoE4BodyDecision_TIS, &b_D0_Hlt2TopoE4BodyDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt2TopoE4BodyDecision_TOS", &D0_Hlt2TopoE4BodyDecision_TOS, &b_D0_Hlt2TopoE4BodyDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt2TopoRad2BodyDecision_Dec", &D0_Hlt2TopoRad2BodyDecision_Dec, &b_D0_Hlt2TopoRad2BodyDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt2TopoRad2BodyDecision_TIS", &D0_Hlt2TopoRad2BodyDecision_TIS, &b_D0_Hlt2TopoRad2BodyDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt2TopoRad2BodyDecision_TOS", &D0_Hlt2TopoRad2BodyDecision_TOS, &b_D0_Hlt2TopoRad2BodyDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt2SingleMuonDecision_Dec", &D0_Hlt2SingleMuonDecision_Dec, &b_D0_Hlt2SingleMuonDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt2SingleMuonDecision_TIS", &D0_Hlt2SingleMuonDecision_TIS, &b_D0_Hlt2SingleMuonDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt2SingleMuonDecision_TOS", &D0_Hlt2SingleMuonDecision_TOS, &b_D0_Hlt2SingleMuonDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt2RadiativeTopoTrackDecision_Dec", &D0_Hlt2RadiativeTopoTrackDecision_Dec, &b_D0_Hlt2RadiativeTopoTrackDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt2RadiativeTopoTrackDecision_TIS", &D0_Hlt2RadiativeTopoTrackDecision_TIS, &b_D0_Hlt2RadiativeTopoTrackDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt2RadiativeTopoTrackDecision_TOS", &D0_Hlt2RadiativeTopoTrackDecision_TOS, &b_D0_Hlt2RadiativeTopoTrackDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt2RadiativeTopoPhotonDecision_Dec", &D0_Hlt2RadiativeTopoPhotonDecision_Dec, &b_D0_Hlt2RadiativeTopoPhotonDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt2RadiativeTopoPhotonDecision_TIS", &D0_Hlt2RadiativeTopoPhotonDecision_TIS, &b_D0_Hlt2RadiativeTopoPhotonDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt2RadiativeTopoPhotonDecision_TOS", &D0_Hlt2RadiativeTopoPhotonDecision_TOS, &b_D0_Hlt2RadiativeTopoPhotonDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt2Topo2BodySimpleDecision_Dec", &D0_Hlt2Topo2BodySimpleDecision_Dec, &b_D0_Hlt2Topo2BodySimpleDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt2Topo2BodySimpleDecision_TIS", &D0_Hlt2Topo2BodySimpleDecision_TIS, &b_D0_Hlt2Topo2BodySimpleDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt2Topo2BodySimpleDecision_TOS", &D0_Hlt2Topo2BodySimpleDecision_TOS, &b_D0_Hlt2Topo2BodySimpleDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt2Topo3BodySimpleDecision_Dec", &D0_Hlt2Topo3BodySimpleDecision_Dec, &b_D0_Hlt2Topo3BodySimpleDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt2Topo3BodySimpleDecision_TIS", &D0_Hlt2Topo3BodySimpleDecision_TIS, &b_D0_Hlt2Topo3BodySimpleDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt2Topo3BodySimpleDecision_TOS", &D0_Hlt2Topo3BodySimpleDecision_TOS, &b_D0_Hlt2Topo3BodySimpleDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt2Topo4BodySimpleDecision_Dec", &D0_Hlt2Topo4BodySimpleDecision_Dec, &b_D0_Hlt2Topo4BodySimpleDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt2Topo4BodySimpleDecision_TIS", &D0_Hlt2Topo4BodySimpleDecision_TIS, &b_D0_Hlt2Topo4BodySimpleDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt2Topo4BodySimpleDecision_TOS", &D0_Hlt2Topo4BodySimpleDecision_TOS, &b_D0_Hlt2Topo4BodySimpleDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt2Topo2BodyBBDTDecision_Dec", &D0_Hlt2Topo2BodyBBDTDecision_Dec, &b_D0_Hlt2Topo2BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt2Topo2BodyBBDTDecision_TIS", &D0_Hlt2Topo2BodyBBDTDecision_TIS, &b_D0_Hlt2Topo2BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt2Topo2BodyBBDTDecision_TOS", &D0_Hlt2Topo2BodyBBDTDecision_TOS, &b_D0_Hlt2Topo2BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt2Topo3BodyBBDTDecision_Dec", &D0_Hlt2Topo3BodyBBDTDecision_Dec, &b_D0_Hlt2Topo3BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt2Topo3BodyBBDTDecision_TIS", &D0_Hlt2Topo3BodyBBDTDecision_TIS, &b_D0_Hlt2Topo3BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt2Topo3BodyBBDTDecision_TOS", &D0_Hlt2Topo3BodyBBDTDecision_TOS, &b_D0_Hlt2Topo3BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt2Topo4BodyBBDTDecision_Dec", &D0_Hlt2Topo4BodyBBDTDecision_Dec, &b_D0_Hlt2Topo4BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt2Topo4BodyBBDTDecision_TIS", &D0_Hlt2Topo4BodyBBDTDecision_TIS, &b_D0_Hlt2Topo4BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt2Topo4BodyBBDTDecision_TOS", &D0_Hlt2Topo4BodyBBDTDecision_TOS, &b_D0_Hlt2Topo4BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt2TopoE2BodyBBDTDecision_Dec", &D0_Hlt2TopoE2BodyBBDTDecision_Dec, &b_D0_Hlt2TopoE2BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt2TopoE2BodyBBDTDecision_TIS", &D0_Hlt2TopoE2BodyBBDTDecision_TIS, &b_D0_Hlt2TopoE2BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt2TopoE2BodyBBDTDecision_TOS", &D0_Hlt2TopoE2BodyBBDTDecision_TOS, &b_D0_Hlt2TopoE2BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt2TopoE3BodyBBDTDecision_Dec", &D0_Hlt2TopoE3BodyBBDTDecision_Dec, &b_D0_Hlt2TopoE3BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt2TopoE3BodyBBDTDecision_TIS", &D0_Hlt2TopoE3BodyBBDTDecision_TIS, &b_D0_Hlt2TopoE3BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt2TopoE3BodyBBDTDecision_TOS", &D0_Hlt2TopoE3BodyBBDTDecision_TOS, &b_D0_Hlt2TopoE3BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt2TopoE4BodyBBDTDecision_Dec", &D0_Hlt2TopoE4BodyBBDTDecision_Dec, &b_D0_Hlt2TopoE4BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt2TopoE4BodyBBDTDecision_TIS", &D0_Hlt2TopoE4BodyBBDTDecision_TIS, &b_D0_Hlt2TopoE4BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt2TopoE4BodyBBDTDecision_TOS", &D0_Hlt2TopoE4BodyBBDTDecision_TOS, &b_D0_Hlt2TopoE4BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt2TopoMu2BodyBBDTDecision_Dec", &D0_Hlt2TopoMu2BodyBBDTDecision_Dec, &b_D0_Hlt2TopoMu2BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt2TopoMu2BodyBBDTDecision_TIS", &D0_Hlt2TopoMu2BodyBBDTDecision_TIS, &b_D0_Hlt2TopoMu2BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt2TopoMu2BodyBBDTDecision_TOS", &D0_Hlt2TopoMu2BodyBBDTDecision_TOS, &b_D0_Hlt2TopoMu2BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt2TopoMu3BodyBBDTDecision_Dec", &D0_Hlt2TopoMu3BodyBBDTDecision_Dec, &b_D0_Hlt2TopoMu3BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt2TopoMu3BodyBBDTDecision_TIS", &D0_Hlt2TopoMu3BodyBBDTDecision_TIS, &b_D0_Hlt2TopoMu3BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt2TopoMu3BodyBBDTDecision_TOS", &D0_Hlt2TopoMu3BodyBBDTDecision_TOS, &b_D0_Hlt2TopoMu3BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt2TopoMu4BodyBBDTDecision_Dec", &D0_Hlt2TopoMu4BodyBBDTDecision_Dec, &b_D0_Hlt2TopoMu4BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt2TopoMu4BodyBBDTDecision_TIS", &D0_Hlt2TopoMu4BodyBBDTDecision_TIS, &b_D0_Hlt2TopoMu4BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt2TopoMu4BodyBBDTDecision_TOS", &D0_Hlt2TopoMu4BodyBBDTDecision_TOS, &b_D0_Hlt2TopoMu4BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt2GlobalDecision_Dec", &D0_Hlt2GlobalDecision_Dec, &b_D0_Hlt2GlobalDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt2GlobalDecision_TIS", &D0_Hlt2GlobalDecision_TIS, &b_D0_Hlt2GlobalDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt2GlobalDecision_TOS", &D0_Hlt2GlobalDecision_TOS, &b_D0_Hlt2GlobalDecision_TOS);
   fChain->SetBranchAddress("D0_Hlt2TopoRad2BodyBBDTDecision_Dec", &D0_Hlt2TopoRad2BodyBBDTDecision_Dec, &b_D0_Hlt2TopoRad2BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("D0_Hlt2TopoRad2BodyBBDTDecision_TIS", &D0_Hlt2TopoRad2BodyBBDTDecision_TIS, &b_D0_Hlt2TopoRad2BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("D0_Hlt2TopoRad2BodyBBDTDecision_TOS", &D0_Hlt2TopoRad2BodyBBDTDecision_TOS, &b_D0_Hlt2TopoRad2BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("pi_MC12TuneV2_ProbNNe", &pi_MC12TuneV2_ProbNNe, &b_pi_MC12TuneV2_ProbNNe);
   fChain->SetBranchAddress("pi_MC12TuneV2_ProbNNmu", &pi_MC12TuneV2_ProbNNmu, &b_pi_MC12TuneV2_ProbNNmu);
   fChain->SetBranchAddress("pi_MC12TuneV2_ProbNNpi", &pi_MC12TuneV2_ProbNNpi, &b_pi_MC12TuneV2_ProbNNpi);
   fChain->SetBranchAddress("pi_MC12TuneV2_ProbNNk", &pi_MC12TuneV2_ProbNNk, &b_pi_MC12TuneV2_ProbNNk);
   fChain->SetBranchAddress("pi_MC12TuneV2_ProbNNp", &pi_MC12TuneV2_ProbNNp, &b_pi_MC12TuneV2_ProbNNp);
   fChain->SetBranchAddress("pi_MC12TuneV2_ProbNNghost", &pi_MC12TuneV2_ProbNNghost, &b_pi_MC12TuneV2_ProbNNghost);
   fChain->SetBranchAddress("pi_MC12TuneV3_ProbNNe", &pi_MC12TuneV3_ProbNNe, &b_pi_MC12TuneV3_ProbNNe);
   fChain->SetBranchAddress("pi_MC12TuneV3_ProbNNmu", &pi_MC12TuneV3_ProbNNmu, &b_pi_MC12TuneV3_ProbNNmu);
   fChain->SetBranchAddress("pi_MC12TuneV3_ProbNNpi", &pi_MC12TuneV3_ProbNNpi, &b_pi_MC12TuneV3_ProbNNpi);
   fChain->SetBranchAddress("pi_MC12TuneV3_ProbNNk", &pi_MC12TuneV3_ProbNNk, &b_pi_MC12TuneV3_ProbNNk);
   fChain->SetBranchAddress("pi_MC12TuneV3_ProbNNp", &pi_MC12TuneV3_ProbNNp, &b_pi_MC12TuneV3_ProbNNp);
   fChain->SetBranchAddress("pi_MC12TuneV3_ProbNNghost", &pi_MC12TuneV3_ProbNNghost, &b_pi_MC12TuneV3_ProbNNghost);
   fChain->SetBranchAddress("pi_MC12TuneV4_ProbNNe", &pi_MC12TuneV4_ProbNNe, &b_pi_MC12TuneV4_ProbNNe);
   fChain->SetBranchAddress("pi_MC12TuneV4_ProbNNmu", &pi_MC12TuneV4_ProbNNmu, &b_pi_MC12TuneV4_ProbNNmu);
   fChain->SetBranchAddress("pi_MC12TuneV4_ProbNNpi", &pi_MC12TuneV4_ProbNNpi, &b_pi_MC12TuneV4_ProbNNpi);
   fChain->SetBranchAddress("pi_MC12TuneV4_ProbNNk", &pi_MC12TuneV4_ProbNNk, &b_pi_MC12TuneV4_ProbNNk);
   fChain->SetBranchAddress("pi_MC12TuneV4_ProbNNp", &pi_MC12TuneV4_ProbNNp, &b_pi_MC12TuneV4_ProbNNp);
   fChain->SetBranchAddress("pi_MC12TuneV4_ProbNNghost", &pi_MC12TuneV4_ProbNNghost, &b_pi_MC12TuneV4_ProbNNghost);
   fChain->SetBranchAddress("pi_MC15TuneV1_ProbNNe", &pi_MC15TuneV1_ProbNNe, &b_pi_MC15TuneV1_ProbNNe);
   fChain->SetBranchAddress("pi_MC15TuneV1_ProbNNmu", &pi_MC15TuneV1_ProbNNmu, &b_pi_MC15TuneV1_ProbNNmu);
   fChain->SetBranchAddress("pi_MC15TuneV1_ProbNNpi", &pi_MC15TuneV1_ProbNNpi, &b_pi_MC15TuneV1_ProbNNpi);
   fChain->SetBranchAddress("pi_MC15TuneV1_ProbNNk", &pi_MC15TuneV1_ProbNNk, &b_pi_MC15TuneV1_ProbNNk);
   fChain->SetBranchAddress("pi_MC15TuneV1_ProbNNp", &pi_MC15TuneV1_ProbNNp, &b_pi_MC15TuneV1_ProbNNp);
   fChain->SetBranchAddress("pi_MC15TuneV1_ProbNNghost", &pi_MC15TuneV1_ProbNNghost, &b_pi_MC15TuneV1_ProbNNghost);
   fChain->SetBranchAddress("pi_CosTheta", &pi_CosTheta, &b_pi_CosTheta);
   fChain->SetBranchAddress("pi_OWNPV_X", &pi_OWNPV_X, &b_pi_OWNPV_X);
   fChain->SetBranchAddress("pi_OWNPV_Y", &pi_OWNPV_Y, &b_pi_OWNPV_Y);
   fChain->SetBranchAddress("pi_OWNPV_Z", &pi_OWNPV_Z, &b_pi_OWNPV_Z);
   fChain->SetBranchAddress("pi_OWNPV_XERR", &pi_OWNPV_XERR, &b_pi_OWNPV_XERR);
   fChain->SetBranchAddress("pi_OWNPV_YERR", &pi_OWNPV_YERR, &b_pi_OWNPV_YERR);
   fChain->SetBranchAddress("pi_OWNPV_ZERR", &pi_OWNPV_ZERR, &b_pi_OWNPV_ZERR);
   fChain->SetBranchAddress("pi_OWNPV_CHI2", &pi_OWNPV_CHI2, &b_pi_OWNPV_CHI2);
   fChain->SetBranchAddress("pi_OWNPV_NDOF", &pi_OWNPV_NDOF, &b_pi_OWNPV_NDOF);
   fChain->SetBranchAddress("pi_OWNPV_COV_", pi_OWNPV_COV_, &b_pi_OWNPV_COV_);
   fChain->SetBranchAddress("pi_IP_OWNPV", &pi_IP_OWNPV, &b_pi_IP_OWNPV);
   fChain->SetBranchAddress("pi_IPCHI2_OWNPV", &pi_IPCHI2_OWNPV, &b_pi_IPCHI2_OWNPV);
   fChain->SetBranchAddress("pi_ORIVX_X", &pi_ORIVX_X, &b_pi_ORIVX_X);
   fChain->SetBranchAddress("pi_ORIVX_Y", &pi_ORIVX_Y, &b_pi_ORIVX_Y);
   fChain->SetBranchAddress("pi_ORIVX_Z", &pi_ORIVX_Z, &b_pi_ORIVX_Z);
   fChain->SetBranchAddress("pi_ORIVX_XERR", &pi_ORIVX_XERR, &b_pi_ORIVX_XERR);
   fChain->SetBranchAddress("pi_ORIVX_YERR", &pi_ORIVX_YERR, &b_pi_ORIVX_YERR);
   fChain->SetBranchAddress("pi_ORIVX_ZERR", &pi_ORIVX_ZERR, &b_pi_ORIVX_ZERR);
   fChain->SetBranchAddress("pi_ORIVX_CHI2", &pi_ORIVX_CHI2, &b_pi_ORIVX_CHI2);
   fChain->SetBranchAddress("pi_ORIVX_NDOF", &pi_ORIVX_NDOF, &b_pi_ORIVX_NDOF);
   fChain->SetBranchAddress("pi_ORIVX_COV_", pi_ORIVX_COV_, &b_pi_ORIVX_COV_);
   fChain->SetBranchAddress("pi_P", &pi_P, &b_pi_P);
   fChain->SetBranchAddress("pi_PT", &pi_PT, &b_pi_PT);
   fChain->SetBranchAddress("pi_PE", &pi_PE, &b_pi_PE);
   fChain->SetBranchAddress("pi_PX", &pi_PX, &b_pi_PX);
   fChain->SetBranchAddress("pi_PY", &pi_PY, &b_pi_PY);
   fChain->SetBranchAddress("pi_PZ", &pi_PZ, &b_pi_PZ);
   fChain->SetBranchAddress("pi_M", &pi_M, &b_pi_M);
   fChain->SetBranchAddress("pi_ID", &pi_ID, &b_pi_ID);
   fChain->SetBranchAddress("pi_PIDe", &pi_PIDe, &b_pi_PIDe);
   fChain->SetBranchAddress("pi_PIDmu", &pi_PIDmu, &b_pi_PIDmu);
   fChain->SetBranchAddress("pi_PIDK", &pi_PIDK, &b_pi_PIDK);
   fChain->SetBranchAddress("pi_PIDp", &pi_PIDp, &b_pi_PIDp);
   fChain->SetBranchAddress("pi_PIDd", &pi_PIDd, &b_pi_PIDd);
   fChain->SetBranchAddress("pi_ProbNNe", &pi_ProbNNe, &b_pi_ProbNNe);
   fChain->SetBranchAddress("pi_ProbNNk", &pi_ProbNNk, &b_pi_ProbNNk);
   fChain->SetBranchAddress("pi_ProbNNp", &pi_ProbNNp, &b_pi_ProbNNp);
   fChain->SetBranchAddress("pi_ProbNNpi", &pi_ProbNNpi, &b_pi_ProbNNpi);
   fChain->SetBranchAddress("pi_ProbNNmu", &pi_ProbNNmu, &b_pi_ProbNNmu);
   fChain->SetBranchAddress("pi_ProbNNd", &pi_ProbNNd, &b_pi_ProbNNd);
   fChain->SetBranchAddress("pi_ProbNNghost", &pi_ProbNNghost, &b_pi_ProbNNghost);
   fChain->SetBranchAddress("pi_hasMuon", &pi_hasMuon, &b_pi_hasMuon);
   fChain->SetBranchAddress("pi_isMuon", &pi_isMuon, &b_pi_isMuon);
   fChain->SetBranchAddress("pi_hasRich", &pi_hasRich, &b_pi_hasRich);
   fChain->SetBranchAddress("pi_UsedRichAerogel", &pi_UsedRichAerogel, &b_pi_UsedRichAerogel);
   fChain->SetBranchAddress("pi_UsedRich1Gas", &pi_UsedRich1Gas, &b_pi_UsedRich1Gas);
   fChain->SetBranchAddress("pi_UsedRich2Gas", &pi_UsedRich2Gas, &b_pi_UsedRich2Gas);
   fChain->SetBranchAddress("pi_RichAboveElThres", &pi_RichAboveElThres, &b_pi_RichAboveElThres);
   fChain->SetBranchAddress("pi_RichAboveMuThres", &pi_RichAboveMuThres, &b_pi_RichAboveMuThres);
   fChain->SetBranchAddress("pi_RichAbovePiThres", &pi_RichAbovePiThres, &b_pi_RichAbovePiThres);
   fChain->SetBranchAddress("pi_RichAboveKaThres", &pi_RichAboveKaThres, &b_pi_RichAboveKaThres);
   fChain->SetBranchAddress("pi_RichAbovePrThres", &pi_RichAbovePrThres, &b_pi_RichAbovePrThres);
   fChain->SetBranchAddress("pi_hasCalo", &pi_hasCalo, &b_pi_hasCalo);
   fChain->SetBranchAddress("pi_L0Global_Dec", &pi_L0Global_Dec, &b_pi_L0Global_Dec);
   fChain->SetBranchAddress("pi_L0Global_TIS", &pi_L0Global_TIS, &b_pi_L0Global_TIS);
   fChain->SetBranchAddress("pi_L0Global_TOS", &pi_L0Global_TOS, &b_pi_L0Global_TOS);
   fChain->SetBranchAddress("pi_Hlt1Global_Dec", &pi_Hlt1Global_Dec, &b_pi_Hlt1Global_Dec);
   fChain->SetBranchAddress("pi_Hlt1Global_TIS", &pi_Hlt1Global_TIS, &b_pi_Hlt1Global_TIS);
   fChain->SetBranchAddress("pi_Hlt1Global_TOS", &pi_Hlt1Global_TOS, &b_pi_Hlt1Global_TOS);
   fChain->SetBranchAddress("pi_Hlt1Phys_Dec", &pi_Hlt1Phys_Dec, &b_pi_Hlt1Phys_Dec);
   fChain->SetBranchAddress("pi_Hlt1Phys_TIS", &pi_Hlt1Phys_TIS, &b_pi_Hlt1Phys_TIS);
   fChain->SetBranchAddress("pi_Hlt1Phys_TOS", &pi_Hlt1Phys_TOS, &b_pi_Hlt1Phys_TOS);
   fChain->SetBranchAddress("pi_Hlt2Global_Dec", &pi_Hlt2Global_Dec, &b_pi_Hlt2Global_Dec);
   fChain->SetBranchAddress("pi_Hlt2Global_TIS", &pi_Hlt2Global_TIS, &b_pi_Hlt2Global_TIS);
   fChain->SetBranchAddress("pi_Hlt2Global_TOS", &pi_Hlt2Global_TOS, &b_pi_Hlt2Global_TOS);
   fChain->SetBranchAddress("pi_Hlt2Phys_Dec", &pi_Hlt2Phys_Dec, &b_pi_Hlt2Phys_Dec);
   fChain->SetBranchAddress("pi_Hlt2Phys_TIS", &pi_Hlt2Phys_TIS, &b_pi_Hlt2Phys_TIS);
   fChain->SetBranchAddress("pi_Hlt2Phys_TOS", &pi_Hlt2Phys_TOS, &b_pi_Hlt2Phys_TOS);
   fChain->SetBranchAddress("pi_TRACK_Type", &pi_TRACK_Type, &b_pi_TRACK_Type);
   fChain->SetBranchAddress("pi_TRACK_Key", &pi_TRACK_Key, &b_pi_TRACK_Key);
   fChain->SetBranchAddress("pi_TRACK_CHI2NDOF", &pi_TRACK_CHI2NDOF, &b_pi_TRACK_CHI2NDOF);
   fChain->SetBranchAddress("pi_TRACK_PCHI2", &pi_TRACK_PCHI2, &b_pi_TRACK_PCHI2);
   fChain->SetBranchAddress("pi_TRACK_MatchCHI2", &pi_TRACK_MatchCHI2, &b_pi_TRACK_MatchCHI2);
   fChain->SetBranchAddress("pi_TRACK_GhostProb", &pi_TRACK_GhostProb, &b_pi_TRACK_GhostProb);
   fChain->SetBranchAddress("pi_TRACK_CloneDist", &pi_TRACK_CloneDist, &b_pi_TRACK_CloneDist);
   fChain->SetBranchAddress("pi_TRACK_Likelihood", &pi_TRACK_Likelihood, &b_pi_TRACK_Likelihood);
   fChain->SetBranchAddress("pi_ETA", &pi_ETA, &b_pi_ETA);
   fChain->SetBranchAddress("pi_KL", &pi_KL, &b_pi_KL);
   fChain->SetBranchAddress("pi_PHI", &pi_PHI, &b_pi_PHI);
   fChain->SetBranchAddress("pi_Y", &pi_Y, &b_pi_Y);
   fChain->SetBranchAddress("K_MC12TuneV2_ProbNNe", &K_MC12TuneV2_ProbNNe, &b_K_MC12TuneV2_ProbNNe);
   fChain->SetBranchAddress("K_MC12TuneV2_ProbNNmu", &K_MC12TuneV2_ProbNNmu, &b_K_MC12TuneV2_ProbNNmu);
   fChain->SetBranchAddress("K_MC12TuneV2_ProbNNpi", &K_MC12TuneV2_ProbNNpi, &b_K_MC12TuneV2_ProbNNpi);
   fChain->SetBranchAddress("K_MC12TuneV2_ProbNNk", &K_MC12TuneV2_ProbNNk, &b_K_MC12TuneV2_ProbNNk);
   fChain->SetBranchAddress("K_MC12TuneV2_ProbNNp", &K_MC12TuneV2_ProbNNp, &b_K_MC12TuneV2_ProbNNp);
   fChain->SetBranchAddress("K_MC12TuneV2_ProbNNghost", &K_MC12TuneV2_ProbNNghost, &b_K_MC12TuneV2_ProbNNghost);
   fChain->SetBranchAddress("K_MC12TuneV3_ProbNNe", &K_MC12TuneV3_ProbNNe, &b_K_MC12TuneV3_ProbNNe);
   fChain->SetBranchAddress("K_MC12TuneV3_ProbNNmu", &K_MC12TuneV3_ProbNNmu, &b_K_MC12TuneV3_ProbNNmu);
   fChain->SetBranchAddress("K_MC12TuneV3_ProbNNpi", &K_MC12TuneV3_ProbNNpi, &b_K_MC12TuneV3_ProbNNpi);
   fChain->SetBranchAddress("K_MC12TuneV3_ProbNNk", &K_MC12TuneV3_ProbNNk, &b_K_MC12TuneV3_ProbNNk);
   fChain->SetBranchAddress("K_MC12TuneV3_ProbNNp", &K_MC12TuneV3_ProbNNp, &b_K_MC12TuneV3_ProbNNp);
   fChain->SetBranchAddress("K_MC12TuneV3_ProbNNghost", &K_MC12TuneV3_ProbNNghost, &b_K_MC12TuneV3_ProbNNghost);
   fChain->SetBranchAddress("K_MC12TuneV4_ProbNNe", &K_MC12TuneV4_ProbNNe, &b_K_MC12TuneV4_ProbNNe);
   fChain->SetBranchAddress("K_MC12TuneV4_ProbNNmu", &K_MC12TuneV4_ProbNNmu, &b_K_MC12TuneV4_ProbNNmu);
   fChain->SetBranchAddress("K_MC12TuneV4_ProbNNpi", &K_MC12TuneV4_ProbNNpi, &b_K_MC12TuneV4_ProbNNpi);
   fChain->SetBranchAddress("K_MC12TuneV4_ProbNNk", &K_MC12TuneV4_ProbNNk, &b_K_MC12TuneV4_ProbNNk);
   fChain->SetBranchAddress("K_MC12TuneV4_ProbNNp", &K_MC12TuneV4_ProbNNp, &b_K_MC12TuneV4_ProbNNp);
   fChain->SetBranchAddress("K_MC12TuneV4_ProbNNghost", &K_MC12TuneV4_ProbNNghost, &b_K_MC12TuneV4_ProbNNghost);
   fChain->SetBranchAddress("K_MC15TuneV1_ProbNNe", &K_MC15TuneV1_ProbNNe, &b_K_MC15TuneV1_ProbNNe);
   fChain->SetBranchAddress("K_MC15TuneV1_ProbNNmu", &K_MC15TuneV1_ProbNNmu, &b_K_MC15TuneV1_ProbNNmu);
   fChain->SetBranchAddress("K_MC15TuneV1_ProbNNpi", &K_MC15TuneV1_ProbNNpi, &b_K_MC15TuneV1_ProbNNpi);
   fChain->SetBranchAddress("K_MC15TuneV1_ProbNNk", &K_MC15TuneV1_ProbNNk, &b_K_MC15TuneV1_ProbNNk);
   fChain->SetBranchAddress("K_MC15TuneV1_ProbNNp", &K_MC15TuneV1_ProbNNp, &b_K_MC15TuneV1_ProbNNp);
   fChain->SetBranchAddress("K_MC15TuneV1_ProbNNghost", &K_MC15TuneV1_ProbNNghost, &b_K_MC15TuneV1_ProbNNghost);
   fChain->SetBranchAddress("K_CosTheta", &K_CosTheta, &b_K_CosTheta);
   fChain->SetBranchAddress("K_OWNPV_X", &K_OWNPV_X, &b_K_OWNPV_X);
   fChain->SetBranchAddress("K_OWNPV_Y", &K_OWNPV_Y, &b_K_OWNPV_Y);
   fChain->SetBranchAddress("K_OWNPV_Z", &K_OWNPV_Z, &b_K_OWNPV_Z);
   fChain->SetBranchAddress("K_OWNPV_XERR", &K_OWNPV_XERR, &b_K_OWNPV_XERR);
   fChain->SetBranchAddress("K_OWNPV_YERR", &K_OWNPV_YERR, &b_K_OWNPV_YERR);
   fChain->SetBranchAddress("K_OWNPV_ZERR", &K_OWNPV_ZERR, &b_K_OWNPV_ZERR);
   fChain->SetBranchAddress("K_OWNPV_CHI2", &K_OWNPV_CHI2, &b_K_OWNPV_CHI2);
   fChain->SetBranchAddress("K_OWNPV_NDOF", &K_OWNPV_NDOF, &b_K_OWNPV_NDOF);
   fChain->SetBranchAddress("K_OWNPV_COV_", K_OWNPV_COV_, &b_K_OWNPV_COV_);
   fChain->SetBranchAddress("K_IP_OWNPV", &K_IP_OWNPV, &b_K_IP_OWNPV);
   fChain->SetBranchAddress("K_IPCHI2_OWNPV", &K_IPCHI2_OWNPV, &b_K_IPCHI2_OWNPV);
   fChain->SetBranchAddress("K_ORIVX_X", &K_ORIVX_X, &b_K_ORIVX_X);
   fChain->SetBranchAddress("K_ORIVX_Y", &K_ORIVX_Y, &b_K_ORIVX_Y);
   fChain->SetBranchAddress("K_ORIVX_Z", &K_ORIVX_Z, &b_K_ORIVX_Z);
   fChain->SetBranchAddress("K_ORIVX_XERR", &K_ORIVX_XERR, &b_K_ORIVX_XERR);
   fChain->SetBranchAddress("K_ORIVX_YERR", &K_ORIVX_YERR, &b_K_ORIVX_YERR);
   fChain->SetBranchAddress("K_ORIVX_ZERR", &K_ORIVX_ZERR, &b_K_ORIVX_ZERR);
   fChain->SetBranchAddress("K_ORIVX_CHI2", &K_ORIVX_CHI2, &b_K_ORIVX_CHI2);
   fChain->SetBranchAddress("K_ORIVX_NDOF", &K_ORIVX_NDOF, &b_K_ORIVX_NDOF);
   fChain->SetBranchAddress("K_ORIVX_COV_", K_ORIVX_COV_, &b_K_ORIVX_COV_);
   fChain->SetBranchAddress("K_P", &K_P, &b_K_P);
   fChain->SetBranchAddress("K_PT", &K_PT, &b_K_PT);
   fChain->SetBranchAddress("K_PE", &K_PE, &b_K_PE);
   fChain->SetBranchAddress("K_PX", &K_PX, &b_K_PX);
   fChain->SetBranchAddress("K_PY", &K_PY, &b_K_PY);
   fChain->SetBranchAddress("K_PZ", &K_PZ, &b_K_PZ);
   fChain->SetBranchAddress("K_M", &K_M, &b_K_M);
   fChain->SetBranchAddress("K_ID", &K_ID, &b_K_ID);
   fChain->SetBranchAddress("K_PIDe", &K_PIDe, &b_K_PIDe);
   fChain->SetBranchAddress("K_PIDmu", &K_PIDmu, &b_K_PIDmu);
   fChain->SetBranchAddress("K_PIDK", &K_PIDK, &b_K_PIDK);
   fChain->SetBranchAddress("K_PIDp", &K_PIDp, &b_K_PIDp);
   fChain->SetBranchAddress("K_PIDd", &K_PIDd, &b_K_PIDd);
   fChain->SetBranchAddress("K_ProbNNe", &K_ProbNNe, &b_K_ProbNNe);
   fChain->SetBranchAddress("K_ProbNNk", &K_ProbNNk, &b_K_ProbNNk);
   fChain->SetBranchAddress("K_ProbNNp", &K_ProbNNp, &b_K_ProbNNp);
   fChain->SetBranchAddress("K_ProbNNpi", &K_ProbNNpi, &b_K_ProbNNpi);
   fChain->SetBranchAddress("K_ProbNNmu", &K_ProbNNmu, &b_K_ProbNNmu);
   fChain->SetBranchAddress("K_ProbNNd", &K_ProbNNd, &b_K_ProbNNd);
   fChain->SetBranchAddress("K_ProbNNghost", &K_ProbNNghost, &b_K_ProbNNghost);
   fChain->SetBranchAddress("K_hasMuon", &K_hasMuon, &b_K_hasMuon);
   fChain->SetBranchAddress("K_isMuon", &K_isMuon, &b_K_isMuon);
   fChain->SetBranchAddress("K_hasRich", &K_hasRich, &b_K_hasRich);
   fChain->SetBranchAddress("K_UsedRichAerogel", &K_UsedRichAerogel, &b_K_UsedRichAerogel);
   fChain->SetBranchAddress("K_UsedRich1Gas", &K_UsedRich1Gas, &b_K_UsedRich1Gas);
   fChain->SetBranchAddress("K_UsedRich2Gas", &K_UsedRich2Gas, &b_K_UsedRich2Gas);
   fChain->SetBranchAddress("K_RichAboveElThres", &K_RichAboveElThres, &b_K_RichAboveElThres);
   fChain->SetBranchAddress("K_RichAboveMuThres", &K_RichAboveMuThres, &b_K_RichAboveMuThres);
   fChain->SetBranchAddress("K_RichAbovePiThres", &K_RichAbovePiThres, &b_K_RichAbovePiThres);
   fChain->SetBranchAddress("K_RichAboveKaThres", &K_RichAboveKaThres, &b_K_RichAboveKaThres);
   fChain->SetBranchAddress("K_RichAbovePrThres", &K_RichAbovePrThres, &b_K_RichAbovePrThres);
   fChain->SetBranchAddress("K_hasCalo", &K_hasCalo, &b_K_hasCalo);
   fChain->SetBranchAddress("K_L0Global_Dec", &K_L0Global_Dec, &b_K_L0Global_Dec);
   fChain->SetBranchAddress("K_L0Global_TIS", &K_L0Global_TIS, &b_K_L0Global_TIS);
   fChain->SetBranchAddress("K_L0Global_TOS", &K_L0Global_TOS, &b_K_L0Global_TOS);
   fChain->SetBranchAddress("K_Hlt1Global_Dec", &K_Hlt1Global_Dec, &b_K_Hlt1Global_Dec);
   fChain->SetBranchAddress("K_Hlt1Global_TIS", &K_Hlt1Global_TIS, &b_K_Hlt1Global_TIS);
   fChain->SetBranchAddress("K_Hlt1Global_TOS", &K_Hlt1Global_TOS, &b_K_Hlt1Global_TOS);
   fChain->SetBranchAddress("K_Hlt1Phys_Dec", &K_Hlt1Phys_Dec, &b_K_Hlt1Phys_Dec);
   fChain->SetBranchAddress("K_Hlt1Phys_TIS", &K_Hlt1Phys_TIS, &b_K_Hlt1Phys_TIS);
   fChain->SetBranchAddress("K_Hlt1Phys_TOS", &K_Hlt1Phys_TOS, &b_K_Hlt1Phys_TOS);
   fChain->SetBranchAddress("K_Hlt2Global_Dec", &K_Hlt2Global_Dec, &b_K_Hlt2Global_Dec);
   fChain->SetBranchAddress("K_Hlt2Global_TIS", &K_Hlt2Global_TIS, &b_K_Hlt2Global_TIS);
   fChain->SetBranchAddress("K_Hlt2Global_TOS", &K_Hlt2Global_TOS, &b_K_Hlt2Global_TOS);
   fChain->SetBranchAddress("K_Hlt2Phys_Dec", &K_Hlt2Phys_Dec, &b_K_Hlt2Phys_Dec);
   fChain->SetBranchAddress("K_Hlt2Phys_TIS", &K_Hlt2Phys_TIS, &b_K_Hlt2Phys_TIS);
   fChain->SetBranchAddress("K_Hlt2Phys_TOS", &K_Hlt2Phys_TOS, &b_K_Hlt2Phys_TOS);
   fChain->SetBranchAddress("K_TRACK_Type", &K_TRACK_Type, &b_K_TRACK_Type);
   fChain->SetBranchAddress("K_TRACK_Key", &K_TRACK_Key, &b_K_TRACK_Key);
   fChain->SetBranchAddress("K_TRACK_CHI2NDOF", &K_TRACK_CHI2NDOF, &b_K_TRACK_CHI2NDOF);
   fChain->SetBranchAddress("K_TRACK_PCHI2", &K_TRACK_PCHI2, &b_K_TRACK_PCHI2);
   fChain->SetBranchAddress("K_TRACK_MatchCHI2", &K_TRACK_MatchCHI2, &b_K_TRACK_MatchCHI2);
   fChain->SetBranchAddress("K_TRACK_GhostProb", &K_TRACK_GhostProb, &b_K_TRACK_GhostProb);
   fChain->SetBranchAddress("K_TRACK_CloneDist", &K_TRACK_CloneDist, &b_K_TRACK_CloneDist);
   fChain->SetBranchAddress("K_TRACK_Likelihood", &K_TRACK_Likelihood, &b_K_TRACK_Likelihood);
   fChain->SetBranchAddress("K_ETA", &K_ETA, &b_K_ETA);
   fChain->SetBranchAddress("K_KL", &K_KL, &b_K_KL);
   fChain->SetBranchAddress("K_PHI", &K_PHI, &b_K_PHI);
   fChain->SetBranchAddress("K_Y", &K_Y, &b_K_Y);
   fChain->SetBranchAddress("R_CosTheta", &R_CosTheta, &b_R_CosTheta);
   fChain->SetBranchAddress("R_ENDVERTEX_X", &R_ENDVERTEX_X, &b_R_ENDVERTEX_X);
   fChain->SetBranchAddress("R_ENDVERTEX_Y", &R_ENDVERTEX_Y, &b_R_ENDVERTEX_Y);
   fChain->SetBranchAddress("R_ENDVERTEX_Z", &R_ENDVERTEX_Z, &b_R_ENDVERTEX_Z);
   fChain->SetBranchAddress("R_ENDVERTEX_XERR", &R_ENDVERTEX_XERR, &b_R_ENDVERTEX_XERR);
   fChain->SetBranchAddress("R_ENDVERTEX_YERR", &R_ENDVERTEX_YERR, &b_R_ENDVERTEX_YERR);
   fChain->SetBranchAddress("R_ENDVERTEX_ZERR", &R_ENDVERTEX_ZERR, &b_R_ENDVERTEX_ZERR);
   fChain->SetBranchAddress("R_ENDVERTEX_CHI2", &R_ENDVERTEX_CHI2, &b_R_ENDVERTEX_CHI2);
   fChain->SetBranchAddress("R_ENDVERTEX_NDOF", &R_ENDVERTEX_NDOF, &b_R_ENDVERTEX_NDOF);
   fChain->SetBranchAddress("R_ENDVERTEX_COV_", R_ENDVERTEX_COV_, &b_R_ENDVERTEX_COV_);
   fChain->SetBranchAddress("R_OWNPV_X", &R_OWNPV_X, &b_R_OWNPV_X);
   fChain->SetBranchAddress("R_OWNPV_Y", &R_OWNPV_Y, &b_R_OWNPV_Y);
   fChain->SetBranchAddress("R_OWNPV_Z", &R_OWNPV_Z, &b_R_OWNPV_Z);
   fChain->SetBranchAddress("R_OWNPV_XERR", &R_OWNPV_XERR, &b_R_OWNPV_XERR);
   fChain->SetBranchAddress("R_OWNPV_YERR", &R_OWNPV_YERR, &b_R_OWNPV_YERR);
   fChain->SetBranchAddress("R_OWNPV_ZERR", &R_OWNPV_ZERR, &b_R_OWNPV_ZERR);
   fChain->SetBranchAddress("R_OWNPV_CHI2", &R_OWNPV_CHI2, &b_R_OWNPV_CHI2);
   fChain->SetBranchAddress("R_OWNPV_NDOF", &R_OWNPV_NDOF, &b_R_OWNPV_NDOF);
   fChain->SetBranchAddress("R_OWNPV_COV_", R_OWNPV_COV_, &b_R_OWNPV_COV_);
   fChain->SetBranchAddress("R_IP_OWNPV", &R_IP_OWNPV, &b_R_IP_OWNPV);
   fChain->SetBranchAddress("R_IPCHI2_OWNPV", &R_IPCHI2_OWNPV, &b_R_IPCHI2_OWNPV);
   fChain->SetBranchAddress("R_FD_OWNPV", &R_FD_OWNPV, &b_R_FD_OWNPV);
   fChain->SetBranchAddress("R_FDCHI2_OWNPV", &R_FDCHI2_OWNPV, &b_R_FDCHI2_OWNPV);
   fChain->SetBranchAddress("R_DIRA_OWNPV", &R_DIRA_OWNPV, &b_R_DIRA_OWNPV);
   fChain->SetBranchAddress("R_ORIVX_X", &R_ORIVX_X, &b_R_ORIVX_X);
   fChain->SetBranchAddress("R_ORIVX_Y", &R_ORIVX_Y, &b_R_ORIVX_Y);
   fChain->SetBranchAddress("R_ORIVX_Z", &R_ORIVX_Z, &b_R_ORIVX_Z);
   fChain->SetBranchAddress("R_ORIVX_XERR", &R_ORIVX_XERR, &b_R_ORIVX_XERR);
   fChain->SetBranchAddress("R_ORIVX_YERR", &R_ORIVX_YERR, &b_R_ORIVX_YERR);
   fChain->SetBranchAddress("R_ORIVX_ZERR", &R_ORIVX_ZERR, &b_R_ORIVX_ZERR);
   fChain->SetBranchAddress("R_ORIVX_CHI2", &R_ORIVX_CHI2, &b_R_ORIVX_CHI2);
   fChain->SetBranchAddress("R_ORIVX_NDOF", &R_ORIVX_NDOF, &b_R_ORIVX_NDOF);
   fChain->SetBranchAddress("R_ORIVX_COV_", R_ORIVX_COV_, &b_R_ORIVX_COV_);
   fChain->SetBranchAddress("R_FD_ORIVX", &R_FD_ORIVX, &b_R_FD_ORIVX);
   fChain->SetBranchAddress("R_FDCHI2_ORIVX", &R_FDCHI2_ORIVX, &b_R_FDCHI2_ORIVX);
   fChain->SetBranchAddress("R_DIRA_ORIVX", &R_DIRA_ORIVX, &b_R_DIRA_ORIVX);
   fChain->SetBranchAddress("R_P", &R_P, &b_R_P);
   fChain->SetBranchAddress("R_PT", &R_PT, &b_R_PT);
   fChain->SetBranchAddress("R_PE", &R_PE, &b_R_PE);
   fChain->SetBranchAddress("R_PX", &R_PX, &b_R_PX);
   fChain->SetBranchAddress("R_PY", &R_PY, &b_R_PY);
   fChain->SetBranchAddress("R_PZ", &R_PZ, &b_R_PZ);
   fChain->SetBranchAddress("R_MM", &R_MM, &b_R_MM);
   fChain->SetBranchAddress("R_MMERR", &R_MMERR, &b_R_MMERR);
   fChain->SetBranchAddress("R_M", &R_M, &b_R_M);
   fChain->SetBranchAddress("R_ID", &R_ID, &b_R_ID);
   fChain->SetBranchAddress("R_TAU", &R_TAU, &b_R_TAU);
   fChain->SetBranchAddress("R_TAUERR", &R_TAUERR, &b_R_TAUERR);
   fChain->SetBranchAddress("R_TAUCHI2", &R_TAUCHI2, &b_R_TAUCHI2);
   fChain->SetBranchAddress("R_L0Global_Dec", &R_L0Global_Dec, &b_R_L0Global_Dec);
   fChain->SetBranchAddress("R_L0Global_TIS", &R_L0Global_TIS, &b_R_L0Global_TIS);
   fChain->SetBranchAddress("R_L0Global_TOS", &R_L0Global_TOS, &b_R_L0Global_TOS);
   fChain->SetBranchAddress("R_Hlt1Global_Dec", &R_Hlt1Global_Dec, &b_R_Hlt1Global_Dec);
   fChain->SetBranchAddress("R_Hlt1Global_TIS", &R_Hlt1Global_TIS, &b_R_Hlt1Global_TIS);
   fChain->SetBranchAddress("R_Hlt1Global_TOS", &R_Hlt1Global_TOS, &b_R_Hlt1Global_TOS);
   fChain->SetBranchAddress("R_Hlt1Phys_Dec", &R_Hlt1Phys_Dec, &b_R_Hlt1Phys_Dec);
   fChain->SetBranchAddress("R_Hlt1Phys_TIS", &R_Hlt1Phys_TIS, &b_R_Hlt1Phys_TIS);
   fChain->SetBranchAddress("R_Hlt1Phys_TOS", &R_Hlt1Phys_TOS, &b_R_Hlt1Phys_TOS);
   fChain->SetBranchAddress("R_Hlt2Global_Dec", &R_Hlt2Global_Dec, &b_R_Hlt2Global_Dec);
   fChain->SetBranchAddress("R_Hlt2Global_TIS", &R_Hlt2Global_TIS, &b_R_Hlt2Global_TIS);
   fChain->SetBranchAddress("R_Hlt2Global_TOS", &R_Hlt2Global_TOS, &b_R_Hlt2Global_TOS);
   fChain->SetBranchAddress("R_Hlt2Phys_Dec", &R_Hlt2Phys_Dec, &b_R_Hlt2Phys_Dec);
   fChain->SetBranchAddress("R_Hlt2Phys_TIS", &R_Hlt2Phys_TIS, &b_R_Hlt2Phys_TIS);
   fChain->SetBranchAddress("R_Hlt2Phys_TOS", &R_Hlt2Phys_TOS, &b_R_Hlt2Phys_TOS);
   fChain->SetBranchAddress("R_NumVtxWithinChi2WindowOneTrack", &R_NumVtxWithinChi2WindowOneTrack, &b_R_NumVtxWithinChi2WindowOneTrack);
   fChain->SetBranchAddress("R_SmallestDeltaChi2OneTrack", &R_SmallestDeltaChi2OneTrack, &b_R_SmallestDeltaChi2OneTrack);
   fChain->SetBranchAddress("R_SmallestDeltaChi2MassOneTrack", &R_SmallestDeltaChi2MassOneTrack, &b_R_SmallestDeltaChi2MassOneTrack);
   fChain->SetBranchAddress("R_SmallestDeltaChi2TwoTracks", &R_SmallestDeltaChi2TwoTracks, &b_R_SmallestDeltaChi2TwoTracks);
   fChain->SetBranchAddress("R_SmallestDeltaChi2MassTwoTracks", &R_SmallestDeltaChi2MassTwoTracks, &b_R_SmallestDeltaChi2MassTwoTracks);
   fChain->SetBranchAddress("R_ETA", &R_ETA, &b_R_ETA);
   fChain->SetBranchAddress("R_LOKI_DIRA", &R_LOKI_DIRA, &b_R_LOKI_DIRA);
   fChain->SetBranchAddress("R_LOKI_FDCHI2", &R_LOKI_FDCHI2, &b_R_LOKI_FDCHI2);
   fChain->SetBranchAddress("R_LOKI_FDS", &R_LOKI_FDS, &b_R_LOKI_FDS);
   fChain->SetBranchAddress("R_LOKI_LV01", &R_LOKI_LV01, &b_R_LOKI_LV01);
   fChain->SetBranchAddress("R_LOKI_LV02", &R_LOKI_LV02, &b_R_LOKI_LV02);
   fChain->SetBranchAddress("R_LOKI_MAXDOCA", &R_LOKI_MAXDOCA, &b_R_LOKI_MAXDOCA);
   fChain->SetBranchAddress("R_LOKI_TAU", &R_LOKI_TAU, &b_R_LOKI_TAU);
   fChain->SetBranchAddress("R_PHI", &R_PHI, &b_R_PHI);
   fChain->SetBranchAddress("R_Y", &R_Y, &b_R_Y);
   fChain->SetBranchAddress("pp_MC12TuneV2_ProbNNe", &pp_MC12TuneV2_ProbNNe, &b_pp_MC12TuneV2_ProbNNe);
   fChain->SetBranchAddress("pp_MC12TuneV2_ProbNNmu", &pp_MC12TuneV2_ProbNNmu, &b_pp_MC12TuneV2_ProbNNmu);
   fChain->SetBranchAddress("pp_MC12TuneV2_ProbNNpi", &pp_MC12TuneV2_ProbNNpi, &b_pp_MC12TuneV2_ProbNNpi);
   fChain->SetBranchAddress("pp_MC12TuneV2_ProbNNk", &pp_MC12TuneV2_ProbNNk, &b_pp_MC12TuneV2_ProbNNk);
   fChain->SetBranchAddress("pp_MC12TuneV2_ProbNNp", &pp_MC12TuneV2_ProbNNp, &b_pp_MC12TuneV2_ProbNNp);
   fChain->SetBranchAddress("pp_MC12TuneV2_ProbNNghost", &pp_MC12TuneV2_ProbNNghost, &b_pp_MC12TuneV2_ProbNNghost);
   fChain->SetBranchAddress("pp_MC12TuneV3_ProbNNe", &pp_MC12TuneV3_ProbNNe, &b_pp_MC12TuneV3_ProbNNe);
   fChain->SetBranchAddress("pp_MC12TuneV3_ProbNNmu", &pp_MC12TuneV3_ProbNNmu, &b_pp_MC12TuneV3_ProbNNmu);
   fChain->SetBranchAddress("pp_MC12TuneV3_ProbNNpi", &pp_MC12TuneV3_ProbNNpi, &b_pp_MC12TuneV3_ProbNNpi);
   fChain->SetBranchAddress("pp_MC12TuneV3_ProbNNk", &pp_MC12TuneV3_ProbNNk, &b_pp_MC12TuneV3_ProbNNk);
   fChain->SetBranchAddress("pp_MC12TuneV3_ProbNNp", &pp_MC12TuneV3_ProbNNp, &b_pp_MC12TuneV3_ProbNNp);
   fChain->SetBranchAddress("pp_MC12TuneV3_ProbNNghost", &pp_MC12TuneV3_ProbNNghost, &b_pp_MC12TuneV3_ProbNNghost);
   fChain->SetBranchAddress("pp_MC12TuneV4_ProbNNe", &pp_MC12TuneV4_ProbNNe, &b_pp_MC12TuneV4_ProbNNe);
   fChain->SetBranchAddress("pp_MC12TuneV4_ProbNNmu", &pp_MC12TuneV4_ProbNNmu, &b_pp_MC12TuneV4_ProbNNmu);
   fChain->SetBranchAddress("pp_MC12TuneV4_ProbNNpi", &pp_MC12TuneV4_ProbNNpi, &b_pp_MC12TuneV4_ProbNNpi);
   fChain->SetBranchAddress("pp_MC12TuneV4_ProbNNk", &pp_MC12TuneV4_ProbNNk, &b_pp_MC12TuneV4_ProbNNk);
   fChain->SetBranchAddress("pp_MC12TuneV4_ProbNNp", &pp_MC12TuneV4_ProbNNp, &b_pp_MC12TuneV4_ProbNNp);
   fChain->SetBranchAddress("pp_MC12TuneV4_ProbNNghost", &pp_MC12TuneV4_ProbNNghost, &b_pp_MC12TuneV4_ProbNNghost);
   fChain->SetBranchAddress("pp_MC15TuneV1_ProbNNe", &pp_MC15TuneV1_ProbNNe, &b_pp_MC15TuneV1_ProbNNe);
   fChain->SetBranchAddress("pp_MC15TuneV1_ProbNNmu", &pp_MC15TuneV1_ProbNNmu, &b_pp_MC15TuneV1_ProbNNmu);
   fChain->SetBranchAddress("pp_MC15TuneV1_ProbNNpi", &pp_MC15TuneV1_ProbNNpi, &b_pp_MC15TuneV1_ProbNNpi);
   fChain->SetBranchAddress("pp_MC15TuneV1_ProbNNk", &pp_MC15TuneV1_ProbNNk, &b_pp_MC15TuneV1_ProbNNk);
   fChain->SetBranchAddress("pp_MC15TuneV1_ProbNNp", &pp_MC15TuneV1_ProbNNp, &b_pp_MC15TuneV1_ProbNNp);
   fChain->SetBranchAddress("pp_MC15TuneV1_ProbNNghost", &pp_MC15TuneV1_ProbNNghost, &b_pp_MC15TuneV1_ProbNNghost);
   fChain->SetBranchAddress("pp_CosTheta", &pp_CosTheta, &b_pp_CosTheta);
   fChain->SetBranchAddress("pp_OWNPV_X", &pp_OWNPV_X, &b_pp_OWNPV_X);
   fChain->SetBranchAddress("pp_OWNPV_Y", &pp_OWNPV_Y, &b_pp_OWNPV_Y);
   fChain->SetBranchAddress("pp_OWNPV_Z", &pp_OWNPV_Z, &b_pp_OWNPV_Z);
   fChain->SetBranchAddress("pp_OWNPV_XERR", &pp_OWNPV_XERR, &b_pp_OWNPV_XERR);
   fChain->SetBranchAddress("pp_OWNPV_YERR", &pp_OWNPV_YERR, &b_pp_OWNPV_YERR);
   fChain->SetBranchAddress("pp_OWNPV_ZERR", &pp_OWNPV_ZERR, &b_pp_OWNPV_ZERR);
   fChain->SetBranchAddress("pp_OWNPV_CHI2", &pp_OWNPV_CHI2, &b_pp_OWNPV_CHI2);
   fChain->SetBranchAddress("pp_OWNPV_NDOF", &pp_OWNPV_NDOF, &b_pp_OWNPV_NDOF);
   fChain->SetBranchAddress("pp_OWNPV_COV_", pp_OWNPV_COV_, &b_pp_OWNPV_COV_);
   fChain->SetBranchAddress("pp_IP_OWNPV", &pp_IP_OWNPV, &b_pp_IP_OWNPV);
   fChain->SetBranchAddress("pp_IPCHI2_OWNPV", &pp_IPCHI2_OWNPV, &b_pp_IPCHI2_OWNPV);
   fChain->SetBranchAddress("pp_ORIVX_X", &pp_ORIVX_X, &b_pp_ORIVX_X);
   fChain->SetBranchAddress("pp_ORIVX_Y", &pp_ORIVX_Y, &b_pp_ORIVX_Y);
   fChain->SetBranchAddress("pp_ORIVX_Z", &pp_ORIVX_Z, &b_pp_ORIVX_Z);
   fChain->SetBranchAddress("pp_ORIVX_XERR", &pp_ORIVX_XERR, &b_pp_ORIVX_XERR);
   fChain->SetBranchAddress("pp_ORIVX_YERR", &pp_ORIVX_YERR, &b_pp_ORIVX_YERR);
   fChain->SetBranchAddress("pp_ORIVX_ZERR", &pp_ORIVX_ZERR, &b_pp_ORIVX_ZERR);
   fChain->SetBranchAddress("pp_ORIVX_CHI2", &pp_ORIVX_CHI2, &b_pp_ORIVX_CHI2);
   fChain->SetBranchAddress("pp_ORIVX_NDOF", &pp_ORIVX_NDOF, &b_pp_ORIVX_NDOF);
   fChain->SetBranchAddress("pp_ORIVX_COV_", pp_ORIVX_COV_, &b_pp_ORIVX_COV_);
   fChain->SetBranchAddress("pp_P", &pp_P, &b_pp_P);
   fChain->SetBranchAddress("pp_PT", &pp_PT, &b_pp_PT);
   fChain->SetBranchAddress("pp_PE", &pp_PE, &b_pp_PE);
   fChain->SetBranchAddress("pp_PX", &pp_PX, &b_pp_PX);
   fChain->SetBranchAddress("pp_PY", &pp_PY, &b_pp_PY);
   fChain->SetBranchAddress("pp_PZ", &pp_PZ, &b_pp_PZ);
   fChain->SetBranchAddress("pp_M", &pp_M, &b_pp_M);
   fChain->SetBranchAddress("pp_ID", &pp_ID, &b_pp_ID);
   fChain->SetBranchAddress("pp_PIDe", &pp_PIDe, &b_pp_PIDe);
   fChain->SetBranchAddress("pp_PIDmu", &pp_PIDmu, &b_pp_PIDmu);
   fChain->SetBranchAddress("pp_PIDK", &pp_PIDK, &b_pp_PIDK);
   fChain->SetBranchAddress("pp_PIDp", &pp_PIDp, &b_pp_PIDp);
   fChain->SetBranchAddress("pp_PIDd", &pp_PIDd, &b_pp_PIDd);
   fChain->SetBranchAddress("pp_ProbNNe", &pp_ProbNNe, &b_pp_ProbNNe);
   fChain->SetBranchAddress("pp_ProbNNk", &pp_ProbNNk, &b_pp_ProbNNk);
   fChain->SetBranchAddress("pp_ProbNNp", &pp_ProbNNp, &b_pp_ProbNNp);
   fChain->SetBranchAddress("pp_ProbNNpi", &pp_ProbNNpi, &b_pp_ProbNNpi);
   fChain->SetBranchAddress("pp_ProbNNmu", &pp_ProbNNmu, &b_pp_ProbNNmu);
   fChain->SetBranchAddress("pp_ProbNNd", &pp_ProbNNd, &b_pp_ProbNNd);
   fChain->SetBranchAddress("pp_ProbNNghost", &pp_ProbNNghost, &b_pp_ProbNNghost);
   fChain->SetBranchAddress("pp_hasMuon", &pp_hasMuon, &b_pp_hasMuon);
   fChain->SetBranchAddress("pp_isMuon", &pp_isMuon, &b_pp_isMuon);
   fChain->SetBranchAddress("pp_hasRich", &pp_hasRich, &b_pp_hasRich);
   fChain->SetBranchAddress("pp_UsedRichAerogel", &pp_UsedRichAerogel, &b_pp_UsedRichAerogel);
   fChain->SetBranchAddress("pp_UsedRich1Gas", &pp_UsedRich1Gas, &b_pp_UsedRich1Gas);
   fChain->SetBranchAddress("pp_UsedRich2Gas", &pp_UsedRich2Gas, &b_pp_UsedRich2Gas);
   fChain->SetBranchAddress("pp_RichAboveElThres", &pp_RichAboveElThres, &b_pp_RichAboveElThres);
   fChain->SetBranchAddress("pp_RichAboveMuThres", &pp_RichAboveMuThres, &b_pp_RichAboveMuThres);
   fChain->SetBranchAddress("pp_RichAbovePiThres", &pp_RichAbovePiThres, &b_pp_RichAbovePiThres);
   fChain->SetBranchAddress("pp_RichAboveKaThres", &pp_RichAboveKaThres, &b_pp_RichAboveKaThres);
   fChain->SetBranchAddress("pp_RichAbovePrThres", &pp_RichAbovePrThres, &b_pp_RichAbovePrThres);
   fChain->SetBranchAddress("pp_hasCalo", &pp_hasCalo, &b_pp_hasCalo);
   fChain->SetBranchAddress("pp_L0Global_Dec", &pp_L0Global_Dec, &b_pp_L0Global_Dec);
   fChain->SetBranchAddress("pp_L0Global_TIS", &pp_L0Global_TIS, &b_pp_L0Global_TIS);
   fChain->SetBranchAddress("pp_L0Global_TOS", &pp_L0Global_TOS, &b_pp_L0Global_TOS);
   fChain->SetBranchAddress("pp_Hlt1Global_Dec", &pp_Hlt1Global_Dec, &b_pp_Hlt1Global_Dec);
   fChain->SetBranchAddress("pp_Hlt1Global_TIS", &pp_Hlt1Global_TIS, &b_pp_Hlt1Global_TIS);
   fChain->SetBranchAddress("pp_Hlt1Global_TOS", &pp_Hlt1Global_TOS, &b_pp_Hlt1Global_TOS);
   fChain->SetBranchAddress("pp_Hlt1Phys_Dec", &pp_Hlt1Phys_Dec, &b_pp_Hlt1Phys_Dec);
   fChain->SetBranchAddress("pp_Hlt1Phys_TIS", &pp_Hlt1Phys_TIS, &b_pp_Hlt1Phys_TIS);
   fChain->SetBranchAddress("pp_Hlt1Phys_TOS", &pp_Hlt1Phys_TOS, &b_pp_Hlt1Phys_TOS);
   fChain->SetBranchAddress("pp_Hlt2Global_Dec", &pp_Hlt2Global_Dec, &b_pp_Hlt2Global_Dec);
   fChain->SetBranchAddress("pp_Hlt2Global_TIS", &pp_Hlt2Global_TIS, &b_pp_Hlt2Global_TIS);
   fChain->SetBranchAddress("pp_Hlt2Global_TOS", &pp_Hlt2Global_TOS, &b_pp_Hlt2Global_TOS);
   fChain->SetBranchAddress("pp_Hlt2Phys_Dec", &pp_Hlt2Phys_Dec, &b_pp_Hlt2Phys_Dec);
   fChain->SetBranchAddress("pp_Hlt2Phys_TIS", &pp_Hlt2Phys_TIS, &b_pp_Hlt2Phys_TIS);
   fChain->SetBranchAddress("pp_Hlt2Phys_TOS", &pp_Hlt2Phys_TOS, &b_pp_Hlt2Phys_TOS);
   fChain->SetBranchAddress("pp_TRACK_Type", &pp_TRACK_Type, &b_pp_TRACK_Type);
   fChain->SetBranchAddress("pp_TRACK_Key", &pp_TRACK_Key, &b_pp_TRACK_Key);
   fChain->SetBranchAddress("pp_TRACK_CHI2NDOF", &pp_TRACK_CHI2NDOF, &b_pp_TRACK_CHI2NDOF);
   fChain->SetBranchAddress("pp_TRACK_PCHI2", &pp_TRACK_PCHI2, &b_pp_TRACK_PCHI2);
   fChain->SetBranchAddress("pp_TRACK_MatchCHI2", &pp_TRACK_MatchCHI2, &b_pp_TRACK_MatchCHI2);
   fChain->SetBranchAddress("pp_TRACK_GhostProb", &pp_TRACK_GhostProb, &b_pp_TRACK_GhostProb);
   fChain->SetBranchAddress("pp_TRACK_CloneDist", &pp_TRACK_CloneDist, &b_pp_TRACK_CloneDist);
   fChain->SetBranchAddress("pp_TRACK_Likelihood", &pp_TRACK_Likelihood, &b_pp_TRACK_Likelihood);
   fChain->SetBranchAddress("pp_ETA", &pp_ETA, &b_pp_ETA);
   fChain->SetBranchAddress("pp_KL", &pp_KL, &b_pp_KL);
   fChain->SetBranchAddress("pp_PHI", &pp_PHI, &b_pp_PHI);
   fChain->SetBranchAddress("pp_Y", &pp_Y, &b_pp_Y);
   fChain->SetBranchAddress("pm_MC12TuneV2_ProbNNe", &pm_MC12TuneV2_ProbNNe, &b_pm_MC12TuneV2_ProbNNe);
   fChain->SetBranchAddress("pm_MC12TuneV2_ProbNNmu", &pm_MC12TuneV2_ProbNNmu, &b_pm_MC12TuneV2_ProbNNmu);
   fChain->SetBranchAddress("pm_MC12TuneV2_ProbNNpi", &pm_MC12TuneV2_ProbNNpi, &b_pm_MC12TuneV2_ProbNNpi);
   fChain->SetBranchAddress("pm_MC12TuneV2_ProbNNk", &pm_MC12TuneV2_ProbNNk, &b_pm_MC12TuneV2_ProbNNk);
   fChain->SetBranchAddress("pm_MC12TuneV2_ProbNNp", &pm_MC12TuneV2_ProbNNp, &b_pm_MC12TuneV2_ProbNNp);
   fChain->SetBranchAddress("pm_MC12TuneV2_ProbNNghost", &pm_MC12TuneV2_ProbNNghost, &b_pm_MC12TuneV2_ProbNNghost);
   fChain->SetBranchAddress("pm_MC12TuneV3_ProbNNe", &pm_MC12TuneV3_ProbNNe, &b_pm_MC12TuneV3_ProbNNe);
   fChain->SetBranchAddress("pm_MC12TuneV3_ProbNNmu", &pm_MC12TuneV3_ProbNNmu, &b_pm_MC12TuneV3_ProbNNmu);
   fChain->SetBranchAddress("pm_MC12TuneV3_ProbNNpi", &pm_MC12TuneV3_ProbNNpi, &b_pm_MC12TuneV3_ProbNNpi);
   fChain->SetBranchAddress("pm_MC12TuneV3_ProbNNk", &pm_MC12TuneV3_ProbNNk, &b_pm_MC12TuneV3_ProbNNk);
   fChain->SetBranchAddress("pm_MC12TuneV3_ProbNNp", &pm_MC12TuneV3_ProbNNp, &b_pm_MC12TuneV3_ProbNNp);
   fChain->SetBranchAddress("pm_MC12TuneV3_ProbNNghost", &pm_MC12TuneV3_ProbNNghost, &b_pm_MC12TuneV3_ProbNNghost);
   fChain->SetBranchAddress("pm_MC12TuneV4_ProbNNe", &pm_MC12TuneV4_ProbNNe, &b_pm_MC12TuneV4_ProbNNe);
   fChain->SetBranchAddress("pm_MC12TuneV4_ProbNNmu", &pm_MC12TuneV4_ProbNNmu, &b_pm_MC12TuneV4_ProbNNmu);
   fChain->SetBranchAddress("pm_MC12TuneV4_ProbNNpi", &pm_MC12TuneV4_ProbNNpi, &b_pm_MC12TuneV4_ProbNNpi);
   fChain->SetBranchAddress("pm_MC12TuneV4_ProbNNk", &pm_MC12TuneV4_ProbNNk, &b_pm_MC12TuneV4_ProbNNk);
   fChain->SetBranchAddress("pm_MC12TuneV4_ProbNNp", &pm_MC12TuneV4_ProbNNp, &b_pm_MC12TuneV4_ProbNNp);
   fChain->SetBranchAddress("pm_MC12TuneV4_ProbNNghost", &pm_MC12TuneV4_ProbNNghost, &b_pm_MC12TuneV4_ProbNNghost);
   fChain->SetBranchAddress("pm_MC15TuneV1_ProbNNe", &pm_MC15TuneV1_ProbNNe, &b_pm_MC15TuneV1_ProbNNe);
   fChain->SetBranchAddress("pm_MC15TuneV1_ProbNNmu", &pm_MC15TuneV1_ProbNNmu, &b_pm_MC15TuneV1_ProbNNmu);
   fChain->SetBranchAddress("pm_MC15TuneV1_ProbNNpi", &pm_MC15TuneV1_ProbNNpi, &b_pm_MC15TuneV1_ProbNNpi);
   fChain->SetBranchAddress("pm_MC15TuneV1_ProbNNk", &pm_MC15TuneV1_ProbNNk, &b_pm_MC15TuneV1_ProbNNk);
   fChain->SetBranchAddress("pm_MC15TuneV1_ProbNNp", &pm_MC15TuneV1_ProbNNp, &b_pm_MC15TuneV1_ProbNNp);
   fChain->SetBranchAddress("pm_MC15TuneV1_ProbNNghost", &pm_MC15TuneV1_ProbNNghost, &b_pm_MC15TuneV1_ProbNNghost);
   fChain->SetBranchAddress("pm_CosTheta", &pm_CosTheta, &b_pm_CosTheta);
   fChain->SetBranchAddress("pm_OWNPV_X", &pm_OWNPV_X, &b_pm_OWNPV_X);
   fChain->SetBranchAddress("pm_OWNPV_Y", &pm_OWNPV_Y, &b_pm_OWNPV_Y);
   fChain->SetBranchAddress("pm_OWNPV_Z", &pm_OWNPV_Z, &b_pm_OWNPV_Z);
   fChain->SetBranchAddress("pm_OWNPV_XERR", &pm_OWNPV_XERR, &b_pm_OWNPV_XERR);
   fChain->SetBranchAddress("pm_OWNPV_YERR", &pm_OWNPV_YERR, &b_pm_OWNPV_YERR);
   fChain->SetBranchAddress("pm_OWNPV_ZERR", &pm_OWNPV_ZERR, &b_pm_OWNPV_ZERR);
   fChain->SetBranchAddress("pm_OWNPV_CHI2", &pm_OWNPV_CHI2, &b_pm_OWNPV_CHI2);
   fChain->SetBranchAddress("pm_OWNPV_NDOF", &pm_OWNPV_NDOF, &b_pm_OWNPV_NDOF);
   fChain->SetBranchAddress("pm_OWNPV_COV_", pm_OWNPV_COV_, &b_pm_OWNPV_COV_);
   fChain->SetBranchAddress("pm_IP_OWNPV", &pm_IP_OWNPV, &b_pm_IP_OWNPV);
   fChain->SetBranchAddress("pm_IPCHI2_OWNPV", &pm_IPCHI2_OWNPV, &b_pm_IPCHI2_OWNPV);
   fChain->SetBranchAddress("pm_ORIVX_X", &pm_ORIVX_X, &b_pm_ORIVX_X);
   fChain->SetBranchAddress("pm_ORIVX_Y", &pm_ORIVX_Y, &b_pm_ORIVX_Y);
   fChain->SetBranchAddress("pm_ORIVX_Z", &pm_ORIVX_Z, &b_pm_ORIVX_Z);
   fChain->SetBranchAddress("pm_ORIVX_XERR", &pm_ORIVX_XERR, &b_pm_ORIVX_XERR);
   fChain->SetBranchAddress("pm_ORIVX_YERR", &pm_ORIVX_YERR, &b_pm_ORIVX_YERR);
   fChain->SetBranchAddress("pm_ORIVX_ZERR", &pm_ORIVX_ZERR, &b_pm_ORIVX_ZERR);
   fChain->SetBranchAddress("pm_ORIVX_CHI2", &pm_ORIVX_CHI2, &b_pm_ORIVX_CHI2);
   fChain->SetBranchAddress("pm_ORIVX_NDOF", &pm_ORIVX_NDOF, &b_pm_ORIVX_NDOF);
   fChain->SetBranchAddress("pm_ORIVX_COV_", pm_ORIVX_COV_, &b_pm_ORIVX_COV_);
   fChain->SetBranchAddress("pm_P", &pm_P, &b_pm_P);
   fChain->SetBranchAddress("pm_PT", &pm_PT, &b_pm_PT);
   fChain->SetBranchAddress("pm_PE", &pm_PE, &b_pm_PE);
   fChain->SetBranchAddress("pm_PX", &pm_PX, &b_pm_PX);
   fChain->SetBranchAddress("pm_PY", &pm_PY, &b_pm_PY);
   fChain->SetBranchAddress("pm_PZ", &pm_PZ, &b_pm_PZ);
   fChain->SetBranchAddress("pm_M", &pm_M, &b_pm_M);
   fChain->SetBranchAddress("pm_ID", &pm_ID, &b_pm_ID);
   fChain->SetBranchAddress("pm_PIDe", &pm_PIDe, &b_pm_PIDe);
   fChain->SetBranchAddress("pm_PIDmu", &pm_PIDmu, &b_pm_PIDmu);
   fChain->SetBranchAddress("pm_PIDK", &pm_PIDK, &b_pm_PIDK);
   fChain->SetBranchAddress("pm_PIDp", &pm_PIDp, &b_pm_PIDp);
   fChain->SetBranchAddress("pm_PIDd", &pm_PIDd, &b_pm_PIDd);
   fChain->SetBranchAddress("pm_ProbNNe", &pm_ProbNNe, &b_pm_ProbNNe);
   fChain->SetBranchAddress("pm_ProbNNk", &pm_ProbNNk, &b_pm_ProbNNk);
   fChain->SetBranchAddress("pm_ProbNNp", &pm_ProbNNp, &b_pm_ProbNNp);
   fChain->SetBranchAddress("pm_ProbNNpi", &pm_ProbNNpi, &b_pm_ProbNNpi);
   fChain->SetBranchAddress("pm_ProbNNmu", &pm_ProbNNmu, &b_pm_ProbNNmu);
   fChain->SetBranchAddress("pm_ProbNNd", &pm_ProbNNd, &b_pm_ProbNNd);
   fChain->SetBranchAddress("pm_ProbNNghost", &pm_ProbNNghost, &b_pm_ProbNNghost);
   fChain->SetBranchAddress("pm_hasMuon", &pm_hasMuon, &b_pm_hasMuon);
   fChain->SetBranchAddress("pm_isMuon", &pm_isMuon, &b_pm_isMuon);
   fChain->SetBranchAddress("pm_hasRich", &pm_hasRich, &b_pm_hasRich);
   fChain->SetBranchAddress("pm_UsedRichAerogel", &pm_UsedRichAerogel, &b_pm_UsedRichAerogel);
   fChain->SetBranchAddress("pm_UsedRich1Gas", &pm_UsedRich1Gas, &b_pm_UsedRich1Gas);
   fChain->SetBranchAddress("pm_UsedRich2Gas", &pm_UsedRich2Gas, &b_pm_UsedRich2Gas);
   fChain->SetBranchAddress("pm_RichAboveElThres", &pm_RichAboveElThres, &b_pm_RichAboveElThres);
   fChain->SetBranchAddress("pm_RichAboveMuThres", &pm_RichAboveMuThres, &b_pm_RichAboveMuThres);
   fChain->SetBranchAddress("pm_RichAbovePiThres", &pm_RichAbovePiThres, &b_pm_RichAbovePiThres);
   fChain->SetBranchAddress("pm_RichAboveKaThres", &pm_RichAboveKaThres, &b_pm_RichAboveKaThres);
   fChain->SetBranchAddress("pm_RichAbovePrThres", &pm_RichAbovePrThres, &b_pm_RichAbovePrThres);
   fChain->SetBranchAddress("pm_hasCalo", &pm_hasCalo, &b_pm_hasCalo);
   fChain->SetBranchAddress("pm_L0Global_Dec", &pm_L0Global_Dec, &b_pm_L0Global_Dec);
   fChain->SetBranchAddress("pm_L0Global_TIS", &pm_L0Global_TIS, &b_pm_L0Global_TIS);
   fChain->SetBranchAddress("pm_L0Global_TOS", &pm_L0Global_TOS, &b_pm_L0Global_TOS);
   fChain->SetBranchAddress("pm_Hlt1Global_Dec", &pm_Hlt1Global_Dec, &b_pm_Hlt1Global_Dec);
   fChain->SetBranchAddress("pm_Hlt1Global_TIS", &pm_Hlt1Global_TIS, &b_pm_Hlt1Global_TIS);
   fChain->SetBranchAddress("pm_Hlt1Global_TOS", &pm_Hlt1Global_TOS, &b_pm_Hlt1Global_TOS);
   fChain->SetBranchAddress("pm_Hlt1Phys_Dec", &pm_Hlt1Phys_Dec, &b_pm_Hlt1Phys_Dec);
   fChain->SetBranchAddress("pm_Hlt1Phys_TIS", &pm_Hlt1Phys_TIS, &b_pm_Hlt1Phys_TIS);
   fChain->SetBranchAddress("pm_Hlt1Phys_TOS", &pm_Hlt1Phys_TOS, &b_pm_Hlt1Phys_TOS);
   fChain->SetBranchAddress("pm_Hlt2Global_Dec", &pm_Hlt2Global_Dec, &b_pm_Hlt2Global_Dec);
   fChain->SetBranchAddress("pm_Hlt2Global_TIS", &pm_Hlt2Global_TIS, &b_pm_Hlt2Global_TIS);
   fChain->SetBranchAddress("pm_Hlt2Global_TOS", &pm_Hlt2Global_TOS, &b_pm_Hlt2Global_TOS);
   fChain->SetBranchAddress("pm_Hlt2Phys_Dec", &pm_Hlt2Phys_Dec, &b_pm_Hlt2Phys_Dec);
   fChain->SetBranchAddress("pm_Hlt2Phys_TIS", &pm_Hlt2Phys_TIS, &b_pm_Hlt2Phys_TIS);
   fChain->SetBranchAddress("pm_Hlt2Phys_TOS", &pm_Hlt2Phys_TOS, &b_pm_Hlt2Phys_TOS);
   fChain->SetBranchAddress("pm_TRACK_Type", &pm_TRACK_Type, &b_pm_TRACK_Type);
   fChain->SetBranchAddress("pm_TRACK_Key", &pm_TRACK_Key, &b_pm_TRACK_Key);
   fChain->SetBranchAddress("pm_TRACK_CHI2NDOF", &pm_TRACK_CHI2NDOF, &b_pm_TRACK_CHI2NDOF);
   fChain->SetBranchAddress("pm_TRACK_PCHI2", &pm_TRACK_PCHI2, &b_pm_TRACK_PCHI2);
   fChain->SetBranchAddress("pm_TRACK_MatchCHI2", &pm_TRACK_MatchCHI2, &b_pm_TRACK_MatchCHI2);
   fChain->SetBranchAddress("pm_TRACK_GhostProb", &pm_TRACK_GhostProb, &b_pm_TRACK_GhostProb);
   fChain->SetBranchAddress("pm_TRACK_CloneDist", &pm_TRACK_CloneDist, &b_pm_TRACK_CloneDist);
   fChain->SetBranchAddress("pm_TRACK_Likelihood", &pm_TRACK_Likelihood, &b_pm_TRACK_Likelihood);
   fChain->SetBranchAddress("pm_ETA", &pm_ETA, &b_pm_ETA);
   fChain->SetBranchAddress("pm_KL", &pm_KL, &b_pm_KL);
   fChain->SetBranchAddress("pm_PHI", &pm_PHI, &b_pm_PHI);
   fChain->SetBranchAddress("pm_Y", &pm_Y, &b_pm_Y);
   fChain->SetBranchAddress("nCandidate", &nCandidate, &b_nCandidate);
   fChain->SetBranchAddress("totCandidates", &totCandidates, &b_totCandidates);
   fChain->SetBranchAddress("EventInSequence", &EventInSequence, &b_EventInSequence);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("BCID", &BCID, &b_BCID);
   fChain->SetBranchAddress("BCType", &BCType, &b_BCType);
   fChain->SetBranchAddress("OdinTCK", &OdinTCK, &b_OdinTCK);
   fChain->SetBranchAddress("L0DUTCK", &L0DUTCK, &b_L0DUTCK);
   fChain->SetBranchAddress("HLT1TCK", &HLT1TCK, &b_HLT1TCK);
   fChain->SetBranchAddress("HLT2TCK", &HLT2TCK, &b_HLT2TCK);
   fChain->SetBranchAddress("GpsTime", &GpsTime, &b_GpsTime);
   fChain->SetBranchAddress("Polarity", &Polarity, &b_Polarity);
   fChain->SetBranchAddress("nPV", &nPV, &b_nPV);
   fChain->SetBranchAddress("PVX", PVX, &b_PVX);
   fChain->SetBranchAddress("PVY", PVY, &b_PVY);
   fChain->SetBranchAddress("PVZ", PVZ, &b_PVZ);
   fChain->SetBranchAddress("PVXERR", PVXERR, &b_PVXERR);
   fChain->SetBranchAddress("PVYERR", PVYERR, &b_PVYERR);
   fChain->SetBranchAddress("PVZERR", PVZERR, &b_PVZERR);
   fChain->SetBranchAddress("PVCHI2", PVCHI2, &b_PVCHI2);
   fChain->SetBranchAddress("PVNDOF", PVNDOF, &b_PVNDOF);
   fChain->SetBranchAddress("PVNTRACKS", PVNTRACKS, &b_PVNTRACKS);
   fChain->SetBranchAddress("nPVs", &nPVs, &b_nPVs);
   fChain->SetBranchAddress("nTracks", &nTracks, &b_nTracks);
   fChain->SetBranchAddress("nLongTracks", &nLongTracks, &b_nLongTracks);
   fChain->SetBranchAddress("nDownstreamTracks", &nDownstreamTracks, &b_nDownstreamTracks);
   fChain->SetBranchAddress("nUpstreamTracks", &nUpstreamTracks, &b_nUpstreamTracks);
   fChain->SetBranchAddress("nVeloTracks", &nVeloTracks, &b_nVeloTracks);
   fChain->SetBranchAddress("nTTracks", &nTTracks, &b_nTTracks);
   fChain->SetBranchAddress("nBackTracks", &nBackTracks, &b_nBackTracks);
   fChain->SetBranchAddress("nRich1Hits", &nRich1Hits, &b_nRich1Hits);
   fChain->SetBranchAddress("nRich2Hits", &nRich2Hits, &b_nRich2Hits);
   fChain->SetBranchAddress("nVeloClusters", &nVeloClusters, &b_nVeloClusters);
   fChain->SetBranchAddress("nITClusters", &nITClusters, &b_nITClusters);
   fChain->SetBranchAddress("nTTClusters", &nTTClusters, &b_nTTClusters);
   fChain->SetBranchAddress("nOTClusters", &nOTClusters, &b_nOTClusters);
   fChain->SetBranchAddress("nSPDHits", &nSPDHits, &b_nSPDHits);
   fChain->SetBranchAddress("nMuonCoordsS0", &nMuonCoordsS0, &b_nMuonCoordsS0);
   fChain->SetBranchAddress("nMuonCoordsS1", &nMuonCoordsS1, &b_nMuonCoordsS1);
   fChain->SetBranchAddress("nMuonCoordsS2", &nMuonCoordsS2, &b_nMuonCoordsS2);
   fChain->SetBranchAddress("nMuonCoordsS3", &nMuonCoordsS3, &b_nMuonCoordsS3);
   fChain->SetBranchAddress("nMuonCoordsS4", &nMuonCoordsS4, &b_nMuonCoordsS4);
   fChain->SetBranchAddress("nMuonTracks", &nMuonTracks, &b_nMuonTracks);
   fChain->SetBranchAddress("mass_pp", &mass_pp, &b_mass_pp);
   fChain->SetBranchAddress("mass_Dp", &mass_Dp, &b_mass_Dp);
   fChain->SetBranchAddress("mass_Dpbar", &mass_Dpbar, &b_mass_Dpbar);
   fChain->SetBranchAddress("B_mass_con", &B_mass_con, &b_B_mass_con);
   fChain->SetBranchAddress("log_B0_PT", &log_B0_PT, &b_log_B0_PT);
   fChain->SetBranchAddress("log_B0_P", &log_B0_P, &b_log_B0_P);
   fChain->SetBranchAddress("log_acos_B0_LOKI_DIRA", &log_acos_B0_LOKI_DIRA, &b_log_acos_B0_LOKI_DIRA);
   fChain->SetBranchAddress("log_abs_DTau", &log_abs_DTau, &b_log_abs_DTau);
   fChain->SetBranchAddress("log_D0_FD_OWNPV", &log_D0_FD_OWNPV, &b_log_D0_FD_OWNPV);
   fChain->SetBranchAddress("log_acos_D0_LOKI_DIRA", &log_acos_D0_LOKI_DIRA, &b_log_acos_D0_LOKI_DIRA);
   fChain->SetBranchAddress("min_D_daughter_pt", &min_D_daughter_pt, &b_min_D_daughter_pt);
   fChain->SetBranchAddress("min_p_pt", &min_p_pt, &b_min_p_pt);
   fChain->SetBranchAddress("max_ip_chi2", &max_ip_chi2, &b_max_ip_chi2);
   fChain->SetBranchAddress("BDZDis", &BDZDis, &b_BDZDis);
   fChain->SetBranchAddress("mvaBDT", &mvaBDT, &b_mvaBDT);
   fChain->SetBranchAddress("Sweight_sig", &Sweight_sig, &b_Sweight_sig);
   fChain->SetBranchAddress("Sweight_bkg", &Sweight_bkg, &b_Sweight_bkg);
   fChain->SetBranchAddress("mppbar", &mppbar, &b_mppbar);
   fChain->SetBranchAddress("mD0p", &mD0p, &b_mD0p);
   fChain->SetBranchAddress("mD0pbar", &mD0pbar, &b_mD0pbar);
   fChain->SetBranchAddress("cosTheta_X", &cosTheta_X, &b_cosTheta_X);
   fChain->SetBranchAddress("phi_X", &phi_X, &b_phi_X);
   fChain->SetBranchAddress("cosTheta_L", &cosTheta_L, &b_cosTheta_L);
   fChain->SetBranchAddress("phi_L", &phi_L, &b_phi_L);
   fChain->SetBranchAddress("cosTheta_p", &cosTheta_p, &b_cosTheta_p);
   fChain->SetBranchAddress("cosTheta_pbar", &cosTheta_pbar, &b_cosTheta_pbar);
   fChain->SetBranchAddress("AFB_cosTheta_p", &AFB_cosTheta_p, &b_AFB_cosTheta_p);
   fChain->SetBranchAddress("eff", &eff, &b_eff);
   Notify();
}

Bool_t mytree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void mytree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t mytree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef mytree_cxx
